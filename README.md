# AI For Cern

This research project aims to develop a real-time anomaly detection system for the first-level trigger systems in the ATLAS experiment using FPGA technology and AI. By harnessing the parallel processing capabilities of FPGAs and the advanced pattern recognition abilities of CNNs, the system aims to promptly identify anomalous data instances. The successful implementation of this AI-driven FPGA-based solution would enhance the performance of the first-level trigger system, improving the overall data quality and facilitating more accurate data analysis in the ATLAS experiment.
