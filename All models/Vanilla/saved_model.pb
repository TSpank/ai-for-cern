�(
��
D
AddV2
x"T
y"T
z"T"
Ttype:
2	��
E
AssignAddVariableOp
resource
value"dtype"
dtypetype�
^
AssignVariableOp
resource
value"dtype"
dtypetype"
validate_shapebool( �
�
BiasAdd

value"T	
bias"T
output"T""
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
N
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype"
Truncatebool( 
8
Const
output"dtype"
valuetensor"
dtypetype
�
Conv2D

input"T
filter"T
output"T"
Ttype:	
2"
strides	list(int)"
use_cudnn_on_gpubool(",
paddingstring:
SAMEVALIDEXPLICIT""
explicit_paddings	list(int)
 "-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

�
Conv2DBackpropInput
input_sizes
filter"T
out_backprop"T
output"T"
Ttype:	
2"
strides	list(int)"
use_cudnn_on_gpubool(",
paddingstring:
SAMEVALIDEXPLICIT""
explicit_paddings	list(int)
 "-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

$
DisableCopyOnRead
resource�
9
DivNoNan
x"T
y"T
z"T"
Ttype:

2
,
Exp
x"T
y"T"
Ttype:

2
.
Identity

input"T
output"T"	
Ttype
u
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:
2	
�
Mean

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( ""
Ttype:
2	"
Tidxtype0:
2	
�
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool("
allow_missing_filesbool( �
?
Mul
x"T
y"T
z"T"
Ttype:
2	�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
�
RandomStandardNormal

shape"T
output"dtype"
seedint "
seed2int "
dtypetype:
2"
Ttype:
2	�
f
Range
start"Tidx
limit"Tidx
delta"Tidx
output"Tidx" 
Tidxtype0:
2
	
@
ReadVariableOp
resource
value"dtype"
dtypetype�
@
RealDiv
x"T
y"T
z"T"
Ttype:
2	
E
Relu
features"T
activations"T"
Ttype:
2	
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
H
ShardedFilename
basename	
shard

num_shards
filename
0
Sigmoid
x"T
y"T"
Ttype:

2
7
Square
x"T
y"T"
Ttype:
2	
G
SquaredDifference
x"T
y"T
z"T"
Ttype:

2	�
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring ��
@
StaticRegexFullMatch	
input

output
"
patternstring
�
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
<
Sub
x"T
y"T
z"T"
Ttype:
2	
�
Sum

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( ""
Ttype:
2	"
Tidxtype0:
2	
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.12.02v2.12.0-rc1-12-g0db597d0d758��#
J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?
L
Const_1Const*
_output_shapes
: *
dtype0*
valueB
 *  zD
L
Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *���=
L
Const_3Const*
_output_shapes
: *
dtype0*
valueB
 *   �
x
add_metric_7/countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameadd_metric_7/count
q
&add_metric_7/count/Read/ReadVariableOpReadVariableOpadd_metric_7/count*
_output_shapes
: *
dtype0
x
add_metric_7/totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameadd_metric_7/total
q
&add_metric_7/total/Read/ReadVariableOpReadVariableOpadd_metric_7/total*
_output_shapes
: *
dtype0
x
add_metric_6/countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameadd_metric_6/count
q
&add_metric_6/count/Read/ReadVariableOpReadVariableOpadd_metric_6/count*
_output_shapes
: *
dtype0
x
add_metric_6/totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameadd_metric_6/total
q
&add_metric_6/total/Read/ReadVariableOpReadVariableOpadd_metric_6/total*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
�
Adam/v/conv2d_19/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*&
shared_nameAdam/v/conv2d_19/bias
{
)Adam/v/conv2d_19/bias/Read/ReadVariableOpReadVariableOpAdam/v/conv2d_19/bias*
_output_shapes
:*
dtype0
�
Adam/m/conv2d_19/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*&
shared_nameAdam/m/conv2d_19/bias
{
)Adam/m/conv2d_19/bias/Read/ReadVariableOpReadVariableOpAdam/m/conv2d_19/bias*
_output_shapes
:*
dtype0
�
Adam/v/conv2d_19/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:*(
shared_nameAdam/v/conv2d_19/kernel
�
+Adam/v/conv2d_19/kernel/Read/ReadVariableOpReadVariableOpAdam/v/conv2d_19/kernel*&
_output_shapes
:*
dtype0
�
Adam/m/conv2d_19/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:*(
shared_nameAdam/m/conv2d_19/kernel
�
+Adam/m/conv2d_19/kernel/Read/ReadVariableOpReadVariableOpAdam/m/conv2d_19/kernel*&
_output_shapes
:*
dtype0
�
Adam/v/conv2d_transpose_15/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*0
shared_name!Adam/v/conv2d_transpose_15/bias
�
3Adam/v/conv2d_transpose_15/bias/Read/ReadVariableOpReadVariableOpAdam/v/conv2d_transpose_15/bias*
_output_shapes
:*
dtype0
�
Adam/m/conv2d_transpose_15/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*0
shared_name!Adam/m/conv2d_transpose_15/bias
�
3Adam/m/conv2d_transpose_15/bias/Read/ReadVariableOpReadVariableOpAdam/m/conv2d_transpose_15/bias*
_output_shapes
:*
dtype0
�
!Adam/v/conv2d_transpose_15/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape: *2
shared_name#!Adam/v/conv2d_transpose_15/kernel
�
5Adam/v/conv2d_transpose_15/kernel/Read/ReadVariableOpReadVariableOp!Adam/v/conv2d_transpose_15/kernel*&
_output_shapes
: *
dtype0
�
!Adam/m/conv2d_transpose_15/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape: *2
shared_name#!Adam/m/conv2d_transpose_15/kernel
�
5Adam/m/conv2d_transpose_15/kernel/Read/ReadVariableOpReadVariableOp!Adam/m/conv2d_transpose_15/kernel*&
_output_shapes
: *
dtype0
�
Adam/v/conv2d_transpose_14/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape: *0
shared_name!Adam/v/conv2d_transpose_14/bias
�
3Adam/v/conv2d_transpose_14/bias/Read/ReadVariableOpReadVariableOpAdam/v/conv2d_transpose_14/bias*
_output_shapes
: *
dtype0
�
Adam/m/conv2d_transpose_14/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape: *0
shared_name!Adam/m/conv2d_transpose_14/bias
�
3Adam/m/conv2d_transpose_14/bias/Read/ReadVariableOpReadVariableOpAdam/m/conv2d_transpose_14/bias*
_output_shapes
: *
dtype0
�
!Adam/v/conv2d_transpose_14/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape: @*2
shared_name#!Adam/v/conv2d_transpose_14/kernel
�
5Adam/v/conv2d_transpose_14/kernel/Read/ReadVariableOpReadVariableOp!Adam/v/conv2d_transpose_14/kernel*&
_output_shapes
: @*
dtype0
�
!Adam/m/conv2d_transpose_14/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape: @*2
shared_name#!Adam/m/conv2d_transpose_14/kernel
�
5Adam/m/conv2d_transpose_14/kernel/Read/ReadVariableOpReadVariableOp!Adam/m/conv2d_transpose_14/kernel*&
_output_shapes
: @*
dtype0
�
Adam/v/conv2d_transpose_13/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*0
shared_name!Adam/v/conv2d_transpose_13/bias
�
3Adam/v/conv2d_transpose_13/bias/Read/ReadVariableOpReadVariableOpAdam/v/conv2d_transpose_13/bias*
_output_shapes
:@*
dtype0
�
Adam/m/conv2d_transpose_13/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*0
shared_name!Adam/m/conv2d_transpose_13/bias
�
3Adam/m/conv2d_transpose_13/bias/Read/ReadVariableOpReadVariableOpAdam/m/conv2d_transpose_13/bias*
_output_shapes
:@*
dtype0
�
!Adam/v/conv2d_transpose_13/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:@�*2
shared_name#!Adam/v/conv2d_transpose_13/kernel
�
5Adam/v/conv2d_transpose_13/kernel/Read/ReadVariableOpReadVariableOp!Adam/v/conv2d_transpose_13/kernel*'
_output_shapes
:@�*
dtype0
�
!Adam/m/conv2d_transpose_13/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:@�*2
shared_name#!Adam/m/conv2d_transpose_13/kernel
�
5Adam/m/conv2d_transpose_13/kernel/Read/ReadVariableOpReadVariableOp!Adam/m/conv2d_transpose_13/kernel*'
_output_shapes
:@�*
dtype0
�
Adam/v/conv2d_transpose_12/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*0
shared_name!Adam/v/conv2d_transpose_12/bias
�
3Adam/v/conv2d_transpose_12/bias/Read/ReadVariableOpReadVariableOpAdam/v/conv2d_transpose_12/bias*
_output_shapes	
:�*
dtype0
�
Adam/m/conv2d_transpose_12/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*0
shared_name!Adam/m/conv2d_transpose_12/bias
�
3Adam/m/conv2d_transpose_12/bias/Read/ReadVariableOpReadVariableOpAdam/m/conv2d_transpose_12/bias*
_output_shapes	
:�*
dtype0
�
!Adam/v/conv2d_transpose_12/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:��*2
shared_name#!Adam/v/conv2d_transpose_12/kernel
�
5Adam/v/conv2d_transpose_12/kernel/Read/ReadVariableOpReadVariableOp!Adam/v/conv2d_transpose_12/kernel*(
_output_shapes
:��*
dtype0
�
!Adam/m/conv2d_transpose_12/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:��*2
shared_name#!Adam/m/conv2d_transpose_12/kernel
�
5Adam/m/conv2d_transpose_12/kernel/Read/ReadVariableOpReadVariableOp!Adam/m/conv2d_transpose_12/kernel*(
_output_shapes
:��*
dtype0
�
Adam/v/dense_11/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:��*%
shared_nameAdam/v/dense_11/bias
{
(Adam/v/dense_11/bias/Read/ReadVariableOpReadVariableOpAdam/v/dense_11/bias*
_output_shapes

:��*
dtype0
�
Adam/m/dense_11/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:��*%
shared_nameAdam/m/dense_11/bias
{
(Adam/m/dense_11/bias/Read/ReadVariableOpReadVariableOpAdam/m/dense_11/bias*
_output_shapes

:��*
dtype0
�
Adam/v/dense_11/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*'
shared_nameAdam/v/dense_11/kernel
�
*Adam/v/dense_11/kernel/Read/ReadVariableOpReadVariableOpAdam/v/dense_11/kernel* 
_output_shapes
:
��*
dtype0
�
Adam/m/dense_11/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*'
shared_nameAdam/m/dense_11/kernel
�
*Adam/m/dense_11/kernel/Read/ReadVariableOpReadVariableOpAdam/m/dense_11/kernel* 
_output_shapes
:
��*
dtype0
�
Adam/v/dense_10/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*%
shared_nameAdam/v/dense_10/bias
y
(Adam/v/dense_10/bias/Read/ReadVariableOpReadVariableOpAdam/v/dense_10/bias*
_output_shapes
:*
dtype0
�
Adam/m/dense_10/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*%
shared_nameAdam/m/dense_10/bias
y
(Adam/m/dense_10/bias/Read/ReadVariableOpReadVariableOpAdam/m/dense_10/bias*
_output_shapes
:*
dtype0
�
Adam/v/dense_10/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*'
shared_nameAdam/v/dense_10/kernel
�
*Adam/v/dense_10/kernel/Read/ReadVariableOpReadVariableOpAdam/v/dense_10/kernel* 
_output_shapes
:
��*
dtype0
�
Adam/m/dense_10/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*'
shared_nameAdam/m/dense_10/kernel
�
*Adam/m/dense_10/kernel/Read/ReadVariableOpReadVariableOpAdam/m/dense_10/kernel* 
_output_shapes
:
��*
dtype0
~
Adam/v/dense_9/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*$
shared_nameAdam/v/dense_9/bias
w
'Adam/v/dense_9/bias/Read/ReadVariableOpReadVariableOpAdam/v/dense_9/bias*
_output_shapes
:*
dtype0
~
Adam/m/dense_9/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*$
shared_nameAdam/m/dense_9/bias
w
'Adam/m/dense_9/bias/Read/ReadVariableOpReadVariableOpAdam/m/dense_9/bias*
_output_shapes
:*
dtype0
�
Adam/v/dense_9/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*&
shared_nameAdam/v/dense_9/kernel
�
)Adam/v/dense_9/kernel/Read/ReadVariableOpReadVariableOpAdam/v/dense_9/kernel* 
_output_shapes
:
��*
dtype0
�
Adam/m/dense_9/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*&
shared_nameAdam/m/dense_9/kernel
�
)Adam/m/dense_9/kernel/Read/ReadVariableOpReadVariableOpAdam/m/dense_9/kernel* 
_output_shapes
:
��*
dtype0
�
Adam/v/conv2d_18/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*&
shared_nameAdam/v/conv2d_18/bias
|
)Adam/v/conv2d_18/bias/Read/ReadVariableOpReadVariableOpAdam/v/conv2d_18/bias*
_output_shapes	
:�*
dtype0
�
Adam/m/conv2d_18/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*&
shared_nameAdam/m/conv2d_18/bias
|
)Adam/m/conv2d_18/bias/Read/ReadVariableOpReadVariableOpAdam/m/conv2d_18/bias*
_output_shapes	
:�*
dtype0
�
Adam/v/conv2d_18/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:��*(
shared_nameAdam/v/conv2d_18/kernel
�
+Adam/v/conv2d_18/kernel/Read/ReadVariableOpReadVariableOpAdam/v/conv2d_18/kernel*(
_output_shapes
:��*
dtype0
�
Adam/m/conv2d_18/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:��*(
shared_nameAdam/m/conv2d_18/kernel
�
+Adam/m/conv2d_18/kernel/Read/ReadVariableOpReadVariableOpAdam/m/conv2d_18/kernel*(
_output_shapes
:��*
dtype0
�
Adam/v/conv2d_17/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*&
shared_nameAdam/v/conv2d_17/bias
|
)Adam/v/conv2d_17/bias/Read/ReadVariableOpReadVariableOpAdam/v/conv2d_17/bias*
_output_shapes	
:�*
dtype0
�
Adam/m/conv2d_17/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*&
shared_nameAdam/m/conv2d_17/bias
|
)Adam/m/conv2d_17/bias/Read/ReadVariableOpReadVariableOpAdam/m/conv2d_17/bias*
_output_shapes	
:�*
dtype0
�
Adam/v/conv2d_17/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape: �*(
shared_nameAdam/v/conv2d_17/kernel
�
+Adam/v/conv2d_17/kernel/Read/ReadVariableOpReadVariableOpAdam/v/conv2d_17/kernel*'
_output_shapes
: �*
dtype0
�
Adam/m/conv2d_17/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape: �*(
shared_nameAdam/m/conv2d_17/kernel
�
+Adam/m/conv2d_17/kernel/Read/ReadVariableOpReadVariableOpAdam/m/conv2d_17/kernel*'
_output_shapes
: �*
dtype0
�
Adam/v/conv2d_16/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape: *&
shared_nameAdam/v/conv2d_16/bias
{
)Adam/v/conv2d_16/bias/Read/ReadVariableOpReadVariableOpAdam/v/conv2d_16/bias*
_output_shapes
: *
dtype0
�
Adam/m/conv2d_16/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape: *&
shared_nameAdam/m/conv2d_16/bias
{
)Adam/m/conv2d_16/bias/Read/ReadVariableOpReadVariableOpAdam/m/conv2d_16/bias*
_output_shapes
: *
dtype0
�
Adam/v/conv2d_16/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape: *(
shared_nameAdam/v/conv2d_16/kernel
�
+Adam/v/conv2d_16/kernel/Read/ReadVariableOpReadVariableOpAdam/v/conv2d_16/kernel*&
_output_shapes
: *
dtype0
�
Adam/m/conv2d_16/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape: *(
shared_nameAdam/m/conv2d_16/kernel
�
+Adam/m/conv2d_16/kernel/Read/ReadVariableOpReadVariableOpAdam/m/conv2d_16/kernel*&
_output_shapes
: *
dtype0
�
Adam/v/conv2d_15/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*&
shared_nameAdam/v/conv2d_15/bias
{
)Adam/v/conv2d_15/bias/Read/ReadVariableOpReadVariableOpAdam/v/conv2d_15/bias*
_output_shapes
:*
dtype0
�
Adam/m/conv2d_15/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*&
shared_nameAdam/m/conv2d_15/bias
{
)Adam/m/conv2d_15/bias/Read/ReadVariableOpReadVariableOpAdam/m/conv2d_15/bias*
_output_shapes
:*
dtype0
�
Adam/v/conv2d_15/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:*(
shared_nameAdam/v/conv2d_15/kernel
�
+Adam/v/conv2d_15/kernel/Read/ReadVariableOpReadVariableOpAdam/v/conv2d_15/kernel*&
_output_shapes
:*
dtype0
�
Adam/m/conv2d_15/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:*(
shared_nameAdam/m/conv2d_15/kernel
�
+Adam/m/conv2d_15/kernel/Read/ReadVariableOpReadVariableOpAdam/m/conv2d_15/kernel*&
_output_shapes
:*
dtype0
n
learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namelearning_rate
g
!learning_rate/Read/ReadVariableOpReadVariableOplearning_rate*
_output_shapes
: *
dtype0
f
	iterationVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	iteration
_
iteration/Read/ReadVariableOpReadVariableOp	iteration*
_output_shapes
: *
dtype0	
t
conv2d_19/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_nameconv2d_19/bias
m
"conv2d_19/bias/Read/ReadVariableOpReadVariableOpconv2d_19/bias*
_output_shapes
:*
dtype0
�
conv2d_19/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:*!
shared_nameconv2d_19/kernel
}
$conv2d_19/kernel/Read/ReadVariableOpReadVariableOpconv2d_19/kernel*&
_output_shapes
:*
dtype0
�
conv2d_transpose_15/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*)
shared_nameconv2d_transpose_15/bias
�
,conv2d_transpose_15/bias/Read/ReadVariableOpReadVariableOpconv2d_transpose_15/bias*
_output_shapes
:*
dtype0
�
conv2d_transpose_15/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape: *+
shared_nameconv2d_transpose_15/kernel
�
.conv2d_transpose_15/kernel/Read/ReadVariableOpReadVariableOpconv2d_transpose_15/kernel*&
_output_shapes
: *
dtype0
�
conv2d_transpose_14/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape: *)
shared_nameconv2d_transpose_14/bias
�
,conv2d_transpose_14/bias/Read/ReadVariableOpReadVariableOpconv2d_transpose_14/bias*
_output_shapes
: *
dtype0
�
conv2d_transpose_14/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape: @*+
shared_nameconv2d_transpose_14/kernel
�
.conv2d_transpose_14/kernel/Read/ReadVariableOpReadVariableOpconv2d_transpose_14/kernel*&
_output_shapes
: @*
dtype0
�
conv2d_transpose_13/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*)
shared_nameconv2d_transpose_13/bias
�
,conv2d_transpose_13/bias/Read/ReadVariableOpReadVariableOpconv2d_transpose_13/bias*
_output_shapes
:@*
dtype0
�
conv2d_transpose_13/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:@�*+
shared_nameconv2d_transpose_13/kernel
�
.conv2d_transpose_13/kernel/Read/ReadVariableOpReadVariableOpconv2d_transpose_13/kernel*'
_output_shapes
:@�*
dtype0
�
conv2d_transpose_12/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*)
shared_nameconv2d_transpose_12/bias
�
,conv2d_transpose_12/bias/Read/ReadVariableOpReadVariableOpconv2d_transpose_12/bias*
_output_shapes	
:�*
dtype0
�
conv2d_transpose_12/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:��*+
shared_nameconv2d_transpose_12/kernel
�
.conv2d_transpose_12/kernel/Read/ReadVariableOpReadVariableOpconv2d_transpose_12/kernel*(
_output_shapes
:��*
dtype0
t
dense_11/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:��*
shared_namedense_11/bias
m
!dense_11/bias/Read/ReadVariableOpReadVariableOpdense_11/bias*
_output_shapes

:��*
dtype0
|
dense_11/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��* 
shared_namedense_11/kernel
u
#dense_11/kernel/Read/ReadVariableOpReadVariableOpdense_11/kernel* 
_output_shapes
:
��*
dtype0
p
dense_9/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namedense_9/bias
i
 dense_9/bias/Read/ReadVariableOpReadVariableOpdense_9/bias*
_output_shapes
:*
dtype0
z
dense_9/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*
shared_namedense_9/kernel
s
"dense_9/kernel/Read/ReadVariableOpReadVariableOpdense_9/kernel* 
_output_shapes
:
��*
dtype0
r
dense_10/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namedense_10/bias
k
!dense_10/bias/Read/ReadVariableOpReadVariableOpdense_10/bias*
_output_shapes
:*
dtype0
|
dense_10/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��* 
shared_namedense_10/kernel
u
#dense_10/kernel/Read/ReadVariableOpReadVariableOpdense_10/kernel* 
_output_shapes
:
��*
dtype0
u
conv2d_18/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*
shared_nameconv2d_18/bias
n
"conv2d_18/bias/Read/ReadVariableOpReadVariableOpconv2d_18/bias*
_output_shapes	
:�*
dtype0
�
conv2d_18/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:��*!
shared_nameconv2d_18/kernel

$conv2d_18/kernel/Read/ReadVariableOpReadVariableOpconv2d_18/kernel*(
_output_shapes
:��*
dtype0
u
conv2d_17/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*
shared_nameconv2d_17/bias
n
"conv2d_17/bias/Read/ReadVariableOpReadVariableOpconv2d_17/bias*
_output_shapes	
:�*
dtype0
�
conv2d_17/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape: �*!
shared_nameconv2d_17/kernel
~
$conv2d_17/kernel/Read/ReadVariableOpReadVariableOpconv2d_17/kernel*'
_output_shapes
: �*
dtype0
t
conv2d_16/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameconv2d_16/bias
m
"conv2d_16/bias/Read/ReadVariableOpReadVariableOpconv2d_16/bias*
_output_shapes
: *
dtype0
�
conv2d_16/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape: *!
shared_nameconv2d_16/kernel
}
$conv2d_16/kernel/Read/ReadVariableOpReadVariableOpconv2d_16/kernel*&
_output_shapes
: *
dtype0
t
conv2d_15/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_nameconv2d_15/bias
m
"conv2d_15/bias/Read/ReadVariableOpReadVariableOpconv2d_15/bias*
_output_shapes
:*
dtype0
�
conv2d_15/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:*!
shared_nameconv2d_15/kernel
}
$conv2d_15/kernel/Read/ReadVariableOpReadVariableOpconv2d_15/kernel*&
_output_shapes
:*
dtype0
x
serving_default_input_7Placeholder*&
_output_shapes
:<<*
dtype0*
shape:<<
�
StatefulPartitionedCallStatefulPartitionedCallserving_default_input_7conv2d_15/kernelconv2d_15/biasconv2d_16/kernelconv2d_16/biasconv2d_17/kernelconv2d_17/biasconv2d_18/kernelconv2d_18/biasdense_9/kerneldense_9/biasdense_10/kerneldense_10/biasdense_11/kerneldense_11/biasconv2d_transpose_12/kernelconv2d_transpose_12/biasconv2d_transpose_13/kernelconv2d_transpose_13/biasconv2d_transpose_14/kernelconv2d_transpose_14/biasconv2d_transpose_15/kernelconv2d_transpose_15/biasconv2d_19/kernelconv2d_19/biasConst_1add_metric_7/totaladd_metric_7/countConstConst_3Const_2add_metric_6/totaladd_metric_6/count*,
Tin%
#2!*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*:
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *-
f(R&
$__inference_signature_wrapper_246818

NoOpNoOp
��
Const_4Const"/device:CPU:0*
_output_shapes
: *
dtype0*��
value��B�� B��
�
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer_with_weights-3
layer-4
layer_with_weights-4
layer-5
layer_with_weights-5
layer-6
layer-7
	layer_with_weights-6
	layer-8

layer_with_weights-7

layer-9
layer-10
layer-11
layer-12
layer-13
layer-14
layer-15
layer-16
layer-17
layer-18
layer-19
layer-20
layer-21
layer-22
layer-23
layer-24
layer-25
layer-26
layer-27
layer-28
layer-29
layer-30
 	variables
!trainable_variables
"regularization_losses
#	keras_api
$__call__
*%&call_and_return_all_conditional_losses
&_default_save_signature
'	optimizer
(loss
)
signatures*
* 
�
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer_with_weights-3
layer-4
layer-5

layer_with_weights-4

layer-6
	layer_with_weights-5
	layer-7
*layer-8
+	variables
,trainable_variables
-regularization_losses
.	keras_api
/__call__
*0&call_and_return_all_conditional_losses*
�
1layer-0
2layer_with_weights-0
2layer-1
3layer-2
4layer_with_weights-1
4layer-3
5layer_with_weights-2
5layer-4
6layer_with_weights-3
6layer-5
7layer_with_weights-4
7layer-6
8layer_with_weights-5
8layer-7
9	variables
:trainable_variables
;regularization_losses
<	keras_api
=__call__
*>&call_and_return_all_conditional_losses*
�
?	variables
@trainable_variables
Aregularization_losses
B	keras_api
C__call__
*D&call_and_return_all_conditional_losses

Ekernel
Fbias
 G_jit_compiled_convolution_op*
�
H	variables
Itrainable_variables
Jregularization_losses
K	keras_api
L__call__
*M&call_and_return_all_conditional_losses

Nkernel
Obias
 P_jit_compiled_convolution_op*
�
Q	variables
Rtrainable_variables
Sregularization_losses
T	keras_api
U__call__
*V&call_and_return_all_conditional_losses

Wkernel
Xbias
 Y_jit_compiled_convolution_op*
�
Z	variables
[trainable_variables
\regularization_losses
]	keras_api
^__call__
*_&call_and_return_all_conditional_losses

`kernel
abias
 b_jit_compiled_convolution_op*
�
c	variables
dtrainable_variables
eregularization_losses
f	keras_api
g__call__
*h&call_and_return_all_conditional_losses* 
�
i	variables
jtrainable_variables
kregularization_losses
l	keras_api
m__call__
*n&call_and_return_all_conditional_losses

okernel
pbias*
�
q	variables
rtrainable_variables
sregularization_losses
t	keras_api
u__call__
*v&call_and_return_all_conditional_losses

wkernel
xbias*

y	keras_api* 

z	keras_api* 

{	keras_api* 

|	keras_api* 

}	keras_api* 

~	keras_api* 

	keras_api* 

�	keras_api* 

�	keras_api* 

�	keras_api* 

�	keras_api* 

�	keras_api* 

�	keras_api* 

�	keras_api* 

�	keras_api* 

�	keras_api* 

�	keras_api* 

�	keras_api* 
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses* 
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses*
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses*
�
E0
F1
N2
O3
W4
X5
`6
a7
w8
x9
o10
p11
�12
�13
�14
�15
�16
�17
�18
�19
�20
�21
�22
�23*
�
E0
F1
N2
O3
W4
X5
`6
a7
w8
x9
o10
p11
�12
�13
�14
�15
�16
�17
�18
�19
�20
�21
�22
�23*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
 	variables
!trainable_variables
"regularization_losses
$__call__
&_default_save_signature
*%&call_and_return_all_conditional_losses
&%"call_and_return_conditional_losses*
:
�trace_0
�trace_1
�trace_2
�trace_3* 
:
�trace_0
�trace_1
�trace_2
�trace_3* 
F
�
capture_24
�
capture_27
�
capture_28
�
capture_29* 
�
�
_variables
�_iterations
�_learning_rate
�_index_dict
�
_momentums
�_velocities
�_update_step_xla*
* 

�serving_default* 
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses* 
Z
E0
F1
N2
O3
W4
X5
`6
a7
w8
x9
o10
p11*
Z
E0
F1
N2
O3
W4
X5
`6
a7
w8
x9
o10
p11*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
+	variables
,trainable_variables
-regularization_losses
/__call__
*0&call_and_return_all_conditional_losses
&0"call_and_return_conditional_losses*
:
�trace_0
�trace_1
�trace_2
�trace_3* 
:
�trace_0
�trace_1
�trace_2
�trace_3* 
* 
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses
�kernel
	�bias*
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses* 
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses
�kernel
	�bias
!�_jit_compiled_convolution_op*
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses
�kernel
	�bias
!�_jit_compiled_convolution_op*
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses
�kernel
	�bias
!�_jit_compiled_convolution_op*
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses
�kernel
	�bias
!�_jit_compiled_convolution_op*
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses
�kernel
	�bias
!�_jit_compiled_convolution_op*
f
�0
�1
�2
�3
�4
�5
�6
�7
�8
�9
�10
�11*
f
�0
�1
�2
�3
�4
�5
�6
�7
�8
�9
�10
�11*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
9	variables
:trainable_variables
;regularization_losses
=__call__
*>&call_and_return_all_conditional_losses
&>"call_and_return_conditional_losses*
:
�trace_0
�trace_1
�trace_2
�trace_3* 
:
�trace_0
�trace_1
�trace_2
�trace_3* 

E0
F1*

E0
F1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
?	variables
@trainable_variables
Aregularization_losses
C__call__
*D&call_and_return_all_conditional_losses
&D"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
`Z
VARIABLE_VALUEconv2d_15/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE*
\V
VARIABLE_VALUEconv2d_15/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE*
* 

N0
O1*

N0
O1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
H	variables
Itrainable_variables
Jregularization_losses
L__call__
*M&call_and_return_all_conditional_losses
&M"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
`Z
VARIABLE_VALUEconv2d_16/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE*
\V
VARIABLE_VALUEconv2d_16/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE*
* 

W0
X1*

W0
X1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
Q	variables
Rtrainable_variables
Sregularization_losses
U__call__
*V&call_and_return_all_conditional_losses
&V"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
`Z
VARIABLE_VALUEconv2d_17/kernel6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUE*
\V
VARIABLE_VALUEconv2d_17/bias4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUE*
* 

`0
a1*

`0
a1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
Z	variables
[trainable_variables
\regularization_losses
^__call__
*_&call_and_return_all_conditional_losses
&_"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
`Z
VARIABLE_VALUEconv2d_18/kernel6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUE*
\V
VARIABLE_VALUEconv2d_18/bias4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUE*
* 
* 
* 
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
c	variables
dtrainable_variables
eregularization_losses
g__call__
*h&call_and_return_all_conditional_losses
&h"call_and_return_conditional_losses* 

�trace_0* 

�trace_0* 

o0
p1*

o0
p1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
i	variables
jtrainable_variables
kregularization_losses
m__call__
*n&call_and_return_all_conditional_losses
&n"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
_Y
VARIABLE_VALUEdense_10/kernel6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUE*
[U
VARIABLE_VALUEdense_10/bias4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUE*

w0
x1*

w0
x1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
q	variables
rtrainable_variables
sregularization_losses
u__call__
*v&call_and_return_all_conditional_losses
&v"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
^X
VARIABLE_VALUEdense_9/kernel6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUE*
ZT
VARIABLE_VALUEdense_9/bias4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUE*
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses* 

�trace_0* 

�trace_0* 
* 
* 
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
* 
* 
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
PJ
VARIABLE_VALUEdense_11/kernel'variables/12/.ATTRIBUTES/VARIABLE_VALUE*
NH
VARIABLE_VALUEdense_11/bias'variables/13/.ATTRIBUTES/VARIABLE_VALUE*
[U
VARIABLE_VALUEconv2d_transpose_12/kernel'variables/14/.ATTRIBUTES/VARIABLE_VALUE*
YS
VARIABLE_VALUEconv2d_transpose_12/bias'variables/15/.ATTRIBUTES/VARIABLE_VALUE*
[U
VARIABLE_VALUEconv2d_transpose_13/kernel'variables/16/.ATTRIBUTES/VARIABLE_VALUE*
YS
VARIABLE_VALUEconv2d_transpose_13/bias'variables/17/.ATTRIBUTES/VARIABLE_VALUE*
[U
VARIABLE_VALUEconv2d_transpose_14/kernel'variables/18/.ATTRIBUTES/VARIABLE_VALUE*
YS
VARIABLE_VALUEconv2d_transpose_14/bias'variables/19/.ATTRIBUTES/VARIABLE_VALUE*
[U
VARIABLE_VALUEconv2d_transpose_15/kernel'variables/20/.ATTRIBUTES/VARIABLE_VALUE*
YS
VARIABLE_VALUEconv2d_transpose_15/bias'variables/21/.ATTRIBUTES/VARIABLE_VALUE*
QK
VARIABLE_VALUEconv2d_19/kernel'variables/22/.ATTRIBUTES/VARIABLE_VALUE*
OI
VARIABLE_VALUEconv2d_19/bias'variables/23/.ATTRIBUTES/VARIABLE_VALUE*
* 
�
0
1
2
3
4
5
6
7
	8

9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30*

�0
�1
�2*
* 
* 
F
�
capture_24
�
capture_27
�
capture_28
�
capture_29* 
F
�
capture_24
�
capture_27
�
capture_28
�
capture_29* 
F
�
capture_24
�
capture_27
�
capture_28
�
capture_29* 
F
�
capture_24
�
capture_27
�
capture_28
�
capture_29* 
F
�
capture_24
�
capture_27
�
capture_28
�
capture_29* 
F
�
capture_24
�
capture_27
�
capture_28
�
capture_29* 
F
�
capture_24
�
capture_27
�
capture_28
�
capture_29* 
F
�
capture_24
�
capture_27
�
capture_28
�
capture_29* 
* 
* 
* 
* 
�
�0
�1
�2
�3
�4
�5
�6
�7
�8
�9
�10
�11
�12
�13
�14
�15
�16
�17
�18
�19
�20
�21
�22
�23
�24
�25
�26
�27
�28
�29
�30
�31
�32
�33
�34
�35
�36
�37
�38
�39
�40
�41
�42
�43
�44
�45
�46
�47
�48*
SM
VARIABLE_VALUE	iteration0optimizer/_iterations/.ATTRIBUTES/VARIABLE_VALUE*
ZT
VARIABLE_VALUElearning_rate3optimizer/_learning_rate/.ATTRIBUTES/VARIABLE_VALUE*
* 
�
�0
�1
�2
�3
�4
�5
�6
�7
�8
�9
�10
�11
�12
�13
�14
�15
�16
�17
�18
�19
�20
�21
�22
�23*
�
�0
�1
�2
�3
�4
�5
�6
�7
�8
�9
�10
�11
�12
�13
�14
�15
�16
�17
�18
�19
�20
�21
�22
�23*
�
�trace_0
�trace_1
�trace_2
�trace_3
�trace_4
�trace_5
�trace_6
�trace_7
�trace_8
�trace_9
�trace_10
�trace_11
�trace_12
�trace_13
�trace_14
�trace_15
�trace_16
�trace_17
�trace_18
�trace_19
�trace_20
�trace_21
�trace_22
�trace_23* 
F
�
capture_24
�
capture_27
�
capture_28
�
capture_29* 
* 
* 
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses* 

�trace_0
�trace_1* 

�trace_0
�trace_1* 
* 
C
0
1
2
3
4
5

6
	7
*8*
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

�0
�1*

�0
�1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
* 
* 
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses* 

�trace_0* 

�trace_0* 

�0
�1*

�0
�1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
* 

�0
�1*

�0
�1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
* 

�0
�1*

�0
�1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
* 

�0
�1*

�0
�1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
* 

�0
�1*

�0
�1*
* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
* 
* 
<
10
21
32
43
54
65
76
87*
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

�0*
* 

�vae_loss*
* 
* 
* 
* 

�0*
* 

�reconstruction_loss*
* 
* 
<
�	variables
�	keras_api

�total

�count*
<
�	variables
�	keras_api

�total

�count*
<
�	variables
�	keras_api

�total

�count*
b\
VARIABLE_VALUEAdam/m/conv2d_15/kernel1optimizer/_variables/1/.ATTRIBUTES/VARIABLE_VALUE*
b\
VARIABLE_VALUEAdam/v/conv2d_15/kernel1optimizer/_variables/2/.ATTRIBUTES/VARIABLE_VALUE*
`Z
VARIABLE_VALUEAdam/m/conv2d_15/bias1optimizer/_variables/3/.ATTRIBUTES/VARIABLE_VALUE*
`Z
VARIABLE_VALUEAdam/v/conv2d_15/bias1optimizer/_variables/4/.ATTRIBUTES/VARIABLE_VALUE*
b\
VARIABLE_VALUEAdam/m/conv2d_16/kernel1optimizer/_variables/5/.ATTRIBUTES/VARIABLE_VALUE*
b\
VARIABLE_VALUEAdam/v/conv2d_16/kernel1optimizer/_variables/6/.ATTRIBUTES/VARIABLE_VALUE*
`Z
VARIABLE_VALUEAdam/m/conv2d_16/bias1optimizer/_variables/7/.ATTRIBUTES/VARIABLE_VALUE*
`Z
VARIABLE_VALUEAdam/v/conv2d_16/bias1optimizer/_variables/8/.ATTRIBUTES/VARIABLE_VALUE*
b\
VARIABLE_VALUEAdam/m/conv2d_17/kernel1optimizer/_variables/9/.ATTRIBUTES/VARIABLE_VALUE*
c]
VARIABLE_VALUEAdam/v/conv2d_17/kernel2optimizer/_variables/10/.ATTRIBUTES/VARIABLE_VALUE*
a[
VARIABLE_VALUEAdam/m/conv2d_17/bias2optimizer/_variables/11/.ATTRIBUTES/VARIABLE_VALUE*
a[
VARIABLE_VALUEAdam/v/conv2d_17/bias2optimizer/_variables/12/.ATTRIBUTES/VARIABLE_VALUE*
c]
VARIABLE_VALUEAdam/m/conv2d_18/kernel2optimizer/_variables/13/.ATTRIBUTES/VARIABLE_VALUE*
c]
VARIABLE_VALUEAdam/v/conv2d_18/kernel2optimizer/_variables/14/.ATTRIBUTES/VARIABLE_VALUE*
a[
VARIABLE_VALUEAdam/m/conv2d_18/bias2optimizer/_variables/15/.ATTRIBUTES/VARIABLE_VALUE*
a[
VARIABLE_VALUEAdam/v/conv2d_18/bias2optimizer/_variables/16/.ATTRIBUTES/VARIABLE_VALUE*
a[
VARIABLE_VALUEAdam/m/dense_9/kernel2optimizer/_variables/17/.ATTRIBUTES/VARIABLE_VALUE*
a[
VARIABLE_VALUEAdam/v/dense_9/kernel2optimizer/_variables/18/.ATTRIBUTES/VARIABLE_VALUE*
_Y
VARIABLE_VALUEAdam/m/dense_9/bias2optimizer/_variables/19/.ATTRIBUTES/VARIABLE_VALUE*
_Y
VARIABLE_VALUEAdam/v/dense_9/bias2optimizer/_variables/20/.ATTRIBUTES/VARIABLE_VALUE*
b\
VARIABLE_VALUEAdam/m/dense_10/kernel2optimizer/_variables/21/.ATTRIBUTES/VARIABLE_VALUE*
b\
VARIABLE_VALUEAdam/v/dense_10/kernel2optimizer/_variables/22/.ATTRIBUTES/VARIABLE_VALUE*
`Z
VARIABLE_VALUEAdam/m/dense_10/bias2optimizer/_variables/23/.ATTRIBUTES/VARIABLE_VALUE*
`Z
VARIABLE_VALUEAdam/v/dense_10/bias2optimizer/_variables/24/.ATTRIBUTES/VARIABLE_VALUE*
b\
VARIABLE_VALUEAdam/m/dense_11/kernel2optimizer/_variables/25/.ATTRIBUTES/VARIABLE_VALUE*
b\
VARIABLE_VALUEAdam/v/dense_11/kernel2optimizer/_variables/26/.ATTRIBUTES/VARIABLE_VALUE*
`Z
VARIABLE_VALUEAdam/m/dense_11/bias2optimizer/_variables/27/.ATTRIBUTES/VARIABLE_VALUE*
`Z
VARIABLE_VALUEAdam/v/dense_11/bias2optimizer/_variables/28/.ATTRIBUTES/VARIABLE_VALUE*
mg
VARIABLE_VALUE!Adam/m/conv2d_transpose_12/kernel2optimizer/_variables/29/.ATTRIBUTES/VARIABLE_VALUE*
mg
VARIABLE_VALUE!Adam/v/conv2d_transpose_12/kernel2optimizer/_variables/30/.ATTRIBUTES/VARIABLE_VALUE*
ke
VARIABLE_VALUEAdam/m/conv2d_transpose_12/bias2optimizer/_variables/31/.ATTRIBUTES/VARIABLE_VALUE*
ke
VARIABLE_VALUEAdam/v/conv2d_transpose_12/bias2optimizer/_variables/32/.ATTRIBUTES/VARIABLE_VALUE*
mg
VARIABLE_VALUE!Adam/m/conv2d_transpose_13/kernel2optimizer/_variables/33/.ATTRIBUTES/VARIABLE_VALUE*
mg
VARIABLE_VALUE!Adam/v/conv2d_transpose_13/kernel2optimizer/_variables/34/.ATTRIBUTES/VARIABLE_VALUE*
ke
VARIABLE_VALUEAdam/m/conv2d_transpose_13/bias2optimizer/_variables/35/.ATTRIBUTES/VARIABLE_VALUE*
ke
VARIABLE_VALUEAdam/v/conv2d_transpose_13/bias2optimizer/_variables/36/.ATTRIBUTES/VARIABLE_VALUE*
mg
VARIABLE_VALUE!Adam/m/conv2d_transpose_14/kernel2optimizer/_variables/37/.ATTRIBUTES/VARIABLE_VALUE*
mg
VARIABLE_VALUE!Adam/v/conv2d_transpose_14/kernel2optimizer/_variables/38/.ATTRIBUTES/VARIABLE_VALUE*
ke
VARIABLE_VALUEAdam/m/conv2d_transpose_14/bias2optimizer/_variables/39/.ATTRIBUTES/VARIABLE_VALUE*
ke
VARIABLE_VALUEAdam/v/conv2d_transpose_14/bias2optimizer/_variables/40/.ATTRIBUTES/VARIABLE_VALUE*
mg
VARIABLE_VALUE!Adam/m/conv2d_transpose_15/kernel2optimizer/_variables/41/.ATTRIBUTES/VARIABLE_VALUE*
mg
VARIABLE_VALUE!Adam/v/conv2d_transpose_15/kernel2optimizer/_variables/42/.ATTRIBUTES/VARIABLE_VALUE*
ke
VARIABLE_VALUEAdam/m/conv2d_transpose_15/bias2optimizer/_variables/43/.ATTRIBUTES/VARIABLE_VALUE*
ke
VARIABLE_VALUEAdam/v/conv2d_transpose_15/bias2optimizer/_variables/44/.ATTRIBUTES/VARIABLE_VALUE*
c]
VARIABLE_VALUEAdam/m/conv2d_19/kernel2optimizer/_variables/45/.ATTRIBUTES/VARIABLE_VALUE*
c]
VARIABLE_VALUEAdam/v/conv2d_19/kernel2optimizer/_variables/46/.ATTRIBUTES/VARIABLE_VALUE*
a[
VARIABLE_VALUEAdam/m/conv2d_19/bias2optimizer/_variables/47/.ATTRIBUTES/VARIABLE_VALUE*
a[
VARIABLE_VALUEAdam/v/conv2d_19/bias2optimizer/_variables/48/.ATTRIBUTES/VARIABLE_VALUE*
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

�0
�1*

�	variables*
SM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE*
SM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE*

�0
�1*

�	variables*
`Z
VARIABLE_VALUEadd_metric_6/total4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE*
`Z
VARIABLE_VALUEadd_metric_6/count4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE*

�0
�1*

�	variables*
`Z
VARIABLE_VALUEadd_metric_7/total4keras_api/metrics/2/total/.ATTRIBUTES/VARIABLE_VALUE*
`Z
VARIABLE_VALUEadd_metric_7/count4keras_api/metrics/2/count/.ATTRIBUTES/VARIABLE_VALUE*
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filenameconv2d_15/kernelconv2d_15/biasconv2d_16/kernelconv2d_16/biasconv2d_17/kernelconv2d_17/biasconv2d_18/kernelconv2d_18/biasdense_10/kerneldense_10/biasdense_9/kerneldense_9/biasdense_11/kerneldense_11/biasconv2d_transpose_12/kernelconv2d_transpose_12/biasconv2d_transpose_13/kernelconv2d_transpose_13/biasconv2d_transpose_14/kernelconv2d_transpose_14/biasconv2d_transpose_15/kernelconv2d_transpose_15/biasconv2d_19/kernelconv2d_19/bias	iterationlearning_rateAdam/m/conv2d_15/kernelAdam/v/conv2d_15/kernelAdam/m/conv2d_15/biasAdam/v/conv2d_15/biasAdam/m/conv2d_16/kernelAdam/v/conv2d_16/kernelAdam/m/conv2d_16/biasAdam/v/conv2d_16/biasAdam/m/conv2d_17/kernelAdam/v/conv2d_17/kernelAdam/m/conv2d_17/biasAdam/v/conv2d_17/biasAdam/m/conv2d_18/kernelAdam/v/conv2d_18/kernelAdam/m/conv2d_18/biasAdam/v/conv2d_18/biasAdam/m/dense_9/kernelAdam/v/dense_9/kernelAdam/m/dense_9/biasAdam/v/dense_9/biasAdam/m/dense_10/kernelAdam/v/dense_10/kernelAdam/m/dense_10/biasAdam/v/dense_10/biasAdam/m/dense_11/kernelAdam/v/dense_11/kernelAdam/m/dense_11/biasAdam/v/dense_11/bias!Adam/m/conv2d_transpose_12/kernel!Adam/v/conv2d_transpose_12/kernelAdam/m/conv2d_transpose_12/biasAdam/v/conv2d_transpose_12/bias!Adam/m/conv2d_transpose_13/kernel!Adam/v/conv2d_transpose_13/kernelAdam/m/conv2d_transpose_13/biasAdam/v/conv2d_transpose_13/bias!Adam/m/conv2d_transpose_14/kernel!Adam/v/conv2d_transpose_14/kernelAdam/m/conv2d_transpose_14/biasAdam/v/conv2d_transpose_14/bias!Adam/m/conv2d_transpose_15/kernel!Adam/v/conv2d_transpose_15/kernelAdam/m/conv2d_transpose_15/biasAdam/v/conv2d_transpose_15/biasAdam/m/conv2d_19/kernelAdam/v/conv2d_19/kernelAdam/m/conv2d_19/biasAdam/v/conv2d_19/biastotalcountadd_metric_6/totaladd_metric_6/countadd_metric_7/totaladd_metric_7/countConst_4*]
TinV
T2R*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *(
f#R!
__inference__traced_save_249042
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenameconv2d_15/kernelconv2d_15/biasconv2d_16/kernelconv2d_16/biasconv2d_17/kernelconv2d_17/biasconv2d_18/kernelconv2d_18/biasdense_10/kerneldense_10/biasdense_9/kerneldense_9/biasdense_11/kerneldense_11/biasconv2d_transpose_12/kernelconv2d_transpose_12/biasconv2d_transpose_13/kernelconv2d_transpose_13/biasconv2d_transpose_14/kernelconv2d_transpose_14/biasconv2d_transpose_15/kernelconv2d_transpose_15/biasconv2d_19/kernelconv2d_19/bias	iterationlearning_rateAdam/m/conv2d_15/kernelAdam/v/conv2d_15/kernelAdam/m/conv2d_15/biasAdam/v/conv2d_15/biasAdam/m/conv2d_16/kernelAdam/v/conv2d_16/kernelAdam/m/conv2d_16/biasAdam/v/conv2d_16/biasAdam/m/conv2d_17/kernelAdam/v/conv2d_17/kernelAdam/m/conv2d_17/biasAdam/v/conv2d_17/biasAdam/m/conv2d_18/kernelAdam/v/conv2d_18/kernelAdam/m/conv2d_18/biasAdam/v/conv2d_18/biasAdam/m/dense_9/kernelAdam/v/dense_9/kernelAdam/m/dense_9/biasAdam/v/dense_9/biasAdam/m/dense_10/kernelAdam/v/dense_10/kernelAdam/m/dense_10/biasAdam/v/dense_10/biasAdam/m/dense_11/kernelAdam/v/dense_11/kernelAdam/m/dense_11/biasAdam/v/dense_11/bias!Adam/m/conv2d_transpose_12/kernel!Adam/v/conv2d_transpose_12/kernelAdam/m/conv2d_transpose_12/biasAdam/v/conv2d_transpose_12/bias!Adam/m/conv2d_transpose_13/kernel!Adam/v/conv2d_transpose_13/kernelAdam/m/conv2d_transpose_13/biasAdam/v/conv2d_transpose_13/bias!Adam/m/conv2d_transpose_14/kernel!Adam/v/conv2d_transpose_14/kernelAdam/m/conv2d_transpose_14/biasAdam/v/conv2d_transpose_14/bias!Adam/m/conv2d_transpose_15/kernel!Adam/v/conv2d_transpose_15/kernelAdam/m/conv2d_transpose_15/biasAdam/v/conv2d_transpose_15/biasAdam/m/conv2d_19/kernelAdam/v/conv2d_19/kernelAdam/m/conv2d_19/biasAdam/v/conv2d_19/biastotalcountadd_metric_6/totaladd_metric_6/countadd_metric_7/totaladd_metric_7/count*\
TinU
S2Q*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *+
f&R$
"__inference__traced_restore_249292��
�V
�
?__inference_vae_layer_call_and_return_conditional_losses_246510

inputs(
encoder_246395:
encoder_246397:(
encoder_246399: 
encoder_246401: )
encoder_246403: �
encoder_246405:	�*
encoder_246407:��
encoder_246409:	�"
encoder_246411:
��
encoder_246413:"
encoder_246415:
��
encoder_246417:"
decoder_246422:
��
decoder_246424:
��*
decoder_246426:��
decoder_246428:	�)
decoder_246430:@�
decoder_246432:@(
decoder_246434: @
decoder_246436: (
decoder_246438: 
decoder_246440:(
decoder_246442:
decoder_246444:
unknown
add_metric_7_246478: 
add_metric_7_246480: 
	unknown_0
	unknown_1
	unknown_2
add_metric_6_246503: 
add_metric_6_246505: 
identity

identity_1��$add_metric_6/StatefulPartitionedCall�$add_metric_7/StatefulPartitionedCall�!conv2d_15/StatefulPartitionedCall�!conv2d_16/StatefulPartitionedCall�!conv2d_17/StatefulPartitionedCall�!conv2d_18/StatefulPartitionedCall�decoder/StatefulPartitionedCall� dense_10/StatefulPartitionedCall�dense_9/StatefulPartitionedCall�encoder/StatefulPartitionedCall�
encoder/StatefulPartitionedCallStatefulPartitionedCallinputsencoder_246395encoder_246397encoder_246399encoder_246401encoder_246403encoder_246405encoder_246407encoder_246409encoder_246411encoder_246413encoder_246415encoder_246417*
Tin
2*
Tout
2*
_collective_manager_ids
 *2
_output_shapes 
:::*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_encoder_layer_call_and_return_conditional_losses_245243�
decoder/StatefulPartitionedCallStatefulPartitionedCall(encoder/StatefulPartitionedCall:output:2decoder_246422decoder_246424decoder_246426decoder_246428decoder_246430decoder_246432decoder_246434decoder_246436decoder_246438decoder_246440decoder_246442decoder_246444*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_decoder_layer_call_and_return_conditional_losses_245811m
tf.reshape_6/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB:
���������s
tf.reshape_6/ReshapeReshapeinputs#tf.reshape_6/Reshape/shape:output:0*
T0*
_output_shapes

:��m
tf.reshape_7/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB:
����������
tf.reshape_7/ReshapeReshape(decoder/StatefulPartitionedCall:output:0#tf.reshape_7/Reshape/shape:output:0*
T0*
_output_shapes

:���
!conv2d_15/StatefulPartitionedCallStatefulPartitionedCallinputsencoder_246395encoder_246397*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_15_layer_call_and_return_conditional_losses_244947�
!conv2d_16/StatefulPartitionedCallStatefulPartitionedCall*conv2d_15/StatefulPartitionedCall:output:0encoder_246399encoder_246401*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
: *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_16_layer_call_and_return_conditional_losses_244964�
.tf.math.squared_difference_3/SquaredDifferenceSquaredDifferencetf.reshape_7/Reshape:output:0tf.reshape_6/Reshape:output:0*
T0*
_output_shapes

:���
!conv2d_17/StatefulPartitionedCallStatefulPartitionedCall*conv2d_16/StatefulPartitionedCall:output:0encoder_246403encoder_246405*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_17_layer_call_and_return_conditional_losses_244981w
,tf.math.reduce_mean_6/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
����������
tf.math.reduce_mean_6/MeanMean2tf.math.squared_difference_3/SquaredDifference:z:05tf.math.reduce_mean_6/Mean/reduction_indices:output:0*
T0*
_output_shapes
: �
!conv2d_18/StatefulPartitionedCallStatefulPartitionedCall*conv2d_17/StatefulPartitionedCall:output:0encoder_246407encoder_246409*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_18_layer_call_and_return_conditional_losses_244998^
tf.math.multiply_12/Mul/yConst*
_output_shapes
: *
dtype0*
valueB
 * �(F�
tf.math.multiply_12/MulMul#tf.math.reduce_mean_6/Mean:output:0"tf.math.multiply_12/Mul/y:output:0*
T0*
_output_shapes
: �
flatten_3/PartitionedCallPartitionedCall*conv2d_18/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 * 
_output_shapes
:
��* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_flatten_3_layer_call_and_return_conditional_losses_245010e
tf.math.multiply_14/MulMulunknowntf.math.multiply_12/Mul:z:0*
T0*
_output_shapes
: �
 dense_10/StatefulPartitionedCallStatefulPartitionedCall"flatten_3/PartitionedCall:output:0encoder_246415encoder_246417*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dense_10_layer_call_and_return_conditional_losses_245038�
dense_9/StatefulPartitionedCallStatefulPartitionedCall"flatten_3/PartitionedCall:output:0encoder_246411encoder_246413*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_dense_9_layer_call_and_return_conditional_losses_245022�
$add_metric_7/StatefulPartitionedCallStatefulPartitionedCalltf.math.multiply_12/Mul:z:0add_metric_7_246478add_metric_7_246480*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_add_metric_7_layer_call_and_return_conditional_losses_246026�
tf.__operators__.add_6/AddV2AddV2	unknown_0)dense_10/StatefulPartitionedCall:output:0*
T0*
_output_shapes

:t
tf.math.square_3/SquareSquare(dense_9/StatefulPartitionedCall:output:0*
T0*
_output_shapes

:l
tf.math.exp_3/ExpExp)dense_10/StatefulPartitionedCall:output:0*
T0*
_output_shapes

:�
tf.math.subtract_6/SubSub tf.__operators__.add_6/AddV2:z:0tf.math.square_3/Square:y:0*
T0*
_output_shapes

:y
tf.math.subtract_7/SubSubtf.math.subtract_6/Sub:z:0tf.math.exp_3/Exp:y:0*
T0*
_output_shapes

:l
*tf.math.reduce_sum_3/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
tf.math.reduce_sum_3/SumSumtf.math.subtract_7/Sub:z:03tf.math.reduce_sum_3/Sum/reduction_indices:output:0*
T0*
_output_shapes
:q
tf.math.multiply_13/MulMul	unknown_1!tf.math.reduce_sum_3/Sum:output:0*
T0*
_output_shapes
:k
tf.math.multiply_15/MulMul	unknown_2tf.math.multiply_13/Mul:z:0*
T0*
_output_shapes
:�
tf.__operators__.add_7/AddV2AddV2tf.math.multiply_14/Mul:z:0tf.math.multiply_15/Mul:z:0*
T0*
_output_shapes
:e
tf.math.reduce_mean_7/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
tf.math.reduce_mean_7/MeanMean tf.__operators__.add_7/AddV2:z:0$tf.math.reduce_mean_7/Const:output:0*
T0*
_output_shapes
: �
add_loss_3/PartitionedCallPartitionedCall#tf.math.reduce_mean_7/Mean:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: : * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_add_loss_3_layer_call_and_return_conditional_losses_246055�
$add_metric_6/StatefulPartitionedCallStatefulPartitionedCall#tf.math.reduce_mean_7/Mean:output:0add_metric_6_246503add_metric_6_246505*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_add_metric_6_layer_call_and_return_conditional_losses_246075v
IdentityIdentity(decoder/StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<c

Identity_1Identity#add_loss_3/PartitionedCall:output:1^NoOp*
T0*
_output_shapes
: �
NoOpNoOp%^add_metric_6/StatefulPartitionedCall%^add_metric_7/StatefulPartitionedCall"^conv2d_15/StatefulPartitionedCall"^conv2d_16/StatefulPartitionedCall"^conv2d_17/StatefulPartitionedCall"^conv2d_18/StatefulPartitionedCall ^decoder/StatefulPartitionedCall!^dense_10/StatefulPartitionedCall ^dense_9/StatefulPartitionedCall ^encoder/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*e
_input_shapesT
R:<<: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2L
$add_metric_6/StatefulPartitionedCall$add_metric_6/StatefulPartitionedCall2L
$add_metric_7/StatefulPartitionedCall$add_metric_7/StatefulPartitionedCall2F
!conv2d_15/StatefulPartitionedCall!conv2d_15/StatefulPartitionedCall2F
!conv2d_16/StatefulPartitionedCall!conv2d_16/StatefulPartitionedCall2F
!conv2d_17/StatefulPartitionedCall!conv2d_17/StatefulPartitionedCall2F
!conv2d_18/StatefulPartitionedCall!conv2d_18/StatefulPartitionedCall2B
decoder/StatefulPartitionedCalldecoder/StatefulPartitionedCall2D
 dense_10/StatefulPartitionedCall dense_10/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2B
encoder/StatefulPartitionedCallencoder/StatefulPartitionedCall:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
Q
#__inference__update_step_xla_247531
gradient
variable:
��*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*!
_input_shapes
:
��: *
	_noinline(:J F
 
_output_shapes
:
��
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�
X
#__inference__update_step_xla_247551
gradient#
variable:@�*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*(
_input_shapes
:@�: *
	_noinline(:Q M
'
_output_shapes
:@�
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�
G
+__inference_add_loss_3_layer_call_fn_248191

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: : * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_add_loss_3_layer_call_and_return_conditional_losses_246055O
IdentityIdentityPartitionedCall:output:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: :> :

_output_shapes
: 
 
_user_specified_nameinputs
�!
�
O__inference_conv2d_transpose_13_layer_call_and_return_conditional_losses_248429

inputsC
(conv2d_transpose_readvariableop_resource:@�-
biasadd_readvariableop_resource:@
identity��BiasAdd/ReadVariableOp�conv2d_transpose/ReadVariableOpI
ShapeShapeinputs*
T0*
_output_shapes
::��]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask_
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_2StridedSliceShape:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskG
mul/yConst*
_output_shapes
: *
dtype0*
value	B :U
mulMulstrided_slice_1:output:0mul/y:output:0*
T0*
_output_shapes
: I
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :Y
mul_1Mulstrided_slice_2:output:0mul_1/y:output:0*
T0*
_output_shapes
: I
stack/3Const*
_output_shapes
: *
dtype0*
value	B :@y
stackPackstrided_slice:output:0mul:z:0	mul_1:z:0stack/3:output:0*
N*
T0*
_output_shapes
:_
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_3StridedSlicestack:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
conv2d_transpose/ReadVariableOpReadVariableOp(conv2d_transpose_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
conv2d_transposeConv2DBackpropInputstack:output:0'conv2d_transpose/ReadVariableOp:value:0inputs*
T0*A
_output_shapes/
-:+���������������������������@*
paddingSAME*
strides
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype0�
BiasAddBiasAddconv2d_transpose:output:0BiasAdd/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+���������������������������@j
ReluReluBiasAdd:output:0*
T0*A
_output_shapes/
-:+���������������������������@{
IdentityIdentityRelu:activations:0^NoOp*
T0*A
_output_shapes/
-:+���������������������������@�
NoOpNoOp^BiasAdd/ReadVariableOp ^conv2d_transpose/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*E
_input_shapes4
2:,����������������������������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2B
conv2d_transpose/ReadVariableOpconv2d_transpose/ReadVariableOp:j f
B
_output_shapes0
.:,����������������������������
 
_user_specified_nameinputs
�
L
#__inference__update_step_xla_247546
gradient
variable:	�*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*
_input_shapes
	:�: *
	_noinline(:E A

_output_shapes	
:�
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�	
�
D__inference_dense_11_layer_call_and_return_conditional_losses_245614

inputs2
matmul_readvariableop_resource:
��/
biasadd_readvariableop_resource:
��
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpv
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0b
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0* 
_output_shapes
:
��t
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes

:��*
dtype0o
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0* 
_output_shapes
:
��I
ReluReluBiasAdd:output:0*
T0* 
_output_shapes
:
��Z
IdentityIdentityRelu:activations:0^NoOp*
T0* 
_output_shapes
:
��w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*!
_input_shapes
:: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:F B

_output_shapes

:
 
_user_specified_nameinputs
�
�
4__inference_conv2d_transpose_13_layer_call_fn_248395

inputs"
unknown:@�
	unknown_0:@
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *A
_output_shapes/
-:+���������������������������@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_13_layer_call_and_return_conditional_losses_245499�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*A
_output_shapes/
-:+���������������������������@`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*E
_input_shapes4
2:,����������������������������: : 22
StatefulPartitionedCallStatefulPartitionedCall:j f
B
_output_shapes0
.:,����������������������������
 
_user_specified_nameinputs
�

�
E__inference_conv2d_19_layer_call_and_return_conditional_losses_248535

inputs8
conv2d_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp|
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0t
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<U
SigmoidSigmoidBiasAdd:output:0*
T0*&
_output_shapes
:<<Y
IdentityIdentitySigmoid:y:0^NoOp*
T0*&
_output_shapes
:<<w
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:<<: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs
�
�
$__inference_vae_layer_call_fn_246578
input_7!
unknown:
	unknown_0:#
	unknown_1: 
	unknown_2: $
	unknown_3: �
	unknown_4:	�%
	unknown_5:��
	unknown_6:	�
	unknown_7:
��
	unknown_8:
	unknown_9:
��

unknown_10:

unknown_11:
��

unknown_12:
��&

unknown_13:��

unknown_14:	�%

unknown_15:@�

unknown_16:@$

unknown_17: @

unknown_18: $

unknown_19: 

unknown_20:$

unknown_21:

unknown_22:

unknown_23

unknown_24: 

unknown_25: 

unknown_26

unknown_27

unknown_28

unknown_29: 

unknown_30: 
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_7unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30*,
Tin%
#2!*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:<<: *:
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *H
fCRA
?__inference_vae_layer_call_and_return_conditional_losses_246510n
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*e
_input_shapesT
R:<<: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
&
_output_shapes
:<<
!
_user_specified_name	input_7:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�	
�
C__inference_dense_9_layer_call_and_return_conditional_losses_248185

inputs2
matmul_readvariableop_resource:
��-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpv
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0`
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0m
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:V
IdentityIdentityBiasAdd:output:0^NoOp*
T0*
_output_shapes

:w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*#
_input_shapes
:
��: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:H D
 
_output_shapes
:
��
 
_user_specified_nameinputs
��
�!
!__inference__wrapped_model_244932
input_7N
4vae_encoder_conv2d_15_conv2d_readvariableop_resource:C
5vae_encoder_conv2d_15_biasadd_readvariableop_resource:N
4vae_encoder_conv2d_16_conv2d_readvariableop_resource: C
5vae_encoder_conv2d_16_biasadd_readvariableop_resource: O
4vae_encoder_conv2d_17_conv2d_readvariableop_resource: �D
5vae_encoder_conv2d_17_biasadd_readvariableop_resource:	�P
4vae_encoder_conv2d_18_conv2d_readvariableop_resource:��D
5vae_encoder_conv2d_18_biasadd_readvariableop_resource:	�F
2vae_encoder_dense_9_matmul_readvariableop_resource:
��A
3vae_encoder_dense_9_biasadd_readvariableop_resource:G
3vae_encoder_dense_10_matmul_readvariableop_resource:
��B
4vae_encoder_dense_10_biasadd_readvariableop_resource:G
3vae_decoder_dense_11_matmul_readvariableop_resource:
��D
4vae_decoder_dense_11_biasadd_readvariableop_resource:
��d
Hvae_decoder_conv2d_transpose_12_conv2d_transpose_readvariableop_resource:��N
?vae_decoder_conv2d_transpose_12_biasadd_readvariableop_resource:	�c
Hvae_decoder_conv2d_transpose_13_conv2d_transpose_readvariableop_resource:@�M
?vae_decoder_conv2d_transpose_13_biasadd_readvariableop_resource:@b
Hvae_decoder_conv2d_transpose_14_conv2d_transpose_readvariableop_resource: @M
?vae_decoder_conv2d_transpose_14_biasadd_readvariableop_resource: b
Hvae_decoder_conv2d_transpose_15_conv2d_transpose_readvariableop_resource: M
?vae_decoder_conv2d_transpose_15_biasadd_readvariableop_resource:N
4vae_decoder_conv2d_19_conv2d_readvariableop_resource:C
5vae_decoder_conv2d_19_biasadd_readvariableop_resource:

vae_2448767
-vae_add_metric_7_assignaddvariableop_resource: 9
/vae_add_metric_7_assignaddvariableop_1_resource: 

vae_244900

vae_244909

vae_2449127
-vae_add_metric_6_assignaddvariableop_resource: 9
/vae_add_metric_6_assignaddvariableop_1_resource: 
identity��$vae/add_metric_6/AssignAddVariableOp�&vae/add_metric_6/AssignAddVariableOp_1�*vae/add_metric_6/div_no_nan/ReadVariableOp�,vae/add_metric_6/div_no_nan/ReadVariableOp_1�$vae/add_metric_7/AssignAddVariableOp�&vae/add_metric_7/AssignAddVariableOp_1�*vae/add_metric_7/div_no_nan/ReadVariableOp�,vae/add_metric_7/div_no_nan/ReadVariableOp_1�$vae/conv2d_15/BiasAdd/ReadVariableOp�#vae/conv2d_15/Conv2D/ReadVariableOp�$vae/conv2d_16/BiasAdd/ReadVariableOp�#vae/conv2d_16/Conv2D/ReadVariableOp�$vae/conv2d_17/BiasAdd/ReadVariableOp�#vae/conv2d_17/Conv2D/ReadVariableOp�$vae/conv2d_18/BiasAdd/ReadVariableOp�#vae/conv2d_18/Conv2D/ReadVariableOp�,vae/decoder/conv2d_19/BiasAdd/ReadVariableOp�+vae/decoder/conv2d_19/Conv2D/ReadVariableOp�6vae/decoder/conv2d_transpose_12/BiasAdd/ReadVariableOp�?vae/decoder/conv2d_transpose_12/conv2d_transpose/ReadVariableOp�6vae/decoder/conv2d_transpose_13/BiasAdd/ReadVariableOp�?vae/decoder/conv2d_transpose_13/conv2d_transpose/ReadVariableOp�6vae/decoder/conv2d_transpose_14/BiasAdd/ReadVariableOp�?vae/decoder/conv2d_transpose_14/conv2d_transpose/ReadVariableOp�6vae/decoder/conv2d_transpose_15/BiasAdd/ReadVariableOp�?vae/decoder/conv2d_transpose_15/conv2d_transpose/ReadVariableOp�+vae/decoder/dense_11/BiasAdd/ReadVariableOp�*vae/decoder/dense_11/MatMul/ReadVariableOp�#vae/dense_10/BiasAdd/ReadVariableOp�"vae/dense_10/MatMul/ReadVariableOp�"vae/dense_9/BiasAdd/ReadVariableOp�!vae/dense_9/MatMul/ReadVariableOp�,vae/encoder/conv2d_15/BiasAdd/ReadVariableOp�+vae/encoder/conv2d_15/Conv2D/ReadVariableOp�,vae/encoder/conv2d_16/BiasAdd/ReadVariableOp�+vae/encoder/conv2d_16/Conv2D/ReadVariableOp�,vae/encoder/conv2d_17/BiasAdd/ReadVariableOp�+vae/encoder/conv2d_17/Conv2D/ReadVariableOp�,vae/encoder/conv2d_18/BiasAdd/ReadVariableOp�+vae/encoder/conv2d_18/Conv2D/ReadVariableOp�+vae/encoder/dense_10/BiasAdd/ReadVariableOp�*vae/encoder/dense_10/MatMul/ReadVariableOp�*vae/encoder/dense_9/BiasAdd/ReadVariableOp�)vae/encoder/dense_9/MatMul/ReadVariableOp�
+vae/encoder/conv2d_15/Conv2D/ReadVariableOpReadVariableOp4vae_encoder_conv2d_15_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
vae/encoder/conv2d_15/Conv2DConv2Dinput_73vae/encoder/conv2d_15/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
�
,vae/encoder/conv2d_15/BiasAdd/ReadVariableOpReadVariableOp5vae_encoder_conv2d_15_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
vae/encoder/conv2d_15/BiasAddBiasAdd%vae/encoder/conv2d_15/Conv2D:output:04vae/encoder/conv2d_15/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<{
vae/encoder/conv2d_15/ReluRelu&vae/encoder/conv2d_15/BiasAdd:output:0*
T0*&
_output_shapes
:<<�
+vae/encoder/conv2d_16/Conv2D/ReadVariableOpReadVariableOp4vae_encoder_conv2d_16_conv2d_readvariableop_resource*&
_output_shapes
: *
dtype0�
vae/encoder/conv2d_16/Conv2DConv2D(vae/encoder/conv2d_15/Relu:activations:03vae/encoder/conv2d_16/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
: *
paddingSAME*
strides
�
,vae/encoder/conv2d_16/BiasAdd/ReadVariableOpReadVariableOp5vae_encoder_conv2d_16_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
vae/encoder/conv2d_16/BiasAddBiasAdd%vae/encoder/conv2d_16/Conv2D:output:04vae/encoder/conv2d_16/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
: {
vae/encoder/conv2d_16/ReluRelu&vae/encoder/conv2d_16/BiasAdd:output:0*
T0*&
_output_shapes
: �
+vae/encoder/conv2d_17/Conv2D/ReadVariableOpReadVariableOp4vae_encoder_conv2d_17_conv2d_readvariableop_resource*'
_output_shapes
: �*
dtype0�
vae/encoder/conv2d_17/Conv2DConv2D(vae/encoder/conv2d_16/Relu:activations:03vae/encoder/conv2d_17/Conv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
,vae/encoder/conv2d_17/BiasAdd/ReadVariableOpReadVariableOp5vae_encoder_conv2d_17_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
vae/encoder/conv2d_17/BiasAddBiasAdd%vae/encoder/conv2d_17/Conv2D:output:04vae/encoder/conv2d_17/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�|
vae/encoder/conv2d_17/ReluRelu&vae/encoder/conv2d_17/BiasAdd:output:0*
T0*'
_output_shapes
:��
+vae/encoder/conv2d_18/Conv2D/ReadVariableOpReadVariableOp4vae_encoder_conv2d_18_conv2d_readvariableop_resource*(
_output_shapes
:��*
dtype0�
vae/encoder/conv2d_18/Conv2DConv2D(vae/encoder/conv2d_17/Relu:activations:03vae/encoder/conv2d_18/Conv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
,vae/encoder/conv2d_18/BiasAdd/ReadVariableOpReadVariableOp5vae_encoder_conv2d_18_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
vae/encoder/conv2d_18/BiasAddBiasAdd%vae/encoder/conv2d_18/Conv2D:output:04vae/encoder/conv2d_18/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�|
vae/encoder/conv2d_18/ReluRelu&vae/encoder/conv2d_18/BiasAdd:output:0*
T0*'
_output_shapes
:�l
vae/encoder/flatten_3/ConstConst*
_output_shapes
:*
dtype0*
valueB"���� � �
vae/encoder/flatten_3/ReshapeReshape(vae/encoder/conv2d_18/Relu:activations:0$vae/encoder/flatten_3/Const:output:0*
T0* 
_output_shapes
:
���
)vae/encoder/dense_9/MatMul/ReadVariableOpReadVariableOp2vae_encoder_dense_9_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
vae/encoder/dense_9/MatMulMatMul&vae/encoder/flatten_3/Reshape:output:01vae/encoder/dense_9/MatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:�
*vae/encoder/dense_9/BiasAdd/ReadVariableOpReadVariableOp3vae_encoder_dense_9_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
vae/encoder/dense_9/BiasAddBiasAdd$vae/encoder/dense_9/MatMul:product:02vae/encoder/dense_9/BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:�
*vae/encoder/dense_10/MatMul/ReadVariableOpReadVariableOp3vae_encoder_dense_10_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
vae/encoder/dense_10/MatMulMatMul&vae/encoder/flatten_3/Reshape:output:02vae/encoder/dense_10/MatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:�
+vae/encoder/dense_10/BiasAdd/ReadVariableOpReadVariableOp4vae_encoder_dense_10_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
vae/encoder/dense_10/BiasAddBiasAdd%vae/encoder/dense_10/MatMul:product:03vae/encoder/dense_10/BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:k
vae/encoder/lambda_3/ShapeConst*
_output_shapes
:*
dtype0*
valueB"      r
(vae/encoder/lambda_3/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: t
*vae/encoder/lambda_3/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:t
*vae/encoder/lambda_3/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
"vae/encoder/lambda_3/strided_sliceStridedSlice#vae/encoder/lambda_3/Shape:output:01vae/encoder/lambda_3/strided_slice/stack:output:03vae/encoder/lambda_3/strided_slice/stack_1:output:03vae/encoder/lambda_3/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskl
*vae/encoder/lambda_3/random_normal/shape/1Const*
_output_shapes
: *
dtype0*
value	B :�
(vae/encoder/lambda_3/random_normal/shapePack+vae/encoder/lambda_3/strided_slice:output:03vae/encoder/lambda_3/random_normal/shape/1:output:0*
N*
T0*
_output_shapes
:l
'vae/encoder/lambda_3/random_normal/meanConst*
_output_shapes
: *
dtype0*
valueB
 *    n
)vae/encoder/lambda_3/random_normal/stddevConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
7vae/encoder/lambda_3/random_normal/RandomStandardNormalRandomStandardNormal1vae/encoder/lambda_3/random_normal/shape:output:0*
T0*
_output_shapes

:*
dtype0*
seed���)*
seed2����
&vae/encoder/lambda_3/random_normal/mulMul@vae/encoder/lambda_3/random_normal/RandomStandardNormal:output:02vae/encoder/lambda_3/random_normal/stddev:output:0*
T0*
_output_shapes

:�
"vae/encoder/lambda_3/random_normalAddV2*vae/encoder/lambda_3/random_normal/mul:z:00vae/encoder/lambda_3/random_normal/mean:output:0*
T0*
_output_shapes

:c
vae/encoder/lambda_3/truediv/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @�
vae/encoder/lambda_3/truedivRealDiv%vae/encoder/dense_10/BiasAdd:output:0'vae/encoder/lambda_3/truediv/y:output:0*
T0*
_output_shapes

:j
vae/encoder/lambda_3/ExpExp vae/encoder/lambda_3/truediv:z:0*
T0*
_output_shapes

:�
vae/encoder/lambda_3/mulMulvae/encoder/lambda_3/Exp:y:0&vae/encoder/lambda_3/random_normal:z:0*
T0*
_output_shapes

:�
vae/encoder/lambda_3/addAddV2$vae/encoder/dense_9/BiasAdd:output:0vae/encoder/lambda_3/mul:z:0*
T0*
_output_shapes

:�
*vae/decoder/dense_11/MatMul/ReadVariableOpReadVariableOp3vae_decoder_dense_11_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
vae/decoder/dense_11/MatMulMatMulvae/encoder/lambda_3/add:z:02vae/decoder/dense_11/MatMul/ReadVariableOp:value:0*
T0* 
_output_shapes
:
���
+vae/decoder/dense_11/BiasAdd/ReadVariableOpReadVariableOp4vae_decoder_dense_11_biasadd_readvariableop_resource*
_output_shapes

:��*
dtype0�
vae/decoder/dense_11/BiasAddBiasAdd%vae/decoder/dense_11/MatMul:product:03vae/decoder/dense_11/BiasAdd/ReadVariableOp:value:0*
T0* 
_output_shapes
:
��s
vae/decoder/dense_11/ReluRelu%vae/decoder/dense_11/BiasAdd:output:0*
T0* 
_output_shapes
:
��l
vae/decoder/reshape_3/ShapeConst*
_output_shapes
:*
dtype0*
valueB"    � s
)vae/decoder/reshape_3/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: u
+vae/decoder/reshape_3/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:u
+vae/decoder/reshape_3/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
#vae/decoder/reshape_3/strided_sliceStridedSlice$vae/decoder/reshape_3/Shape:output:02vae/decoder/reshape_3/strided_slice/stack:output:04vae/decoder/reshape_3/strided_slice/stack_1:output:04vae/decoder/reshape_3/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskg
%vae/decoder/reshape_3/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :g
%vae/decoder/reshape_3/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :h
%vae/decoder/reshape_3/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value
B :��
#vae/decoder/reshape_3/Reshape/shapePack,vae/decoder/reshape_3/strided_slice:output:0.vae/decoder/reshape_3/Reshape/shape/1:output:0.vae/decoder/reshape_3/Reshape/shape/2:output:0.vae/decoder/reshape_3/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:�
vae/decoder/reshape_3/ReshapeReshape'vae/decoder/dense_11/Relu:activations:0,vae/decoder/reshape_3/Reshape/shape:output:0*
T0*'
_output_shapes
:�~
%vae/decoder/conv2d_transpose_12/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"            }
3vae/decoder/conv2d_transpose_12/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 
5vae/decoder/conv2d_transpose_12/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:
5vae/decoder/conv2d_transpose_12/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
-vae/decoder/conv2d_transpose_12/strided_sliceStridedSlice.vae/decoder/conv2d_transpose_12/Shape:output:0<vae/decoder/conv2d_transpose_12/strided_slice/stack:output:0>vae/decoder/conv2d_transpose_12/strided_slice/stack_1:output:0>vae/decoder/conv2d_transpose_12/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maski
'vae/decoder/conv2d_transpose_12/stack/1Const*
_output_shapes
: *
dtype0*
value	B :i
'vae/decoder/conv2d_transpose_12/stack/2Const*
_output_shapes
: *
dtype0*
value	B :j
'vae/decoder/conv2d_transpose_12/stack/3Const*
_output_shapes
: *
dtype0*
value
B :��
%vae/decoder/conv2d_transpose_12/stackPack6vae/decoder/conv2d_transpose_12/strided_slice:output:00vae/decoder/conv2d_transpose_12/stack/1:output:00vae/decoder/conv2d_transpose_12/stack/2:output:00vae/decoder/conv2d_transpose_12/stack/3:output:0*
N*
T0*
_output_shapes
:
5vae/decoder/conv2d_transpose_12/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
7vae/decoder/conv2d_transpose_12/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
7vae/decoder/conv2d_transpose_12/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
/vae/decoder/conv2d_transpose_12/strided_slice_1StridedSlice.vae/decoder/conv2d_transpose_12/stack:output:0>vae/decoder/conv2d_transpose_12/strided_slice_1/stack:output:0@vae/decoder/conv2d_transpose_12/strided_slice_1/stack_1:output:0@vae/decoder/conv2d_transpose_12/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
?vae/decoder/conv2d_transpose_12/conv2d_transpose/ReadVariableOpReadVariableOpHvae_decoder_conv2d_transpose_12_conv2d_transpose_readvariableop_resource*(
_output_shapes
:��*
dtype0�
0vae/decoder/conv2d_transpose_12/conv2d_transposeConv2DBackpropInput.vae/decoder/conv2d_transpose_12/stack:output:0Gvae/decoder/conv2d_transpose_12/conv2d_transpose/ReadVariableOp:value:0&vae/decoder/reshape_3/Reshape:output:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
6vae/decoder/conv2d_transpose_12/BiasAdd/ReadVariableOpReadVariableOp?vae_decoder_conv2d_transpose_12_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
'vae/decoder/conv2d_transpose_12/BiasAddBiasAdd9vae/decoder/conv2d_transpose_12/conv2d_transpose:output:0>vae/decoder/conv2d_transpose_12/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:��
$vae/decoder/conv2d_transpose_12/ReluRelu0vae/decoder/conv2d_transpose_12/BiasAdd:output:0*
T0*'
_output_shapes
:�~
%vae/decoder/conv2d_transpose_13/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"         �   }
3vae/decoder/conv2d_transpose_13/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 
5vae/decoder/conv2d_transpose_13/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:
5vae/decoder/conv2d_transpose_13/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
-vae/decoder/conv2d_transpose_13/strided_sliceStridedSlice.vae/decoder/conv2d_transpose_13/Shape:output:0<vae/decoder/conv2d_transpose_13/strided_slice/stack:output:0>vae/decoder/conv2d_transpose_13/strided_slice/stack_1:output:0>vae/decoder/conv2d_transpose_13/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maski
'vae/decoder/conv2d_transpose_13/stack/1Const*
_output_shapes
: *
dtype0*
value	B :<i
'vae/decoder/conv2d_transpose_13/stack/2Const*
_output_shapes
: *
dtype0*
value	B :<i
'vae/decoder/conv2d_transpose_13/stack/3Const*
_output_shapes
: *
dtype0*
value	B :@�
%vae/decoder/conv2d_transpose_13/stackPack6vae/decoder/conv2d_transpose_13/strided_slice:output:00vae/decoder/conv2d_transpose_13/stack/1:output:00vae/decoder/conv2d_transpose_13/stack/2:output:00vae/decoder/conv2d_transpose_13/stack/3:output:0*
N*
T0*
_output_shapes
:
5vae/decoder/conv2d_transpose_13/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
7vae/decoder/conv2d_transpose_13/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
7vae/decoder/conv2d_transpose_13/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
/vae/decoder/conv2d_transpose_13/strided_slice_1StridedSlice.vae/decoder/conv2d_transpose_13/stack:output:0>vae/decoder/conv2d_transpose_13/strided_slice_1/stack:output:0@vae/decoder/conv2d_transpose_13/strided_slice_1/stack_1:output:0@vae/decoder/conv2d_transpose_13/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
?vae/decoder/conv2d_transpose_13/conv2d_transpose/ReadVariableOpReadVariableOpHvae_decoder_conv2d_transpose_13_conv2d_transpose_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
0vae/decoder/conv2d_transpose_13/conv2d_transposeConv2DBackpropInput.vae/decoder/conv2d_transpose_13/stack:output:0Gvae/decoder/conv2d_transpose_13/conv2d_transpose/ReadVariableOp:value:02vae/decoder/conv2d_transpose_12/Relu:activations:0*
T0*&
_output_shapes
:<<@*
paddingSAME*
strides
�
6vae/decoder/conv2d_transpose_13/BiasAdd/ReadVariableOpReadVariableOp?vae_decoder_conv2d_transpose_13_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype0�
'vae/decoder/conv2d_transpose_13/BiasAddBiasAdd9vae/decoder/conv2d_transpose_13/conv2d_transpose:output:0>vae/decoder/conv2d_transpose_13/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<@�
$vae/decoder/conv2d_transpose_13/ReluRelu0vae/decoder/conv2d_transpose_13/BiasAdd:output:0*
T0*&
_output_shapes
:<<@~
%vae/decoder/conv2d_transpose_14/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"   <   <   @   }
3vae/decoder/conv2d_transpose_14/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 
5vae/decoder/conv2d_transpose_14/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:
5vae/decoder/conv2d_transpose_14/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
-vae/decoder/conv2d_transpose_14/strided_sliceStridedSlice.vae/decoder/conv2d_transpose_14/Shape:output:0<vae/decoder/conv2d_transpose_14/strided_slice/stack:output:0>vae/decoder/conv2d_transpose_14/strided_slice/stack_1:output:0>vae/decoder/conv2d_transpose_14/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maski
'vae/decoder/conv2d_transpose_14/stack/1Const*
_output_shapes
: *
dtype0*
value	B :<i
'vae/decoder/conv2d_transpose_14/stack/2Const*
_output_shapes
: *
dtype0*
value	B :<i
'vae/decoder/conv2d_transpose_14/stack/3Const*
_output_shapes
: *
dtype0*
value	B : �
%vae/decoder/conv2d_transpose_14/stackPack6vae/decoder/conv2d_transpose_14/strided_slice:output:00vae/decoder/conv2d_transpose_14/stack/1:output:00vae/decoder/conv2d_transpose_14/stack/2:output:00vae/decoder/conv2d_transpose_14/stack/3:output:0*
N*
T0*
_output_shapes
:
5vae/decoder/conv2d_transpose_14/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
7vae/decoder/conv2d_transpose_14/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
7vae/decoder/conv2d_transpose_14/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
/vae/decoder/conv2d_transpose_14/strided_slice_1StridedSlice.vae/decoder/conv2d_transpose_14/stack:output:0>vae/decoder/conv2d_transpose_14/strided_slice_1/stack:output:0@vae/decoder/conv2d_transpose_14/strided_slice_1/stack_1:output:0@vae/decoder/conv2d_transpose_14/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
?vae/decoder/conv2d_transpose_14/conv2d_transpose/ReadVariableOpReadVariableOpHvae_decoder_conv2d_transpose_14_conv2d_transpose_readvariableop_resource*&
_output_shapes
: @*
dtype0�
0vae/decoder/conv2d_transpose_14/conv2d_transposeConv2DBackpropInput.vae/decoder/conv2d_transpose_14/stack:output:0Gvae/decoder/conv2d_transpose_14/conv2d_transpose/ReadVariableOp:value:02vae/decoder/conv2d_transpose_13/Relu:activations:0*
T0*&
_output_shapes
:<< *
paddingSAME*
strides
�
6vae/decoder/conv2d_transpose_14/BiasAdd/ReadVariableOpReadVariableOp?vae_decoder_conv2d_transpose_14_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
'vae/decoder/conv2d_transpose_14/BiasAddBiasAdd9vae/decoder/conv2d_transpose_14/conv2d_transpose:output:0>vae/decoder/conv2d_transpose_14/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<< �
$vae/decoder/conv2d_transpose_14/ReluRelu0vae/decoder/conv2d_transpose_14/BiasAdd:output:0*
T0*&
_output_shapes
:<< ~
%vae/decoder/conv2d_transpose_15/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"   <   <       }
3vae/decoder/conv2d_transpose_15/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: 
5vae/decoder/conv2d_transpose_15/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:
5vae/decoder/conv2d_transpose_15/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
-vae/decoder/conv2d_transpose_15/strided_sliceStridedSlice.vae/decoder/conv2d_transpose_15/Shape:output:0<vae/decoder/conv2d_transpose_15/strided_slice/stack:output:0>vae/decoder/conv2d_transpose_15/strided_slice/stack_1:output:0>vae/decoder/conv2d_transpose_15/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maski
'vae/decoder/conv2d_transpose_15/stack/1Const*
_output_shapes
: *
dtype0*
value	B :<i
'vae/decoder/conv2d_transpose_15/stack/2Const*
_output_shapes
: *
dtype0*
value	B :<i
'vae/decoder/conv2d_transpose_15/stack/3Const*
_output_shapes
: *
dtype0*
value	B :�
%vae/decoder/conv2d_transpose_15/stackPack6vae/decoder/conv2d_transpose_15/strided_slice:output:00vae/decoder/conv2d_transpose_15/stack/1:output:00vae/decoder/conv2d_transpose_15/stack/2:output:00vae/decoder/conv2d_transpose_15/stack/3:output:0*
N*
T0*
_output_shapes
:
5vae/decoder/conv2d_transpose_15/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
7vae/decoder/conv2d_transpose_15/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
7vae/decoder/conv2d_transpose_15/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
/vae/decoder/conv2d_transpose_15/strided_slice_1StridedSlice.vae/decoder/conv2d_transpose_15/stack:output:0>vae/decoder/conv2d_transpose_15/strided_slice_1/stack:output:0@vae/decoder/conv2d_transpose_15/strided_slice_1/stack_1:output:0@vae/decoder/conv2d_transpose_15/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
?vae/decoder/conv2d_transpose_15/conv2d_transpose/ReadVariableOpReadVariableOpHvae_decoder_conv2d_transpose_15_conv2d_transpose_readvariableop_resource*&
_output_shapes
: *
dtype0�
0vae/decoder/conv2d_transpose_15/conv2d_transposeConv2DBackpropInput.vae/decoder/conv2d_transpose_15/stack:output:0Gvae/decoder/conv2d_transpose_15/conv2d_transpose/ReadVariableOp:value:02vae/decoder/conv2d_transpose_14/Relu:activations:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
�
6vae/decoder/conv2d_transpose_15/BiasAdd/ReadVariableOpReadVariableOp?vae_decoder_conv2d_transpose_15_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
'vae/decoder/conv2d_transpose_15/BiasAddBiasAdd9vae/decoder/conv2d_transpose_15/conv2d_transpose:output:0>vae/decoder/conv2d_transpose_15/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<�
$vae/decoder/conv2d_transpose_15/ReluRelu0vae/decoder/conv2d_transpose_15/BiasAdd:output:0*
T0*&
_output_shapes
:<<�
+vae/decoder/conv2d_19/Conv2D/ReadVariableOpReadVariableOp4vae_decoder_conv2d_19_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
vae/decoder/conv2d_19/Conv2DConv2D2vae/decoder/conv2d_transpose_15/Relu:activations:03vae/decoder/conv2d_19/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
�
,vae/decoder/conv2d_19/BiasAdd/ReadVariableOpReadVariableOp5vae_decoder_conv2d_19_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
vae/decoder/conv2d_19/BiasAddBiasAdd%vae/decoder/conv2d_19/Conv2D:output:04vae/decoder/conv2d_19/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<�
vae/decoder/conv2d_19/SigmoidSigmoid&vae/decoder/conv2d_19/BiasAdd:output:0*
T0*&
_output_shapes
:<<q
vae/tf.reshape_6/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB:
���������|
vae/tf.reshape_6/ReshapeReshapeinput_7'vae/tf.reshape_6/Reshape/shape:output:0*
T0*
_output_shapes

:��q
vae/tf.reshape_7/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB:
����������
vae/tf.reshape_7/ReshapeReshape!vae/decoder/conv2d_19/Sigmoid:y:0'vae/tf.reshape_7/Reshape/shape:output:0*
T0*
_output_shapes

:���
#vae/conv2d_15/Conv2D/ReadVariableOpReadVariableOp4vae_encoder_conv2d_15_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
vae/conv2d_15/Conv2DConv2Dinput_7+vae/conv2d_15/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
�
$vae/conv2d_15/BiasAdd/ReadVariableOpReadVariableOp5vae_encoder_conv2d_15_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
vae/conv2d_15/BiasAddBiasAddvae/conv2d_15/Conv2D:output:0,vae/conv2d_15/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<k
vae/conv2d_15/ReluReluvae/conv2d_15/BiasAdd:output:0*
T0*&
_output_shapes
:<<�
#vae/conv2d_16/Conv2D/ReadVariableOpReadVariableOp4vae_encoder_conv2d_16_conv2d_readvariableop_resource*&
_output_shapes
: *
dtype0�
vae/conv2d_16/Conv2DConv2D vae/conv2d_15/Relu:activations:0+vae/conv2d_16/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
: *
paddingSAME*
strides
�
$vae/conv2d_16/BiasAdd/ReadVariableOpReadVariableOp5vae_encoder_conv2d_16_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
vae/conv2d_16/BiasAddBiasAddvae/conv2d_16/Conv2D:output:0,vae/conv2d_16/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
: k
vae/conv2d_16/ReluReluvae/conv2d_16/BiasAdd:output:0*
T0*&
_output_shapes
: �
2vae/tf.math.squared_difference_3/SquaredDifferenceSquaredDifference!vae/tf.reshape_7/Reshape:output:0!vae/tf.reshape_6/Reshape:output:0*
T0*
_output_shapes

:���
#vae/conv2d_17/Conv2D/ReadVariableOpReadVariableOp4vae_encoder_conv2d_17_conv2d_readvariableop_resource*'
_output_shapes
: �*
dtype0�
vae/conv2d_17/Conv2DConv2D vae/conv2d_16/Relu:activations:0+vae/conv2d_17/Conv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
$vae/conv2d_17/BiasAdd/ReadVariableOpReadVariableOp5vae_encoder_conv2d_17_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
vae/conv2d_17/BiasAddBiasAddvae/conv2d_17/Conv2D:output:0,vae/conv2d_17/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�l
vae/conv2d_17/ReluReluvae/conv2d_17/BiasAdd:output:0*
T0*'
_output_shapes
:�{
0vae/tf.math.reduce_mean_6/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
����������
vae/tf.math.reduce_mean_6/MeanMean6vae/tf.math.squared_difference_3/SquaredDifference:z:09vae/tf.math.reduce_mean_6/Mean/reduction_indices:output:0*
T0*
_output_shapes
: �
#vae/conv2d_18/Conv2D/ReadVariableOpReadVariableOp4vae_encoder_conv2d_18_conv2d_readvariableop_resource*(
_output_shapes
:��*
dtype0�
vae/conv2d_18/Conv2DConv2D vae/conv2d_17/Relu:activations:0+vae/conv2d_18/Conv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
$vae/conv2d_18/BiasAdd/ReadVariableOpReadVariableOp5vae_encoder_conv2d_18_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
vae/conv2d_18/BiasAddBiasAddvae/conv2d_18/Conv2D:output:0,vae/conv2d_18/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�l
vae/conv2d_18/ReluReluvae/conv2d_18/BiasAdd:output:0*
T0*'
_output_shapes
:�b
vae/tf.math.multiply_12/Mul/yConst*
_output_shapes
: *
dtype0*
valueB
 * �(F�
vae/tf.math.multiply_12/MulMul'vae/tf.math.reduce_mean_6/Mean:output:0&vae/tf.math.multiply_12/Mul/y:output:0*
T0*
_output_shapes
: d
vae/flatten_3/ConstConst*
_output_shapes
:*
dtype0*
valueB"���� � �
vae/flatten_3/ReshapeReshape vae/conv2d_18/Relu:activations:0vae/flatten_3/Const:output:0*
T0* 
_output_shapes
:
��p
vae/tf.math.multiply_14/MulMul
vae_244876vae/tf.math.multiply_12/Mul:z:0*
T0*
_output_shapes
: �
"vae/dense_10/MatMul/ReadVariableOpReadVariableOp3vae_encoder_dense_10_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
vae/dense_10/MatMulMatMulvae/flatten_3/Reshape:output:0*vae/dense_10/MatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:�
#vae/dense_10/BiasAdd/ReadVariableOpReadVariableOp4vae_encoder_dense_10_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
vae/dense_10/BiasAddBiasAddvae/dense_10/MatMul:product:0+vae/dense_10/BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:�
!vae/dense_9/MatMul/ReadVariableOpReadVariableOp2vae_encoder_dense_9_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
vae/dense_9/MatMulMatMulvae/flatten_3/Reshape:output:0)vae/dense_9/MatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:�
"vae/dense_9/BiasAdd/ReadVariableOpReadVariableOp3vae_encoder_dense_9_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
vae/dense_9/BiasAddBiasAddvae/dense_9/MatMul:product:0*vae/dense_9/BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:W
vae/add_metric_7/RankConst*
_output_shapes
: *
dtype0*
value	B : ^
vae/add_metric_7/range/startConst*
_output_shapes
: *
dtype0*
value	B : ^
vae/add_metric_7/range/deltaConst*
_output_shapes
: *
dtype0*
value	B :�
vae/add_metric_7/rangeRange%vae/add_metric_7/range/start:output:0vae/add_metric_7/Rank:output:0%vae/add_metric_7/range/delta:output:0*
_output_shapes
: �
vae/add_metric_7/SumSumvae/tf.math.multiply_12/Mul:z:0vae/add_metric_7/range:output:0*
T0*&
 _has_manual_control_dependencies(*
_output_shapes
: �
$vae/add_metric_7/AssignAddVariableOpAssignAddVariableOp-vae_add_metric_7_assignaddvariableop_resourcevae/add_metric_7/Sum:output:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0W
vae/add_metric_7/SizeConst*
_output_shapes
: *
dtype0*
value	B :m
vae/add_metric_7/CastCastvae/add_metric_7/Size:output:0*

DstT0*

SrcT0*
_output_shapes
: �
&vae/add_metric_7/AssignAddVariableOp_1AssignAddVariableOp/vae_add_metric_7_assignaddvariableop_1_resourcevae/add_metric_7/Cast:y:0%^vae/add_metric_7/AssignAddVariableOp*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0�
*vae/add_metric_7/div_no_nan/ReadVariableOpReadVariableOp-vae_add_metric_7_assignaddvariableop_resource%^vae/add_metric_7/AssignAddVariableOp'^vae/add_metric_7/AssignAddVariableOp_1*
_output_shapes
: *
dtype0�
,vae/add_metric_7/div_no_nan/ReadVariableOp_1ReadVariableOp/vae_add_metric_7_assignaddvariableop_1_resource'^vae/add_metric_7/AssignAddVariableOp_1*
_output_shapes
: *
dtype0�
vae/add_metric_7/div_no_nanDivNoNan2vae/add_metric_7/div_no_nan/ReadVariableOp:value:04vae/add_metric_7/div_no_nan/ReadVariableOp_1:value:0*
T0*
_output_shapes
: g
vae/add_metric_7/IdentityIdentityvae/add_metric_7/div_no_nan:z:0*
T0*
_output_shapes
: }
 vae/tf.__operators__.add_6/AddV2AddV2
vae_244900vae/dense_10/BiasAdd:output:0*
T0*
_output_shapes

:l
vae/tf.math.square_3/SquareSquarevae/dense_9/BiasAdd:output:0*
T0*
_output_shapes

:d
vae/tf.math.exp_3/ExpExpvae/dense_10/BiasAdd:output:0*
T0*
_output_shapes

:�
vae/tf.math.subtract_6/SubSub$vae/tf.__operators__.add_6/AddV2:z:0vae/tf.math.square_3/Square:y:0*
T0*
_output_shapes

:�
vae/tf.math.subtract_7/SubSubvae/tf.math.subtract_6/Sub:z:0vae/tf.math.exp_3/Exp:y:0*
T0*
_output_shapes

:p
.vae/tf.math.reduce_sum_3/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
vae/tf.math.reduce_sum_3/SumSumvae/tf.math.subtract_7/Sub:z:07vae/tf.math.reduce_sum_3/Sum/reduction_indices:output:0*
T0*
_output_shapes
:z
vae/tf.math.multiply_13/MulMul
vae_244909%vae/tf.math.reduce_sum_3/Sum:output:0*
T0*
_output_shapes
:t
vae/tf.math.multiply_15/MulMul
vae_244912vae/tf.math.multiply_13/Mul:z:0*
T0*
_output_shapes
:�
 vae/tf.__operators__.add_7/AddV2AddV2vae/tf.math.multiply_14/Mul:z:0vae/tf.math.multiply_15/Mul:z:0*
T0*
_output_shapes
:i
vae/tf.math.reduce_mean_7/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
vae/tf.math.reduce_mean_7/MeanMean$vae/tf.__operators__.add_7/AddV2:z:0(vae/tf.math.reduce_mean_7/Const:output:0*
T0*
_output_shapes
: W
vae/add_metric_6/RankConst*
_output_shapes
: *
dtype0*
value	B : ^
vae/add_metric_6/range/startConst*
_output_shapes
: *
dtype0*
value	B : ^
vae/add_metric_6/range/deltaConst*
_output_shapes
: *
dtype0*
value	B :�
vae/add_metric_6/rangeRange%vae/add_metric_6/range/start:output:0vae/add_metric_6/Rank:output:0%vae/add_metric_6/range/delta:output:0*
_output_shapes
: �
vae/add_metric_6/SumSum'vae/tf.math.reduce_mean_7/Mean:output:0vae/add_metric_6/range:output:0*
T0*&
 _has_manual_control_dependencies(*
_output_shapes
: �
$vae/add_metric_6/AssignAddVariableOpAssignAddVariableOp-vae_add_metric_6_assignaddvariableop_resourcevae/add_metric_6/Sum:output:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0W
vae/add_metric_6/SizeConst*
_output_shapes
: *
dtype0*
value	B :m
vae/add_metric_6/CastCastvae/add_metric_6/Size:output:0*

DstT0*

SrcT0*
_output_shapes
: �
&vae/add_metric_6/AssignAddVariableOp_1AssignAddVariableOp/vae_add_metric_6_assignaddvariableop_1_resourcevae/add_metric_6/Cast:y:0%^vae/add_metric_6/AssignAddVariableOp*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0�
*vae/add_metric_6/div_no_nan/ReadVariableOpReadVariableOp-vae_add_metric_6_assignaddvariableop_resource%^vae/add_metric_6/AssignAddVariableOp'^vae/add_metric_6/AssignAddVariableOp_1*
_output_shapes
: *
dtype0�
,vae/add_metric_6/div_no_nan/ReadVariableOp_1ReadVariableOp/vae_add_metric_6_assignaddvariableop_1_resource'^vae/add_metric_6/AssignAddVariableOp_1*
_output_shapes
: *
dtype0�
vae/add_metric_6/div_no_nanDivNoNan2vae/add_metric_6/div_no_nan/ReadVariableOp:value:04vae/add_metric_6/div_no_nan/ReadVariableOp_1:value:0*
T0*
_output_shapes
: g
vae/add_metric_6/IdentityIdentityvae/add_metric_6/div_no_nan:z:0*
T0*
_output_shapes
: o
IdentityIdentity!vae/decoder/conv2d_19/Sigmoid:y:0^NoOp*
T0*&
_output_shapes
:<<�
NoOpNoOp%^vae/add_metric_6/AssignAddVariableOp'^vae/add_metric_6/AssignAddVariableOp_1+^vae/add_metric_6/div_no_nan/ReadVariableOp-^vae/add_metric_6/div_no_nan/ReadVariableOp_1%^vae/add_metric_7/AssignAddVariableOp'^vae/add_metric_7/AssignAddVariableOp_1+^vae/add_metric_7/div_no_nan/ReadVariableOp-^vae/add_metric_7/div_no_nan/ReadVariableOp_1%^vae/conv2d_15/BiasAdd/ReadVariableOp$^vae/conv2d_15/Conv2D/ReadVariableOp%^vae/conv2d_16/BiasAdd/ReadVariableOp$^vae/conv2d_16/Conv2D/ReadVariableOp%^vae/conv2d_17/BiasAdd/ReadVariableOp$^vae/conv2d_17/Conv2D/ReadVariableOp%^vae/conv2d_18/BiasAdd/ReadVariableOp$^vae/conv2d_18/Conv2D/ReadVariableOp-^vae/decoder/conv2d_19/BiasAdd/ReadVariableOp,^vae/decoder/conv2d_19/Conv2D/ReadVariableOp7^vae/decoder/conv2d_transpose_12/BiasAdd/ReadVariableOp@^vae/decoder/conv2d_transpose_12/conv2d_transpose/ReadVariableOp7^vae/decoder/conv2d_transpose_13/BiasAdd/ReadVariableOp@^vae/decoder/conv2d_transpose_13/conv2d_transpose/ReadVariableOp7^vae/decoder/conv2d_transpose_14/BiasAdd/ReadVariableOp@^vae/decoder/conv2d_transpose_14/conv2d_transpose/ReadVariableOp7^vae/decoder/conv2d_transpose_15/BiasAdd/ReadVariableOp@^vae/decoder/conv2d_transpose_15/conv2d_transpose/ReadVariableOp,^vae/decoder/dense_11/BiasAdd/ReadVariableOp+^vae/decoder/dense_11/MatMul/ReadVariableOp$^vae/dense_10/BiasAdd/ReadVariableOp#^vae/dense_10/MatMul/ReadVariableOp#^vae/dense_9/BiasAdd/ReadVariableOp"^vae/dense_9/MatMul/ReadVariableOp-^vae/encoder/conv2d_15/BiasAdd/ReadVariableOp,^vae/encoder/conv2d_15/Conv2D/ReadVariableOp-^vae/encoder/conv2d_16/BiasAdd/ReadVariableOp,^vae/encoder/conv2d_16/Conv2D/ReadVariableOp-^vae/encoder/conv2d_17/BiasAdd/ReadVariableOp,^vae/encoder/conv2d_17/Conv2D/ReadVariableOp-^vae/encoder/conv2d_18/BiasAdd/ReadVariableOp,^vae/encoder/conv2d_18/Conv2D/ReadVariableOp,^vae/encoder/dense_10/BiasAdd/ReadVariableOp+^vae/encoder/dense_10/MatMul/ReadVariableOp+^vae/encoder/dense_9/BiasAdd/ReadVariableOp*^vae/encoder/dense_9/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*e
_input_shapesT
R:<<: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2L
$vae/add_metric_6/AssignAddVariableOp$vae/add_metric_6/AssignAddVariableOp2P
&vae/add_metric_6/AssignAddVariableOp_1&vae/add_metric_6/AssignAddVariableOp_12X
*vae/add_metric_6/div_no_nan/ReadVariableOp*vae/add_metric_6/div_no_nan/ReadVariableOp2\
,vae/add_metric_6/div_no_nan/ReadVariableOp_1,vae/add_metric_6/div_no_nan/ReadVariableOp_12L
$vae/add_metric_7/AssignAddVariableOp$vae/add_metric_7/AssignAddVariableOp2P
&vae/add_metric_7/AssignAddVariableOp_1&vae/add_metric_7/AssignAddVariableOp_12X
*vae/add_metric_7/div_no_nan/ReadVariableOp*vae/add_metric_7/div_no_nan/ReadVariableOp2\
,vae/add_metric_7/div_no_nan/ReadVariableOp_1,vae/add_metric_7/div_no_nan/ReadVariableOp_12L
$vae/conv2d_15/BiasAdd/ReadVariableOp$vae/conv2d_15/BiasAdd/ReadVariableOp2J
#vae/conv2d_15/Conv2D/ReadVariableOp#vae/conv2d_15/Conv2D/ReadVariableOp2L
$vae/conv2d_16/BiasAdd/ReadVariableOp$vae/conv2d_16/BiasAdd/ReadVariableOp2J
#vae/conv2d_16/Conv2D/ReadVariableOp#vae/conv2d_16/Conv2D/ReadVariableOp2L
$vae/conv2d_17/BiasAdd/ReadVariableOp$vae/conv2d_17/BiasAdd/ReadVariableOp2J
#vae/conv2d_17/Conv2D/ReadVariableOp#vae/conv2d_17/Conv2D/ReadVariableOp2L
$vae/conv2d_18/BiasAdd/ReadVariableOp$vae/conv2d_18/BiasAdd/ReadVariableOp2J
#vae/conv2d_18/Conv2D/ReadVariableOp#vae/conv2d_18/Conv2D/ReadVariableOp2\
,vae/decoder/conv2d_19/BiasAdd/ReadVariableOp,vae/decoder/conv2d_19/BiasAdd/ReadVariableOp2Z
+vae/decoder/conv2d_19/Conv2D/ReadVariableOp+vae/decoder/conv2d_19/Conv2D/ReadVariableOp2p
6vae/decoder/conv2d_transpose_12/BiasAdd/ReadVariableOp6vae/decoder/conv2d_transpose_12/BiasAdd/ReadVariableOp2�
?vae/decoder/conv2d_transpose_12/conv2d_transpose/ReadVariableOp?vae/decoder/conv2d_transpose_12/conv2d_transpose/ReadVariableOp2p
6vae/decoder/conv2d_transpose_13/BiasAdd/ReadVariableOp6vae/decoder/conv2d_transpose_13/BiasAdd/ReadVariableOp2�
?vae/decoder/conv2d_transpose_13/conv2d_transpose/ReadVariableOp?vae/decoder/conv2d_transpose_13/conv2d_transpose/ReadVariableOp2p
6vae/decoder/conv2d_transpose_14/BiasAdd/ReadVariableOp6vae/decoder/conv2d_transpose_14/BiasAdd/ReadVariableOp2�
?vae/decoder/conv2d_transpose_14/conv2d_transpose/ReadVariableOp?vae/decoder/conv2d_transpose_14/conv2d_transpose/ReadVariableOp2p
6vae/decoder/conv2d_transpose_15/BiasAdd/ReadVariableOp6vae/decoder/conv2d_transpose_15/BiasAdd/ReadVariableOp2�
?vae/decoder/conv2d_transpose_15/conv2d_transpose/ReadVariableOp?vae/decoder/conv2d_transpose_15/conv2d_transpose/ReadVariableOp2Z
+vae/decoder/dense_11/BiasAdd/ReadVariableOp+vae/decoder/dense_11/BiasAdd/ReadVariableOp2X
*vae/decoder/dense_11/MatMul/ReadVariableOp*vae/decoder/dense_11/MatMul/ReadVariableOp2J
#vae/dense_10/BiasAdd/ReadVariableOp#vae/dense_10/BiasAdd/ReadVariableOp2H
"vae/dense_10/MatMul/ReadVariableOp"vae/dense_10/MatMul/ReadVariableOp2H
"vae/dense_9/BiasAdd/ReadVariableOp"vae/dense_9/BiasAdd/ReadVariableOp2F
!vae/dense_9/MatMul/ReadVariableOp!vae/dense_9/MatMul/ReadVariableOp2\
,vae/encoder/conv2d_15/BiasAdd/ReadVariableOp,vae/encoder/conv2d_15/BiasAdd/ReadVariableOp2Z
+vae/encoder/conv2d_15/Conv2D/ReadVariableOp+vae/encoder/conv2d_15/Conv2D/ReadVariableOp2\
,vae/encoder/conv2d_16/BiasAdd/ReadVariableOp,vae/encoder/conv2d_16/BiasAdd/ReadVariableOp2Z
+vae/encoder/conv2d_16/Conv2D/ReadVariableOp+vae/encoder/conv2d_16/Conv2D/ReadVariableOp2\
,vae/encoder/conv2d_17/BiasAdd/ReadVariableOp,vae/encoder/conv2d_17/BiasAdd/ReadVariableOp2Z
+vae/encoder/conv2d_17/Conv2D/ReadVariableOp+vae/encoder/conv2d_17/Conv2D/ReadVariableOp2\
,vae/encoder/conv2d_18/BiasAdd/ReadVariableOp,vae/encoder/conv2d_18/BiasAdd/ReadVariableOp2Z
+vae/encoder/conv2d_18/Conv2D/ReadVariableOp+vae/encoder/conv2d_18/Conv2D/ReadVariableOp2Z
+vae/encoder/dense_10/BiasAdd/ReadVariableOp+vae/encoder/dense_10/BiasAdd/ReadVariableOp2X
*vae/encoder/dense_10/MatMul/ReadVariableOp*vae/encoder/dense_10/MatMul/ReadVariableOp2X
*vae/encoder/dense_9/BiasAdd/ReadVariableOp*vae/encoder/dense_9/BiasAdd/ReadVariableOp2V
)vae/encoder/dense_9/MatMul/ReadVariableOp)vae/encoder/dense_9/MatMul/ReadVariableOp:O K
&
_output_shapes
:<<
!
_user_specified_name	input_7:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�!
�
O__inference_conv2d_transpose_13_layer_call_and_return_conditional_losses_245499

inputsC
(conv2d_transpose_readvariableop_resource:@�-
biasadd_readvariableop_resource:@
identity��BiasAdd/ReadVariableOp�conv2d_transpose/ReadVariableOpI
ShapeShapeinputs*
T0*
_output_shapes
::��]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask_
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_2StridedSliceShape:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskG
mul/yConst*
_output_shapes
: *
dtype0*
value	B :U
mulMulstrided_slice_1:output:0mul/y:output:0*
T0*
_output_shapes
: I
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :Y
mul_1Mulstrided_slice_2:output:0mul_1/y:output:0*
T0*
_output_shapes
: I
stack/3Const*
_output_shapes
: *
dtype0*
value	B :@y
stackPackstrided_slice:output:0mul:z:0	mul_1:z:0stack/3:output:0*
N*
T0*
_output_shapes
:_
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_3StridedSlicestack:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
conv2d_transpose/ReadVariableOpReadVariableOp(conv2d_transpose_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
conv2d_transposeConv2DBackpropInputstack:output:0'conv2d_transpose/ReadVariableOp:value:0inputs*
T0*A
_output_shapes/
-:+���������������������������@*
paddingSAME*
strides
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:@*
dtype0�
BiasAddBiasAddconv2d_transpose:output:0BiasAdd/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+���������������������������@j
ReluReluBiasAdd:output:0*
T0*A
_output_shapes/
-:+���������������������������@{
IdentityIdentityRelu:activations:0^NoOp*
T0*A
_output_shapes/
-:+���������������������������@�
NoOpNoOp^BiasAdd/ReadVariableOp ^conv2d_transpose/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*E
_input_shapes4
2:,����������������������������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2B
conv2d_transpose/ReadVariableOpconv2d_transpose/ReadVariableOp:j f
B
_output_shapes0
.:,����������������������������
 
_user_specified_nameinputs
�	
�
D__inference_dense_11_layer_call_and_return_conditional_losses_248324

inputs2
matmul_readvariableop_resource:
��/
biasadd_readvariableop_resource:
��
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpv
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0b
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0* 
_output_shapes
:
��t
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes

:��*
dtype0o
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0* 
_output_shapes
:
��I
ReluReluBiasAdd:output:0*
T0* 
_output_shapes
:
��Z
IdentityIdentityRelu:activations:0^NoOp*
T0* 
_output_shapes
:
��w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*!
_input_shapes
:: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:F B

_output_shapes

:
 
_user_specified_nameinputs
�!
�
O__inference_conv2d_transpose_15_layer_call_and_return_conditional_losses_245589

inputsB
(conv2d_transpose_readvariableop_resource: -
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�conv2d_transpose/ReadVariableOpI
ShapeShapeinputs*
T0*
_output_shapes
::��]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask_
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_2StridedSliceShape:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskG
mul/yConst*
_output_shapes
: *
dtype0*
value	B :U
mulMulstrided_slice_1:output:0mul/y:output:0*
T0*
_output_shapes
: I
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :Y
mul_1Mulstrided_slice_2:output:0mul_1/y:output:0*
T0*
_output_shapes
: I
stack/3Const*
_output_shapes
: *
dtype0*
value	B :y
stackPackstrided_slice:output:0mul:z:0	mul_1:z:0stack/3:output:0*
N*
T0*
_output_shapes
:_
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_3StridedSlicestack:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
conv2d_transpose/ReadVariableOpReadVariableOp(conv2d_transpose_readvariableop_resource*&
_output_shapes
: *
dtype0�
conv2d_transposeConv2DBackpropInputstack:output:0'conv2d_transpose/ReadVariableOp:value:0inputs*
T0*A
_output_shapes/
-:+���������������������������*
paddingSAME*
strides
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
BiasAddBiasAddconv2d_transpose:output:0BiasAdd/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+���������������������������j
ReluReluBiasAdd:output:0*
T0*A
_output_shapes/
-:+���������������������������{
IdentityIdentityRelu:activations:0^NoOp*
T0*A
_output_shapes/
-:+����������������������������
NoOpNoOp^BiasAdd/ReadVariableOp ^conv2d_transpose/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*D
_input_shapes3
1:+��������������������������� : : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2B
conv2d_transpose/ReadVariableOpconv2d_transpose/ReadVariableOp:i e
A
_output_shapes/
-:+��������������������������� 
 
_user_specified_nameinputs
�'
�
C__inference_decoder_layer_call_and_return_conditional_losses_245709
input_8#
dense_11_245677:
��
dense_11_245679:
��6
conv2d_transpose_12_245683:��)
conv2d_transpose_12_245685:	�5
conv2d_transpose_13_245688:@�(
conv2d_transpose_13_245690:@4
conv2d_transpose_14_245693: @(
conv2d_transpose_14_245695: 4
conv2d_transpose_15_245698: (
conv2d_transpose_15_245700:*
conv2d_19_245703:
conv2d_19_245705:
identity��!conv2d_19/StatefulPartitionedCall�+conv2d_transpose_12/StatefulPartitionedCall�+conv2d_transpose_13/StatefulPartitionedCall�+conv2d_transpose_14/StatefulPartitionedCall�+conv2d_transpose_15/StatefulPartitionedCall� dense_11/StatefulPartitionedCall�
 dense_11/StatefulPartitionedCallStatefulPartitionedCallinput_8dense_11_245677dense_11_245679*
Tin
2*
Tout
2*
_collective_manager_ids
 * 
_output_shapes
:
��*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dense_11_layer_call_and_return_conditional_losses_245614�
reshape_3/PartitionedCallPartitionedCall)dense_11/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_reshape_3_layer_call_and_return_conditional_losses_245634�
+conv2d_transpose_12/StatefulPartitionedCallStatefulPartitionedCall"reshape_3/PartitionedCall:output:0conv2d_transpose_12_245683conv2d_transpose_12_245685*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_12_layer_call_and_return_conditional_losses_245454�
+conv2d_transpose_13/StatefulPartitionedCallStatefulPartitionedCall4conv2d_transpose_12/StatefulPartitionedCall:output:0conv2d_transpose_13_245688conv2d_transpose_13_245690*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_13_layer_call_and_return_conditional_losses_245499�
+conv2d_transpose_14/StatefulPartitionedCallStatefulPartitionedCall4conv2d_transpose_13/StatefulPartitionedCall:output:0conv2d_transpose_14_245693conv2d_transpose_14_245695*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<< *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_14_layer_call_and_return_conditional_losses_245544�
+conv2d_transpose_15/StatefulPartitionedCallStatefulPartitionedCall4conv2d_transpose_14/StatefulPartitionedCall:output:0conv2d_transpose_15_245698conv2d_transpose_15_245700*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_15_layer_call_and_return_conditional_losses_245589�
!conv2d_19/StatefulPartitionedCallStatefulPartitionedCall4conv2d_transpose_15/StatefulPartitionedCall:output:0conv2d_19_245703conv2d_19_245705*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_19_layer_call_and_return_conditional_losses_245667x
IdentityIdentity*conv2d_19/StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<�
NoOpNoOp"^conv2d_19/StatefulPartitionedCall,^conv2d_transpose_12/StatefulPartitionedCall,^conv2d_transpose_13/StatefulPartitionedCall,^conv2d_transpose_14/StatefulPartitionedCall,^conv2d_transpose_15/StatefulPartitionedCall!^dense_11/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":: : : : : : : : : : : : 2F
!conv2d_19/StatefulPartitionedCall!conv2d_19/StatefulPartitionedCall2Z
+conv2d_transpose_12/StatefulPartitionedCall+conv2d_transpose_12/StatefulPartitionedCall2Z
+conv2d_transpose_13/StatefulPartitionedCall+conv2d_transpose_13/StatefulPartitionedCall2Z
+conv2d_transpose_14/StatefulPartitionedCall+conv2d_transpose_14/StatefulPartitionedCall2Z
+conv2d_transpose_15/StatefulPartitionedCall+conv2d_transpose_15/StatefulPartitionedCall2D
 dense_11/StatefulPartitionedCall dense_11/StatefulPartitionedCall:G C

_output_shapes

:
!
_user_specified_name	input_8
�
�
)__inference_dense_10_layer_call_fn_248156

inputs
unknown:
��
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dense_10_layer_call_and_return_conditional_losses_245038f
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*#
_input_shapes
:
��: : 22
StatefulPartitionedCallStatefulPartitionedCall:H D
 
_output_shapes
:
��
 
_user_specified_nameinputs
�
Q
#__inference__update_step_xla_247521
gradient
variable:
��*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*!
_input_shapes
:
��: *
	_noinline(:J F
 
_output_shapes
:
��
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�
q
D__inference_lambda_3_layer_call_and_return_conditional_losses_245126

inputs
inputs_1
identity�V
ShapeConst*
_output_shapes
:*
dtype0*
valueB"      ]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskW
random_normal/shape/1Const*
_output_shapes
: *
dtype0*
value	B :�
random_normal/shapePackstrided_slice:output:0random_normal/shape/1:output:0*
N*
T0*
_output_shapes
:W
random_normal/meanConst*
_output_shapes
: *
dtype0*
valueB
 *    Y
random_normal/stddevConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
"random_normal/RandomStandardNormalRandomStandardNormalrandom_normal/shape:output:0*
T0*
_output_shapes

:*
dtype0*
seed���)*
seed2����
random_normal/mulMul+random_normal/RandomStandardNormal:output:0random_normal/stddev:output:0*
T0*
_output_shapes

:s
random_normalAddV2random_normal/mul:z:0random_normal/mean:output:0*
T0*
_output_shapes

:N
	truediv/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @Y
truedivRealDivinputs_1truediv/y:output:0*
T0*
_output_shapes

:@
ExpExptruediv:z:0*
T0*
_output_shapes

:O
mulMulExp:y:0random_normal:z:0*
T0*
_output_shapes

:F
addAddV2inputsmul:z:0*
T0*
_output_shapes

:F
IdentityIdentityadd:z:0*
T0*
_output_shapes

:"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:::F B

_output_shapes

:
 
_user_specified_nameinputs:FB

_output_shapes

:
 
_user_specified_nameinputs
�
a
E__inference_reshape_3_layer_call_and_return_conditional_losses_245634

inputs
identityV
ShapeConst*
_output_shapes
:*
dtype0*
valueB"    � ]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskQ
Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :Q
Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :R
Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value
B :��
Reshape/shapePackstrided_slice:output:0Reshape/shape/1:output:0Reshape/shape/2:output:0Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:d
ReshapeReshapeinputsReshape/shape:output:0*
T0*'
_output_shapes
:�X
IdentityIdentityReshape:output:0*
T0*'
_output_shapes
:�"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
:
��:H D
 
_output_shapes
:
��
 
_user_specified_nameinputs
�
�
(__inference_encoder_layer_call_fn_247652

inputs!
unknown:
	unknown_0:#
	unknown_1: 
	unknown_2: $
	unknown_3: �
	unknown_4:	�%
	unknown_5:��
	unknown_6:	�
	unknown_7:
��
	unknown_8:
	unknown_9:
��

unknown_10:
identity

identity_1

identity_2��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10*
Tin
2*
Tout
2*
_collective_manager_ids
 *2
_output_shapes 
:::*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_encoder_layer_call_and_return_conditional_losses_245243f
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:h

Identity_1Identity StatefulPartitionedCall:output:1^NoOp*
T0*
_output_shapes

:h

Identity_2Identity StatefulPartitionedCall:output:2^NoOp*
T0*
_output_shapes

:`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*=
_input_shapes,
*:<<: : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs
�
�
*__inference_conv2d_19_layer_call_fn_248524

inputs!
unknown:
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_19_layer_call_and_return_conditional_losses_245667n
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:<<: : 22
StatefulPartitionedCallStatefulPartitionedCall:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs
�
a
E__inference_flatten_3_layer_call_and_return_conditional_losses_248147

inputs
identityV
ConstConst*
_output_shapes
:*
dtype0*
valueB"���� � U
ReshapeReshapeinputsConst:output:0*
T0* 
_output_shapes
:
��Q
IdentityIdentityReshape:output:0*
T0* 
_output_shapes
:
��"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:�:O K
'
_output_shapes
:�
 
_user_specified_nameinputs
��
�5
"__inference__traced_restore_249292
file_prefix;
!assignvariableop_conv2d_15_kernel:/
!assignvariableop_1_conv2d_15_bias:=
#assignvariableop_2_conv2d_16_kernel: /
!assignvariableop_3_conv2d_16_bias: >
#assignvariableop_4_conv2d_17_kernel: �0
!assignvariableop_5_conv2d_17_bias:	�?
#assignvariableop_6_conv2d_18_kernel:��0
!assignvariableop_7_conv2d_18_bias:	�6
"assignvariableop_8_dense_10_kernel:
��.
 assignvariableop_9_dense_10_bias:6
"assignvariableop_10_dense_9_kernel:
��.
 assignvariableop_11_dense_9_bias:7
#assignvariableop_12_dense_11_kernel:
��1
!assignvariableop_13_dense_11_bias:
��J
.assignvariableop_14_conv2d_transpose_12_kernel:��;
,assignvariableop_15_conv2d_transpose_12_bias:	�I
.assignvariableop_16_conv2d_transpose_13_kernel:@�:
,assignvariableop_17_conv2d_transpose_13_bias:@H
.assignvariableop_18_conv2d_transpose_14_kernel: @:
,assignvariableop_19_conv2d_transpose_14_bias: H
.assignvariableop_20_conv2d_transpose_15_kernel: :
,assignvariableop_21_conv2d_transpose_15_bias:>
$assignvariableop_22_conv2d_19_kernel:0
"assignvariableop_23_conv2d_19_bias:'
assignvariableop_24_iteration:	 +
!assignvariableop_25_learning_rate: E
+assignvariableop_26_adam_m_conv2d_15_kernel:E
+assignvariableop_27_adam_v_conv2d_15_kernel:7
)assignvariableop_28_adam_m_conv2d_15_bias:7
)assignvariableop_29_adam_v_conv2d_15_bias:E
+assignvariableop_30_adam_m_conv2d_16_kernel: E
+assignvariableop_31_adam_v_conv2d_16_kernel: 7
)assignvariableop_32_adam_m_conv2d_16_bias: 7
)assignvariableop_33_adam_v_conv2d_16_bias: F
+assignvariableop_34_adam_m_conv2d_17_kernel: �F
+assignvariableop_35_adam_v_conv2d_17_kernel: �8
)assignvariableop_36_adam_m_conv2d_17_bias:	�8
)assignvariableop_37_adam_v_conv2d_17_bias:	�G
+assignvariableop_38_adam_m_conv2d_18_kernel:��G
+assignvariableop_39_adam_v_conv2d_18_kernel:��8
)assignvariableop_40_adam_m_conv2d_18_bias:	�8
)assignvariableop_41_adam_v_conv2d_18_bias:	�=
)assignvariableop_42_adam_m_dense_9_kernel:
��=
)assignvariableop_43_adam_v_dense_9_kernel:
��5
'assignvariableop_44_adam_m_dense_9_bias:5
'assignvariableop_45_adam_v_dense_9_bias:>
*assignvariableop_46_adam_m_dense_10_kernel:
��>
*assignvariableop_47_adam_v_dense_10_kernel:
��6
(assignvariableop_48_adam_m_dense_10_bias:6
(assignvariableop_49_adam_v_dense_10_bias:>
*assignvariableop_50_adam_m_dense_11_kernel:
��>
*assignvariableop_51_adam_v_dense_11_kernel:
��8
(assignvariableop_52_adam_m_dense_11_bias:
��8
(assignvariableop_53_adam_v_dense_11_bias:
��Q
5assignvariableop_54_adam_m_conv2d_transpose_12_kernel:��Q
5assignvariableop_55_adam_v_conv2d_transpose_12_kernel:��B
3assignvariableop_56_adam_m_conv2d_transpose_12_bias:	�B
3assignvariableop_57_adam_v_conv2d_transpose_12_bias:	�P
5assignvariableop_58_adam_m_conv2d_transpose_13_kernel:@�P
5assignvariableop_59_adam_v_conv2d_transpose_13_kernel:@�A
3assignvariableop_60_adam_m_conv2d_transpose_13_bias:@A
3assignvariableop_61_adam_v_conv2d_transpose_13_bias:@O
5assignvariableop_62_adam_m_conv2d_transpose_14_kernel: @O
5assignvariableop_63_adam_v_conv2d_transpose_14_kernel: @A
3assignvariableop_64_adam_m_conv2d_transpose_14_bias: A
3assignvariableop_65_adam_v_conv2d_transpose_14_bias: O
5assignvariableop_66_adam_m_conv2d_transpose_15_kernel: O
5assignvariableop_67_adam_v_conv2d_transpose_15_kernel: A
3assignvariableop_68_adam_m_conv2d_transpose_15_bias:A
3assignvariableop_69_adam_v_conv2d_transpose_15_bias:E
+assignvariableop_70_adam_m_conv2d_19_kernel:E
+assignvariableop_71_adam_v_conv2d_19_kernel:7
)assignvariableop_72_adam_m_conv2d_19_bias:7
)assignvariableop_73_adam_v_conv2d_19_bias:#
assignvariableop_74_total: #
assignvariableop_75_count: 0
&assignvariableop_76_add_metric_6_total: 0
&assignvariableop_77_add_metric_6_count: 0
&assignvariableop_78_add_metric_7_total: 0
&assignvariableop_79_add_metric_7_count: 
identity_81��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_24�AssignVariableOp_25�AssignVariableOp_26�AssignVariableOp_27�AssignVariableOp_28�AssignVariableOp_29�AssignVariableOp_3�AssignVariableOp_30�AssignVariableOp_31�AssignVariableOp_32�AssignVariableOp_33�AssignVariableOp_34�AssignVariableOp_35�AssignVariableOp_36�AssignVariableOp_37�AssignVariableOp_38�AssignVariableOp_39�AssignVariableOp_4�AssignVariableOp_40�AssignVariableOp_41�AssignVariableOp_42�AssignVariableOp_43�AssignVariableOp_44�AssignVariableOp_45�AssignVariableOp_46�AssignVariableOp_47�AssignVariableOp_48�AssignVariableOp_49�AssignVariableOp_5�AssignVariableOp_50�AssignVariableOp_51�AssignVariableOp_52�AssignVariableOp_53�AssignVariableOp_54�AssignVariableOp_55�AssignVariableOp_56�AssignVariableOp_57�AssignVariableOp_58�AssignVariableOp_59�AssignVariableOp_6�AssignVariableOp_60�AssignVariableOp_61�AssignVariableOp_62�AssignVariableOp_63�AssignVariableOp_64�AssignVariableOp_65�AssignVariableOp_66�AssignVariableOp_67�AssignVariableOp_68�AssignVariableOp_69�AssignVariableOp_7�AssignVariableOp_70�AssignVariableOp_71�AssignVariableOp_72�AssignVariableOp_73�AssignVariableOp_74�AssignVariableOp_75�AssignVariableOp_76�AssignVariableOp_77�AssignVariableOp_78�AssignVariableOp_79�AssignVariableOp_8�AssignVariableOp_9� 
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:Q*
dtype0*� 
value� B� QB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB'variables/12/.ATTRIBUTES/VARIABLE_VALUEB'variables/13/.ATTRIBUTES/VARIABLE_VALUEB'variables/14/.ATTRIBUTES/VARIABLE_VALUEB'variables/15/.ATTRIBUTES/VARIABLE_VALUEB'variables/16/.ATTRIBUTES/VARIABLE_VALUEB'variables/17/.ATTRIBUTES/VARIABLE_VALUEB'variables/18/.ATTRIBUTES/VARIABLE_VALUEB'variables/19/.ATTRIBUTES/VARIABLE_VALUEB'variables/20/.ATTRIBUTES/VARIABLE_VALUEB'variables/21/.ATTRIBUTES/VARIABLE_VALUEB'variables/22/.ATTRIBUTES/VARIABLE_VALUEB'variables/23/.ATTRIBUTES/VARIABLE_VALUEB0optimizer/_iterations/.ATTRIBUTES/VARIABLE_VALUEB3optimizer/_learning_rate/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/1/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/2/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/3/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/4/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/5/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/6/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/7/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/8/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/9/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/10/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/11/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/12/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/13/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/14/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/15/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/16/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/17/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/18/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/19/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/20/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/21/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/22/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/23/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/24/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/25/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/26/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/27/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/28/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/29/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/30/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/31/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/32/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/33/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/34/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/35/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/36/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/37/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/38/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/39/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/40/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/41/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/42/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/43/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/44/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/45/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/46/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/47/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/48/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/2/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/2/count/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:Q*
dtype0*�
value�B�QB B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B �
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*�
_output_shapes�
�:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*_
dtypesU
S2Q	[
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOpAssignVariableOp!assignvariableop_conv2d_15_kernelIdentity:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_1AssignVariableOp!assignvariableop_1_conv2d_15_biasIdentity_1:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_2AssignVariableOp#assignvariableop_2_conv2d_16_kernelIdentity_2:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_3AssignVariableOp!assignvariableop_3_conv2d_16_biasIdentity_3:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_4AssignVariableOp#assignvariableop_4_conv2d_17_kernelIdentity_4:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_5AssignVariableOp!assignvariableop_5_conv2d_17_biasIdentity_5:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_6AssignVariableOp#assignvariableop_6_conv2d_18_kernelIdentity_6:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_7AssignVariableOp!assignvariableop_7_conv2d_18_biasIdentity_7:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_8AssignVariableOp"assignvariableop_8_dense_10_kernelIdentity_8:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_9AssignVariableOp assignvariableop_9_dense_10_biasIdentity_9:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_10AssignVariableOp"assignvariableop_10_dense_9_kernelIdentity_10:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_11AssignVariableOp assignvariableop_11_dense_9_biasIdentity_11:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_12AssignVariableOp#assignvariableop_12_dense_11_kernelIdentity_12:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_13AssignVariableOp!assignvariableop_13_dense_11_biasIdentity_13:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_14AssignVariableOp.assignvariableop_14_conv2d_transpose_12_kernelIdentity_14:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_15AssignVariableOp,assignvariableop_15_conv2d_transpose_12_biasIdentity_15:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_16AssignVariableOp.assignvariableop_16_conv2d_transpose_13_kernelIdentity_16:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_17AssignVariableOp,assignvariableop_17_conv2d_transpose_13_biasIdentity_17:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_18AssignVariableOp.assignvariableop_18_conv2d_transpose_14_kernelIdentity_18:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_19AssignVariableOp,assignvariableop_19_conv2d_transpose_14_biasIdentity_19:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_20AssignVariableOp.assignvariableop_20_conv2d_transpose_15_kernelIdentity_20:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_21AssignVariableOp,assignvariableop_21_conv2d_transpose_15_biasIdentity_21:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_22AssignVariableOp$assignvariableop_22_conv2d_19_kernelIdentity_22:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_23AssignVariableOp"assignvariableop_23_conv2d_19_biasIdentity_23:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_24IdentityRestoreV2:tensors:24"/device:CPU:0*
T0	*
_output_shapes
:�
AssignVariableOp_24AssignVariableOpassignvariableop_24_iterationIdentity_24:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0	_
Identity_25IdentityRestoreV2:tensors:25"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_25AssignVariableOp!assignvariableop_25_learning_rateIdentity_25:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_26IdentityRestoreV2:tensors:26"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_26AssignVariableOp+assignvariableop_26_adam_m_conv2d_15_kernelIdentity_26:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_27IdentityRestoreV2:tensors:27"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_27AssignVariableOp+assignvariableop_27_adam_v_conv2d_15_kernelIdentity_27:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_28IdentityRestoreV2:tensors:28"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_28AssignVariableOp)assignvariableop_28_adam_m_conv2d_15_biasIdentity_28:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_29IdentityRestoreV2:tensors:29"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_29AssignVariableOp)assignvariableop_29_adam_v_conv2d_15_biasIdentity_29:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_30IdentityRestoreV2:tensors:30"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_30AssignVariableOp+assignvariableop_30_adam_m_conv2d_16_kernelIdentity_30:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_31IdentityRestoreV2:tensors:31"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_31AssignVariableOp+assignvariableop_31_adam_v_conv2d_16_kernelIdentity_31:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_32IdentityRestoreV2:tensors:32"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_32AssignVariableOp)assignvariableop_32_adam_m_conv2d_16_biasIdentity_32:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_33IdentityRestoreV2:tensors:33"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_33AssignVariableOp)assignvariableop_33_adam_v_conv2d_16_biasIdentity_33:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_34IdentityRestoreV2:tensors:34"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_34AssignVariableOp+assignvariableop_34_adam_m_conv2d_17_kernelIdentity_34:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_35IdentityRestoreV2:tensors:35"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_35AssignVariableOp+assignvariableop_35_adam_v_conv2d_17_kernelIdentity_35:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_36IdentityRestoreV2:tensors:36"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_36AssignVariableOp)assignvariableop_36_adam_m_conv2d_17_biasIdentity_36:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_37IdentityRestoreV2:tensors:37"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_37AssignVariableOp)assignvariableop_37_adam_v_conv2d_17_biasIdentity_37:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_38IdentityRestoreV2:tensors:38"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_38AssignVariableOp+assignvariableop_38_adam_m_conv2d_18_kernelIdentity_38:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_39IdentityRestoreV2:tensors:39"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_39AssignVariableOp+assignvariableop_39_adam_v_conv2d_18_kernelIdentity_39:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_40IdentityRestoreV2:tensors:40"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_40AssignVariableOp)assignvariableop_40_adam_m_conv2d_18_biasIdentity_40:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_41IdentityRestoreV2:tensors:41"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_41AssignVariableOp)assignvariableop_41_adam_v_conv2d_18_biasIdentity_41:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_42IdentityRestoreV2:tensors:42"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_42AssignVariableOp)assignvariableop_42_adam_m_dense_9_kernelIdentity_42:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_43IdentityRestoreV2:tensors:43"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_43AssignVariableOp)assignvariableop_43_adam_v_dense_9_kernelIdentity_43:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_44IdentityRestoreV2:tensors:44"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_44AssignVariableOp'assignvariableop_44_adam_m_dense_9_biasIdentity_44:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_45IdentityRestoreV2:tensors:45"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_45AssignVariableOp'assignvariableop_45_adam_v_dense_9_biasIdentity_45:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_46IdentityRestoreV2:tensors:46"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_46AssignVariableOp*assignvariableop_46_adam_m_dense_10_kernelIdentity_46:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_47IdentityRestoreV2:tensors:47"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_47AssignVariableOp*assignvariableop_47_adam_v_dense_10_kernelIdentity_47:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_48IdentityRestoreV2:tensors:48"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_48AssignVariableOp(assignvariableop_48_adam_m_dense_10_biasIdentity_48:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_49IdentityRestoreV2:tensors:49"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_49AssignVariableOp(assignvariableop_49_adam_v_dense_10_biasIdentity_49:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_50IdentityRestoreV2:tensors:50"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_50AssignVariableOp*assignvariableop_50_adam_m_dense_11_kernelIdentity_50:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_51IdentityRestoreV2:tensors:51"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_51AssignVariableOp*assignvariableop_51_adam_v_dense_11_kernelIdentity_51:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_52IdentityRestoreV2:tensors:52"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_52AssignVariableOp(assignvariableop_52_adam_m_dense_11_biasIdentity_52:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_53IdentityRestoreV2:tensors:53"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_53AssignVariableOp(assignvariableop_53_adam_v_dense_11_biasIdentity_53:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_54IdentityRestoreV2:tensors:54"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_54AssignVariableOp5assignvariableop_54_adam_m_conv2d_transpose_12_kernelIdentity_54:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_55IdentityRestoreV2:tensors:55"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_55AssignVariableOp5assignvariableop_55_adam_v_conv2d_transpose_12_kernelIdentity_55:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_56IdentityRestoreV2:tensors:56"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_56AssignVariableOp3assignvariableop_56_adam_m_conv2d_transpose_12_biasIdentity_56:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_57IdentityRestoreV2:tensors:57"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_57AssignVariableOp3assignvariableop_57_adam_v_conv2d_transpose_12_biasIdentity_57:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_58IdentityRestoreV2:tensors:58"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_58AssignVariableOp5assignvariableop_58_adam_m_conv2d_transpose_13_kernelIdentity_58:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_59IdentityRestoreV2:tensors:59"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_59AssignVariableOp5assignvariableop_59_adam_v_conv2d_transpose_13_kernelIdentity_59:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_60IdentityRestoreV2:tensors:60"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_60AssignVariableOp3assignvariableop_60_adam_m_conv2d_transpose_13_biasIdentity_60:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_61IdentityRestoreV2:tensors:61"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_61AssignVariableOp3assignvariableop_61_adam_v_conv2d_transpose_13_biasIdentity_61:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_62IdentityRestoreV2:tensors:62"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_62AssignVariableOp5assignvariableop_62_adam_m_conv2d_transpose_14_kernelIdentity_62:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_63IdentityRestoreV2:tensors:63"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_63AssignVariableOp5assignvariableop_63_adam_v_conv2d_transpose_14_kernelIdentity_63:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_64IdentityRestoreV2:tensors:64"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_64AssignVariableOp3assignvariableop_64_adam_m_conv2d_transpose_14_biasIdentity_64:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_65IdentityRestoreV2:tensors:65"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_65AssignVariableOp3assignvariableop_65_adam_v_conv2d_transpose_14_biasIdentity_65:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_66IdentityRestoreV2:tensors:66"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_66AssignVariableOp5assignvariableop_66_adam_m_conv2d_transpose_15_kernelIdentity_66:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_67IdentityRestoreV2:tensors:67"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_67AssignVariableOp5assignvariableop_67_adam_v_conv2d_transpose_15_kernelIdentity_67:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_68IdentityRestoreV2:tensors:68"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_68AssignVariableOp3assignvariableop_68_adam_m_conv2d_transpose_15_biasIdentity_68:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_69IdentityRestoreV2:tensors:69"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_69AssignVariableOp3assignvariableop_69_adam_v_conv2d_transpose_15_biasIdentity_69:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_70IdentityRestoreV2:tensors:70"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_70AssignVariableOp+assignvariableop_70_adam_m_conv2d_19_kernelIdentity_70:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_71IdentityRestoreV2:tensors:71"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_71AssignVariableOp+assignvariableop_71_adam_v_conv2d_19_kernelIdentity_71:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_72IdentityRestoreV2:tensors:72"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_72AssignVariableOp)assignvariableop_72_adam_m_conv2d_19_biasIdentity_72:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_73IdentityRestoreV2:tensors:73"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_73AssignVariableOp)assignvariableop_73_adam_v_conv2d_19_biasIdentity_73:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_74IdentityRestoreV2:tensors:74"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_74AssignVariableOpassignvariableop_74_totalIdentity_74:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_75IdentityRestoreV2:tensors:75"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_75AssignVariableOpassignvariableop_75_countIdentity_75:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_76IdentityRestoreV2:tensors:76"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_76AssignVariableOp&assignvariableop_76_add_metric_6_totalIdentity_76:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_77IdentityRestoreV2:tensors:77"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_77AssignVariableOp&assignvariableop_77_add_metric_6_countIdentity_77:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_78IdentityRestoreV2:tensors:78"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_78AssignVariableOp&assignvariableop_78_add_metric_7_totalIdentity_78:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_79IdentityRestoreV2:tensors:79"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_79AssignVariableOp&assignvariableop_79_add_metric_7_countIdentity_79:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0Y
NoOpNoOp"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 �
Identity_80Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_47^AssignVariableOp_48^AssignVariableOp_49^AssignVariableOp_5^AssignVariableOp_50^AssignVariableOp_51^AssignVariableOp_52^AssignVariableOp_53^AssignVariableOp_54^AssignVariableOp_55^AssignVariableOp_56^AssignVariableOp_57^AssignVariableOp_58^AssignVariableOp_59^AssignVariableOp_6^AssignVariableOp_60^AssignVariableOp_61^AssignVariableOp_62^AssignVariableOp_63^AssignVariableOp_64^AssignVariableOp_65^AssignVariableOp_66^AssignVariableOp_67^AssignVariableOp_68^AssignVariableOp_69^AssignVariableOp_7^AssignVariableOp_70^AssignVariableOp_71^AssignVariableOp_72^AssignVariableOp_73^AssignVariableOp_74^AssignVariableOp_75^AssignVariableOp_76^AssignVariableOp_77^AssignVariableOp_78^AssignVariableOp_79^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: W
Identity_81IdentityIdentity_80:output:0^NoOp_1*
T0*
_output_shapes
: �
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_24^AssignVariableOp_25^AssignVariableOp_26^AssignVariableOp_27^AssignVariableOp_28^AssignVariableOp_29^AssignVariableOp_3^AssignVariableOp_30^AssignVariableOp_31^AssignVariableOp_32^AssignVariableOp_33^AssignVariableOp_34^AssignVariableOp_35^AssignVariableOp_36^AssignVariableOp_37^AssignVariableOp_38^AssignVariableOp_39^AssignVariableOp_4^AssignVariableOp_40^AssignVariableOp_41^AssignVariableOp_42^AssignVariableOp_43^AssignVariableOp_44^AssignVariableOp_45^AssignVariableOp_46^AssignVariableOp_47^AssignVariableOp_48^AssignVariableOp_49^AssignVariableOp_5^AssignVariableOp_50^AssignVariableOp_51^AssignVariableOp_52^AssignVariableOp_53^AssignVariableOp_54^AssignVariableOp_55^AssignVariableOp_56^AssignVariableOp_57^AssignVariableOp_58^AssignVariableOp_59^AssignVariableOp_6^AssignVariableOp_60^AssignVariableOp_61^AssignVariableOp_62^AssignVariableOp_63^AssignVariableOp_64^AssignVariableOp_65^AssignVariableOp_66^AssignVariableOp_67^AssignVariableOp_68^AssignVariableOp_69^AssignVariableOp_7^AssignVariableOp_70^AssignVariableOp_71^AssignVariableOp_72^AssignVariableOp_73^AssignVariableOp_74^AssignVariableOp_75^AssignVariableOp_76^AssignVariableOp_77^AssignVariableOp_78^AssignVariableOp_79^AssignVariableOp_8^AssignVariableOp_9*"
_acd_function_control_output(*
_output_shapes
 "#
identity_81Identity_81:output:0*�
_input_shapes�
�: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232*
AssignVariableOp_24AssignVariableOp_242*
AssignVariableOp_25AssignVariableOp_252*
AssignVariableOp_26AssignVariableOp_262*
AssignVariableOp_27AssignVariableOp_272*
AssignVariableOp_28AssignVariableOp_282*
AssignVariableOp_29AssignVariableOp_292(
AssignVariableOp_3AssignVariableOp_32*
AssignVariableOp_30AssignVariableOp_302*
AssignVariableOp_31AssignVariableOp_312*
AssignVariableOp_32AssignVariableOp_322*
AssignVariableOp_33AssignVariableOp_332*
AssignVariableOp_34AssignVariableOp_342*
AssignVariableOp_35AssignVariableOp_352*
AssignVariableOp_36AssignVariableOp_362*
AssignVariableOp_37AssignVariableOp_372*
AssignVariableOp_38AssignVariableOp_382*
AssignVariableOp_39AssignVariableOp_392(
AssignVariableOp_4AssignVariableOp_42*
AssignVariableOp_40AssignVariableOp_402*
AssignVariableOp_41AssignVariableOp_412*
AssignVariableOp_42AssignVariableOp_422*
AssignVariableOp_43AssignVariableOp_432*
AssignVariableOp_44AssignVariableOp_442*
AssignVariableOp_45AssignVariableOp_452*
AssignVariableOp_46AssignVariableOp_462*
AssignVariableOp_47AssignVariableOp_472*
AssignVariableOp_48AssignVariableOp_482*
AssignVariableOp_49AssignVariableOp_492(
AssignVariableOp_5AssignVariableOp_52*
AssignVariableOp_50AssignVariableOp_502*
AssignVariableOp_51AssignVariableOp_512*
AssignVariableOp_52AssignVariableOp_522*
AssignVariableOp_53AssignVariableOp_532*
AssignVariableOp_54AssignVariableOp_542*
AssignVariableOp_55AssignVariableOp_552*
AssignVariableOp_56AssignVariableOp_562*
AssignVariableOp_57AssignVariableOp_572*
AssignVariableOp_58AssignVariableOp_582*
AssignVariableOp_59AssignVariableOp_592(
AssignVariableOp_6AssignVariableOp_62*
AssignVariableOp_60AssignVariableOp_602*
AssignVariableOp_61AssignVariableOp_612*
AssignVariableOp_62AssignVariableOp_622*
AssignVariableOp_63AssignVariableOp_632*
AssignVariableOp_64AssignVariableOp_642*
AssignVariableOp_65AssignVariableOp_652*
AssignVariableOp_66AssignVariableOp_662*
AssignVariableOp_67AssignVariableOp_672*
AssignVariableOp_68AssignVariableOp_682*
AssignVariableOp_69AssignVariableOp_692(
AssignVariableOp_7AssignVariableOp_72*
AssignVariableOp_70AssignVariableOp_702*
AssignVariableOp_71AssignVariableOp_712*
AssignVariableOp_72AssignVariableOp_722*
AssignVariableOp_73AssignVariableOp_732*
AssignVariableOp_74AssignVariableOp_742*
AssignVariableOp_75AssignVariableOp_752*
AssignVariableOp_76AssignVariableOp_762*
AssignVariableOp_77AssignVariableOp_772*
AssignVariableOp_78AssignVariableOp_782*
AssignVariableOp_79AssignVariableOp_792(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
�
M
#__inference__update_step_xla_247536
gradient
variable:
��*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*
_input_shapes

:��: *
	_noinline(:F B

_output_shapes

:��
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�
�
$__inference_vae_layer_call_fn_246390
input_7!
unknown:
	unknown_0:#
	unknown_1: 
	unknown_2: $
	unknown_3: �
	unknown_4:	�%
	unknown_5:��
	unknown_6:	�
	unknown_7:
��
	unknown_8:
	unknown_9:
��

unknown_10:

unknown_11:
��

unknown_12:
��&

unknown_13:��

unknown_14:	�%

unknown_15:@�

unknown_16:@$

unknown_17: @

unknown_18: $

unknown_19: 

unknown_20:$

unknown_21:

unknown_22:

unknown_23

unknown_24: 

unknown_25: 

unknown_26

unknown_27

unknown_28

unknown_29: 

unknown_30: 
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_7unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30*,
Tin%
#2!*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:<<: *:
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *H
fCRA
?__inference_vae_layer_call_and_return_conditional_losses_246322n
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*e
_input_shapesT
R:<<: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
&
_output_shapes
:<<
!
_user_specified_name	input_7:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
(__inference_encoder_layer_call_fn_245274
input_7!
unknown:
	unknown_0:#
	unknown_1: 
	unknown_2: $
	unknown_3: �
	unknown_4:	�%
	unknown_5:��
	unknown_6:	�
	unknown_7:
��
	unknown_8:
	unknown_9:
��

unknown_10:
identity

identity_1

identity_2��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_7unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10*
Tin
2*
Tout
2*
_collective_manager_ids
 *2
_output_shapes 
:::*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_encoder_layer_call_and_return_conditional_losses_245243f
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:h

Identity_1Identity StatefulPartitionedCall:output:1^NoOp*
T0*
_output_shapes

:h

Identity_2Identity StatefulPartitionedCall:output:2^NoOp*
T0*
_output_shapes

:`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*=
_input_shapes,
*:<<: : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
&
_output_shapes
:<<
!
_user_specified_name	input_7
�'
�
C__inference_decoder_layer_call_and_return_conditional_losses_245811

inputs#
dense_11_245779:
��
dense_11_245781:
��6
conv2d_transpose_12_245785:��)
conv2d_transpose_12_245787:	�5
conv2d_transpose_13_245790:@�(
conv2d_transpose_13_245792:@4
conv2d_transpose_14_245795: @(
conv2d_transpose_14_245797: 4
conv2d_transpose_15_245800: (
conv2d_transpose_15_245802:*
conv2d_19_245805:
conv2d_19_245807:
identity��!conv2d_19/StatefulPartitionedCall�+conv2d_transpose_12/StatefulPartitionedCall�+conv2d_transpose_13/StatefulPartitionedCall�+conv2d_transpose_14/StatefulPartitionedCall�+conv2d_transpose_15/StatefulPartitionedCall� dense_11/StatefulPartitionedCall�
 dense_11/StatefulPartitionedCallStatefulPartitionedCallinputsdense_11_245779dense_11_245781*
Tin
2*
Tout
2*
_collective_manager_ids
 * 
_output_shapes
:
��*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dense_11_layer_call_and_return_conditional_losses_245614�
reshape_3/PartitionedCallPartitionedCall)dense_11/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_reshape_3_layer_call_and_return_conditional_losses_245634�
+conv2d_transpose_12/StatefulPartitionedCallStatefulPartitionedCall"reshape_3/PartitionedCall:output:0conv2d_transpose_12_245785conv2d_transpose_12_245787*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_12_layer_call_and_return_conditional_losses_245454�
+conv2d_transpose_13/StatefulPartitionedCallStatefulPartitionedCall4conv2d_transpose_12/StatefulPartitionedCall:output:0conv2d_transpose_13_245790conv2d_transpose_13_245792*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_13_layer_call_and_return_conditional_losses_245499�
+conv2d_transpose_14/StatefulPartitionedCallStatefulPartitionedCall4conv2d_transpose_13/StatefulPartitionedCall:output:0conv2d_transpose_14_245795conv2d_transpose_14_245797*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<< *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_14_layer_call_and_return_conditional_losses_245544�
+conv2d_transpose_15/StatefulPartitionedCallStatefulPartitionedCall4conv2d_transpose_14/StatefulPartitionedCall:output:0conv2d_transpose_15_245800conv2d_transpose_15_245802*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_15_layer_call_and_return_conditional_losses_245589�
!conv2d_19/StatefulPartitionedCallStatefulPartitionedCall4conv2d_transpose_15/StatefulPartitionedCall:output:0conv2d_19_245805conv2d_19_245807*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_19_layer_call_and_return_conditional_losses_245667x
IdentityIdentity*conv2d_19/StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<�
NoOpNoOp"^conv2d_19/StatefulPartitionedCall,^conv2d_transpose_12/StatefulPartitionedCall,^conv2d_transpose_13/StatefulPartitionedCall,^conv2d_transpose_14/StatefulPartitionedCall,^conv2d_transpose_15/StatefulPartitionedCall!^dense_11/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":: : : : : : : : : : : : 2F
!conv2d_19/StatefulPartitionedCall!conv2d_19/StatefulPartitionedCall2Z
+conv2d_transpose_12/StatefulPartitionedCall+conv2d_transpose_12/StatefulPartitionedCall2Z
+conv2d_transpose_13/StatefulPartitionedCall+conv2d_transpose_13/StatefulPartitionedCall2Z
+conv2d_transpose_14/StatefulPartitionedCall+conv2d_transpose_14/StatefulPartitionedCall2Z
+conv2d_transpose_15/StatefulPartitionedCall+conv2d_transpose_15/StatefulPartitionedCall2D
 dense_11/StatefulPartitionedCall dense_11/StatefulPartitionedCall:F B

_output_shapes

:
 
_user_specified_nameinputs
�
�
(__inference_dense_9_layer_call_fn_248175

inputs
unknown:
��
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_dense_9_layer_call_and_return_conditional_losses_245022f
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*#
_input_shapes
:
��: : 22
StatefulPartitionedCallStatefulPartitionedCall:H D
 
_output_shapes
:
��
 
_user_specified_nameinputs
�'
�
C__inference_decoder_layer_call_and_return_conditional_losses_245674
input_8#
dense_11_245615:
��
dense_11_245617:
��6
conv2d_transpose_12_245636:��)
conv2d_transpose_12_245638:	�5
conv2d_transpose_13_245641:@�(
conv2d_transpose_13_245643:@4
conv2d_transpose_14_245646: @(
conv2d_transpose_14_245648: 4
conv2d_transpose_15_245651: (
conv2d_transpose_15_245653:*
conv2d_19_245668:
conv2d_19_245670:
identity��!conv2d_19/StatefulPartitionedCall�+conv2d_transpose_12/StatefulPartitionedCall�+conv2d_transpose_13/StatefulPartitionedCall�+conv2d_transpose_14/StatefulPartitionedCall�+conv2d_transpose_15/StatefulPartitionedCall� dense_11/StatefulPartitionedCall�
 dense_11/StatefulPartitionedCallStatefulPartitionedCallinput_8dense_11_245615dense_11_245617*
Tin
2*
Tout
2*
_collective_manager_ids
 * 
_output_shapes
:
��*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dense_11_layer_call_and_return_conditional_losses_245614�
reshape_3/PartitionedCallPartitionedCall)dense_11/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_reshape_3_layer_call_and_return_conditional_losses_245634�
+conv2d_transpose_12/StatefulPartitionedCallStatefulPartitionedCall"reshape_3/PartitionedCall:output:0conv2d_transpose_12_245636conv2d_transpose_12_245638*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_12_layer_call_and_return_conditional_losses_245454�
+conv2d_transpose_13/StatefulPartitionedCallStatefulPartitionedCall4conv2d_transpose_12/StatefulPartitionedCall:output:0conv2d_transpose_13_245641conv2d_transpose_13_245643*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_13_layer_call_and_return_conditional_losses_245499�
+conv2d_transpose_14/StatefulPartitionedCallStatefulPartitionedCall4conv2d_transpose_13/StatefulPartitionedCall:output:0conv2d_transpose_14_245646conv2d_transpose_14_245648*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<< *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_14_layer_call_and_return_conditional_losses_245544�
+conv2d_transpose_15/StatefulPartitionedCallStatefulPartitionedCall4conv2d_transpose_14/StatefulPartitionedCall:output:0conv2d_transpose_15_245651conv2d_transpose_15_245653*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_15_layer_call_and_return_conditional_losses_245589�
!conv2d_19/StatefulPartitionedCallStatefulPartitionedCall4conv2d_transpose_15/StatefulPartitionedCall:output:0conv2d_19_245668conv2d_19_245670*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_19_layer_call_and_return_conditional_losses_245667x
IdentityIdentity*conv2d_19/StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<�
NoOpNoOp"^conv2d_19/StatefulPartitionedCall,^conv2d_transpose_12/StatefulPartitionedCall,^conv2d_transpose_13/StatefulPartitionedCall,^conv2d_transpose_14/StatefulPartitionedCall,^conv2d_transpose_15/StatefulPartitionedCall!^dense_11/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":: : : : : : : : : : : : 2F
!conv2d_19/StatefulPartitionedCall!conv2d_19/StatefulPartitionedCall2Z
+conv2d_transpose_12/StatefulPartitionedCall+conv2d_transpose_12/StatefulPartitionedCall2Z
+conv2d_transpose_13/StatefulPartitionedCall+conv2d_transpose_13/StatefulPartitionedCall2Z
+conv2d_transpose_14/StatefulPartitionedCall+conv2d_transpose_14/StatefulPartitionedCall2Z
+conv2d_transpose_15/StatefulPartitionedCall+conv2d_transpose_15/StatefulPartitionedCall2D
 dense_11/StatefulPartitionedCall dense_11/StatefulPartitionedCall:G C

_output_shapes

:
!
_user_specified_name	input_8
�
K
#__inference__update_step_xla_247576
gradient
variable:*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*
_input_shapes

:: *
	_noinline(:D @

_output_shapes
:
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�J
�	
C__inference_encoder_layer_call_and_return_conditional_losses_247717

inputsB
(conv2d_15_conv2d_readvariableop_resource:7
)conv2d_15_biasadd_readvariableop_resource:B
(conv2d_16_conv2d_readvariableop_resource: 7
)conv2d_16_biasadd_readvariableop_resource: C
(conv2d_17_conv2d_readvariableop_resource: �8
)conv2d_17_biasadd_readvariableop_resource:	�D
(conv2d_18_conv2d_readvariableop_resource:��8
)conv2d_18_biasadd_readvariableop_resource:	�:
&dense_9_matmul_readvariableop_resource:
��5
'dense_9_biasadd_readvariableop_resource:;
'dense_10_matmul_readvariableop_resource:
��6
(dense_10_biasadd_readvariableop_resource:
identity

identity_1

identity_2�� conv2d_15/BiasAdd/ReadVariableOp�conv2d_15/Conv2D/ReadVariableOp� conv2d_16/BiasAdd/ReadVariableOp�conv2d_16/Conv2D/ReadVariableOp� conv2d_17/BiasAdd/ReadVariableOp�conv2d_17/Conv2D/ReadVariableOp� conv2d_18/BiasAdd/ReadVariableOp�conv2d_18/Conv2D/ReadVariableOp�dense_10/BiasAdd/ReadVariableOp�dense_10/MatMul/ReadVariableOp�dense_9/BiasAdd/ReadVariableOp�dense_9/MatMul/ReadVariableOp�
conv2d_15/Conv2D/ReadVariableOpReadVariableOp(conv2d_15_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
conv2d_15/Conv2DConv2Dinputs'conv2d_15/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
�
 conv2d_15/BiasAdd/ReadVariableOpReadVariableOp)conv2d_15_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
conv2d_15/BiasAddBiasAddconv2d_15/Conv2D:output:0(conv2d_15/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<c
conv2d_15/ReluReluconv2d_15/BiasAdd:output:0*
T0*&
_output_shapes
:<<�
conv2d_16/Conv2D/ReadVariableOpReadVariableOp(conv2d_16_conv2d_readvariableop_resource*&
_output_shapes
: *
dtype0�
conv2d_16/Conv2DConv2Dconv2d_15/Relu:activations:0'conv2d_16/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
: *
paddingSAME*
strides
�
 conv2d_16/BiasAdd/ReadVariableOpReadVariableOp)conv2d_16_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
conv2d_16/BiasAddBiasAddconv2d_16/Conv2D:output:0(conv2d_16/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
: c
conv2d_16/ReluReluconv2d_16/BiasAdd:output:0*
T0*&
_output_shapes
: �
conv2d_17/Conv2D/ReadVariableOpReadVariableOp(conv2d_17_conv2d_readvariableop_resource*'
_output_shapes
: �*
dtype0�
conv2d_17/Conv2DConv2Dconv2d_16/Relu:activations:0'conv2d_17/Conv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
 conv2d_17/BiasAdd/ReadVariableOpReadVariableOp)conv2d_17_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
conv2d_17/BiasAddBiasAddconv2d_17/Conv2D:output:0(conv2d_17/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�d
conv2d_17/ReluReluconv2d_17/BiasAdd:output:0*
T0*'
_output_shapes
:��
conv2d_18/Conv2D/ReadVariableOpReadVariableOp(conv2d_18_conv2d_readvariableop_resource*(
_output_shapes
:��*
dtype0�
conv2d_18/Conv2DConv2Dconv2d_17/Relu:activations:0'conv2d_18/Conv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
 conv2d_18/BiasAdd/ReadVariableOpReadVariableOp)conv2d_18_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
conv2d_18/BiasAddBiasAddconv2d_18/Conv2D:output:0(conv2d_18/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�d
conv2d_18/ReluReluconv2d_18/BiasAdd:output:0*
T0*'
_output_shapes
:�`
flatten_3/ConstConst*
_output_shapes
:*
dtype0*
valueB"���� � 
flatten_3/ReshapeReshapeconv2d_18/Relu:activations:0flatten_3/Const:output:0*
T0* 
_output_shapes
:
���
dense_9/MatMul/ReadVariableOpReadVariableOp&dense_9_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
dense_9/MatMulMatMulflatten_3/Reshape:output:0%dense_9/MatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:�
dense_9/BiasAdd/ReadVariableOpReadVariableOp'dense_9_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense_9/BiasAddBiasAdddense_9/MatMul:product:0&dense_9/BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:�
dense_10/MatMul/ReadVariableOpReadVariableOp'dense_10_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
dense_10/MatMulMatMulflatten_3/Reshape:output:0&dense_10/MatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:�
dense_10/BiasAdd/ReadVariableOpReadVariableOp(dense_10_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense_10/BiasAddBiasAdddense_10/MatMul:product:0'dense_10/BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:_
lambda_3/ShapeConst*
_output_shapes
:*
dtype0*
valueB"      f
lambda_3/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: h
lambda_3/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:h
lambda_3/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
lambda_3/strided_sliceStridedSlicelambda_3/Shape:output:0%lambda_3/strided_slice/stack:output:0'lambda_3/strided_slice/stack_1:output:0'lambda_3/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask`
lambda_3/random_normal/shape/1Const*
_output_shapes
: *
dtype0*
value	B :�
lambda_3/random_normal/shapePacklambda_3/strided_slice:output:0'lambda_3/random_normal/shape/1:output:0*
N*
T0*
_output_shapes
:`
lambda_3/random_normal/meanConst*
_output_shapes
: *
dtype0*
valueB
 *    b
lambda_3/random_normal/stddevConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
+lambda_3/random_normal/RandomStandardNormalRandomStandardNormal%lambda_3/random_normal/shape:output:0*
T0*
_output_shapes

:*
dtype0*
seed���)*
seed2����
lambda_3/random_normal/mulMul4lambda_3/random_normal/RandomStandardNormal:output:0&lambda_3/random_normal/stddev:output:0*
T0*
_output_shapes

:�
lambda_3/random_normalAddV2lambda_3/random_normal/mul:z:0$lambda_3/random_normal/mean:output:0*
T0*
_output_shapes

:W
lambda_3/truediv/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @|
lambda_3/truedivRealDivdense_10/BiasAdd:output:0lambda_3/truediv/y:output:0*
T0*
_output_shapes

:R
lambda_3/ExpExplambda_3/truediv:z:0*
T0*
_output_shapes

:j
lambda_3/mulMullambda_3/Exp:y:0lambda_3/random_normal:z:0*
T0*
_output_shapes

:j
lambda_3/addAddV2dense_9/BiasAdd:output:0lambda_3/mul:z:0*
T0*
_output_shapes

:^
IdentityIdentitydense_9/BiasAdd:output:0^NoOp*
T0*
_output_shapes

:a

Identity_1Identitydense_10/BiasAdd:output:0^NoOp*
T0*
_output_shapes

:X

Identity_2Identitylambda_3/add:z:0^NoOp*
T0*
_output_shapes

:�
NoOpNoOp!^conv2d_15/BiasAdd/ReadVariableOp ^conv2d_15/Conv2D/ReadVariableOp!^conv2d_16/BiasAdd/ReadVariableOp ^conv2d_16/Conv2D/ReadVariableOp!^conv2d_17/BiasAdd/ReadVariableOp ^conv2d_17/Conv2D/ReadVariableOp!^conv2d_18/BiasAdd/ReadVariableOp ^conv2d_18/Conv2D/ReadVariableOp ^dense_10/BiasAdd/ReadVariableOp^dense_10/MatMul/ReadVariableOp^dense_9/BiasAdd/ReadVariableOp^dense_9/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*=
_input_shapes,
*:<<: : : : : : : : : : : : 2D
 conv2d_15/BiasAdd/ReadVariableOp conv2d_15/BiasAdd/ReadVariableOp2B
conv2d_15/Conv2D/ReadVariableOpconv2d_15/Conv2D/ReadVariableOp2D
 conv2d_16/BiasAdd/ReadVariableOp conv2d_16/BiasAdd/ReadVariableOp2B
conv2d_16/Conv2D/ReadVariableOpconv2d_16/Conv2D/ReadVariableOp2D
 conv2d_17/BiasAdd/ReadVariableOp conv2d_17/BiasAdd/ReadVariableOp2B
conv2d_17/Conv2D/ReadVariableOpconv2d_17/Conv2D/ReadVariableOp2D
 conv2d_18/BiasAdd/ReadVariableOp conv2d_18/BiasAdd/ReadVariableOp2B
conv2d_18/Conv2D/ReadVariableOpconv2d_18/Conv2D/ReadVariableOp2B
dense_10/BiasAdd/ReadVariableOpdense_10/BiasAdd/ReadVariableOp2@
dense_10/MatMul/ReadVariableOpdense_10/MatMul/ReadVariableOp2@
dense_9/BiasAdd/ReadVariableOpdense_9/BiasAdd/ReadVariableOp2>
dense_9/MatMul/ReadVariableOpdense_9/MatMul/ReadVariableOp:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs
�
a
E__inference_reshape_3_layer_call_and_return_conditional_losses_248343

inputs
identityV
ShapeConst*
_output_shapes
:*
dtype0*
valueB"    � ]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskQ
Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :Q
Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :R
Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value
B :��
Reshape/shapePackstrided_slice:output:0Reshape/shape/1:output:0Reshape/shape/2:output:0Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:d
ReshapeReshapeinputsReshape/shape:output:0*
T0*'
_output_shapes
:�X
IdentityIdentityReshape:output:0*
T0*'
_output_shapes
:�"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
:
��:H D
 
_output_shapes
:
��
 
_user_specified_nameinputs
��
�L
__inference__traced_save_249042
file_prefixA
'read_disablecopyonread_conv2d_15_kernel:5
'read_1_disablecopyonread_conv2d_15_bias:C
)read_2_disablecopyonread_conv2d_16_kernel: 5
'read_3_disablecopyonread_conv2d_16_bias: D
)read_4_disablecopyonread_conv2d_17_kernel: �6
'read_5_disablecopyonread_conv2d_17_bias:	�E
)read_6_disablecopyonread_conv2d_18_kernel:��6
'read_7_disablecopyonread_conv2d_18_bias:	�<
(read_8_disablecopyonread_dense_10_kernel:
��4
&read_9_disablecopyonread_dense_10_bias:<
(read_10_disablecopyonread_dense_9_kernel:
��4
&read_11_disablecopyonread_dense_9_bias:=
)read_12_disablecopyonread_dense_11_kernel:
��7
'read_13_disablecopyonread_dense_11_bias:
��P
4read_14_disablecopyonread_conv2d_transpose_12_kernel:��A
2read_15_disablecopyonread_conv2d_transpose_12_bias:	�O
4read_16_disablecopyonread_conv2d_transpose_13_kernel:@�@
2read_17_disablecopyonread_conv2d_transpose_13_bias:@N
4read_18_disablecopyonread_conv2d_transpose_14_kernel: @@
2read_19_disablecopyonread_conv2d_transpose_14_bias: N
4read_20_disablecopyonread_conv2d_transpose_15_kernel: @
2read_21_disablecopyonread_conv2d_transpose_15_bias:D
*read_22_disablecopyonread_conv2d_19_kernel:6
(read_23_disablecopyonread_conv2d_19_bias:-
#read_24_disablecopyonread_iteration:	 1
'read_25_disablecopyonread_learning_rate: K
1read_26_disablecopyonread_adam_m_conv2d_15_kernel:K
1read_27_disablecopyonread_adam_v_conv2d_15_kernel:=
/read_28_disablecopyonread_adam_m_conv2d_15_bias:=
/read_29_disablecopyonread_adam_v_conv2d_15_bias:K
1read_30_disablecopyonread_adam_m_conv2d_16_kernel: K
1read_31_disablecopyonread_adam_v_conv2d_16_kernel: =
/read_32_disablecopyonread_adam_m_conv2d_16_bias: =
/read_33_disablecopyonread_adam_v_conv2d_16_bias: L
1read_34_disablecopyonread_adam_m_conv2d_17_kernel: �L
1read_35_disablecopyonread_adam_v_conv2d_17_kernel: �>
/read_36_disablecopyonread_adam_m_conv2d_17_bias:	�>
/read_37_disablecopyonread_adam_v_conv2d_17_bias:	�M
1read_38_disablecopyonread_adam_m_conv2d_18_kernel:��M
1read_39_disablecopyonread_adam_v_conv2d_18_kernel:��>
/read_40_disablecopyonread_adam_m_conv2d_18_bias:	�>
/read_41_disablecopyonread_adam_v_conv2d_18_bias:	�C
/read_42_disablecopyonread_adam_m_dense_9_kernel:
��C
/read_43_disablecopyonread_adam_v_dense_9_kernel:
��;
-read_44_disablecopyonread_adam_m_dense_9_bias:;
-read_45_disablecopyonread_adam_v_dense_9_bias:D
0read_46_disablecopyonread_adam_m_dense_10_kernel:
��D
0read_47_disablecopyonread_adam_v_dense_10_kernel:
��<
.read_48_disablecopyonread_adam_m_dense_10_bias:<
.read_49_disablecopyonread_adam_v_dense_10_bias:D
0read_50_disablecopyonread_adam_m_dense_11_kernel:
��D
0read_51_disablecopyonread_adam_v_dense_11_kernel:
��>
.read_52_disablecopyonread_adam_m_dense_11_bias:
��>
.read_53_disablecopyonread_adam_v_dense_11_bias:
��W
;read_54_disablecopyonread_adam_m_conv2d_transpose_12_kernel:��W
;read_55_disablecopyonread_adam_v_conv2d_transpose_12_kernel:��H
9read_56_disablecopyonread_adam_m_conv2d_transpose_12_bias:	�H
9read_57_disablecopyonread_adam_v_conv2d_transpose_12_bias:	�V
;read_58_disablecopyonread_adam_m_conv2d_transpose_13_kernel:@�V
;read_59_disablecopyonread_adam_v_conv2d_transpose_13_kernel:@�G
9read_60_disablecopyonread_adam_m_conv2d_transpose_13_bias:@G
9read_61_disablecopyonread_adam_v_conv2d_transpose_13_bias:@U
;read_62_disablecopyonread_adam_m_conv2d_transpose_14_kernel: @U
;read_63_disablecopyonread_adam_v_conv2d_transpose_14_kernel: @G
9read_64_disablecopyonread_adam_m_conv2d_transpose_14_bias: G
9read_65_disablecopyonread_adam_v_conv2d_transpose_14_bias: U
;read_66_disablecopyonread_adam_m_conv2d_transpose_15_kernel: U
;read_67_disablecopyonread_adam_v_conv2d_transpose_15_kernel: G
9read_68_disablecopyonread_adam_m_conv2d_transpose_15_bias:G
9read_69_disablecopyonread_adam_v_conv2d_transpose_15_bias:K
1read_70_disablecopyonread_adam_m_conv2d_19_kernel:K
1read_71_disablecopyonread_adam_v_conv2d_19_kernel:=
/read_72_disablecopyonread_adam_m_conv2d_19_bias:=
/read_73_disablecopyonread_adam_v_conv2d_19_bias:)
read_74_disablecopyonread_total: )
read_75_disablecopyonread_count: 6
,read_76_disablecopyonread_add_metric_6_total: 6
,read_77_disablecopyonread_add_metric_6_count: 6
,read_78_disablecopyonread_add_metric_7_total: 6
,read_79_disablecopyonread_add_metric_7_count: 
savev2_const_4
identity_161��MergeV2Checkpoints�Read/DisableCopyOnRead�Read/ReadVariableOp�Read_1/DisableCopyOnRead�Read_1/ReadVariableOp�Read_10/DisableCopyOnRead�Read_10/ReadVariableOp�Read_11/DisableCopyOnRead�Read_11/ReadVariableOp�Read_12/DisableCopyOnRead�Read_12/ReadVariableOp�Read_13/DisableCopyOnRead�Read_13/ReadVariableOp�Read_14/DisableCopyOnRead�Read_14/ReadVariableOp�Read_15/DisableCopyOnRead�Read_15/ReadVariableOp�Read_16/DisableCopyOnRead�Read_16/ReadVariableOp�Read_17/DisableCopyOnRead�Read_17/ReadVariableOp�Read_18/DisableCopyOnRead�Read_18/ReadVariableOp�Read_19/DisableCopyOnRead�Read_19/ReadVariableOp�Read_2/DisableCopyOnRead�Read_2/ReadVariableOp�Read_20/DisableCopyOnRead�Read_20/ReadVariableOp�Read_21/DisableCopyOnRead�Read_21/ReadVariableOp�Read_22/DisableCopyOnRead�Read_22/ReadVariableOp�Read_23/DisableCopyOnRead�Read_23/ReadVariableOp�Read_24/DisableCopyOnRead�Read_24/ReadVariableOp�Read_25/DisableCopyOnRead�Read_25/ReadVariableOp�Read_26/DisableCopyOnRead�Read_26/ReadVariableOp�Read_27/DisableCopyOnRead�Read_27/ReadVariableOp�Read_28/DisableCopyOnRead�Read_28/ReadVariableOp�Read_29/DisableCopyOnRead�Read_29/ReadVariableOp�Read_3/DisableCopyOnRead�Read_3/ReadVariableOp�Read_30/DisableCopyOnRead�Read_30/ReadVariableOp�Read_31/DisableCopyOnRead�Read_31/ReadVariableOp�Read_32/DisableCopyOnRead�Read_32/ReadVariableOp�Read_33/DisableCopyOnRead�Read_33/ReadVariableOp�Read_34/DisableCopyOnRead�Read_34/ReadVariableOp�Read_35/DisableCopyOnRead�Read_35/ReadVariableOp�Read_36/DisableCopyOnRead�Read_36/ReadVariableOp�Read_37/DisableCopyOnRead�Read_37/ReadVariableOp�Read_38/DisableCopyOnRead�Read_38/ReadVariableOp�Read_39/DisableCopyOnRead�Read_39/ReadVariableOp�Read_4/DisableCopyOnRead�Read_4/ReadVariableOp�Read_40/DisableCopyOnRead�Read_40/ReadVariableOp�Read_41/DisableCopyOnRead�Read_41/ReadVariableOp�Read_42/DisableCopyOnRead�Read_42/ReadVariableOp�Read_43/DisableCopyOnRead�Read_43/ReadVariableOp�Read_44/DisableCopyOnRead�Read_44/ReadVariableOp�Read_45/DisableCopyOnRead�Read_45/ReadVariableOp�Read_46/DisableCopyOnRead�Read_46/ReadVariableOp�Read_47/DisableCopyOnRead�Read_47/ReadVariableOp�Read_48/DisableCopyOnRead�Read_48/ReadVariableOp�Read_49/DisableCopyOnRead�Read_49/ReadVariableOp�Read_5/DisableCopyOnRead�Read_5/ReadVariableOp�Read_50/DisableCopyOnRead�Read_50/ReadVariableOp�Read_51/DisableCopyOnRead�Read_51/ReadVariableOp�Read_52/DisableCopyOnRead�Read_52/ReadVariableOp�Read_53/DisableCopyOnRead�Read_53/ReadVariableOp�Read_54/DisableCopyOnRead�Read_54/ReadVariableOp�Read_55/DisableCopyOnRead�Read_55/ReadVariableOp�Read_56/DisableCopyOnRead�Read_56/ReadVariableOp�Read_57/DisableCopyOnRead�Read_57/ReadVariableOp�Read_58/DisableCopyOnRead�Read_58/ReadVariableOp�Read_59/DisableCopyOnRead�Read_59/ReadVariableOp�Read_6/DisableCopyOnRead�Read_6/ReadVariableOp�Read_60/DisableCopyOnRead�Read_60/ReadVariableOp�Read_61/DisableCopyOnRead�Read_61/ReadVariableOp�Read_62/DisableCopyOnRead�Read_62/ReadVariableOp�Read_63/DisableCopyOnRead�Read_63/ReadVariableOp�Read_64/DisableCopyOnRead�Read_64/ReadVariableOp�Read_65/DisableCopyOnRead�Read_65/ReadVariableOp�Read_66/DisableCopyOnRead�Read_66/ReadVariableOp�Read_67/DisableCopyOnRead�Read_67/ReadVariableOp�Read_68/DisableCopyOnRead�Read_68/ReadVariableOp�Read_69/DisableCopyOnRead�Read_69/ReadVariableOp�Read_7/DisableCopyOnRead�Read_7/ReadVariableOp�Read_70/DisableCopyOnRead�Read_70/ReadVariableOp�Read_71/DisableCopyOnRead�Read_71/ReadVariableOp�Read_72/DisableCopyOnRead�Read_72/ReadVariableOp�Read_73/DisableCopyOnRead�Read_73/ReadVariableOp�Read_74/DisableCopyOnRead�Read_74/ReadVariableOp�Read_75/DisableCopyOnRead�Read_75/ReadVariableOp�Read_76/DisableCopyOnRead�Read_76/ReadVariableOp�Read_77/DisableCopyOnRead�Read_77/ReadVariableOp�Read_78/DisableCopyOnRead�Read_78/ReadVariableOp�Read_79/DisableCopyOnRead�Read_79/ReadVariableOp�Read_8/DisableCopyOnRead�Read_8/ReadVariableOp�Read_9/DisableCopyOnRead�Read_9/ReadVariableOpw
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*Z
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.parta
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: f

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: L

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :f
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : �
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: y
Read/DisableCopyOnReadDisableCopyOnRead'read_disablecopyonread_conv2d_15_kernel"/device:CPU:0*
_output_shapes
 �
Read/ReadVariableOpReadVariableOp'read_disablecopyonread_conv2d_15_kernel^Read/DisableCopyOnRead"/device:CPU:0*&
_output_shapes
:*
dtype0q
IdentityIdentityRead/ReadVariableOp:value:0"/device:CPU:0*
T0*&
_output_shapes
:i

Identity_1IdentityIdentity:output:0"/device:CPU:0*
T0*&
_output_shapes
:{
Read_1/DisableCopyOnReadDisableCopyOnRead'read_1_disablecopyonread_conv2d_15_bias"/device:CPU:0*
_output_shapes
 �
Read_1/ReadVariableOpReadVariableOp'read_1_disablecopyonread_conv2d_15_bias^Read_1/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0i

Identity_2IdentityRead_1/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:_

Identity_3IdentityIdentity_2:output:0"/device:CPU:0*
T0*
_output_shapes
:}
Read_2/DisableCopyOnReadDisableCopyOnRead)read_2_disablecopyonread_conv2d_16_kernel"/device:CPU:0*
_output_shapes
 �
Read_2/ReadVariableOpReadVariableOp)read_2_disablecopyonread_conv2d_16_kernel^Read_2/DisableCopyOnRead"/device:CPU:0*&
_output_shapes
: *
dtype0u

Identity_4IdentityRead_2/ReadVariableOp:value:0"/device:CPU:0*
T0*&
_output_shapes
: k

Identity_5IdentityIdentity_4:output:0"/device:CPU:0*
T0*&
_output_shapes
: {
Read_3/DisableCopyOnReadDisableCopyOnRead'read_3_disablecopyonread_conv2d_16_bias"/device:CPU:0*
_output_shapes
 �
Read_3/ReadVariableOpReadVariableOp'read_3_disablecopyonread_conv2d_16_bias^Read_3/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0i

Identity_6IdentityRead_3/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: _

Identity_7IdentityIdentity_6:output:0"/device:CPU:0*
T0*
_output_shapes
: }
Read_4/DisableCopyOnReadDisableCopyOnRead)read_4_disablecopyonread_conv2d_17_kernel"/device:CPU:0*
_output_shapes
 �
Read_4/ReadVariableOpReadVariableOp)read_4_disablecopyonread_conv2d_17_kernel^Read_4/DisableCopyOnRead"/device:CPU:0*'
_output_shapes
: �*
dtype0v

Identity_8IdentityRead_4/ReadVariableOp:value:0"/device:CPU:0*
T0*'
_output_shapes
: �l

Identity_9IdentityIdentity_8:output:0"/device:CPU:0*
T0*'
_output_shapes
: �{
Read_5/DisableCopyOnReadDisableCopyOnRead'read_5_disablecopyonread_conv2d_17_bias"/device:CPU:0*
_output_shapes
 �
Read_5/ReadVariableOpReadVariableOp'read_5_disablecopyonread_conv2d_17_bias^Read_5/DisableCopyOnRead"/device:CPU:0*
_output_shapes	
:�*
dtype0k
Identity_10IdentityRead_5/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes	
:�b
Identity_11IdentityIdentity_10:output:0"/device:CPU:0*
T0*
_output_shapes	
:�}
Read_6/DisableCopyOnReadDisableCopyOnRead)read_6_disablecopyonread_conv2d_18_kernel"/device:CPU:0*
_output_shapes
 �
Read_6/ReadVariableOpReadVariableOp)read_6_disablecopyonread_conv2d_18_kernel^Read_6/DisableCopyOnRead"/device:CPU:0*(
_output_shapes
:��*
dtype0x
Identity_12IdentityRead_6/ReadVariableOp:value:0"/device:CPU:0*
T0*(
_output_shapes
:��o
Identity_13IdentityIdentity_12:output:0"/device:CPU:0*
T0*(
_output_shapes
:��{
Read_7/DisableCopyOnReadDisableCopyOnRead'read_7_disablecopyonread_conv2d_18_bias"/device:CPU:0*
_output_shapes
 �
Read_7/ReadVariableOpReadVariableOp'read_7_disablecopyonread_conv2d_18_bias^Read_7/DisableCopyOnRead"/device:CPU:0*
_output_shapes	
:�*
dtype0k
Identity_14IdentityRead_7/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes	
:�b
Identity_15IdentityIdentity_14:output:0"/device:CPU:0*
T0*
_output_shapes	
:�|
Read_8/DisableCopyOnReadDisableCopyOnRead(read_8_disablecopyonread_dense_10_kernel"/device:CPU:0*
_output_shapes
 �
Read_8/ReadVariableOpReadVariableOp(read_8_disablecopyonread_dense_10_kernel^Read_8/DisableCopyOnRead"/device:CPU:0* 
_output_shapes
:
��*
dtype0p
Identity_16IdentityRead_8/ReadVariableOp:value:0"/device:CPU:0*
T0* 
_output_shapes
:
��g
Identity_17IdentityIdentity_16:output:0"/device:CPU:0*
T0* 
_output_shapes
:
��z
Read_9/DisableCopyOnReadDisableCopyOnRead&read_9_disablecopyonread_dense_10_bias"/device:CPU:0*
_output_shapes
 �
Read_9/ReadVariableOpReadVariableOp&read_9_disablecopyonread_dense_10_bias^Read_9/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0j
Identity_18IdentityRead_9/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:a
Identity_19IdentityIdentity_18:output:0"/device:CPU:0*
T0*
_output_shapes
:}
Read_10/DisableCopyOnReadDisableCopyOnRead(read_10_disablecopyonread_dense_9_kernel"/device:CPU:0*
_output_shapes
 �
Read_10/ReadVariableOpReadVariableOp(read_10_disablecopyonread_dense_9_kernel^Read_10/DisableCopyOnRead"/device:CPU:0* 
_output_shapes
:
��*
dtype0q
Identity_20IdentityRead_10/ReadVariableOp:value:0"/device:CPU:0*
T0* 
_output_shapes
:
��g
Identity_21IdentityIdentity_20:output:0"/device:CPU:0*
T0* 
_output_shapes
:
��{
Read_11/DisableCopyOnReadDisableCopyOnRead&read_11_disablecopyonread_dense_9_bias"/device:CPU:0*
_output_shapes
 �
Read_11/ReadVariableOpReadVariableOp&read_11_disablecopyonread_dense_9_bias^Read_11/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0k
Identity_22IdentityRead_11/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:a
Identity_23IdentityIdentity_22:output:0"/device:CPU:0*
T0*
_output_shapes
:~
Read_12/DisableCopyOnReadDisableCopyOnRead)read_12_disablecopyonread_dense_11_kernel"/device:CPU:0*
_output_shapes
 �
Read_12/ReadVariableOpReadVariableOp)read_12_disablecopyonread_dense_11_kernel^Read_12/DisableCopyOnRead"/device:CPU:0* 
_output_shapes
:
��*
dtype0q
Identity_24IdentityRead_12/ReadVariableOp:value:0"/device:CPU:0*
T0* 
_output_shapes
:
��g
Identity_25IdentityIdentity_24:output:0"/device:CPU:0*
T0* 
_output_shapes
:
��|
Read_13/DisableCopyOnReadDisableCopyOnRead'read_13_disablecopyonread_dense_11_bias"/device:CPU:0*
_output_shapes
 �
Read_13/ReadVariableOpReadVariableOp'read_13_disablecopyonread_dense_11_bias^Read_13/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:��*
dtype0m
Identity_26IdentityRead_13/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:��c
Identity_27IdentityIdentity_26:output:0"/device:CPU:0*
T0*
_output_shapes

:���
Read_14/DisableCopyOnReadDisableCopyOnRead4read_14_disablecopyonread_conv2d_transpose_12_kernel"/device:CPU:0*
_output_shapes
 �
Read_14/ReadVariableOpReadVariableOp4read_14_disablecopyonread_conv2d_transpose_12_kernel^Read_14/DisableCopyOnRead"/device:CPU:0*(
_output_shapes
:��*
dtype0y
Identity_28IdentityRead_14/ReadVariableOp:value:0"/device:CPU:0*
T0*(
_output_shapes
:��o
Identity_29IdentityIdentity_28:output:0"/device:CPU:0*
T0*(
_output_shapes
:���
Read_15/DisableCopyOnReadDisableCopyOnRead2read_15_disablecopyonread_conv2d_transpose_12_bias"/device:CPU:0*
_output_shapes
 �
Read_15/ReadVariableOpReadVariableOp2read_15_disablecopyonread_conv2d_transpose_12_bias^Read_15/DisableCopyOnRead"/device:CPU:0*
_output_shapes	
:�*
dtype0l
Identity_30IdentityRead_15/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes	
:�b
Identity_31IdentityIdentity_30:output:0"/device:CPU:0*
T0*
_output_shapes	
:��
Read_16/DisableCopyOnReadDisableCopyOnRead4read_16_disablecopyonread_conv2d_transpose_13_kernel"/device:CPU:0*
_output_shapes
 �
Read_16/ReadVariableOpReadVariableOp4read_16_disablecopyonread_conv2d_transpose_13_kernel^Read_16/DisableCopyOnRead"/device:CPU:0*'
_output_shapes
:@�*
dtype0x
Identity_32IdentityRead_16/ReadVariableOp:value:0"/device:CPU:0*
T0*'
_output_shapes
:@�n
Identity_33IdentityIdentity_32:output:0"/device:CPU:0*
T0*'
_output_shapes
:@��
Read_17/DisableCopyOnReadDisableCopyOnRead2read_17_disablecopyonread_conv2d_transpose_13_bias"/device:CPU:0*
_output_shapes
 �
Read_17/ReadVariableOpReadVariableOp2read_17_disablecopyonread_conv2d_transpose_13_bias^Read_17/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:@*
dtype0k
Identity_34IdentityRead_17/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:@a
Identity_35IdentityIdentity_34:output:0"/device:CPU:0*
T0*
_output_shapes
:@�
Read_18/DisableCopyOnReadDisableCopyOnRead4read_18_disablecopyonread_conv2d_transpose_14_kernel"/device:CPU:0*
_output_shapes
 �
Read_18/ReadVariableOpReadVariableOp4read_18_disablecopyonread_conv2d_transpose_14_kernel^Read_18/DisableCopyOnRead"/device:CPU:0*&
_output_shapes
: @*
dtype0w
Identity_36IdentityRead_18/ReadVariableOp:value:0"/device:CPU:0*
T0*&
_output_shapes
: @m
Identity_37IdentityIdentity_36:output:0"/device:CPU:0*
T0*&
_output_shapes
: @�
Read_19/DisableCopyOnReadDisableCopyOnRead2read_19_disablecopyonread_conv2d_transpose_14_bias"/device:CPU:0*
_output_shapes
 �
Read_19/ReadVariableOpReadVariableOp2read_19_disablecopyonread_conv2d_transpose_14_bias^Read_19/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0k
Identity_38IdentityRead_19/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: a
Identity_39IdentityIdentity_38:output:0"/device:CPU:0*
T0*
_output_shapes
: �
Read_20/DisableCopyOnReadDisableCopyOnRead4read_20_disablecopyonread_conv2d_transpose_15_kernel"/device:CPU:0*
_output_shapes
 �
Read_20/ReadVariableOpReadVariableOp4read_20_disablecopyonread_conv2d_transpose_15_kernel^Read_20/DisableCopyOnRead"/device:CPU:0*&
_output_shapes
: *
dtype0w
Identity_40IdentityRead_20/ReadVariableOp:value:0"/device:CPU:0*
T0*&
_output_shapes
: m
Identity_41IdentityIdentity_40:output:0"/device:CPU:0*
T0*&
_output_shapes
: �
Read_21/DisableCopyOnReadDisableCopyOnRead2read_21_disablecopyonread_conv2d_transpose_15_bias"/device:CPU:0*
_output_shapes
 �
Read_21/ReadVariableOpReadVariableOp2read_21_disablecopyonread_conv2d_transpose_15_bias^Read_21/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0k
Identity_42IdentityRead_21/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:a
Identity_43IdentityIdentity_42:output:0"/device:CPU:0*
T0*
_output_shapes
:
Read_22/DisableCopyOnReadDisableCopyOnRead*read_22_disablecopyonread_conv2d_19_kernel"/device:CPU:0*
_output_shapes
 �
Read_22/ReadVariableOpReadVariableOp*read_22_disablecopyonread_conv2d_19_kernel^Read_22/DisableCopyOnRead"/device:CPU:0*&
_output_shapes
:*
dtype0w
Identity_44IdentityRead_22/ReadVariableOp:value:0"/device:CPU:0*
T0*&
_output_shapes
:m
Identity_45IdentityIdentity_44:output:0"/device:CPU:0*
T0*&
_output_shapes
:}
Read_23/DisableCopyOnReadDisableCopyOnRead(read_23_disablecopyonread_conv2d_19_bias"/device:CPU:0*
_output_shapes
 �
Read_23/ReadVariableOpReadVariableOp(read_23_disablecopyonread_conv2d_19_bias^Read_23/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0k
Identity_46IdentityRead_23/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:a
Identity_47IdentityIdentity_46:output:0"/device:CPU:0*
T0*
_output_shapes
:x
Read_24/DisableCopyOnReadDisableCopyOnRead#read_24_disablecopyonread_iteration"/device:CPU:0*
_output_shapes
 �
Read_24/ReadVariableOpReadVariableOp#read_24_disablecopyonread_iteration^Read_24/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0	g
Identity_48IdentityRead_24/ReadVariableOp:value:0"/device:CPU:0*
T0	*
_output_shapes
: ]
Identity_49IdentityIdentity_48:output:0"/device:CPU:0*
T0	*
_output_shapes
: |
Read_25/DisableCopyOnReadDisableCopyOnRead'read_25_disablecopyonread_learning_rate"/device:CPU:0*
_output_shapes
 �
Read_25/ReadVariableOpReadVariableOp'read_25_disablecopyonread_learning_rate^Read_25/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0g
Identity_50IdentityRead_25/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: ]
Identity_51IdentityIdentity_50:output:0"/device:CPU:0*
T0*
_output_shapes
: �
Read_26/DisableCopyOnReadDisableCopyOnRead1read_26_disablecopyonread_adam_m_conv2d_15_kernel"/device:CPU:0*
_output_shapes
 �
Read_26/ReadVariableOpReadVariableOp1read_26_disablecopyonread_adam_m_conv2d_15_kernel^Read_26/DisableCopyOnRead"/device:CPU:0*&
_output_shapes
:*
dtype0w
Identity_52IdentityRead_26/ReadVariableOp:value:0"/device:CPU:0*
T0*&
_output_shapes
:m
Identity_53IdentityIdentity_52:output:0"/device:CPU:0*
T0*&
_output_shapes
:�
Read_27/DisableCopyOnReadDisableCopyOnRead1read_27_disablecopyonread_adam_v_conv2d_15_kernel"/device:CPU:0*
_output_shapes
 �
Read_27/ReadVariableOpReadVariableOp1read_27_disablecopyonread_adam_v_conv2d_15_kernel^Read_27/DisableCopyOnRead"/device:CPU:0*&
_output_shapes
:*
dtype0w
Identity_54IdentityRead_27/ReadVariableOp:value:0"/device:CPU:0*
T0*&
_output_shapes
:m
Identity_55IdentityIdentity_54:output:0"/device:CPU:0*
T0*&
_output_shapes
:�
Read_28/DisableCopyOnReadDisableCopyOnRead/read_28_disablecopyonread_adam_m_conv2d_15_bias"/device:CPU:0*
_output_shapes
 �
Read_28/ReadVariableOpReadVariableOp/read_28_disablecopyonread_adam_m_conv2d_15_bias^Read_28/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0k
Identity_56IdentityRead_28/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:a
Identity_57IdentityIdentity_56:output:0"/device:CPU:0*
T0*
_output_shapes
:�
Read_29/DisableCopyOnReadDisableCopyOnRead/read_29_disablecopyonread_adam_v_conv2d_15_bias"/device:CPU:0*
_output_shapes
 �
Read_29/ReadVariableOpReadVariableOp/read_29_disablecopyonread_adam_v_conv2d_15_bias^Read_29/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0k
Identity_58IdentityRead_29/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:a
Identity_59IdentityIdentity_58:output:0"/device:CPU:0*
T0*
_output_shapes
:�
Read_30/DisableCopyOnReadDisableCopyOnRead1read_30_disablecopyonread_adam_m_conv2d_16_kernel"/device:CPU:0*
_output_shapes
 �
Read_30/ReadVariableOpReadVariableOp1read_30_disablecopyonread_adam_m_conv2d_16_kernel^Read_30/DisableCopyOnRead"/device:CPU:0*&
_output_shapes
: *
dtype0w
Identity_60IdentityRead_30/ReadVariableOp:value:0"/device:CPU:0*
T0*&
_output_shapes
: m
Identity_61IdentityIdentity_60:output:0"/device:CPU:0*
T0*&
_output_shapes
: �
Read_31/DisableCopyOnReadDisableCopyOnRead1read_31_disablecopyonread_adam_v_conv2d_16_kernel"/device:CPU:0*
_output_shapes
 �
Read_31/ReadVariableOpReadVariableOp1read_31_disablecopyonread_adam_v_conv2d_16_kernel^Read_31/DisableCopyOnRead"/device:CPU:0*&
_output_shapes
: *
dtype0w
Identity_62IdentityRead_31/ReadVariableOp:value:0"/device:CPU:0*
T0*&
_output_shapes
: m
Identity_63IdentityIdentity_62:output:0"/device:CPU:0*
T0*&
_output_shapes
: �
Read_32/DisableCopyOnReadDisableCopyOnRead/read_32_disablecopyonread_adam_m_conv2d_16_bias"/device:CPU:0*
_output_shapes
 �
Read_32/ReadVariableOpReadVariableOp/read_32_disablecopyonread_adam_m_conv2d_16_bias^Read_32/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0k
Identity_64IdentityRead_32/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: a
Identity_65IdentityIdentity_64:output:0"/device:CPU:0*
T0*
_output_shapes
: �
Read_33/DisableCopyOnReadDisableCopyOnRead/read_33_disablecopyonread_adam_v_conv2d_16_bias"/device:CPU:0*
_output_shapes
 �
Read_33/ReadVariableOpReadVariableOp/read_33_disablecopyonread_adam_v_conv2d_16_bias^Read_33/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0k
Identity_66IdentityRead_33/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: a
Identity_67IdentityIdentity_66:output:0"/device:CPU:0*
T0*
_output_shapes
: �
Read_34/DisableCopyOnReadDisableCopyOnRead1read_34_disablecopyonread_adam_m_conv2d_17_kernel"/device:CPU:0*
_output_shapes
 �
Read_34/ReadVariableOpReadVariableOp1read_34_disablecopyonread_adam_m_conv2d_17_kernel^Read_34/DisableCopyOnRead"/device:CPU:0*'
_output_shapes
: �*
dtype0x
Identity_68IdentityRead_34/ReadVariableOp:value:0"/device:CPU:0*
T0*'
_output_shapes
: �n
Identity_69IdentityIdentity_68:output:0"/device:CPU:0*
T0*'
_output_shapes
: ��
Read_35/DisableCopyOnReadDisableCopyOnRead1read_35_disablecopyonread_adam_v_conv2d_17_kernel"/device:CPU:0*
_output_shapes
 �
Read_35/ReadVariableOpReadVariableOp1read_35_disablecopyonread_adam_v_conv2d_17_kernel^Read_35/DisableCopyOnRead"/device:CPU:0*'
_output_shapes
: �*
dtype0x
Identity_70IdentityRead_35/ReadVariableOp:value:0"/device:CPU:0*
T0*'
_output_shapes
: �n
Identity_71IdentityIdentity_70:output:0"/device:CPU:0*
T0*'
_output_shapes
: ��
Read_36/DisableCopyOnReadDisableCopyOnRead/read_36_disablecopyonread_adam_m_conv2d_17_bias"/device:CPU:0*
_output_shapes
 �
Read_36/ReadVariableOpReadVariableOp/read_36_disablecopyonread_adam_m_conv2d_17_bias^Read_36/DisableCopyOnRead"/device:CPU:0*
_output_shapes	
:�*
dtype0l
Identity_72IdentityRead_36/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes	
:�b
Identity_73IdentityIdentity_72:output:0"/device:CPU:0*
T0*
_output_shapes	
:��
Read_37/DisableCopyOnReadDisableCopyOnRead/read_37_disablecopyonread_adam_v_conv2d_17_bias"/device:CPU:0*
_output_shapes
 �
Read_37/ReadVariableOpReadVariableOp/read_37_disablecopyonread_adam_v_conv2d_17_bias^Read_37/DisableCopyOnRead"/device:CPU:0*
_output_shapes	
:�*
dtype0l
Identity_74IdentityRead_37/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes	
:�b
Identity_75IdentityIdentity_74:output:0"/device:CPU:0*
T0*
_output_shapes	
:��
Read_38/DisableCopyOnReadDisableCopyOnRead1read_38_disablecopyonread_adam_m_conv2d_18_kernel"/device:CPU:0*
_output_shapes
 �
Read_38/ReadVariableOpReadVariableOp1read_38_disablecopyonread_adam_m_conv2d_18_kernel^Read_38/DisableCopyOnRead"/device:CPU:0*(
_output_shapes
:��*
dtype0y
Identity_76IdentityRead_38/ReadVariableOp:value:0"/device:CPU:0*
T0*(
_output_shapes
:��o
Identity_77IdentityIdentity_76:output:0"/device:CPU:0*
T0*(
_output_shapes
:���
Read_39/DisableCopyOnReadDisableCopyOnRead1read_39_disablecopyonread_adam_v_conv2d_18_kernel"/device:CPU:0*
_output_shapes
 �
Read_39/ReadVariableOpReadVariableOp1read_39_disablecopyonread_adam_v_conv2d_18_kernel^Read_39/DisableCopyOnRead"/device:CPU:0*(
_output_shapes
:��*
dtype0y
Identity_78IdentityRead_39/ReadVariableOp:value:0"/device:CPU:0*
T0*(
_output_shapes
:��o
Identity_79IdentityIdentity_78:output:0"/device:CPU:0*
T0*(
_output_shapes
:���
Read_40/DisableCopyOnReadDisableCopyOnRead/read_40_disablecopyonread_adam_m_conv2d_18_bias"/device:CPU:0*
_output_shapes
 �
Read_40/ReadVariableOpReadVariableOp/read_40_disablecopyonread_adam_m_conv2d_18_bias^Read_40/DisableCopyOnRead"/device:CPU:0*
_output_shapes	
:�*
dtype0l
Identity_80IdentityRead_40/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes	
:�b
Identity_81IdentityIdentity_80:output:0"/device:CPU:0*
T0*
_output_shapes	
:��
Read_41/DisableCopyOnReadDisableCopyOnRead/read_41_disablecopyonread_adam_v_conv2d_18_bias"/device:CPU:0*
_output_shapes
 �
Read_41/ReadVariableOpReadVariableOp/read_41_disablecopyonread_adam_v_conv2d_18_bias^Read_41/DisableCopyOnRead"/device:CPU:0*
_output_shapes	
:�*
dtype0l
Identity_82IdentityRead_41/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes	
:�b
Identity_83IdentityIdentity_82:output:0"/device:CPU:0*
T0*
_output_shapes	
:��
Read_42/DisableCopyOnReadDisableCopyOnRead/read_42_disablecopyonread_adam_m_dense_9_kernel"/device:CPU:0*
_output_shapes
 �
Read_42/ReadVariableOpReadVariableOp/read_42_disablecopyonread_adam_m_dense_9_kernel^Read_42/DisableCopyOnRead"/device:CPU:0* 
_output_shapes
:
��*
dtype0q
Identity_84IdentityRead_42/ReadVariableOp:value:0"/device:CPU:0*
T0* 
_output_shapes
:
��g
Identity_85IdentityIdentity_84:output:0"/device:CPU:0*
T0* 
_output_shapes
:
���
Read_43/DisableCopyOnReadDisableCopyOnRead/read_43_disablecopyonread_adam_v_dense_9_kernel"/device:CPU:0*
_output_shapes
 �
Read_43/ReadVariableOpReadVariableOp/read_43_disablecopyonread_adam_v_dense_9_kernel^Read_43/DisableCopyOnRead"/device:CPU:0* 
_output_shapes
:
��*
dtype0q
Identity_86IdentityRead_43/ReadVariableOp:value:0"/device:CPU:0*
T0* 
_output_shapes
:
��g
Identity_87IdentityIdentity_86:output:0"/device:CPU:0*
T0* 
_output_shapes
:
���
Read_44/DisableCopyOnReadDisableCopyOnRead-read_44_disablecopyonread_adam_m_dense_9_bias"/device:CPU:0*
_output_shapes
 �
Read_44/ReadVariableOpReadVariableOp-read_44_disablecopyonread_adam_m_dense_9_bias^Read_44/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0k
Identity_88IdentityRead_44/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:a
Identity_89IdentityIdentity_88:output:0"/device:CPU:0*
T0*
_output_shapes
:�
Read_45/DisableCopyOnReadDisableCopyOnRead-read_45_disablecopyonread_adam_v_dense_9_bias"/device:CPU:0*
_output_shapes
 �
Read_45/ReadVariableOpReadVariableOp-read_45_disablecopyonread_adam_v_dense_9_bias^Read_45/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0k
Identity_90IdentityRead_45/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:a
Identity_91IdentityIdentity_90:output:0"/device:CPU:0*
T0*
_output_shapes
:�
Read_46/DisableCopyOnReadDisableCopyOnRead0read_46_disablecopyonread_adam_m_dense_10_kernel"/device:CPU:0*
_output_shapes
 �
Read_46/ReadVariableOpReadVariableOp0read_46_disablecopyonread_adam_m_dense_10_kernel^Read_46/DisableCopyOnRead"/device:CPU:0* 
_output_shapes
:
��*
dtype0q
Identity_92IdentityRead_46/ReadVariableOp:value:0"/device:CPU:0*
T0* 
_output_shapes
:
��g
Identity_93IdentityIdentity_92:output:0"/device:CPU:0*
T0* 
_output_shapes
:
���
Read_47/DisableCopyOnReadDisableCopyOnRead0read_47_disablecopyonread_adam_v_dense_10_kernel"/device:CPU:0*
_output_shapes
 �
Read_47/ReadVariableOpReadVariableOp0read_47_disablecopyonread_adam_v_dense_10_kernel^Read_47/DisableCopyOnRead"/device:CPU:0* 
_output_shapes
:
��*
dtype0q
Identity_94IdentityRead_47/ReadVariableOp:value:0"/device:CPU:0*
T0* 
_output_shapes
:
��g
Identity_95IdentityIdentity_94:output:0"/device:CPU:0*
T0* 
_output_shapes
:
���
Read_48/DisableCopyOnReadDisableCopyOnRead.read_48_disablecopyonread_adam_m_dense_10_bias"/device:CPU:0*
_output_shapes
 �
Read_48/ReadVariableOpReadVariableOp.read_48_disablecopyonread_adam_m_dense_10_bias^Read_48/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0k
Identity_96IdentityRead_48/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:a
Identity_97IdentityIdentity_96:output:0"/device:CPU:0*
T0*
_output_shapes
:�
Read_49/DisableCopyOnReadDisableCopyOnRead.read_49_disablecopyonread_adam_v_dense_10_bias"/device:CPU:0*
_output_shapes
 �
Read_49/ReadVariableOpReadVariableOp.read_49_disablecopyonread_adam_v_dense_10_bias^Read_49/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0k
Identity_98IdentityRead_49/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:a
Identity_99IdentityIdentity_98:output:0"/device:CPU:0*
T0*
_output_shapes
:�
Read_50/DisableCopyOnReadDisableCopyOnRead0read_50_disablecopyonread_adam_m_dense_11_kernel"/device:CPU:0*
_output_shapes
 �
Read_50/ReadVariableOpReadVariableOp0read_50_disablecopyonread_adam_m_dense_11_kernel^Read_50/DisableCopyOnRead"/device:CPU:0* 
_output_shapes
:
��*
dtype0r
Identity_100IdentityRead_50/ReadVariableOp:value:0"/device:CPU:0*
T0* 
_output_shapes
:
��i
Identity_101IdentityIdentity_100:output:0"/device:CPU:0*
T0* 
_output_shapes
:
���
Read_51/DisableCopyOnReadDisableCopyOnRead0read_51_disablecopyonread_adam_v_dense_11_kernel"/device:CPU:0*
_output_shapes
 �
Read_51/ReadVariableOpReadVariableOp0read_51_disablecopyonread_adam_v_dense_11_kernel^Read_51/DisableCopyOnRead"/device:CPU:0* 
_output_shapes
:
��*
dtype0r
Identity_102IdentityRead_51/ReadVariableOp:value:0"/device:CPU:0*
T0* 
_output_shapes
:
��i
Identity_103IdentityIdentity_102:output:0"/device:CPU:0*
T0* 
_output_shapes
:
���
Read_52/DisableCopyOnReadDisableCopyOnRead.read_52_disablecopyonread_adam_m_dense_11_bias"/device:CPU:0*
_output_shapes
 �
Read_52/ReadVariableOpReadVariableOp.read_52_disablecopyonread_adam_m_dense_11_bias^Read_52/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:��*
dtype0n
Identity_104IdentityRead_52/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:��e
Identity_105IdentityIdentity_104:output:0"/device:CPU:0*
T0*
_output_shapes

:���
Read_53/DisableCopyOnReadDisableCopyOnRead.read_53_disablecopyonread_adam_v_dense_11_bias"/device:CPU:0*
_output_shapes
 �
Read_53/ReadVariableOpReadVariableOp.read_53_disablecopyonread_adam_v_dense_11_bias^Read_53/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:��*
dtype0n
Identity_106IdentityRead_53/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:��e
Identity_107IdentityIdentity_106:output:0"/device:CPU:0*
T0*
_output_shapes

:���
Read_54/DisableCopyOnReadDisableCopyOnRead;read_54_disablecopyonread_adam_m_conv2d_transpose_12_kernel"/device:CPU:0*
_output_shapes
 �
Read_54/ReadVariableOpReadVariableOp;read_54_disablecopyonread_adam_m_conv2d_transpose_12_kernel^Read_54/DisableCopyOnRead"/device:CPU:0*(
_output_shapes
:��*
dtype0z
Identity_108IdentityRead_54/ReadVariableOp:value:0"/device:CPU:0*
T0*(
_output_shapes
:��q
Identity_109IdentityIdentity_108:output:0"/device:CPU:0*
T0*(
_output_shapes
:���
Read_55/DisableCopyOnReadDisableCopyOnRead;read_55_disablecopyonread_adam_v_conv2d_transpose_12_kernel"/device:CPU:0*
_output_shapes
 �
Read_55/ReadVariableOpReadVariableOp;read_55_disablecopyonread_adam_v_conv2d_transpose_12_kernel^Read_55/DisableCopyOnRead"/device:CPU:0*(
_output_shapes
:��*
dtype0z
Identity_110IdentityRead_55/ReadVariableOp:value:0"/device:CPU:0*
T0*(
_output_shapes
:��q
Identity_111IdentityIdentity_110:output:0"/device:CPU:0*
T0*(
_output_shapes
:���
Read_56/DisableCopyOnReadDisableCopyOnRead9read_56_disablecopyonread_adam_m_conv2d_transpose_12_bias"/device:CPU:0*
_output_shapes
 �
Read_56/ReadVariableOpReadVariableOp9read_56_disablecopyonread_adam_m_conv2d_transpose_12_bias^Read_56/DisableCopyOnRead"/device:CPU:0*
_output_shapes	
:�*
dtype0m
Identity_112IdentityRead_56/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes	
:�d
Identity_113IdentityIdentity_112:output:0"/device:CPU:0*
T0*
_output_shapes	
:��
Read_57/DisableCopyOnReadDisableCopyOnRead9read_57_disablecopyonread_adam_v_conv2d_transpose_12_bias"/device:CPU:0*
_output_shapes
 �
Read_57/ReadVariableOpReadVariableOp9read_57_disablecopyonread_adam_v_conv2d_transpose_12_bias^Read_57/DisableCopyOnRead"/device:CPU:0*
_output_shapes	
:�*
dtype0m
Identity_114IdentityRead_57/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes	
:�d
Identity_115IdentityIdentity_114:output:0"/device:CPU:0*
T0*
_output_shapes	
:��
Read_58/DisableCopyOnReadDisableCopyOnRead;read_58_disablecopyonread_adam_m_conv2d_transpose_13_kernel"/device:CPU:0*
_output_shapes
 �
Read_58/ReadVariableOpReadVariableOp;read_58_disablecopyonread_adam_m_conv2d_transpose_13_kernel^Read_58/DisableCopyOnRead"/device:CPU:0*'
_output_shapes
:@�*
dtype0y
Identity_116IdentityRead_58/ReadVariableOp:value:0"/device:CPU:0*
T0*'
_output_shapes
:@�p
Identity_117IdentityIdentity_116:output:0"/device:CPU:0*
T0*'
_output_shapes
:@��
Read_59/DisableCopyOnReadDisableCopyOnRead;read_59_disablecopyonread_adam_v_conv2d_transpose_13_kernel"/device:CPU:0*
_output_shapes
 �
Read_59/ReadVariableOpReadVariableOp;read_59_disablecopyonread_adam_v_conv2d_transpose_13_kernel^Read_59/DisableCopyOnRead"/device:CPU:0*'
_output_shapes
:@�*
dtype0y
Identity_118IdentityRead_59/ReadVariableOp:value:0"/device:CPU:0*
T0*'
_output_shapes
:@�p
Identity_119IdentityIdentity_118:output:0"/device:CPU:0*
T0*'
_output_shapes
:@��
Read_60/DisableCopyOnReadDisableCopyOnRead9read_60_disablecopyonread_adam_m_conv2d_transpose_13_bias"/device:CPU:0*
_output_shapes
 �
Read_60/ReadVariableOpReadVariableOp9read_60_disablecopyonread_adam_m_conv2d_transpose_13_bias^Read_60/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:@*
dtype0l
Identity_120IdentityRead_60/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:@c
Identity_121IdentityIdentity_120:output:0"/device:CPU:0*
T0*
_output_shapes
:@�
Read_61/DisableCopyOnReadDisableCopyOnRead9read_61_disablecopyonread_adam_v_conv2d_transpose_13_bias"/device:CPU:0*
_output_shapes
 �
Read_61/ReadVariableOpReadVariableOp9read_61_disablecopyonread_adam_v_conv2d_transpose_13_bias^Read_61/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:@*
dtype0l
Identity_122IdentityRead_61/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:@c
Identity_123IdentityIdentity_122:output:0"/device:CPU:0*
T0*
_output_shapes
:@�
Read_62/DisableCopyOnReadDisableCopyOnRead;read_62_disablecopyonread_adam_m_conv2d_transpose_14_kernel"/device:CPU:0*
_output_shapes
 �
Read_62/ReadVariableOpReadVariableOp;read_62_disablecopyonread_adam_m_conv2d_transpose_14_kernel^Read_62/DisableCopyOnRead"/device:CPU:0*&
_output_shapes
: @*
dtype0x
Identity_124IdentityRead_62/ReadVariableOp:value:0"/device:CPU:0*
T0*&
_output_shapes
: @o
Identity_125IdentityIdentity_124:output:0"/device:CPU:0*
T0*&
_output_shapes
: @�
Read_63/DisableCopyOnReadDisableCopyOnRead;read_63_disablecopyonread_adam_v_conv2d_transpose_14_kernel"/device:CPU:0*
_output_shapes
 �
Read_63/ReadVariableOpReadVariableOp;read_63_disablecopyonread_adam_v_conv2d_transpose_14_kernel^Read_63/DisableCopyOnRead"/device:CPU:0*&
_output_shapes
: @*
dtype0x
Identity_126IdentityRead_63/ReadVariableOp:value:0"/device:CPU:0*
T0*&
_output_shapes
: @o
Identity_127IdentityIdentity_126:output:0"/device:CPU:0*
T0*&
_output_shapes
: @�
Read_64/DisableCopyOnReadDisableCopyOnRead9read_64_disablecopyonread_adam_m_conv2d_transpose_14_bias"/device:CPU:0*
_output_shapes
 �
Read_64/ReadVariableOpReadVariableOp9read_64_disablecopyonread_adam_m_conv2d_transpose_14_bias^Read_64/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0l
Identity_128IdentityRead_64/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: c
Identity_129IdentityIdentity_128:output:0"/device:CPU:0*
T0*
_output_shapes
: �
Read_65/DisableCopyOnReadDisableCopyOnRead9read_65_disablecopyonread_adam_v_conv2d_transpose_14_bias"/device:CPU:0*
_output_shapes
 �
Read_65/ReadVariableOpReadVariableOp9read_65_disablecopyonread_adam_v_conv2d_transpose_14_bias^Read_65/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0l
Identity_130IdentityRead_65/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: c
Identity_131IdentityIdentity_130:output:0"/device:CPU:0*
T0*
_output_shapes
: �
Read_66/DisableCopyOnReadDisableCopyOnRead;read_66_disablecopyonread_adam_m_conv2d_transpose_15_kernel"/device:CPU:0*
_output_shapes
 �
Read_66/ReadVariableOpReadVariableOp;read_66_disablecopyonread_adam_m_conv2d_transpose_15_kernel^Read_66/DisableCopyOnRead"/device:CPU:0*&
_output_shapes
: *
dtype0x
Identity_132IdentityRead_66/ReadVariableOp:value:0"/device:CPU:0*
T0*&
_output_shapes
: o
Identity_133IdentityIdentity_132:output:0"/device:CPU:0*
T0*&
_output_shapes
: �
Read_67/DisableCopyOnReadDisableCopyOnRead;read_67_disablecopyonread_adam_v_conv2d_transpose_15_kernel"/device:CPU:0*
_output_shapes
 �
Read_67/ReadVariableOpReadVariableOp;read_67_disablecopyonread_adam_v_conv2d_transpose_15_kernel^Read_67/DisableCopyOnRead"/device:CPU:0*&
_output_shapes
: *
dtype0x
Identity_134IdentityRead_67/ReadVariableOp:value:0"/device:CPU:0*
T0*&
_output_shapes
: o
Identity_135IdentityIdentity_134:output:0"/device:CPU:0*
T0*&
_output_shapes
: �
Read_68/DisableCopyOnReadDisableCopyOnRead9read_68_disablecopyonread_adam_m_conv2d_transpose_15_bias"/device:CPU:0*
_output_shapes
 �
Read_68/ReadVariableOpReadVariableOp9read_68_disablecopyonread_adam_m_conv2d_transpose_15_bias^Read_68/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0l
Identity_136IdentityRead_68/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:c
Identity_137IdentityIdentity_136:output:0"/device:CPU:0*
T0*
_output_shapes
:�
Read_69/DisableCopyOnReadDisableCopyOnRead9read_69_disablecopyonread_adam_v_conv2d_transpose_15_bias"/device:CPU:0*
_output_shapes
 �
Read_69/ReadVariableOpReadVariableOp9read_69_disablecopyonread_adam_v_conv2d_transpose_15_bias^Read_69/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0l
Identity_138IdentityRead_69/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:c
Identity_139IdentityIdentity_138:output:0"/device:CPU:0*
T0*
_output_shapes
:�
Read_70/DisableCopyOnReadDisableCopyOnRead1read_70_disablecopyonread_adam_m_conv2d_19_kernel"/device:CPU:0*
_output_shapes
 �
Read_70/ReadVariableOpReadVariableOp1read_70_disablecopyonread_adam_m_conv2d_19_kernel^Read_70/DisableCopyOnRead"/device:CPU:0*&
_output_shapes
:*
dtype0x
Identity_140IdentityRead_70/ReadVariableOp:value:0"/device:CPU:0*
T0*&
_output_shapes
:o
Identity_141IdentityIdentity_140:output:0"/device:CPU:0*
T0*&
_output_shapes
:�
Read_71/DisableCopyOnReadDisableCopyOnRead1read_71_disablecopyonread_adam_v_conv2d_19_kernel"/device:CPU:0*
_output_shapes
 �
Read_71/ReadVariableOpReadVariableOp1read_71_disablecopyonread_adam_v_conv2d_19_kernel^Read_71/DisableCopyOnRead"/device:CPU:0*&
_output_shapes
:*
dtype0x
Identity_142IdentityRead_71/ReadVariableOp:value:0"/device:CPU:0*
T0*&
_output_shapes
:o
Identity_143IdentityIdentity_142:output:0"/device:CPU:0*
T0*&
_output_shapes
:�
Read_72/DisableCopyOnReadDisableCopyOnRead/read_72_disablecopyonread_adam_m_conv2d_19_bias"/device:CPU:0*
_output_shapes
 �
Read_72/ReadVariableOpReadVariableOp/read_72_disablecopyonread_adam_m_conv2d_19_bias^Read_72/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0l
Identity_144IdentityRead_72/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:c
Identity_145IdentityIdentity_144:output:0"/device:CPU:0*
T0*
_output_shapes
:�
Read_73/DisableCopyOnReadDisableCopyOnRead/read_73_disablecopyonread_adam_v_conv2d_19_bias"/device:CPU:0*
_output_shapes
 �
Read_73/ReadVariableOpReadVariableOp/read_73_disablecopyonread_adam_v_conv2d_19_bias^Read_73/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0l
Identity_146IdentityRead_73/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:c
Identity_147IdentityIdentity_146:output:0"/device:CPU:0*
T0*
_output_shapes
:t
Read_74/DisableCopyOnReadDisableCopyOnReadread_74_disablecopyonread_total"/device:CPU:0*
_output_shapes
 �
Read_74/ReadVariableOpReadVariableOpread_74_disablecopyonread_total^Read_74/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0h
Identity_148IdentityRead_74/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: _
Identity_149IdentityIdentity_148:output:0"/device:CPU:0*
T0*
_output_shapes
: t
Read_75/DisableCopyOnReadDisableCopyOnReadread_75_disablecopyonread_count"/device:CPU:0*
_output_shapes
 �
Read_75/ReadVariableOpReadVariableOpread_75_disablecopyonread_count^Read_75/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0h
Identity_150IdentityRead_75/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: _
Identity_151IdentityIdentity_150:output:0"/device:CPU:0*
T0*
_output_shapes
: �
Read_76/DisableCopyOnReadDisableCopyOnRead,read_76_disablecopyonread_add_metric_6_total"/device:CPU:0*
_output_shapes
 �
Read_76/ReadVariableOpReadVariableOp,read_76_disablecopyonread_add_metric_6_total^Read_76/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0h
Identity_152IdentityRead_76/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: _
Identity_153IdentityIdentity_152:output:0"/device:CPU:0*
T0*
_output_shapes
: �
Read_77/DisableCopyOnReadDisableCopyOnRead,read_77_disablecopyonread_add_metric_6_count"/device:CPU:0*
_output_shapes
 �
Read_77/ReadVariableOpReadVariableOp,read_77_disablecopyonread_add_metric_6_count^Read_77/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0h
Identity_154IdentityRead_77/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: _
Identity_155IdentityIdentity_154:output:0"/device:CPU:0*
T0*
_output_shapes
: �
Read_78/DisableCopyOnReadDisableCopyOnRead,read_78_disablecopyonread_add_metric_7_total"/device:CPU:0*
_output_shapes
 �
Read_78/ReadVariableOpReadVariableOp,read_78_disablecopyonread_add_metric_7_total^Read_78/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0h
Identity_156IdentityRead_78/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: _
Identity_157IdentityIdentity_156:output:0"/device:CPU:0*
T0*
_output_shapes
: �
Read_79/DisableCopyOnReadDisableCopyOnRead,read_79_disablecopyonread_add_metric_7_count"/device:CPU:0*
_output_shapes
 �
Read_79/ReadVariableOpReadVariableOp,read_79_disablecopyonread_add_metric_7_count^Read_79/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0h
Identity_158IdentityRead_79/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: _
Identity_159IdentityIdentity_158:output:0"/device:CPU:0*
T0*
_output_shapes
: � 
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:Q*
dtype0*� 
value� B� QB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB'variables/12/.ATTRIBUTES/VARIABLE_VALUEB'variables/13/.ATTRIBUTES/VARIABLE_VALUEB'variables/14/.ATTRIBUTES/VARIABLE_VALUEB'variables/15/.ATTRIBUTES/VARIABLE_VALUEB'variables/16/.ATTRIBUTES/VARIABLE_VALUEB'variables/17/.ATTRIBUTES/VARIABLE_VALUEB'variables/18/.ATTRIBUTES/VARIABLE_VALUEB'variables/19/.ATTRIBUTES/VARIABLE_VALUEB'variables/20/.ATTRIBUTES/VARIABLE_VALUEB'variables/21/.ATTRIBUTES/VARIABLE_VALUEB'variables/22/.ATTRIBUTES/VARIABLE_VALUEB'variables/23/.ATTRIBUTES/VARIABLE_VALUEB0optimizer/_iterations/.ATTRIBUTES/VARIABLE_VALUEB3optimizer/_learning_rate/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/1/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/2/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/3/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/4/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/5/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/6/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/7/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/8/.ATTRIBUTES/VARIABLE_VALUEB1optimizer/_variables/9/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/10/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/11/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/12/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/13/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/14/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/15/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/16/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/17/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/18/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/19/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/20/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/21/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/22/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/23/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/24/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/25/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/26/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/27/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/28/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/29/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/30/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/31/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/32/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/33/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/34/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/35/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/36/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/37/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/38/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/39/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/40/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/41/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/42/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/43/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/44/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/45/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/46/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/47/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/_variables/48/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/2/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/2/count/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:Q*
dtype0*�
value�B�QB B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B �
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0Identity_1:output:0Identity_3:output:0Identity_5:output:0Identity_7:output:0Identity_9:output:0Identity_11:output:0Identity_13:output:0Identity_15:output:0Identity_17:output:0Identity_19:output:0Identity_21:output:0Identity_23:output:0Identity_25:output:0Identity_27:output:0Identity_29:output:0Identity_31:output:0Identity_33:output:0Identity_35:output:0Identity_37:output:0Identity_39:output:0Identity_41:output:0Identity_43:output:0Identity_45:output:0Identity_47:output:0Identity_49:output:0Identity_51:output:0Identity_53:output:0Identity_55:output:0Identity_57:output:0Identity_59:output:0Identity_61:output:0Identity_63:output:0Identity_65:output:0Identity_67:output:0Identity_69:output:0Identity_71:output:0Identity_73:output:0Identity_75:output:0Identity_77:output:0Identity_79:output:0Identity_81:output:0Identity_83:output:0Identity_85:output:0Identity_87:output:0Identity_89:output:0Identity_91:output:0Identity_93:output:0Identity_95:output:0Identity_97:output:0Identity_99:output:0Identity_101:output:0Identity_103:output:0Identity_105:output:0Identity_107:output:0Identity_109:output:0Identity_111:output:0Identity_113:output:0Identity_115:output:0Identity_117:output:0Identity_119:output:0Identity_121:output:0Identity_123:output:0Identity_125:output:0Identity_127:output:0Identity_129:output:0Identity_131:output:0Identity_133:output:0Identity_135:output:0Identity_137:output:0Identity_139:output:0Identity_141:output:0Identity_143:output:0Identity_145:output:0Identity_147:output:0Identity_149:output:0Identity_151:output:0Identity_153:output:0Identity_155:output:0Identity_157:output:0Identity_159:output:0savev2_const_4"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *_
dtypesU
S2Q	�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 j
Identity_160Identityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: W
Identity_161IdentityIdentity_160:output:0^NoOp*
T0*
_output_shapes
: �!
NoOpNoOp^MergeV2Checkpoints^Read/DisableCopyOnRead^Read/ReadVariableOp^Read_1/DisableCopyOnRead^Read_1/ReadVariableOp^Read_10/DisableCopyOnRead^Read_10/ReadVariableOp^Read_11/DisableCopyOnRead^Read_11/ReadVariableOp^Read_12/DisableCopyOnRead^Read_12/ReadVariableOp^Read_13/DisableCopyOnRead^Read_13/ReadVariableOp^Read_14/DisableCopyOnRead^Read_14/ReadVariableOp^Read_15/DisableCopyOnRead^Read_15/ReadVariableOp^Read_16/DisableCopyOnRead^Read_16/ReadVariableOp^Read_17/DisableCopyOnRead^Read_17/ReadVariableOp^Read_18/DisableCopyOnRead^Read_18/ReadVariableOp^Read_19/DisableCopyOnRead^Read_19/ReadVariableOp^Read_2/DisableCopyOnRead^Read_2/ReadVariableOp^Read_20/DisableCopyOnRead^Read_20/ReadVariableOp^Read_21/DisableCopyOnRead^Read_21/ReadVariableOp^Read_22/DisableCopyOnRead^Read_22/ReadVariableOp^Read_23/DisableCopyOnRead^Read_23/ReadVariableOp^Read_24/DisableCopyOnRead^Read_24/ReadVariableOp^Read_25/DisableCopyOnRead^Read_25/ReadVariableOp^Read_26/DisableCopyOnRead^Read_26/ReadVariableOp^Read_27/DisableCopyOnRead^Read_27/ReadVariableOp^Read_28/DisableCopyOnRead^Read_28/ReadVariableOp^Read_29/DisableCopyOnRead^Read_29/ReadVariableOp^Read_3/DisableCopyOnRead^Read_3/ReadVariableOp^Read_30/DisableCopyOnRead^Read_30/ReadVariableOp^Read_31/DisableCopyOnRead^Read_31/ReadVariableOp^Read_32/DisableCopyOnRead^Read_32/ReadVariableOp^Read_33/DisableCopyOnRead^Read_33/ReadVariableOp^Read_34/DisableCopyOnRead^Read_34/ReadVariableOp^Read_35/DisableCopyOnRead^Read_35/ReadVariableOp^Read_36/DisableCopyOnRead^Read_36/ReadVariableOp^Read_37/DisableCopyOnRead^Read_37/ReadVariableOp^Read_38/DisableCopyOnRead^Read_38/ReadVariableOp^Read_39/DisableCopyOnRead^Read_39/ReadVariableOp^Read_4/DisableCopyOnRead^Read_4/ReadVariableOp^Read_40/DisableCopyOnRead^Read_40/ReadVariableOp^Read_41/DisableCopyOnRead^Read_41/ReadVariableOp^Read_42/DisableCopyOnRead^Read_42/ReadVariableOp^Read_43/DisableCopyOnRead^Read_43/ReadVariableOp^Read_44/DisableCopyOnRead^Read_44/ReadVariableOp^Read_45/DisableCopyOnRead^Read_45/ReadVariableOp^Read_46/DisableCopyOnRead^Read_46/ReadVariableOp^Read_47/DisableCopyOnRead^Read_47/ReadVariableOp^Read_48/DisableCopyOnRead^Read_48/ReadVariableOp^Read_49/DisableCopyOnRead^Read_49/ReadVariableOp^Read_5/DisableCopyOnRead^Read_5/ReadVariableOp^Read_50/DisableCopyOnRead^Read_50/ReadVariableOp^Read_51/DisableCopyOnRead^Read_51/ReadVariableOp^Read_52/DisableCopyOnRead^Read_52/ReadVariableOp^Read_53/DisableCopyOnRead^Read_53/ReadVariableOp^Read_54/DisableCopyOnRead^Read_54/ReadVariableOp^Read_55/DisableCopyOnRead^Read_55/ReadVariableOp^Read_56/DisableCopyOnRead^Read_56/ReadVariableOp^Read_57/DisableCopyOnRead^Read_57/ReadVariableOp^Read_58/DisableCopyOnRead^Read_58/ReadVariableOp^Read_59/DisableCopyOnRead^Read_59/ReadVariableOp^Read_6/DisableCopyOnRead^Read_6/ReadVariableOp^Read_60/DisableCopyOnRead^Read_60/ReadVariableOp^Read_61/DisableCopyOnRead^Read_61/ReadVariableOp^Read_62/DisableCopyOnRead^Read_62/ReadVariableOp^Read_63/DisableCopyOnRead^Read_63/ReadVariableOp^Read_64/DisableCopyOnRead^Read_64/ReadVariableOp^Read_65/DisableCopyOnRead^Read_65/ReadVariableOp^Read_66/DisableCopyOnRead^Read_66/ReadVariableOp^Read_67/DisableCopyOnRead^Read_67/ReadVariableOp^Read_68/DisableCopyOnRead^Read_68/ReadVariableOp^Read_69/DisableCopyOnRead^Read_69/ReadVariableOp^Read_7/DisableCopyOnRead^Read_7/ReadVariableOp^Read_70/DisableCopyOnRead^Read_70/ReadVariableOp^Read_71/DisableCopyOnRead^Read_71/ReadVariableOp^Read_72/DisableCopyOnRead^Read_72/ReadVariableOp^Read_73/DisableCopyOnRead^Read_73/ReadVariableOp^Read_74/DisableCopyOnRead^Read_74/ReadVariableOp^Read_75/DisableCopyOnRead^Read_75/ReadVariableOp^Read_76/DisableCopyOnRead^Read_76/ReadVariableOp^Read_77/DisableCopyOnRead^Read_77/ReadVariableOp^Read_78/DisableCopyOnRead^Read_78/ReadVariableOp^Read_79/DisableCopyOnRead^Read_79/ReadVariableOp^Read_8/DisableCopyOnRead^Read_8/ReadVariableOp^Read_9/DisableCopyOnRead^Read_9/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "%
identity_161Identity_161:output:0*�
_input_shapes�
�: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2(
MergeV2CheckpointsMergeV2Checkpoints20
Read/DisableCopyOnReadRead/DisableCopyOnRead2*
Read/ReadVariableOpRead/ReadVariableOp24
Read_1/DisableCopyOnReadRead_1/DisableCopyOnRead2.
Read_1/ReadVariableOpRead_1/ReadVariableOp26
Read_10/DisableCopyOnReadRead_10/DisableCopyOnRead20
Read_10/ReadVariableOpRead_10/ReadVariableOp26
Read_11/DisableCopyOnReadRead_11/DisableCopyOnRead20
Read_11/ReadVariableOpRead_11/ReadVariableOp26
Read_12/DisableCopyOnReadRead_12/DisableCopyOnRead20
Read_12/ReadVariableOpRead_12/ReadVariableOp26
Read_13/DisableCopyOnReadRead_13/DisableCopyOnRead20
Read_13/ReadVariableOpRead_13/ReadVariableOp26
Read_14/DisableCopyOnReadRead_14/DisableCopyOnRead20
Read_14/ReadVariableOpRead_14/ReadVariableOp26
Read_15/DisableCopyOnReadRead_15/DisableCopyOnRead20
Read_15/ReadVariableOpRead_15/ReadVariableOp26
Read_16/DisableCopyOnReadRead_16/DisableCopyOnRead20
Read_16/ReadVariableOpRead_16/ReadVariableOp26
Read_17/DisableCopyOnReadRead_17/DisableCopyOnRead20
Read_17/ReadVariableOpRead_17/ReadVariableOp26
Read_18/DisableCopyOnReadRead_18/DisableCopyOnRead20
Read_18/ReadVariableOpRead_18/ReadVariableOp26
Read_19/DisableCopyOnReadRead_19/DisableCopyOnRead20
Read_19/ReadVariableOpRead_19/ReadVariableOp24
Read_2/DisableCopyOnReadRead_2/DisableCopyOnRead2.
Read_2/ReadVariableOpRead_2/ReadVariableOp26
Read_20/DisableCopyOnReadRead_20/DisableCopyOnRead20
Read_20/ReadVariableOpRead_20/ReadVariableOp26
Read_21/DisableCopyOnReadRead_21/DisableCopyOnRead20
Read_21/ReadVariableOpRead_21/ReadVariableOp26
Read_22/DisableCopyOnReadRead_22/DisableCopyOnRead20
Read_22/ReadVariableOpRead_22/ReadVariableOp26
Read_23/DisableCopyOnReadRead_23/DisableCopyOnRead20
Read_23/ReadVariableOpRead_23/ReadVariableOp26
Read_24/DisableCopyOnReadRead_24/DisableCopyOnRead20
Read_24/ReadVariableOpRead_24/ReadVariableOp26
Read_25/DisableCopyOnReadRead_25/DisableCopyOnRead20
Read_25/ReadVariableOpRead_25/ReadVariableOp26
Read_26/DisableCopyOnReadRead_26/DisableCopyOnRead20
Read_26/ReadVariableOpRead_26/ReadVariableOp26
Read_27/DisableCopyOnReadRead_27/DisableCopyOnRead20
Read_27/ReadVariableOpRead_27/ReadVariableOp26
Read_28/DisableCopyOnReadRead_28/DisableCopyOnRead20
Read_28/ReadVariableOpRead_28/ReadVariableOp26
Read_29/DisableCopyOnReadRead_29/DisableCopyOnRead20
Read_29/ReadVariableOpRead_29/ReadVariableOp24
Read_3/DisableCopyOnReadRead_3/DisableCopyOnRead2.
Read_3/ReadVariableOpRead_3/ReadVariableOp26
Read_30/DisableCopyOnReadRead_30/DisableCopyOnRead20
Read_30/ReadVariableOpRead_30/ReadVariableOp26
Read_31/DisableCopyOnReadRead_31/DisableCopyOnRead20
Read_31/ReadVariableOpRead_31/ReadVariableOp26
Read_32/DisableCopyOnReadRead_32/DisableCopyOnRead20
Read_32/ReadVariableOpRead_32/ReadVariableOp26
Read_33/DisableCopyOnReadRead_33/DisableCopyOnRead20
Read_33/ReadVariableOpRead_33/ReadVariableOp26
Read_34/DisableCopyOnReadRead_34/DisableCopyOnRead20
Read_34/ReadVariableOpRead_34/ReadVariableOp26
Read_35/DisableCopyOnReadRead_35/DisableCopyOnRead20
Read_35/ReadVariableOpRead_35/ReadVariableOp26
Read_36/DisableCopyOnReadRead_36/DisableCopyOnRead20
Read_36/ReadVariableOpRead_36/ReadVariableOp26
Read_37/DisableCopyOnReadRead_37/DisableCopyOnRead20
Read_37/ReadVariableOpRead_37/ReadVariableOp26
Read_38/DisableCopyOnReadRead_38/DisableCopyOnRead20
Read_38/ReadVariableOpRead_38/ReadVariableOp26
Read_39/DisableCopyOnReadRead_39/DisableCopyOnRead20
Read_39/ReadVariableOpRead_39/ReadVariableOp24
Read_4/DisableCopyOnReadRead_4/DisableCopyOnRead2.
Read_4/ReadVariableOpRead_4/ReadVariableOp26
Read_40/DisableCopyOnReadRead_40/DisableCopyOnRead20
Read_40/ReadVariableOpRead_40/ReadVariableOp26
Read_41/DisableCopyOnReadRead_41/DisableCopyOnRead20
Read_41/ReadVariableOpRead_41/ReadVariableOp26
Read_42/DisableCopyOnReadRead_42/DisableCopyOnRead20
Read_42/ReadVariableOpRead_42/ReadVariableOp26
Read_43/DisableCopyOnReadRead_43/DisableCopyOnRead20
Read_43/ReadVariableOpRead_43/ReadVariableOp26
Read_44/DisableCopyOnReadRead_44/DisableCopyOnRead20
Read_44/ReadVariableOpRead_44/ReadVariableOp26
Read_45/DisableCopyOnReadRead_45/DisableCopyOnRead20
Read_45/ReadVariableOpRead_45/ReadVariableOp26
Read_46/DisableCopyOnReadRead_46/DisableCopyOnRead20
Read_46/ReadVariableOpRead_46/ReadVariableOp26
Read_47/DisableCopyOnReadRead_47/DisableCopyOnRead20
Read_47/ReadVariableOpRead_47/ReadVariableOp26
Read_48/DisableCopyOnReadRead_48/DisableCopyOnRead20
Read_48/ReadVariableOpRead_48/ReadVariableOp26
Read_49/DisableCopyOnReadRead_49/DisableCopyOnRead20
Read_49/ReadVariableOpRead_49/ReadVariableOp24
Read_5/DisableCopyOnReadRead_5/DisableCopyOnRead2.
Read_5/ReadVariableOpRead_5/ReadVariableOp26
Read_50/DisableCopyOnReadRead_50/DisableCopyOnRead20
Read_50/ReadVariableOpRead_50/ReadVariableOp26
Read_51/DisableCopyOnReadRead_51/DisableCopyOnRead20
Read_51/ReadVariableOpRead_51/ReadVariableOp26
Read_52/DisableCopyOnReadRead_52/DisableCopyOnRead20
Read_52/ReadVariableOpRead_52/ReadVariableOp26
Read_53/DisableCopyOnReadRead_53/DisableCopyOnRead20
Read_53/ReadVariableOpRead_53/ReadVariableOp26
Read_54/DisableCopyOnReadRead_54/DisableCopyOnRead20
Read_54/ReadVariableOpRead_54/ReadVariableOp26
Read_55/DisableCopyOnReadRead_55/DisableCopyOnRead20
Read_55/ReadVariableOpRead_55/ReadVariableOp26
Read_56/DisableCopyOnReadRead_56/DisableCopyOnRead20
Read_56/ReadVariableOpRead_56/ReadVariableOp26
Read_57/DisableCopyOnReadRead_57/DisableCopyOnRead20
Read_57/ReadVariableOpRead_57/ReadVariableOp26
Read_58/DisableCopyOnReadRead_58/DisableCopyOnRead20
Read_58/ReadVariableOpRead_58/ReadVariableOp26
Read_59/DisableCopyOnReadRead_59/DisableCopyOnRead20
Read_59/ReadVariableOpRead_59/ReadVariableOp24
Read_6/DisableCopyOnReadRead_6/DisableCopyOnRead2.
Read_6/ReadVariableOpRead_6/ReadVariableOp26
Read_60/DisableCopyOnReadRead_60/DisableCopyOnRead20
Read_60/ReadVariableOpRead_60/ReadVariableOp26
Read_61/DisableCopyOnReadRead_61/DisableCopyOnRead20
Read_61/ReadVariableOpRead_61/ReadVariableOp26
Read_62/DisableCopyOnReadRead_62/DisableCopyOnRead20
Read_62/ReadVariableOpRead_62/ReadVariableOp26
Read_63/DisableCopyOnReadRead_63/DisableCopyOnRead20
Read_63/ReadVariableOpRead_63/ReadVariableOp26
Read_64/DisableCopyOnReadRead_64/DisableCopyOnRead20
Read_64/ReadVariableOpRead_64/ReadVariableOp26
Read_65/DisableCopyOnReadRead_65/DisableCopyOnRead20
Read_65/ReadVariableOpRead_65/ReadVariableOp26
Read_66/DisableCopyOnReadRead_66/DisableCopyOnRead20
Read_66/ReadVariableOpRead_66/ReadVariableOp26
Read_67/DisableCopyOnReadRead_67/DisableCopyOnRead20
Read_67/ReadVariableOpRead_67/ReadVariableOp26
Read_68/DisableCopyOnReadRead_68/DisableCopyOnRead20
Read_68/ReadVariableOpRead_68/ReadVariableOp26
Read_69/DisableCopyOnReadRead_69/DisableCopyOnRead20
Read_69/ReadVariableOpRead_69/ReadVariableOp24
Read_7/DisableCopyOnReadRead_7/DisableCopyOnRead2.
Read_7/ReadVariableOpRead_7/ReadVariableOp26
Read_70/DisableCopyOnReadRead_70/DisableCopyOnRead20
Read_70/ReadVariableOpRead_70/ReadVariableOp26
Read_71/DisableCopyOnReadRead_71/DisableCopyOnRead20
Read_71/ReadVariableOpRead_71/ReadVariableOp26
Read_72/DisableCopyOnReadRead_72/DisableCopyOnRead20
Read_72/ReadVariableOpRead_72/ReadVariableOp26
Read_73/DisableCopyOnReadRead_73/DisableCopyOnRead20
Read_73/ReadVariableOpRead_73/ReadVariableOp26
Read_74/DisableCopyOnReadRead_74/DisableCopyOnRead20
Read_74/ReadVariableOpRead_74/ReadVariableOp26
Read_75/DisableCopyOnReadRead_75/DisableCopyOnRead20
Read_75/ReadVariableOpRead_75/ReadVariableOp26
Read_76/DisableCopyOnReadRead_76/DisableCopyOnRead20
Read_76/ReadVariableOpRead_76/ReadVariableOp26
Read_77/DisableCopyOnReadRead_77/DisableCopyOnRead20
Read_77/ReadVariableOpRead_77/ReadVariableOp26
Read_78/DisableCopyOnReadRead_78/DisableCopyOnRead20
Read_78/ReadVariableOpRead_78/ReadVariableOp26
Read_79/DisableCopyOnReadRead_79/DisableCopyOnRead20
Read_79/ReadVariableOpRead_79/ReadVariableOp24
Read_8/DisableCopyOnReadRead_8/DisableCopyOnRead2.
Read_8/ReadVariableOpRead_8/ReadVariableOp24
Read_9/DisableCopyOnReadRead_9/DisableCopyOnRead2.
Read_9/ReadVariableOpRead_9/ReadVariableOp:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:Q

_output_shapes
: 
�
�
)__inference_dense_11_layer_call_fn_248313

inputs
unknown:
��
	unknown_0:
��
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 * 
_output_shapes
:
��*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dense_11_layer_call_and_return_conditional_losses_245614h
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0* 
_output_shapes
:
��`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*!
_input_shapes
:: : 22
StatefulPartitionedCallStatefulPartitionedCall:F B

_output_shapes

:
 
_user_specified_nameinputs
�!
�
O__inference_conv2d_transpose_14_layer_call_and_return_conditional_losses_248472

inputsB
(conv2d_transpose_readvariableop_resource: @-
biasadd_readvariableop_resource: 
identity��BiasAdd/ReadVariableOp�conv2d_transpose/ReadVariableOpI
ShapeShapeinputs*
T0*
_output_shapes
::��]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask_
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_2StridedSliceShape:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskG
mul/yConst*
_output_shapes
: *
dtype0*
value	B :U
mulMulstrided_slice_1:output:0mul/y:output:0*
T0*
_output_shapes
: I
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :Y
mul_1Mulstrided_slice_2:output:0mul_1/y:output:0*
T0*
_output_shapes
: I
stack/3Const*
_output_shapes
: *
dtype0*
value	B : y
stackPackstrided_slice:output:0mul:z:0	mul_1:z:0stack/3:output:0*
N*
T0*
_output_shapes
:_
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_3StridedSlicestack:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
conv2d_transpose/ReadVariableOpReadVariableOp(conv2d_transpose_readvariableop_resource*&
_output_shapes
: @*
dtype0�
conv2d_transposeConv2DBackpropInputstack:output:0'conv2d_transpose/ReadVariableOp:value:0inputs*
T0*A
_output_shapes/
-:+��������������������������� *
paddingSAME*
strides
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
BiasAddBiasAddconv2d_transpose:output:0BiasAdd/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+��������������������������� j
ReluReluBiasAdd:output:0*
T0*A
_output_shapes/
-:+��������������������������� {
IdentityIdentityRelu:activations:0^NoOp*
T0*A
_output_shapes/
-:+��������������������������� �
NoOpNoOp^BiasAdd/ReadVariableOp ^conv2d_transpose/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*D
_input_shapes3
1:+���������������������������@: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2B
conv2d_transpose/ReadVariableOpconv2d_transpose/ReadVariableOp:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs
�
q
D__inference_lambda_3_layer_call_and_return_conditional_losses_245066

inputs
inputs_1
identity�V
ShapeConst*
_output_shapes
:*
dtype0*
valueB"      ]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskW
random_normal/shape/1Const*
_output_shapes
: *
dtype0*
value	B :�
random_normal/shapePackstrided_slice:output:0random_normal/shape/1:output:0*
N*
T0*
_output_shapes
:W
random_normal/meanConst*
_output_shapes
: *
dtype0*
valueB
 *    Y
random_normal/stddevConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
"random_normal/RandomStandardNormalRandomStandardNormalrandom_normal/shape:output:0*
T0*
_output_shapes

:*
dtype0*
seed���)*
seed2��4�
random_normal/mulMul+random_normal/RandomStandardNormal:output:0random_normal/stddev:output:0*
T0*
_output_shapes

:s
random_normalAddV2random_normal/mul:z:0random_normal/mean:output:0*
T0*
_output_shapes

:N
	truediv/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @Y
truedivRealDivinputs_1truediv/y:output:0*
T0*
_output_shapes

:@
ExpExptruediv:z:0*
T0*
_output_shapes

:O
mulMulExp:y:0random_normal:z:0*
T0*
_output_shapes

:F
addAddV2inputsmul:z:0*
T0*
_output_shapes

:F
IdentityIdentityadd:z:0*
T0*
_output_shapes

:"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:::F B

_output_shapes

:
 
_user_specified_nameinputs:FB

_output_shapes

:
 
_user_specified_nameinputs
�
Q
#__inference__update_step_xla_247511
gradient
variable:
��*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*!
_input_shapes
:
��: *
	_noinline(:J F
 
_output_shapes
:
��
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�	
�
D__inference_dense_10_layer_call_and_return_conditional_losses_245038

inputs2
matmul_readvariableop_resource:
��-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpv
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0`
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0m
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:V
IdentityIdentityBiasAdd:output:0^NoOp*
T0*
_output_shapes

:w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*#
_input_shapes
:
��: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:H D
 
_output_shapes
:
��
 
_user_specified_nameinputs
�

�
E__inference_conv2d_15_layer_call_and_return_conditional_losses_248076

inputs8
conv2d_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp|
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0t
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<O
ReluReluBiasAdd:output:0*
T0*&
_output_shapes
:<<`
IdentityIdentityRelu:activations:0^NoOp*
T0*&
_output_shapes
:<<w
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:<<: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs
�V
�
?__inference_vae_layer_call_and_return_conditional_losses_246322

inputs(
encoder_246207:
encoder_246209:(
encoder_246211: 
encoder_246213: )
encoder_246215: �
encoder_246217:	�*
encoder_246219:��
encoder_246221:	�"
encoder_246223:
��
encoder_246225:"
encoder_246227:
��
encoder_246229:"
decoder_246234:
��
decoder_246236:
��*
decoder_246238:��
decoder_246240:	�)
decoder_246242:@�
decoder_246244:@(
decoder_246246: @
decoder_246248: (
decoder_246250: 
decoder_246252:(
decoder_246254:
decoder_246256:
unknown
add_metric_7_246290: 
add_metric_7_246292: 
	unknown_0
	unknown_1
	unknown_2
add_metric_6_246315: 
add_metric_6_246317: 
identity

identity_1��$add_metric_6/StatefulPartitionedCall�$add_metric_7/StatefulPartitionedCall�!conv2d_15/StatefulPartitionedCall�!conv2d_16/StatefulPartitionedCall�!conv2d_17/StatefulPartitionedCall�!conv2d_18/StatefulPartitionedCall�decoder/StatefulPartitionedCall� dense_10/StatefulPartitionedCall�dense_9/StatefulPartitionedCall�encoder/StatefulPartitionedCall�
encoder/StatefulPartitionedCallStatefulPartitionedCallinputsencoder_246207encoder_246209encoder_246211encoder_246213encoder_246215encoder_246217encoder_246219encoder_246221encoder_246223encoder_246225encoder_246227encoder_246229*
Tin
2*
Tout
2*
_collective_manager_ids
 *2
_output_shapes 
:::*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_encoder_layer_call_and_return_conditional_losses_245172�
decoder/StatefulPartitionedCallStatefulPartitionedCall(encoder/StatefulPartitionedCall:output:2decoder_246234decoder_246236decoder_246238decoder_246240decoder_246242decoder_246244decoder_246246decoder_246248decoder_246250decoder_246252decoder_246254decoder_246256*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_decoder_layer_call_and_return_conditional_losses_245747m
tf.reshape_6/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB:
���������s
tf.reshape_6/ReshapeReshapeinputs#tf.reshape_6/Reshape/shape:output:0*
T0*
_output_shapes

:��m
tf.reshape_7/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB:
����������
tf.reshape_7/ReshapeReshape(decoder/StatefulPartitionedCall:output:0#tf.reshape_7/Reshape/shape:output:0*
T0*
_output_shapes

:���
!conv2d_15/StatefulPartitionedCallStatefulPartitionedCallinputsencoder_246207encoder_246209*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_15_layer_call_and_return_conditional_losses_244947�
!conv2d_16/StatefulPartitionedCallStatefulPartitionedCall*conv2d_15/StatefulPartitionedCall:output:0encoder_246211encoder_246213*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
: *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_16_layer_call_and_return_conditional_losses_244964�
.tf.math.squared_difference_3/SquaredDifferenceSquaredDifferencetf.reshape_7/Reshape:output:0tf.reshape_6/Reshape:output:0*
T0*
_output_shapes

:���
!conv2d_17/StatefulPartitionedCallStatefulPartitionedCall*conv2d_16/StatefulPartitionedCall:output:0encoder_246215encoder_246217*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_17_layer_call_and_return_conditional_losses_244981w
,tf.math.reduce_mean_6/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
����������
tf.math.reduce_mean_6/MeanMean2tf.math.squared_difference_3/SquaredDifference:z:05tf.math.reduce_mean_6/Mean/reduction_indices:output:0*
T0*
_output_shapes
: �
!conv2d_18/StatefulPartitionedCallStatefulPartitionedCall*conv2d_17/StatefulPartitionedCall:output:0encoder_246219encoder_246221*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_18_layer_call_and_return_conditional_losses_244998^
tf.math.multiply_12/Mul/yConst*
_output_shapes
: *
dtype0*
valueB
 * �(F�
tf.math.multiply_12/MulMul#tf.math.reduce_mean_6/Mean:output:0"tf.math.multiply_12/Mul/y:output:0*
T0*
_output_shapes
: �
flatten_3/PartitionedCallPartitionedCall*conv2d_18/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 * 
_output_shapes
:
��* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_flatten_3_layer_call_and_return_conditional_losses_245010e
tf.math.multiply_14/MulMulunknowntf.math.multiply_12/Mul:z:0*
T0*
_output_shapes
: �
 dense_10/StatefulPartitionedCallStatefulPartitionedCall"flatten_3/PartitionedCall:output:0encoder_246227encoder_246229*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dense_10_layer_call_and_return_conditional_losses_245038�
dense_9/StatefulPartitionedCallStatefulPartitionedCall"flatten_3/PartitionedCall:output:0encoder_246223encoder_246225*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_dense_9_layer_call_and_return_conditional_losses_245022�
$add_metric_7/StatefulPartitionedCallStatefulPartitionedCalltf.math.multiply_12/Mul:z:0add_metric_7_246290add_metric_7_246292*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_add_metric_7_layer_call_and_return_conditional_losses_246026�
tf.__operators__.add_6/AddV2AddV2	unknown_0)dense_10/StatefulPartitionedCall:output:0*
T0*
_output_shapes

:t
tf.math.square_3/SquareSquare(dense_9/StatefulPartitionedCall:output:0*
T0*
_output_shapes

:l
tf.math.exp_3/ExpExp)dense_10/StatefulPartitionedCall:output:0*
T0*
_output_shapes

:�
tf.math.subtract_6/SubSub tf.__operators__.add_6/AddV2:z:0tf.math.square_3/Square:y:0*
T0*
_output_shapes

:y
tf.math.subtract_7/SubSubtf.math.subtract_6/Sub:z:0tf.math.exp_3/Exp:y:0*
T0*
_output_shapes

:l
*tf.math.reduce_sum_3/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
tf.math.reduce_sum_3/SumSumtf.math.subtract_7/Sub:z:03tf.math.reduce_sum_3/Sum/reduction_indices:output:0*
T0*
_output_shapes
:q
tf.math.multiply_13/MulMul	unknown_1!tf.math.reduce_sum_3/Sum:output:0*
T0*
_output_shapes
:k
tf.math.multiply_15/MulMul	unknown_2tf.math.multiply_13/Mul:z:0*
T0*
_output_shapes
:�
tf.__operators__.add_7/AddV2AddV2tf.math.multiply_14/Mul:z:0tf.math.multiply_15/Mul:z:0*
T0*
_output_shapes
:e
tf.math.reduce_mean_7/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
tf.math.reduce_mean_7/MeanMean tf.__operators__.add_7/AddV2:z:0$tf.math.reduce_mean_7/Const:output:0*
T0*
_output_shapes
: �
add_loss_3/PartitionedCallPartitionedCall#tf.math.reduce_mean_7/Mean:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: : * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_add_loss_3_layer_call_and_return_conditional_losses_246055�
$add_metric_6/StatefulPartitionedCallStatefulPartitionedCall#tf.math.reduce_mean_7/Mean:output:0add_metric_6_246315add_metric_6_246317*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_add_metric_6_layer_call_and_return_conditional_losses_246075v
IdentityIdentity(decoder/StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<c

Identity_1Identity#add_loss_3/PartitionedCall:output:1^NoOp*
T0*
_output_shapes
: �
NoOpNoOp%^add_metric_6/StatefulPartitionedCall%^add_metric_7/StatefulPartitionedCall"^conv2d_15/StatefulPartitionedCall"^conv2d_16/StatefulPartitionedCall"^conv2d_17/StatefulPartitionedCall"^conv2d_18/StatefulPartitionedCall ^decoder/StatefulPartitionedCall!^dense_10/StatefulPartitionedCall ^dense_9/StatefulPartitionedCall ^encoder/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*e
_input_shapesT
R:<<: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2L
$add_metric_6/StatefulPartitionedCall$add_metric_6/StatefulPartitionedCall2L
$add_metric_7/StatefulPartitionedCall$add_metric_7/StatefulPartitionedCall2F
!conv2d_15/StatefulPartitionedCall!conv2d_15/StatefulPartitionedCall2F
!conv2d_16/StatefulPartitionedCall!conv2d_16/StatefulPartitionedCall2F
!conv2d_17/StatefulPartitionedCall!conv2d_17/StatefulPartitionedCall2F
!conv2d_18/StatefulPartitionedCall!conv2d_18/StatefulPartitionedCall2B
decoder/StatefulPartitionedCalldecoder/StatefulPartitionedCall2D
 dense_10/StatefulPartitionedCall dense_10/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2B
encoder/StatefulPartitionedCallencoder/StatefulPartitionedCall:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
*__inference_conv2d_15_layer_call_fn_248065

inputs!
unknown:
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_15_layer_call_and_return_conditional_losses_244947n
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:<<: : 22
StatefulPartitionedCallStatefulPartitionedCall:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs
�
�
(__inference_decoder_layer_call_fn_245838
input_8
unknown:
��
	unknown_0:
��%
	unknown_1:��
	unknown_2:	�$
	unknown_3:@�
	unknown_4:@#
	unknown_5: @
	unknown_6: #
	unknown_7: 
	unknown_8:#
	unknown_9:

unknown_10:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_8unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_decoder_layer_call_and_return_conditional_losses_245811n
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":: : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:G C

_output_shapes

:
!
_user_specified_name	input_8
�V
�
?__inference_vae_layer_call_and_return_conditional_losses_246083
input_7(
encoder_245926:
encoder_245928:(
encoder_245930: 
encoder_245932: )
encoder_245934: �
encoder_245936:	�*
encoder_245938:��
encoder_245940:	�"
encoder_245942:
��
encoder_245944:"
encoder_245946:
��
encoder_245948:"
decoder_245953:
��
decoder_245955:
��*
decoder_245957:��
decoder_245959:	�)
decoder_245961:@�
decoder_245963:@(
decoder_245965: @
decoder_245967: (
decoder_245969: 
decoder_245971:(
decoder_245973:
decoder_245975:
unknown
add_metric_7_246027: 
add_metric_7_246029: 
	unknown_0
	unknown_1
	unknown_2
add_metric_6_246076: 
add_metric_6_246078: 
identity

identity_1��$add_metric_6/StatefulPartitionedCall�$add_metric_7/StatefulPartitionedCall�!conv2d_15/StatefulPartitionedCall�!conv2d_16/StatefulPartitionedCall�!conv2d_17/StatefulPartitionedCall�!conv2d_18/StatefulPartitionedCall�decoder/StatefulPartitionedCall� dense_10/StatefulPartitionedCall�dense_9/StatefulPartitionedCall�encoder/StatefulPartitionedCall�
encoder/StatefulPartitionedCallStatefulPartitionedCallinput_7encoder_245926encoder_245928encoder_245930encoder_245932encoder_245934encoder_245936encoder_245938encoder_245940encoder_245942encoder_245944encoder_245946encoder_245948*
Tin
2*
Tout
2*
_collective_manager_ids
 *2
_output_shapes 
:::*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_encoder_layer_call_and_return_conditional_losses_245172�
decoder/StatefulPartitionedCallStatefulPartitionedCall(encoder/StatefulPartitionedCall:output:2decoder_245953decoder_245955decoder_245957decoder_245959decoder_245961decoder_245963decoder_245965decoder_245967decoder_245969decoder_245971decoder_245973decoder_245975*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_decoder_layer_call_and_return_conditional_losses_245747m
tf.reshape_6/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB:
���������t
tf.reshape_6/ReshapeReshapeinput_7#tf.reshape_6/Reshape/shape:output:0*
T0*
_output_shapes

:��m
tf.reshape_7/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB:
����������
tf.reshape_7/ReshapeReshape(decoder/StatefulPartitionedCall:output:0#tf.reshape_7/Reshape/shape:output:0*
T0*
_output_shapes

:���
!conv2d_15/StatefulPartitionedCallStatefulPartitionedCallinput_7encoder_245926encoder_245928*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_15_layer_call_and_return_conditional_losses_244947�
!conv2d_16/StatefulPartitionedCallStatefulPartitionedCall*conv2d_15/StatefulPartitionedCall:output:0encoder_245930encoder_245932*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
: *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_16_layer_call_and_return_conditional_losses_244964�
.tf.math.squared_difference_3/SquaredDifferenceSquaredDifferencetf.reshape_7/Reshape:output:0tf.reshape_6/Reshape:output:0*
T0*
_output_shapes

:���
!conv2d_17/StatefulPartitionedCallStatefulPartitionedCall*conv2d_16/StatefulPartitionedCall:output:0encoder_245934encoder_245936*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_17_layer_call_and_return_conditional_losses_244981w
,tf.math.reduce_mean_6/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
����������
tf.math.reduce_mean_6/MeanMean2tf.math.squared_difference_3/SquaredDifference:z:05tf.math.reduce_mean_6/Mean/reduction_indices:output:0*
T0*
_output_shapes
: �
!conv2d_18/StatefulPartitionedCallStatefulPartitionedCall*conv2d_17/StatefulPartitionedCall:output:0encoder_245938encoder_245940*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_18_layer_call_and_return_conditional_losses_244998^
tf.math.multiply_12/Mul/yConst*
_output_shapes
: *
dtype0*
valueB
 * �(F�
tf.math.multiply_12/MulMul#tf.math.reduce_mean_6/Mean:output:0"tf.math.multiply_12/Mul/y:output:0*
T0*
_output_shapes
: �
flatten_3/PartitionedCallPartitionedCall*conv2d_18/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 * 
_output_shapes
:
��* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_flatten_3_layer_call_and_return_conditional_losses_245010e
tf.math.multiply_14/MulMulunknowntf.math.multiply_12/Mul:z:0*
T0*
_output_shapes
: �
 dense_10/StatefulPartitionedCallStatefulPartitionedCall"flatten_3/PartitionedCall:output:0encoder_245946encoder_245948*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dense_10_layer_call_and_return_conditional_losses_245038�
dense_9/StatefulPartitionedCallStatefulPartitionedCall"flatten_3/PartitionedCall:output:0encoder_245942encoder_245944*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_dense_9_layer_call_and_return_conditional_losses_245022�
$add_metric_7/StatefulPartitionedCallStatefulPartitionedCalltf.math.multiply_12/Mul:z:0add_metric_7_246027add_metric_7_246029*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_add_metric_7_layer_call_and_return_conditional_losses_246026�
tf.__operators__.add_6/AddV2AddV2	unknown_0)dense_10/StatefulPartitionedCall:output:0*
T0*
_output_shapes

:t
tf.math.square_3/SquareSquare(dense_9/StatefulPartitionedCall:output:0*
T0*
_output_shapes

:l
tf.math.exp_3/ExpExp)dense_10/StatefulPartitionedCall:output:0*
T0*
_output_shapes

:�
tf.math.subtract_6/SubSub tf.__operators__.add_6/AddV2:z:0tf.math.square_3/Square:y:0*
T0*
_output_shapes

:y
tf.math.subtract_7/SubSubtf.math.subtract_6/Sub:z:0tf.math.exp_3/Exp:y:0*
T0*
_output_shapes

:l
*tf.math.reduce_sum_3/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
tf.math.reduce_sum_3/SumSumtf.math.subtract_7/Sub:z:03tf.math.reduce_sum_3/Sum/reduction_indices:output:0*
T0*
_output_shapes
:q
tf.math.multiply_13/MulMul	unknown_1!tf.math.reduce_sum_3/Sum:output:0*
T0*
_output_shapes
:k
tf.math.multiply_15/MulMul	unknown_2tf.math.multiply_13/Mul:z:0*
T0*
_output_shapes
:�
tf.__operators__.add_7/AddV2AddV2tf.math.multiply_14/Mul:z:0tf.math.multiply_15/Mul:z:0*
T0*
_output_shapes
:e
tf.math.reduce_mean_7/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
tf.math.reduce_mean_7/MeanMean tf.__operators__.add_7/AddV2:z:0$tf.math.reduce_mean_7/Const:output:0*
T0*
_output_shapes
: �
add_loss_3/PartitionedCallPartitionedCall#tf.math.reduce_mean_7/Mean:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: : * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_add_loss_3_layer_call_and_return_conditional_losses_246055�
$add_metric_6/StatefulPartitionedCallStatefulPartitionedCall#tf.math.reduce_mean_7/Mean:output:0add_metric_6_246076add_metric_6_246078*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_add_metric_6_layer_call_and_return_conditional_losses_246075v
IdentityIdentity(decoder/StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<c

Identity_1Identity#add_loss_3/PartitionedCall:output:1^NoOp*
T0*
_output_shapes
: �
NoOpNoOp%^add_metric_6/StatefulPartitionedCall%^add_metric_7/StatefulPartitionedCall"^conv2d_15/StatefulPartitionedCall"^conv2d_16/StatefulPartitionedCall"^conv2d_17/StatefulPartitionedCall"^conv2d_18/StatefulPartitionedCall ^decoder/StatefulPartitionedCall!^dense_10/StatefulPartitionedCall ^dense_9/StatefulPartitionedCall ^encoder/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*e
_input_shapesT
R:<<: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2L
$add_metric_6/StatefulPartitionedCall$add_metric_6/StatefulPartitionedCall2L
$add_metric_7/StatefulPartitionedCall$add_metric_7/StatefulPartitionedCall2F
!conv2d_15/StatefulPartitionedCall!conv2d_15/StatefulPartitionedCall2F
!conv2d_16/StatefulPartitionedCall!conv2d_16/StatefulPartitionedCall2F
!conv2d_17/StatefulPartitionedCall!conv2d_17/StatefulPartitionedCall2F
!conv2d_18/StatefulPartitionedCall!conv2d_18/StatefulPartitionedCall2B
decoder/StatefulPartitionedCalldecoder/StatefulPartitionedCall2D
 dense_10/StatefulPartitionedCall dense_10/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2B
encoder/StatefulPartitionedCallencoder/StatefulPartitionedCall:O K
&
_output_shapes
:<<
!
_user_specified_name	input_7:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
4__inference_conv2d_transpose_12_layer_call_fn_248352

inputs#
unknown:��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *B
_output_shapes0
.:,����������������������������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_12_layer_call_and_return_conditional_losses_245454�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*B
_output_shapes0
.:,����������������������������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*E
_input_shapes4
2:,����������������������������: : 22
StatefulPartitionedCallStatefulPartitionedCall:j f
B
_output_shapes0
.:,����������������������������
 
_user_specified_nameinputs
�
K
#__inference__update_step_xla_247486
gradient
variable: *
_XlaMustCompile(*(
_construction_contextkEagerRuntime*
_input_shapes

: : *
	_noinline(:D @

_output_shapes
: 
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�J
�	
C__inference_encoder_layer_call_and_return_conditional_losses_247782

inputsB
(conv2d_15_conv2d_readvariableop_resource:7
)conv2d_15_biasadd_readvariableop_resource:B
(conv2d_16_conv2d_readvariableop_resource: 7
)conv2d_16_biasadd_readvariableop_resource: C
(conv2d_17_conv2d_readvariableop_resource: �8
)conv2d_17_biasadd_readvariableop_resource:	�D
(conv2d_18_conv2d_readvariableop_resource:��8
)conv2d_18_biasadd_readvariableop_resource:	�:
&dense_9_matmul_readvariableop_resource:
��5
'dense_9_biasadd_readvariableop_resource:;
'dense_10_matmul_readvariableop_resource:
��6
(dense_10_biasadd_readvariableop_resource:
identity

identity_1

identity_2�� conv2d_15/BiasAdd/ReadVariableOp�conv2d_15/Conv2D/ReadVariableOp� conv2d_16/BiasAdd/ReadVariableOp�conv2d_16/Conv2D/ReadVariableOp� conv2d_17/BiasAdd/ReadVariableOp�conv2d_17/Conv2D/ReadVariableOp� conv2d_18/BiasAdd/ReadVariableOp�conv2d_18/Conv2D/ReadVariableOp�dense_10/BiasAdd/ReadVariableOp�dense_10/MatMul/ReadVariableOp�dense_9/BiasAdd/ReadVariableOp�dense_9/MatMul/ReadVariableOp�
conv2d_15/Conv2D/ReadVariableOpReadVariableOp(conv2d_15_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
conv2d_15/Conv2DConv2Dinputs'conv2d_15/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
�
 conv2d_15/BiasAdd/ReadVariableOpReadVariableOp)conv2d_15_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
conv2d_15/BiasAddBiasAddconv2d_15/Conv2D:output:0(conv2d_15/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<c
conv2d_15/ReluReluconv2d_15/BiasAdd:output:0*
T0*&
_output_shapes
:<<�
conv2d_16/Conv2D/ReadVariableOpReadVariableOp(conv2d_16_conv2d_readvariableop_resource*&
_output_shapes
: *
dtype0�
conv2d_16/Conv2DConv2Dconv2d_15/Relu:activations:0'conv2d_16/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
: *
paddingSAME*
strides
�
 conv2d_16/BiasAdd/ReadVariableOpReadVariableOp)conv2d_16_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
conv2d_16/BiasAddBiasAddconv2d_16/Conv2D:output:0(conv2d_16/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
: c
conv2d_16/ReluReluconv2d_16/BiasAdd:output:0*
T0*&
_output_shapes
: �
conv2d_17/Conv2D/ReadVariableOpReadVariableOp(conv2d_17_conv2d_readvariableop_resource*'
_output_shapes
: �*
dtype0�
conv2d_17/Conv2DConv2Dconv2d_16/Relu:activations:0'conv2d_17/Conv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
 conv2d_17/BiasAdd/ReadVariableOpReadVariableOp)conv2d_17_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
conv2d_17/BiasAddBiasAddconv2d_17/Conv2D:output:0(conv2d_17/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�d
conv2d_17/ReluReluconv2d_17/BiasAdd:output:0*
T0*'
_output_shapes
:��
conv2d_18/Conv2D/ReadVariableOpReadVariableOp(conv2d_18_conv2d_readvariableop_resource*(
_output_shapes
:��*
dtype0�
conv2d_18/Conv2DConv2Dconv2d_17/Relu:activations:0'conv2d_18/Conv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
 conv2d_18/BiasAdd/ReadVariableOpReadVariableOp)conv2d_18_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
conv2d_18/BiasAddBiasAddconv2d_18/Conv2D:output:0(conv2d_18/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�d
conv2d_18/ReluReluconv2d_18/BiasAdd:output:0*
T0*'
_output_shapes
:�`
flatten_3/ConstConst*
_output_shapes
:*
dtype0*
valueB"���� � 
flatten_3/ReshapeReshapeconv2d_18/Relu:activations:0flatten_3/Const:output:0*
T0* 
_output_shapes
:
���
dense_9/MatMul/ReadVariableOpReadVariableOp&dense_9_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
dense_9/MatMulMatMulflatten_3/Reshape:output:0%dense_9/MatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:�
dense_9/BiasAdd/ReadVariableOpReadVariableOp'dense_9_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense_9/BiasAddBiasAdddense_9/MatMul:product:0&dense_9/BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:�
dense_10/MatMul/ReadVariableOpReadVariableOp'dense_10_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
dense_10/MatMulMatMulflatten_3/Reshape:output:0&dense_10/MatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:�
dense_10/BiasAdd/ReadVariableOpReadVariableOp(dense_10_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense_10/BiasAddBiasAdddense_10/MatMul:product:0'dense_10/BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:_
lambda_3/ShapeConst*
_output_shapes
:*
dtype0*
valueB"      f
lambda_3/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: h
lambda_3/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:h
lambda_3/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
lambda_3/strided_sliceStridedSlicelambda_3/Shape:output:0%lambda_3/strided_slice/stack:output:0'lambda_3/strided_slice/stack_1:output:0'lambda_3/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask`
lambda_3/random_normal/shape/1Const*
_output_shapes
: *
dtype0*
value	B :�
lambda_3/random_normal/shapePacklambda_3/strided_slice:output:0'lambda_3/random_normal/shape/1:output:0*
N*
T0*
_output_shapes
:`
lambda_3/random_normal/meanConst*
_output_shapes
: *
dtype0*
valueB
 *    b
lambda_3/random_normal/stddevConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
+lambda_3/random_normal/RandomStandardNormalRandomStandardNormal%lambda_3/random_normal/shape:output:0*
T0*
_output_shapes

:*
dtype0*
seed���)*
seed2���
lambda_3/random_normal/mulMul4lambda_3/random_normal/RandomStandardNormal:output:0&lambda_3/random_normal/stddev:output:0*
T0*
_output_shapes

:�
lambda_3/random_normalAddV2lambda_3/random_normal/mul:z:0$lambda_3/random_normal/mean:output:0*
T0*
_output_shapes

:W
lambda_3/truediv/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @|
lambda_3/truedivRealDivdense_10/BiasAdd:output:0lambda_3/truediv/y:output:0*
T0*
_output_shapes

:R
lambda_3/ExpExplambda_3/truediv:z:0*
T0*
_output_shapes

:j
lambda_3/mulMullambda_3/Exp:y:0lambda_3/random_normal:z:0*
T0*
_output_shapes

:j
lambda_3/addAddV2dense_9/BiasAdd:output:0lambda_3/mul:z:0*
T0*
_output_shapes

:^
IdentityIdentitydense_9/BiasAdd:output:0^NoOp*
T0*
_output_shapes

:a

Identity_1Identitydense_10/BiasAdd:output:0^NoOp*
T0*
_output_shapes

:X

Identity_2Identitylambda_3/add:z:0^NoOp*
T0*
_output_shapes

:�
NoOpNoOp!^conv2d_15/BiasAdd/ReadVariableOp ^conv2d_15/Conv2D/ReadVariableOp!^conv2d_16/BiasAdd/ReadVariableOp ^conv2d_16/Conv2D/ReadVariableOp!^conv2d_17/BiasAdd/ReadVariableOp ^conv2d_17/Conv2D/ReadVariableOp!^conv2d_18/BiasAdd/ReadVariableOp ^conv2d_18/Conv2D/ReadVariableOp ^dense_10/BiasAdd/ReadVariableOp^dense_10/MatMul/ReadVariableOp^dense_9/BiasAdd/ReadVariableOp^dense_9/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*=
_input_shapes,
*:<<: : : : : : : : : : : : 2D
 conv2d_15/BiasAdd/ReadVariableOp conv2d_15/BiasAdd/ReadVariableOp2B
conv2d_15/Conv2D/ReadVariableOpconv2d_15/Conv2D/ReadVariableOp2D
 conv2d_16/BiasAdd/ReadVariableOp conv2d_16/BiasAdd/ReadVariableOp2B
conv2d_16/Conv2D/ReadVariableOpconv2d_16/Conv2D/ReadVariableOp2D
 conv2d_17/BiasAdd/ReadVariableOp conv2d_17/BiasAdd/ReadVariableOp2B
conv2d_17/Conv2D/ReadVariableOpconv2d_17/Conv2D/ReadVariableOp2D
 conv2d_18/BiasAdd/ReadVariableOp conv2d_18/BiasAdd/ReadVariableOp2B
conv2d_18/Conv2D/ReadVariableOpconv2d_18/Conv2D/ReadVariableOp2B
dense_10/BiasAdd/ReadVariableOpdense_10/BiasAdd/ReadVariableOp2@
dense_10/MatMul/ReadVariableOpdense_10/MatMul/ReadVariableOp2@
dense_9/BiasAdd/ReadVariableOpdense_9/BiasAdd/ReadVariableOp2>
dense_9/MatMul/ReadVariableOpdense_9/MatMul/ReadVariableOp:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs
�*
�
C__inference_encoder_layer_call_and_return_conditional_losses_245243

inputs*
conv2d_15_245208:
conv2d_15_245210:*
conv2d_16_245213: 
conv2d_16_245215: +
conv2d_17_245218: �
conv2d_17_245220:	�,
conv2d_18_245223:��
conv2d_18_245225:	�"
dense_9_245229:
��
dense_9_245231:#
dense_10_245234:
��
dense_10_245236:
identity

identity_1

identity_2��!conv2d_15/StatefulPartitionedCall�!conv2d_16/StatefulPartitionedCall�!conv2d_17/StatefulPartitionedCall�!conv2d_18/StatefulPartitionedCall� dense_10/StatefulPartitionedCall�dense_9/StatefulPartitionedCall� lambda_3/StatefulPartitionedCall�
!conv2d_15/StatefulPartitionedCallStatefulPartitionedCallinputsconv2d_15_245208conv2d_15_245210*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_15_layer_call_and_return_conditional_losses_244947�
!conv2d_16/StatefulPartitionedCallStatefulPartitionedCall*conv2d_15/StatefulPartitionedCall:output:0conv2d_16_245213conv2d_16_245215*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
: *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_16_layer_call_and_return_conditional_losses_244964�
!conv2d_17/StatefulPartitionedCallStatefulPartitionedCall*conv2d_16/StatefulPartitionedCall:output:0conv2d_17_245218conv2d_17_245220*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_17_layer_call_and_return_conditional_losses_244981�
!conv2d_18/StatefulPartitionedCallStatefulPartitionedCall*conv2d_17/StatefulPartitionedCall:output:0conv2d_18_245223conv2d_18_245225*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_18_layer_call_and_return_conditional_losses_244998�
flatten_3/PartitionedCallPartitionedCall*conv2d_18/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 * 
_output_shapes
:
��* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_flatten_3_layer_call_and_return_conditional_losses_245010�
dense_9/StatefulPartitionedCallStatefulPartitionedCall"flatten_3/PartitionedCall:output:0dense_9_245229dense_9_245231*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_dense_9_layer_call_and_return_conditional_losses_245022�
 dense_10/StatefulPartitionedCallStatefulPartitionedCall"flatten_3/PartitionedCall:output:0dense_10_245234dense_10_245236*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dense_10_layer_call_and_return_conditional_losses_245038�
 lambda_3/StatefulPartitionedCallStatefulPartitionedCall(dense_9/StatefulPartitionedCall:output:0)dense_10/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_lambda_3_layer_call_and_return_conditional_losses_245126n
IdentityIdentity(dense_9/StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:q

Identity_1Identity)dense_10/StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:q

Identity_2Identity)lambda_3/StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:�
NoOpNoOp"^conv2d_15/StatefulPartitionedCall"^conv2d_16/StatefulPartitionedCall"^conv2d_17/StatefulPartitionedCall"^conv2d_18/StatefulPartitionedCall!^dense_10/StatefulPartitionedCall ^dense_9/StatefulPartitionedCall!^lambda_3/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*=
_input_shapes,
*:<<: : : : : : : : : : : : 2F
!conv2d_15/StatefulPartitionedCall!conv2d_15/StatefulPartitionedCall2F
!conv2d_16/StatefulPartitionedCall!conv2d_16/StatefulPartitionedCall2F
!conv2d_17/StatefulPartitionedCall!conv2d_17/StatefulPartitionedCall2F
!conv2d_18/StatefulPartitionedCall!conv2d_18/StatefulPartitionedCall2D
 dense_10/StatefulPartitionedCall dense_10/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2D
 lambda_3/StatefulPartitionedCall lambda_3/StatefulPartitionedCall:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs
�
�
*__inference_conv2d_17_layer_call_fn_248105

inputs"
unknown: �
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_17_layer_call_and_return_conditional_losses_244981o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:�`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
: : : 22
StatefulPartitionedCallStatefulPartitionedCall:N J
&
_output_shapes
: 
 
_user_specified_nameinputs
�

�
E__inference_conv2d_15_layer_call_and_return_conditional_losses_244947

inputs8
conv2d_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp|
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0t
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<O
ReluReluBiasAdd:output:0*
T0*&
_output_shapes
:<<`
IdentityIdentityRelu:activations:0^NoOp*
T0*&
_output_shapes
:<<w
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:<<: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs
�
�
H__inference_add_metric_7_layer_call_and_return_conditional_losses_246026

inputs&
assignaddvariableop_resource: (
assignaddvariableop_1_resource: 

identity_1��AssignAddVariableOp�AssignAddVariableOp_1�div_no_nan/ReadVariableOp�div_no_nan/ReadVariableOp_1F
RankConst*
_output_shapes
: *
dtype0*
value	B : M
range/startConst*
_output_shapes
: *
dtype0*
value	B : M
range/deltaConst*
_output_shapes
: *
dtype0*
value	B :c
rangeRangerange/start:output:0Rank:output:0range/delta:output:0*
_output_shapes
: k
SumSuminputsrange:output:0*
T0*&
 _has_manual_control_dependencies(*
_output_shapes
: �
AssignAddVariableOpAssignAddVariableOpassignaddvariableop_resourceSum:output:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0F
SizeConst*
_output_shapes
: *
dtype0*
value	B :K
CastCastSize:output:0*

DstT0*

SrcT0*
_output_shapes
: �
AssignAddVariableOp_1AssignAddVariableOpassignaddvariableop_1_resourceCast:y:0^AssignAddVariableOp*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0�
div_no_nan/ReadVariableOpReadVariableOpassignaddvariableop_resource^AssignAddVariableOp^AssignAddVariableOp_1*
_output_shapes
: *
dtype0�
div_no_nan/ReadVariableOp_1ReadVariableOpassignaddvariableop_1_resource^AssignAddVariableOp_1*
_output_shapes
: *
dtype0

div_no_nanDivNoNan!div_no_nan/ReadVariableOp:value:0#div_no_nan/ReadVariableOp_1:value:0*
T0*
_output_shapes
: E
IdentityIdentitydiv_no_nan:z:0*
T0*
_output_shapes
: F

Identity_1Identityinputs^NoOp*
T0*
_output_shapes
: �
NoOpNoOp^AssignAddVariableOp^AssignAddVariableOp_1^div_no_nan/ReadVariableOp^div_no_nan/ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: : : 2*
AssignAddVariableOpAssignAddVariableOp2.
AssignAddVariableOp_1AssignAddVariableOp_126
div_no_nan/ReadVariableOpdiv_no_nan/ReadVariableOp2:
div_no_nan/ReadVariableOp_1div_no_nan/ReadVariableOp_1:> :

_output_shapes
: 
 
_user_specified_nameinputs
�
r
)__inference_lambda_3_layer_call_fn_248254
inputs_0
inputs_1
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs_0inputs_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_lambda_3_layer_call_and_return_conditional_losses_245066f
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
::22
StatefulPartitionedCallStatefulPartitionedCall:H D

_output_shapes

:
"
_user_specified_name
inputs_0:HD

_output_shapes

:
"
_user_specified_name
inputs_1
�
r
F__inference_add_loss_3_layer_call_and_return_conditional_losses_248196

inputs
identity

identity_1=
IdentityIdentityinputs*
T0*
_output_shapes
: ?

Identity_1Identityinputs*
T0*
_output_shapes
: "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: :> :

_output_shapes
: 
 
_user_specified_nameinputs
�
�
(__inference_encoder_layer_call_fn_247619

inputs!
unknown:
	unknown_0:#
	unknown_1: 
	unknown_2: $
	unknown_3: �
	unknown_4:	�%
	unknown_5:��
	unknown_6:	�
	unknown_7:
��
	unknown_8:
	unknown_9:
��

unknown_10:
identity

identity_1

identity_2��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10*
Tin
2*
Tout
2*
_collective_manager_ids
 *2
_output_shapes 
:::*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_encoder_layer_call_and_return_conditional_losses_245172f
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:h

Identity_1Identity StatefulPartitionedCall:output:1^NoOp*
T0*
_output_shapes

:h

Identity_2Identity StatefulPartitionedCall:output:2^NoOp*
T0*
_output_shapes

:`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*=
_input_shapes,
*:<<: : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs
�
L
#__inference__update_step_xla_247496
gradient
variable:	�*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*
_input_shapes
	:�: *
	_noinline(:E A

_output_shapes	
:�
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�
K
#__inference__update_step_xla_247586
gradient
variable:*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*
_input_shapes

:: *
	_noinline(:D @

_output_shapes
:
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�
�
H__inference_add_metric_6_layer_call_and_return_conditional_losses_248222

inputs&
assignaddvariableop_resource: (
assignaddvariableop_1_resource: 

identity_1��AssignAddVariableOp�AssignAddVariableOp_1�div_no_nan/ReadVariableOp�div_no_nan/ReadVariableOp_1F
RankConst*
_output_shapes
: *
dtype0*
value	B : M
range/startConst*
_output_shapes
: *
dtype0*
value	B : M
range/deltaConst*
_output_shapes
: *
dtype0*
value	B :c
rangeRangerange/start:output:0Rank:output:0range/delta:output:0*
_output_shapes
: k
SumSuminputsrange:output:0*
T0*&
 _has_manual_control_dependencies(*
_output_shapes
: �
AssignAddVariableOpAssignAddVariableOpassignaddvariableop_resourceSum:output:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0F
SizeConst*
_output_shapes
: *
dtype0*
value	B :K
CastCastSize:output:0*

DstT0*

SrcT0*
_output_shapes
: �
AssignAddVariableOp_1AssignAddVariableOpassignaddvariableop_1_resourceCast:y:0^AssignAddVariableOp*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0�
div_no_nan/ReadVariableOpReadVariableOpassignaddvariableop_resource^AssignAddVariableOp^AssignAddVariableOp_1*
_output_shapes
: *
dtype0�
div_no_nan/ReadVariableOp_1ReadVariableOpassignaddvariableop_1_resource^AssignAddVariableOp_1*
_output_shapes
: *
dtype0

div_no_nanDivNoNan!div_no_nan/ReadVariableOp:value:0#div_no_nan/ReadVariableOp_1:value:0*
T0*
_output_shapes
: E
IdentityIdentitydiv_no_nan:z:0*
T0*
_output_shapes
: F

Identity_1Identityinputs^NoOp*
T0*
_output_shapes
: �
NoOpNoOp^AssignAddVariableOp^AssignAddVariableOp_1^div_no_nan/ReadVariableOp^div_no_nan/ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: : : 2*
AssignAddVariableOpAssignAddVariableOp2.
AssignAddVariableOp_1AssignAddVariableOp_126
div_no_nan/ReadVariableOpdiv_no_nan/ReadVariableOp2:
div_no_nan/ReadVariableOp_1div_no_nan/ReadVariableOp_1:> :

_output_shapes
: 
 
_user_specified_nameinputs
�!
�
O__inference_conv2d_transpose_12_layer_call_and_return_conditional_losses_248386

inputsD
(conv2d_transpose_readvariableop_resource:��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�conv2d_transpose/ReadVariableOpI
ShapeShapeinputs*
T0*
_output_shapes
::��]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask_
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_2StridedSliceShape:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskG
mul/yConst*
_output_shapes
: *
dtype0*
value	B :U
mulMulstrided_slice_1:output:0mul/y:output:0*
T0*
_output_shapes
: I
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :Y
mul_1Mulstrided_slice_2:output:0mul_1/y:output:0*
T0*
_output_shapes
: J
stack/3Const*
_output_shapes
: *
dtype0*
value
B :�y
stackPackstrided_slice:output:0mul:z:0	mul_1:z:0stack/3:output:0*
N*
T0*
_output_shapes
:_
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_3StridedSlicestack:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
conv2d_transpose/ReadVariableOpReadVariableOp(conv2d_transpose_readvariableop_resource*(
_output_shapes
:��*
dtype0�
conv2d_transposeConv2DBackpropInputstack:output:0'conv2d_transpose/ReadVariableOp:value:0inputs*
T0*B
_output_shapes0
.:,����������������������������*
paddingSAME*
strides
s
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
BiasAddBiasAddconv2d_transpose:output:0BiasAdd/ReadVariableOp:value:0*
T0*B
_output_shapes0
.:,����������������������������k
ReluReluBiasAdd:output:0*
T0*B
_output_shapes0
.:,����������������������������|
IdentityIdentityRelu:activations:0^NoOp*
T0*B
_output_shapes0
.:,�����������������������������
NoOpNoOp^BiasAdd/ReadVariableOp ^conv2d_transpose/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*E
_input_shapes4
2:,����������������������������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2B
conv2d_transpose/ReadVariableOpconv2d_transpose/ReadVariableOp:j f
B
_output_shapes0
.:,����������������������������
 
_user_specified_nameinputs
�	
�
C__inference_dense_9_layer_call_and_return_conditional_losses_245022

inputs2
matmul_readvariableop_resource:
��-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpv
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0`
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0m
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:V
IdentityIdentityBiasAdd:output:0^NoOp*
T0*
_output_shapes

:w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*#
_input_shapes
:
��: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:H D
 
_output_shapes
:
��
 
_user_specified_nameinputs
�
a
E__inference_flatten_3_layer_call_and_return_conditional_losses_245010

inputs
identityV
ConstConst*
_output_shapes
:*
dtype0*
valueB"���� � U
ReshapeReshapeinputsConst:output:0*
T0* 
_output_shapes
:
��Q
IdentityIdentityReshape:output:0*
T0* 
_output_shapes
:
��"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:�:O K
'
_output_shapes
:�
 
_user_specified_nameinputs
�
s
D__inference_lambda_3_layer_call_and_return_conditional_losses_248282
inputs_0
inputs_1
identity�V
ShapeConst*
_output_shapes
:*
dtype0*
valueB"      ]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskW
random_normal/shape/1Const*
_output_shapes
: *
dtype0*
value	B :�
random_normal/shapePackstrided_slice:output:0random_normal/shape/1:output:0*
N*
T0*
_output_shapes
:W
random_normal/meanConst*
_output_shapes
: *
dtype0*
valueB
 *    Y
random_normal/stddevConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
"random_normal/RandomStandardNormalRandomStandardNormalrandom_normal/shape:output:0*
T0*
_output_shapes

:*
dtype0*
seed���)*
seed2����
random_normal/mulMul+random_normal/RandomStandardNormal:output:0random_normal/stddev:output:0*
T0*
_output_shapes

:s
random_normalAddV2random_normal/mul:z:0random_normal/mean:output:0*
T0*
_output_shapes

:N
	truediv/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @Y
truedivRealDivinputs_1truediv/y:output:0*
T0*
_output_shapes

:@
ExpExptruediv:z:0*
T0*
_output_shapes

:O
mulMulExp:y:0random_normal:z:0*
T0*
_output_shapes

:H
addAddV2inputs_0mul:z:0*
T0*
_output_shapes

:F
IdentityIdentityadd:z:0*
T0*
_output_shapes

:"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:::H D

_output_shapes

:
"
_user_specified_name
inputs_0:HD

_output_shapes

:
"
_user_specified_name
inputs_1
�
�
(__inference_decoder_layer_call_fn_247840

inputs
unknown:
��
	unknown_0:
��%
	unknown_1:��
	unknown_2:	�$
	unknown_3:@�
	unknown_4:@#
	unknown_5: @
	unknown_6: #
	unknown_7: 
	unknown_8:#
	unknown_9:

unknown_10:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_decoder_layer_call_and_return_conditional_losses_245811n
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":: : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:F B

_output_shapes

:
 
_user_specified_nameinputs
�
r
)__inference_lambda_3_layer_call_fn_248260
inputs_0
inputs_1
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs_0inputs_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_lambda_3_layer_call_and_return_conditional_losses_245126f
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
::22
StatefulPartitionedCallStatefulPartitionedCall:H D

_output_shapes

:
"
_user_specified_name
inputs_0:HD

_output_shapes

:
"
_user_specified_name
inputs_1
�
F
*__inference_flatten_3_layer_call_fn_248141

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 * 
_output_shapes
:
��* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_flatten_3_layer_call_and_return_conditional_losses_245010Y
IdentityIdentityPartitionedCall:output:0*
T0* 
_output_shapes
:
��"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:�:O K
'
_output_shapes
:�
 
_user_specified_nameinputs
�

�
E__inference_conv2d_17_layer_call_and_return_conditional_losses_248116

inputs9
conv2d_readvariableop_resource: �.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp}
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*'
_output_shapes
: �*
dtype0�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
s
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0u
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�P
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:�a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:�w
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
: : : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:N J
&
_output_shapes
: 
 
_user_specified_nameinputs
�
L
#__inference__update_step_xla_247506
gradient
variable:	�*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*
_input_shapes
	:�: *
	_noinline(:E A

_output_shapes	
:�
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�
r
F__inference_add_loss_3_layer_call_and_return_conditional_losses_246055

inputs
identity

identity_1=
IdentityIdentityinputs*
T0*
_output_shapes
: ?

Identity_1Identityinputs*
T0*
_output_shapes
: "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: :> :

_output_shapes
: 
 
_user_specified_nameinputs
�
�
-__inference_add_metric_7_layer_call_fn_248231

inputs
unknown: 
	unknown_0: 
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_add_metric_7_layer_call_and_return_conditional_losses_246026^
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes
: `
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: : : 22
StatefulPartitionedCallStatefulPartitionedCall:> :

_output_shapes
: 
 
_user_specified_nameinputs
�!
�
O__inference_conv2d_transpose_15_layer_call_and_return_conditional_losses_248515

inputsB
(conv2d_transpose_readvariableop_resource: -
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�conv2d_transpose/ReadVariableOpI
ShapeShapeinputs*
T0*
_output_shapes
::��]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask_
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_2StridedSliceShape:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskG
mul/yConst*
_output_shapes
: *
dtype0*
value	B :U
mulMulstrided_slice_1:output:0mul/y:output:0*
T0*
_output_shapes
: I
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :Y
mul_1Mulstrided_slice_2:output:0mul_1/y:output:0*
T0*
_output_shapes
: I
stack/3Const*
_output_shapes
: *
dtype0*
value	B :y
stackPackstrided_slice:output:0mul:z:0	mul_1:z:0stack/3:output:0*
N*
T0*
_output_shapes
:_
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_3StridedSlicestack:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
conv2d_transpose/ReadVariableOpReadVariableOp(conv2d_transpose_readvariableop_resource*&
_output_shapes
: *
dtype0�
conv2d_transposeConv2DBackpropInputstack:output:0'conv2d_transpose/ReadVariableOp:value:0inputs*
T0*A
_output_shapes/
-:+���������������������������*
paddingSAME*
strides
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
BiasAddBiasAddconv2d_transpose:output:0BiasAdd/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+���������������������������j
ReluReluBiasAdd:output:0*
T0*A
_output_shapes/
-:+���������������������������{
IdentityIdentityRelu:activations:0^NoOp*
T0*A
_output_shapes/
-:+����������������������������
NoOpNoOp^BiasAdd/ReadVariableOp ^conv2d_transpose/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*D
_input_shapes3
1:+��������������������������� : : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2B
conv2d_transpose/ReadVariableOpconv2d_transpose/ReadVariableOp:i e
A
_output_shapes/
-:+��������������������������� 
 
_user_specified_nameinputs
�*
�
C__inference_encoder_layer_call_and_return_conditional_losses_245131
input_7*
conv2d_15_245074:
conv2d_15_245076:*
conv2d_16_245079: 
conv2d_16_245081: +
conv2d_17_245084: �
conv2d_17_245086:	�,
conv2d_18_245089:��
conv2d_18_245091:	�"
dense_9_245095:
��
dense_9_245097:#
dense_10_245100:
��
dense_10_245102:
identity

identity_1

identity_2��!conv2d_15/StatefulPartitionedCall�!conv2d_16/StatefulPartitionedCall�!conv2d_17/StatefulPartitionedCall�!conv2d_18/StatefulPartitionedCall� dense_10/StatefulPartitionedCall�dense_9/StatefulPartitionedCall� lambda_3/StatefulPartitionedCall�
!conv2d_15/StatefulPartitionedCallStatefulPartitionedCallinput_7conv2d_15_245074conv2d_15_245076*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_15_layer_call_and_return_conditional_losses_244947�
!conv2d_16/StatefulPartitionedCallStatefulPartitionedCall*conv2d_15/StatefulPartitionedCall:output:0conv2d_16_245079conv2d_16_245081*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
: *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_16_layer_call_and_return_conditional_losses_244964�
!conv2d_17/StatefulPartitionedCallStatefulPartitionedCall*conv2d_16/StatefulPartitionedCall:output:0conv2d_17_245084conv2d_17_245086*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_17_layer_call_and_return_conditional_losses_244981�
!conv2d_18/StatefulPartitionedCallStatefulPartitionedCall*conv2d_17/StatefulPartitionedCall:output:0conv2d_18_245089conv2d_18_245091*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_18_layer_call_and_return_conditional_losses_244998�
flatten_3/PartitionedCallPartitionedCall*conv2d_18/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 * 
_output_shapes
:
��* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_flatten_3_layer_call_and_return_conditional_losses_245010�
dense_9/StatefulPartitionedCallStatefulPartitionedCall"flatten_3/PartitionedCall:output:0dense_9_245095dense_9_245097*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_dense_9_layer_call_and_return_conditional_losses_245022�
 dense_10/StatefulPartitionedCallStatefulPartitionedCall"flatten_3/PartitionedCall:output:0dense_10_245100dense_10_245102*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dense_10_layer_call_and_return_conditional_losses_245038�
 lambda_3/StatefulPartitionedCallStatefulPartitionedCall(dense_9/StatefulPartitionedCall:output:0)dense_10/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_lambda_3_layer_call_and_return_conditional_losses_245126n
IdentityIdentity(dense_9/StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:q

Identity_1Identity)dense_10/StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:q

Identity_2Identity)lambda_3/StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:�
NoOpNoOp"^conv2d_15/StatefulPartitionedCall"^conv2d_16/StatefulPartitionedCall"^conv2d_17/StatefulPartitionedCall"^conv2d_18/StatefulPartitionedCall!^dense_10/StatefulPartitionedCall ^dense_9/StatefulPartitionedCall!^lambda_3/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*=
_input_shapes,
*:<<: : : : : : : : : : : : 2F
!conv2d_15/StatefulPartitionedCall!conv2d_15/StatefulPartitionedCall2F
!conv2d_16/StatefulPartitionedCall!conv2d_16/StatefulPartitionedCall2F
!conv2d_17/StatefulPartitionedCall!conv2d_17/StatefulPartitionedCall2F
!conv2d_18/StatefulPartitionedCall!conv2d_18/StatefulPartitionedCall2D
 dense_10/StatefulPartitionedCall dense_10/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2D
 lambda_3/StatefulPartitionedCall lambda_3/StatefulPartitionedCall:O K
&
_output_shapes
:<<
!
_user_specified_name	input_7
�
�
4__inference_conv2d_transpose_14_layer_call_fn_248438

inputs!
unknown: @
	unknown_0: 
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *A
_output_shapes/
-:+��������������������������� *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_14_layer_call_and_return_conditional_losses_245544�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*A
_output_shapes/
-:+��������������������������� `
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*D
_input_shapes3
1:+���������������������������@: : 22
StatefulPartitionedCallStatefulPartitionedCall:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs
�
W
#__inference__update_step_xla_247471
gradient"
variable:*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*'
_input_shapes
:: *
	_noinline(:P L
&
_output_shapes
:
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�
K
#__inference__update_step_xla_247516
gradient
variable:*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*
_input_shapes

:: *
	_noinline(:D @

_output_shapes
:
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�
�
4__inference_conv2d_transpose_15_layer_call_fn_248481

inputs!
unknown: 
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *A
_output_shapes/
-:+���������������������������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_15_layer_call_and_return_conditional_losses_245589�
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*A
_output_shapes/
-:+���������������������������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*D
_input_shapes3
1:+��������������������������� : : 22
StatefulPartitionedCallStatefulPartitionedCall:i e
A
_output_shapes/
-:+��������������������������� 
 
_user_specified_nameinputs
�
W
#__inference__update_step_xla_247561
gradient"
variable: @*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*'
_input_shapes
: @: *
	_noinline(:P L
&
_output_shapes
: @
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�*
�
C__inference_encoder_layer_call_and_return_conditional_losses_245172

inputs*
conv2d_15_245137:
conv2d_15_245139:*
conv2d_16_245142: 
conv2d_16_245144: +
conv2d_17_245147: �
conv2d_17_245149:	�,
conv2d_18_245152:��
conv2d_18_245154:	�"
dense_9_245158:
��
dense_9_245160:#
dense_10_245163:
��
dense_10_245165:
identity

identity_1

identity_2��!conv2d_15/StatefulPartitionedCall�!conv2d_16/StatefulPartitionedCall�!conv2d_17/StatefulPartitionedCall�!conv2d_18/StatefulPartitionedCall� dense_10/StatefulPartitionedCall�dense_9/StatefulPartitionedCall� lambda_3/StatefulPartitionedCall�
!conv2d_15/StatefulPartitionedCallStatefulPartitionedCallinputsconv2d_15_245137conv2d_15_245139*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_15_layer_call_and_return_conditional_losses_244947�
!conv2d_16/StatefulPartitionedCallStatefulPartitionedCall*conv2d_15/StatefulPartitionedCall:output:0conv2d_16_245142conv2d_16_245144*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
: *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_16_layer_call_and_return_conditional_losses_244964�
!conv2d_17/StatefulPartitionedCallStatefulPartitionedCall*conv2d_16/StatefulPartitionedCall:output:0conv2d_17_245147conv2d_17_245149*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_17_layer_call_and_return_conditional_losses_244981�
!conv2d_18/StatefulPartitionedCallStatefulPartitionedCall*conv2d_17/StatefulPartitionedCall:output:0conv2d_18_245152conv2d_18_245154*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_18_layer_call_and_return_conditional_losses_244998�
flatten_3/PartitionedCallPartitionedCall*conv2d_18/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 * 
_output_shapes
:
��* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_flatten_3_layer_call_and_return_conditional_losses_245010�
dense_9/StatefulPartitionedCallStatefulPartitionedCall"flatten_3/PartitionedCall:output:0dense_9_245158dense_9_245160*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_dense_9_layer_call_and_return_conditional_losses_245022�
 dense_10/StatefulPartitionedCallStatefulPartitionedCall"flatten_3/PartitionedCall:output:0dense_10_245163dense_10_245165*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dense_10_layer_call_and_return_conditional_losses_245038�
 lambda_3/StatefulPartitionedCallStatefulPartitionedCall(dense_9/StatefulPartitionedCall:output:0)dense_10/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_lambda_3_layer_call_and_return_conditional_losses_245066n
IdentityIdentity(dense_9/StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:q

Identity_1Identity)dense_10/StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:q

Identity_2Identity)lambda_3/StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:�
NoOpNoOp"^conv2d_15/StatefulPartitionedCall"^conv2d_16/StatefulPartitionedCall"^conv2d_17/StatefulPartitionedCall"^conv2d_18/StatefulPartitionedCall!^dense_10/StatefulPartitionedCall ^dense_9/StatefulPartitionedCall!^lambda_3/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*=
_input_shapes,
*:<<: : : : : : : : : : : : 2F
!conv2d_15/StatefulPartitionedCall!conv2d_15/StatefulPartitionedCall2F
!conv2d_16/StatefulPartitionedCall!conv2d_16/StatefulPartitionedCall2F
!conv2d_17/StatefulPartitionedCall!conv2d_17/StatefulPartitionedCall2F
!conv2d_18/StatefulPartitionedCall!conv2d_18/StatefulPartitionedCall2D
 dense_10/StatefulPartitionedCall dense_10/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2D
 lambda_3/StatefulPartitionedCall lambda_3/StatefulPartitionedCall:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs
�

�
E__inference_conv2d_18_layer_call_and_return_conditional_losses_248136

inputs:
conv2d_readvariableop_resource:��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp~
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*(
_output_shapes
:��*
dtype0�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
s
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0u
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�P
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:�a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:�w
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:�: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:O K
'
_output_shapes
:�
 
_user_specified_nameinputs
�
�
$__inference_signature_wrapper_246818
input_7!
unknown:
	unknown_0:#
	unknown_1: 
	unknown_2: $
	unknown_3: �
	unknown_4:	�%
	unknown_5:��
	unknown_6:	�
	unknown_7:
��
	unknown_8:
	unknown_9:
��

unknown_10:

unknown_11:
��

unknown_12:
��&

unknown_13:��

unknown_14:	�%

unknown_15:@�

unknown_16:@$

unknown_17: @

unknown_18: $

unknown_19: 

unknown_20:$

unknown_21:

unknown_22:

unknown_23

unknown_24: 

unknown_25: 

unknown_26

unknown_27

unknown_28

unknown_29: 

unknown_30: 
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_7unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30*,
Tin%
#2!*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*:
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� **
f%R#
!__inference__wrapped_model_244932n
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*e
_input_shapesT
R:<<: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
&
_output_shapes
:<<
!
_user_specified_name	input_7:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
��
�
?__inference_vae_layer_call_and_return_conditional_losses_247466

inputsJ
0encoder_conv2d_15_conv2d_readvariableop_resource:?
1encoder_conv2d_15_biasadd_readvariableop_resource:J
0encoder_conv2d_16_conv2d_readvariableop_resource: ?
1encoder_conv2d_16_biasadd_readvariableop_resource: K
0encoder_conv2d_17_conv2d_readvariableop_resource: �@
1encoder_conv2d_17_biasadd_readvariableop_resource:	�L
0encoder_conv2d_18_conv2d_readvariableop_resource:��@
1encoder_conv2d_18_biasadd_readvariableop_resource:	�B
.encoder_dense_9_matmul_readvariableop_resource:
��=
/encoder_dense_9_biasadd_readvariableop_resource:C
/encoder_dense_10_matmul_readvariableop_resource:
��>
0encoder_dense_10_biasadd_readvariableop_resource:C
/decoder_dense_11_matmul_readvariableop_resource:
��@
0decoder_dense_11_biasadd_readvariableop_resource:
��`
Ddecoder_conv2d_transpose_12_conv2d_transpose_readvariableop_resource:��J
;decoder_conv2d_transpose_12_biasadd_readvariableop_resource:	�_
Ddecoder_conv2d_transpose_13_conv2d_transpose_readvariableop_resource:@�I
;decoder_conv2d_transpose_13_biasadd_readvariableop_resource:@^
Ddecoder_conv2d_transpose_14_conv2d_transpose_readvariableop_resource: @I
;decoder_conv2d_transpose_14_biasadd_readvariableop_resource: ^
Ddecoder_conv2d_transpose_15_conv2d_transpose_readvariableop_resource: I
;decoder_conv2d_transpose_15_biasadd_readvariableop_resource:J
0decoder_conv2d_19_conv2d_readvariableop_resource:?
1decoder_conv2d_19_biasadd_readvariableop_resource:
unknown3
)add_metric_7_assignaddvariableop_resource: 5
+add_metric_7_assignaddvariableop_1_resource: 
	unknown_0
	unknown_1
	unknown_23
)add_metric_6_assignaddvariableop_resource: 5
+add_metric_6_assignaddvariableop_1_resource: 
identity

identity_1�� add_metric_6/AssignAddVariableOp�"add_metric_6/AssignAddVariableOp_1�&add_metric_6/div_no_nan/ReadVariableOp�(add_metric_6/div_no_nan/ReadVariableOp_1� add_metric_7/AssignAddVariableOp�"add_metric_7/AssignAddVariableOp_1�&add_metric_7/div_no_nan/ReadVariableOp�(add_metric_7/div_no_nan/ReadVariableOp_1� conv2d_15/BiasAdd/ReadVariableOp�conv2d_15/Conv2D/ReadVariableOp� conv2d_16/BiasAdd/ReadVariableOp�conv2d_16/Conv2D/ReadVariableOp� conv2d_17/BiasAdd/ReadVariableOp�conv2d_17/Conv2D/ReadVariableOp� conv2d_18/BiasAdd/ReadVariableOp�conv2d_18/Conv2D/ReadVariableOp�(decoder/conv2d_19/BiasAdd/ReadVariableOp�'decoder/conv2d_19/Conv2D/ReadVariableOp�2decoder/conv2d_transpose_12/BiasAdd/ReadVariableOp�;decoder/conv2d_transpose_12/conv2d_transpose/ReadVariableOp�2decoder/conv2d_transpose_13/BiasAdd/ReadVariableOp�;decoder/conv2d_transpose_13/conv2d_transpose/ReadVariableOp�2decoder/conv2d_transpose_14/BiasAdd/ReadVariableOp�;decoder/conv2d_transpose_14/conv2d_transpose/ReadVariableOp�2decoder/conv2d_transpose_15/BiasAdd/ReadVariableOp�;decoder/conv2d_transpose_15/conv2d_transpose/ReadVariableOp�'decoder/dense_11/BiasAdd/ReadVariableOp�&decoder/dense_11/MatMul/ReadVariableOp�dense_10/BiasAdd/ReadVariableOp�dense_10/MatMul/ReadVariableOp�dense_9/BiasAdd/ReadVariableOp�dense_9/MatMul/ReadVariableOp�(encoder/conv2d_15/BiasAdd/ReadVariableOp�'encoder/conv2d_15/Conv2D/ReadVariableOp�(encoder/conv2d_16/BiasAdd/ReadVariableOp�'encoder/conv2d_16/Conv2D/ReadVariableOp�(encoder/conv2d_17/BiasAdd/ReadVariableOp�'encoder/conv2d_17/Conv2D/ReadVariableOp�(encoder/conv2d_18/BiasAdd/ReadVariableOp�'encoder/conv2d_18/Conv2D/ReadVariableOp�'encoder/dense_10/BiasAdd/ReadVariableOp�&encoder/dense_10/MatMul/ReadVariableOp�&encoder/dense_9/BiasAdd/ReadVariableOp�%encoder/dense_9/MatMul/ReadVariableOp�
'encoder/conv2d_15/Conv2D/ReadVariableOpReadVariableOp0encoder_conv2d_15_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
encoder/conv2d_15/Conv2DConv2Dinputs/encoder/conv2d_15/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
�
(encoder/conv2d_15/BiasAdd/ReadVariableOpReadVariableOp1encoder_conv2d_15_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
encoder/conv2d_15/BiasAddBiasAdd!encoder/conv2d_15/Conv2D:output:00encoder/conv2d_15/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<s
encoder/conv2d_15/ReluRelu"encoder/conv2d_15/BiasAdd:output:0*
T0*&
_output_shapes
:<<�
'encoder/conv2d_16/Conv2D/ReadVariableOpReadVariableOp0encoder_conv2d_16_conv2d_readvariableop_resource*&
_output_shapes
: *
dtype0�
encoder/conv2d_16/Conv2DConv2D$encoder/conv2d_15/Relu:activations:0/encoder/conv2d_16/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
: *
paddingSAME*
strides
�
(encoder/conv2d_16/BiasAdd/ReadVariableOpReadVariableOp1encoder_conv2d_16_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
encoder/conv2d_16/BiasAddBiasAdd!encoder/conv2d_16/Conv2D:output:00encoder/conv2d_16/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
: s
encoder/conv2d_16/ReluRelu"encoder/conv2d_16/BiasAdd:output:0*
T0*&
_output_shapes
: �
'encoder/conv2d_17/Conv2D/ReadVariableOpReadVariableOp0encoder_conv2d_17_conv2d_readvariableop_resource*'
_output_shapes
: �*
dtype0�
encoder/conv2d_17/Conv2DConv2D$encoder/conv2d_16/Relu:activations:0/encoder/conv2d_17/Conv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
(encoder/conv2d_17/BiasAdd/ReadVariableOpReadVariableOp1encoder_conv2d_17_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
encoder/conv2d_17/BiasAddBiasAdd!encoder/conv2d_17/Conv2D:output:00encoder/conv2d_17/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�t
encoder/conv2d_17/ReluRelu"encoder/conv2d_17/BiasAdd:output:0*
T0*'
_output_shapes
:��
'encoder/conv2d_18/Conv2D/ReadVariableOpReadVariableOp0encoder_conv2d_18_conv2d_readvariableop_resource*(
_output_shapes
:��*
dtype0�
encoder/conv2d_18/Conv2DConv2D$encoder/conv2d_17/Relu:activations:0/encoder/conv2d_18/Conv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
(encoder/conv2d_18/BiasAdd/ReadVariableOpReadVariableOp1encoder_conv2d_18_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
encoder/conv2d_18/BiasAddBiasAdd!encoder/conv2d_18/Conv2D:output:00encoder/conv2d_18/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�t
encoder/conv2d_18/ReluRelu"encoder/conv2d_18/BiasAdd:output:0*
T0*'
_output_shapes
:�h
encoder/flatten_3/ConstConst*
_output_shapes
:*
dtype0*
valueB"���� � �
encoder/flatten_3/ReshapeReshape$encoder/conv2d_18/Relu:activations:0 encoder/flatten_3/Const:output:0*
T0* 
_output_shapes
:
���
%encoder/dense_9/MatMul/ReadVariableOpReadVariableOp.encoder_dense_9_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
encoder/dense_9/MatMulMatMul"encoder/flatten_3/Reshape:output:0-encoder/dense_9/MatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:�
&encoder/dense_9/BiasAdd/ReadVariableOpReadVariableOp/encoder_dense_9_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
encoder/dense_9/BiasAddBiasAdd encoder/dense_9/MatMul:product:0.encoder/dense_9/BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:�
&encoder/dense_10/MatMul/ReadVariableOpReadVariableOp/encoder_dense_10_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
encoder/dense_10/MatMulMatMul"encoder/flatten_3/Reshape:output:0.encoder/dense_10/MatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:�
'encoder/dense_10/BiasAdd/ReadVariableOpReadVariableOp0encoder_dense_10_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
encoder/dense_10/BiasAddBiasAdd!encoder/dense_10/MatMul:product:0/encoder/dense_10/BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:g
encoder/lambda_3/ShapeConst*
_output_shapes
:*
dtype0*
valueB"      n
$encoder/lambda_3/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: p
&encoder/lambda_3/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:p
&encoder/lambda_3/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
encoder/lambda_3/strided_sliceStridedSliceencoder/lambda_3/Shape:output:0-encoder/lambda_3/strided_slice/stack:output:0/encoder/lambda_3/strided_slice/stack_1:output:0/encoder/lambda_3/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskh
&encoder/lambda_3/random_normal/shape/1Const*
_output_shapes
: *
dtype0*
value	B :�
$encoder/lambda_3/random_normal/shapePack'encoder/lambda_3/strided_slice:output:0/encoder/lambda_3/random_normal/shape/1:output:0*
N*
T0*
_output_shapes
:h
#encoder/lambda_3/random_normal/meanConst*
_output_shapes
: *
dtype0*
valueB
 *    j
%encoder/lambda_3/random_normal/stddevConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
3encoder/lambda_3/random_normal/RandomStandardNormalRandomStandardNormal-encoder/lambda_3/random_normal/shape:output:0*
T0*
_output_shapes

:*
dtype0*
seed���)*
seed2����
"encoder/lambda_3/random_normal/mulMul<encoder/lambda_3/random_normal/RandomStandardNormal:output:0.encoder/lambda_3/random_normal/stddev:output:0*
T0*
_output_shapes

:�
encoder/lambda_3/random_normalAddV2&encoder/lambda_3/random_normal/mul:z:0,encoder/lambda_3/random_normal/mean:output:0*
T0*
_output_shapes

:_
encoder/lambda_3/truediv/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @�
encoder/lambda_3/truedivRealDiv!encoder/dense_10/BiasAdd:output:0#encoder/lambda_3/truediv/y:output:0*
T0*
_output_shapes

:b
encoder/lambda_3/ExpExpencoder/lambda_3/truediv:z:0*
T0*
_output_shapes

:�
encoder/lambda_3/mulMulencoder/lambda_3/Exp:y:0"encoder/lambda_3/random_normal:z:0*
T0*
_output_shapes

:�
encoder/lambda_3/addAddV2 encoder/dense_9/BiasAdd:output:0encoder/lambda_3/mul:z:0*
T0*
_output_shapes

:�
&decoder/dense_11/MatMul/ReadVariableOpReadVariableOp/decoder_dense_11_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
decoder/dense_11/MatMulMatMulencoder/lambda_3/add:z:0.decoder/dense_11/MatMul/ReadVariableOp:value:0*
T0* 
_output_shapes
:
���
'decoder/dense_11/BiasAdd/ReadVariableOpReadVariableOp0decoder_dense_11_biasadd_readvariableop_resource*
_output_shapes

:��*
dtype0�
decoder/dense_11/BiasAddBiasAdd!decoder/dense_11/MatMul:product:0/decoder/dense_11/BiasAdd/ReadVariableOp:value:0*
T0* 
_output_shapes
:
��k
decoder/dense_11/ReluRelu!decoder/dense_11/BiasAdd:output:0*
T0* 
_output_shapes
:
��h
decoder/reshape_3/ShapeConst*
_output_shapes
:*
dtype0*
valueB"    � o
%decoder/reshape_3/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: q
'decoder/reshape_3/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:q
'decoder/reshape_3/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
decoder/reshape_3/strided_sliceStridedSlice decoder/reshape_3/Shape:output:0.decoder/reshape_3/strided_slice/stack:output:00decoder/reshape_3/strided_slice/stack_1:output:00decoder/reshape_3/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskc
!decoder/reshape_3/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :c
!decoder/reshape_3/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :d
!decoder/reshape_3/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value
B :��
decoder/reshape_3/Reshape/shapePack(decoder/reshape_3/strided_slice:output:0*decoder/reshape_3/Reshape/shape/1:output:0*decoder/reshape_3/Reshape/shape/2:output:0*decoder/reshape_3/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:�
decoder/reshape_3/ReshapeReshape#decoder/dense_11/Relu:activations:0(decoder/reshape_3/Reshape/shape:output:0*
T0*'
_output_shapes
:�z
!decoder/conv2d_transpose_12/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"            y
/decoder/conv2d_transpose_12/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: {
1decoder/conv2d_transpose_12/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:{
1decoder/conv2d_transpose_12/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
)decoder/conv2d_transpose_12/strided_sliceStridedSlice*decoder/conv2d_transpose_12/Shape:output:08decoder/conv2d_transpose_12/strided_slice/stack:output:0:decoder/conv2d_transpose_12/strided_slice/stack_1:output:0:decoder/conv2d_transpose_12/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maske
#decoder/conv2d_transpose_12/stack/1Const*
_output_shapes
: *
dtype0*
value	B :e
#decoder/conv2d_transpose_12/stack/2Const*
_output_shapes
: *
dtype0*
value	B :f
#decoder/conv2d_transpose_12/stack/3Const*
_output_shapes
: *
dtype0*
value
B :��
!decoder/conv2d_transpose_12/stackPack2decoder/conv2d_transpose_12/strided_slice:output:0,decoder/conv2d_transpose_12/stack/1:output:0,decoder/conv2d_transpose_12/stack/2:output:0,decoder/conv2d_transpose_12/stack/3:output:0*
N*
T0*
_output_shapes
:{
1decoder/conv2d_transpose_12/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: }
3decoder/conv2d_transpose_12/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:}
3decoder/conv2d_transpose_12/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
+decoder/conv2d_transpose_12/strided_slice_1StridedSlice*decoder/conv2d_transpose_12/stack:output:0:decoder/conv2d_transpose_12/strided_slice_1/stack:output:0<decoder/conv2d_transpose_12/strided_slice_1/stack_1:output:0<decoder/conv2d_transpose_12/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
;decoder/conv2d_transpose_12/conv2d_transpose/ReadVariableOpReadVariableOpDdecoder_conv2d_transpose_12_conv2d_transpose_readvariableop_resource*(
_output_shapes
:��*
dtype0�
,decoder/conv2d_transpose_12/conv2d_transposeConv2DBackpropInput*decoder/conv2d_transpose_12/stack:output:0Cdecoder/conv2d_transpose_12/conv2d_transpose/ReadVariableOp:value:0"decoder/reshape_3/Reshape:output:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
2decoder/conv2d_transpose_12/BiasAdd/ReadVariableOpReadVariableOp;decoder_conv2d_transpose_12_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
#decoder/conv2d_transpose_12/BiasAddBiasAdd5decoder/conv2d_transpose_12/conv2d_transpose:output:0:decoder/conv2d_transpose_12/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:��
 decoder/conv2d_transpose_12/ReluRelu,decoder/conv2d_transpose_12/BiasAdd:output:0*
T0*'
_output_shapes
:�z
!decoder/conv2d_transpose_13/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"         �   y
/decoder/conv2d_transpose_13/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: {
1decoder/conv2d_transpose_13/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:{
1decoder/conv2d_transpose_13/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
)decoder/conv2d_transpose_13/strided_sliceStridedSlice*decoder/conv2d_transpose_13/Shape:output:08decoder/conv2d_transpose_13/strided_slice/stack:output:0:decoder/conv2d_transpose_13/strided_slice/stack_1:output:0:decoder/conv2d_transpose_13/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maske
#decoder/conv2d_transpose_13/stack/1Const*
_output_shapes
: *
dtype0*
value	B :<e
#decoder/conv2d_transpose_13/stack/2Const*
_output_shapes
: *
dtype0*
value	B :<e
#decoder/conv2d_transpose_13/stack/3Const*
_output_shapes
: *
dtype0*
value	B :@�
!decoder/conv2d_transpose_13/stackPack2decoder/conv2d_transpose_13/strided_slice:output:0,decoder/conv2d_transpose_13/stack/1:output:0,decoder/conv2d_transpose_13/stack/2:output:0,decoder/conv2d_transpose_13/stack/3:output:0*
N*
T0*
_output_shapes
:{
1decoder/conv2d_transpose_13/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: }
3decoder/conv2d_transpose_13/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:}
3decoder/conv2d_transpose_13/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
+decoder/conv2d_transpose_13/strided_slice_1StridedSlice*decoder/conv2d_transpose_13/stack:output:0:decoder/conv2d_transpose_13/strided_slice_1/stack:output:0<decoder/conv2d_transpose_13/strided_slice_1/stack_1:output:0<decoder/conv2d_transpose_13/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
;decoder/conv2d_transpose_13/conv2d_transpose/ReadVariableOpReadVariableOpDdecoder_conv2d_transpose_13_conv2d_transpose_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
,decoder/conv2d_transpose_13/conv2d_transposeConv2DBackpropInput*decoder/conv2d_transpose_13/stack:output:0Cdecoder/conv2d_transpose_13/conv2d_transpose/ReadVariableOp:value:0.decoder/conv2d_transpose_12/Relu:activations:0*
T0*&
_output_shapes
:<<@*
paddingSAME*
strides
�
2decoder/conv2d_transpose_13/BiasAdd/ReadVariableOpReadVariableOp;decoder_conv2d_transpose_13_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype0�
#decoder/conv2d_transpose_13/BiasAddBiasAdd5decoder/conv2d_transpose_13/conv2d_transpose:output:0:decoder/conv2d_transpose_13/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<@�
 decoder/conv2d_transpose_13/ReluRelu,decoder/conv2d_transpose_13/BiasAdd:output:0*
T0*&
_output_shapes
:<<@z
!decoder/conv2d_transpose_14/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"   <   <   @   y
/decoder/conv2d_transpose_14/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: {
1decoder/conv2d_transpose_14/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:{
1decoder/conv2d_transpose_14/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
)decoder/conv2d_transpose_14/strided_sliceStridedSlice*decoder/conv2d_transpose_14/Shape:output:08decoder/conv2d_transpose_14/strided_slice/stack:output:0:decoder/conv2d_transpose_14/strided_slice/stack_1:output:0:decoder/conv2d_transpose_14/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maske
#decoder/conv2d_transpose_14/stack/1Const*
_output_shapes
: *
dtype0*
value	B :<e
#decoder/conv2d_transpose_14/stack/2Const*
_output_shapes
: *
dtype0*
value	B :<e
#decoder/conv2d_transpose_14/stack/3Const*
_output_shapes
: *
dtype0*
value	B : �
!decoder/conv2d_transpose_14/stackPack2decoder/conv2d_transpose_14/strided_slice:output:0,decoder/conv2d_transpose_14/stack/1:output:0,decoder/conv2d_transpose_14/stack/2:output:0,decoder/conv2d_transpose_14/stack/3:output:0*
N*
T0*
_output_shapes
:{
1decoder/conv2d_transpose_14/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: }
3decoder/conv2d_transpose_14/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:}
3decoder/conv2d_transpose_14/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
+decoder/conv2d_transpose_14/strided_slice_1StridedSlice*decoder/conv2d_transpose_14/stack:output:0:decoder/conv2d_transpose_14/strided_slice_1/stack:output:0<decoder/conv2d_transpose_14/strided_slice_1/stack_1:output:0<decoder/conv2d_transpose_14/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
;decoder/conv2d_transpose_14/conv2d_transpose/ReadVariableOpReadVariableOpDdecoder_conv2d_transpose_14_conv2d_transpose_readvariableop_resource*&
_output_shapes
: @*
dtype0�
,decoder/conv2d_transpose_14/conv2d_transposeConv2DBackpropInput*decoder/conv2d_transpose_14/stack:output:0Cdecoder/conv2d_transpose_14/conv2d_transpose/ReadVariableOp:value:0.decoder/conv2d_transpose_13/Relu:activations:0*
T0*&
_output_shapes
:<< *
paddingSAME*
strides
�
2decoder/conv2d_transpose_14/BiasAdd/ReadVariableOpReadVariableOp;decoder_conv2d_transpose_14_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
#decoder/conv2d_transpose_14/BiasAddBiasAdd5decoder/conv2d_transpose_14/conv2d_transpose:output:0:decoder/conv2d_transpose_14/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<< �
 decoder/conv2d_transpose_14/ReluRelu,decoder/conv2d_transpose_14/BiasAdd:output:0*
T0*&
_output_shapes
:<< z
!decoder/conv2d_transpose_15/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"   <   <       y
/decoder/conv2d_transpose_15/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: {
1decoder/conv2d_transpose_15/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:{
1decoder/conv2d_transpose_15/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
)decoder/conv2d_transpose_15/strided_sliceStridedSlice*decoder/conv2d_transpose_15/Shape:output:08decoder/conv2d_transpose_15/strided_slice/stack:output:0:decoder/conv2d_transpose_15/strided_slice/stack_1:output:0:decoder/conv2d_transpose_15/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maske
#decoder/conv2d_transpose_15/stack/1Const*
_output_shapes
: *
dtype0*
value	B :<e
#decoder/conv2d_transpose_15/stack/2Const*
_output_shapes
: *
dtype0*
value	B :<e
#decoder/conv2d_transpose_15/stack/3Const*
_output_shapes
: *
dtype0*
value	B :�
!decoder/conv2d_transpose_15/stackPack2decoder/conv2d_transpose_15/strided_slice:output:0,decoder/conv2d_transpose_15/stack/1:output:0,decoder/conv2d_transpose_15/stack/2:output:0,decoder/conv2d_transpose_15/stack/3:output:0*
N*
T0*
_output_shapes
:{
1decoder/conv2d_transpose_15/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: }
3decoder/conv2d_transpose_15/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:}
3decoder/conv2d_transpose_15/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
+decoder/conv2d_transpose_15/strided_slice_1StridedSlice*decoder/conv2d_transpose_15/stack:output:0:decoder/conv2d_transpose_15/strided_slice_1/stack:output:0<decoder/conv2d_transpose_15/strided_slice_1/stack_1:output:0<decoder/conv2d_transpose_15/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
;decoder/conv2d_transpose_15/conv2d_transpose/ReadVariableOpReadVariableOpDdecoder_conv2d_transpose_15_conv2d_transpose_readvariableop_resource*&
_output_shapes
: *
dtype0�
,decoder/conv2d_transpose_15/conv2d_transposeConv2DBackpropInput*decoder/conv2d_transpose_15/stack:output:0Cdecoder/conv2d_transpose_15/conv2d_transpose/ReadVariableOp:value:0.decoder/conv2d_transpose_14/Relu:activations:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
�
2decoder/conv2d_transpose_15/BiasAdd/ReadVariableOpReadVariableOp;decoder_conv2d_transpose_15_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
#decoder/conv2d_transpose_15/BiasAddBiasAdd5decoder/conv2d_transpose_15/conv2d_transpose:output:0:decoder/conv2d_transpose_15/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<�
 decoder/conv2d_transpose_15/ReluRelu,decoder/conv2d_transpose_15/BiasAdd:output:0*
T0*&
_output_shapes
:<<�
'decoder/conv2d_19/Conv2D/ReadVariableOpReadVariableOp0decoder_conv2d_19_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
decoder/conv2d_19/Conv2DConv2D.decoder/conv2d_transpose_15/Relu:activations:0/decoder/conv2d_19/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
�
(decoder/conv2d_19/BiasAdd/ReadVariableOpReadVariableOp1decoder_conv2d_19_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
decoder/conv2d_19/BiasAddBiasAdd!decoder/conv2d_19/Conv2D:output:00decoder/conv2d_19/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<y
decoder/conv2d_19/SigmoidSigmoid"decoder/conv2d_19/BiasAdd:output:0*
T0*&
_output_shapes
:<<m
tf.reshape_6/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB:
���������s
tf.reshape_6/ReshapeReshapeinputs#tf.reshape_6/Reshape/shape:output:0*
T0*
_output_shapes

:��m
tf.reshape_7/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB:
����������
tf.reshape_7/ReshapeReshapedecoder/conv2d_19/Sigmoid:y:0#tf.reshape_7/Reshape/shape:output:0*
T0*
_output_shapes

:���
conv2d_15/Conv2D/ReadVariableOpReadVariableOp0encoder_conv2d_15_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
conv2d_15/Conv2DConv2Dinputs'conv2d_15/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
�
 conv2d_15/BiasAdd/ReadVariableOpReadVariableOp1encoder_conv2d_15_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
conv2d_15/BiasAddBiasAddconv2d_15/Conv2D:output:0(conv2d_15/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<c
conv2d_15/ReluReluconv2d_15/BiasAdd:output:0*
T0*&
_output_shapes
:<<�
conv2d_16/Conv2D/ReadVariableOpReadVariableOp0encoder_conv2d_16_conv2d_readvariableop_resource*&
_output_shapes
: *
dtype0�
conv2d_16/Conv2DConv2Dconv2d_15/Relu:activations:0'conv2d_16/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
: *
paddingSAME*
strides
�
 conv2d_16/BiasAdd/ReadVariableOpReadVariableOp1encoder_conv2d_16_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
conv2d_16/BiasAddBiasAddconv2d_16/Conv2D:output:0(conv2d_16/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
: c
conv2d_16/ReluReluconv2d_16/BiasAdd:output:0*
T0*&
_output_shapes
: �
.tf.math.squared_difference_3/SquaredDifferenceSquaredDifferencetf.reshape_7/Reshape:output:0tf.reshape_6/Reshape:output:0*
T0*
_output_shapes

:���
conv2d_17/Conv2D/ReadVariableOpReadVariableOp0encoder_conv2d_17_conv2d_readvariableop_resource*'
_output_shapes
: �*
dtype0�
conv2d_17/Conv2DConv2Dconv2d_16/Relu:activations:0'conv2d_17/Conv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
 conv2d_17/BiasAdd/ReadVariableOpReadVariableOp1encoder_conv2d_17_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
conv2d_17/BiasAddBiasAddconv2d_17/Conv2D:output:0(conv2d_17/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�d
conv2d_17/ReluReluconv2d_17/BiasAdd:output:0*
T0*'
_output_shapes
:�w
,tf.math.reduce_mean_6/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
����������
tf.math.reduce_mean_6/MeanMean2tf.math.squared_difference_3/SquaredDifference:z:05tf.math.reduce_mean_6/Mean/reduction_indices:output:0*
T0*
_output_shapes
: �
conv2d_18/Conv2D/ReadVariableOpReadVariableOp0encoder_conv2d_18_conv2d_readvariableop_resource*(
_output_shapes
:��*
dtype0�
conv2d_18/Conv2DConv2Dconv2d_17/Relu:activations:0'conv2d_18/Conv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
 conv2d_18/BiasAdd/ReadVariableOpReadVariableOp1encoder_conv2d_18_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
conv2d_18/BiasAddBiasAddconv2d_18/Conv2D:output:0(conv2d_18/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�d
conv2d_18/ReluReluconv2d_18/BiasAdd:output:0*
T0*'
_output_shapes
:�^
tf.math.multiply_12/Mul/yConst*
_output_shapes
: *
dtype0*
valueB
 * �(F�
tf.math.multiply_12/MulMul#tf.math.reduce_mean_6/Mean:output:0"tf.math.multiply_12/Mul/y:output:0*
T0*
_output_shapes
: `
flatten_3/ConstConst*
_output_shapes
:*
dtype0*
valueB"���� � 
flatten_3/ReshapeReshapeconv2d_18/Relu:activations:0flatten_3/Const:output:0*
T0* 
_output_shapes
:
��e
tf.math.multiply_14/MulMulunknowntf.math.multiply_12/Mul:z:0*
T0*
_output_shapes
: �
dense_10/MatMul/ReadVariableOpReadVariableOp/encoder_dense_10_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
dense_10/MatMulMatMulflatten_3/Reshape:output:0&dense_10/MatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:�
dense_10/BiasAdd/ReadVariableOpReadVariableOp0encoder_dense_10_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense_10/BiasAddBiasAdddense_10/MatMul:product:0'dense_10/BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:�
dense_9/MatMul/ReadVariableOpReadVariableOp.encoder_dense_9_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
dense_9/MatMulMatMulflatten_3/Reshape:output:0%dense_9/MatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:�
dense_9/BiasAdd/ReadVariableOpReadVariableOp/encoder_dense_9_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense_9/BiasAddBiasAdddense_9/MatMul:product:0&dense_9/BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:S
add_metric_7/RankConst*
_output_shapes
: *
dtype0*
value	B : Z
add_metric_7/range/startConst*
_output_shapes
: *
dtype0*
value	B : Z
add_metric_7/range/deltaConst*
_output_shapes
: *
dtype0*
value	B :�
add_metric_7/rangeRange!add_metric_7/range/start:output:0add_metric_7/Rank:output:0!add_metric_7/range/delta:output:0*
_output_shapes
: �
add_metric_7/SumSumtf.math.multiply_12/Mul:z:0add_metric_7/range:output:0*
T0*&
 _has_manual_control_dependencies(*
_output_shapes
: �
 add_metric_7/AssignAddVariableOpAssignAddVariableOp)add_metric_7_assignaddvariableop_resourceadd_metric_7/Sum:output:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0S
add_metric_7/SizeConst*
_output_shapes
: *
dtype0*
value	B :e
add_metric_7/CastCastadd_metric_7/Size:output:0*

DstT0*

SrcT0*
_output_shapes
: �
"add_metric_7/AssignAddVariableOp_1AssignAddVariableOp+add_metric_7_assignaddvariableop_1_resourceadd_metric_7/Cast:y:0!^add_metric_7/AssignAddVariableOp*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0�
&add_metric_7/div_no_nan/ReadVariableOpReadVariableOp)add_metric_7_assignaddvariableop_resource!^add_metric_7/AssignAddVariableOp#^add_metric_7/AssignAddVariableOp_1*
_output_shapes
: *
dtype0�
(add_metric_7/div_no_nan/ReadVariableOp_1ReadVariableOp+add_metric_7_assignaddvariableop_1_resource#^add_metric_7/AssignAddVariableOp_1*
_output_shapes
: *
dtype0�
add_metric_7/div_no_nanDivNoNan.add_metric_7/div_no_nan/ReadVariableOp:value:00add_metric_7/div_no_nan/ReadVariableOp_1:value:0*
T0*
_output_shapes
: _
add_metric_7/IdentityIdentityadd_metric_7/div_no_nan:z:0*
T0*
_output_shapes
: t
tf.__operators__.add_6/AddV2AddV2	unknown_0dense_10/BiasAdd:output:0*
T0*
_output_shapes

:d
tf.math.square_3/SquareSquaredense_9/BiasAdd:output:0*
T0*
_output_shapes

:\
tf.math.exp_3/ExpExpdense_10/BiasAdd:output:0*
T0*
_output_shapes

:�
tf.math.subtract_6/SubSub tf.__operators__.add_6/AddV2:z:0tf.math.square_3/Square:y:0*
T0*
_output_shapes

:y
tf.math.subtract_7/SubSubtf.math.subtract_6/Sub:z:0tf.math.exp_3/Exp:y:0*
T0*
_output_shapes

:l
*tf.math.reduce_sum_3/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
tf.math.reduce_sum_3/SumSumtf.math.subtract_7/Sub:z:03tf.math.reduce_sum_3/Sum/reduction_indices:output:0*
T0*
_output_shapes
:q
tf.math.multiply_13/MulMul	unknown_1!tf.math.reduce_sum_3/Sum:output:0*
T0*
_output_shapes
:k
tf.math.multiply_15/MulMul	unknown_2tf.math.multiply_13/Mul:z:0*
T0*
_output_shapes
:�
tf.__operators__.add_7/AddV2AddV2tf.math.multiply_14/Mul:z:0tf.math.multiply_15/Mul:z:0*
T0*
_output_shapes
:e
tf.math.reduce_mean_7/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
tf.math.reduce_mean_7/MeanMean tf.__operators__.add_7/AddV2:z:0$tf.math.reduce_mean_7/Const:output:0*
T0*
_output_shapes
: S
add_metric_6/RankConst*
_output_shapes
: *
dtype0*
value	B : Z
add_metric_6/range/startConst*
_output_shapes
: *
dtype0*
value	B : Z
add_metric_6/range/deltaConst*
_output_shapes
: *
dtype0*
value	B :�
add_metric_6/rangeRange!add_metric_6/range/start:output:0add_metric_6/Rank:output:0!add_metric_6/range/delta:output:0*
_output_shapes
: �
add_metric_6/SumSum#tf.math.reduce_mean_7/Mean:output:0add_metric_6/range:output:0*
T0*&
 _has_manual_control_dependencies(*
_output_shapes
: �
 add_metric_6/AssignAddVariableOpAssignAddVariableOp)add_metric_6_assignaddvariableop_resourceadd_metric_6/Sum:output:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0S
add_metric_6/SizeConst*
_output_shapes
: *
dtype0*
value	B :e
add_metric_6/CastCastadd_metric_6/Size:output:0*

DstT0*

SrcT0*
_output_shapes
: �
"add_metric_6/AssignAddVariableOp_1AssignAddVariableOp+add_metric_6_assignaddvariableop_1_resourceadd_metric_6/Cast:y:0!^add_metric_6/AssignAddVariableOp*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0�
&add_metric_6/div_no_nan/ReadVariableOpReadVariableOp)add_metric_6_assignaddvariableop_resource!^add_metric_6/AssignAddVariableOp#^add_metric_6/AssignAddVariableOp_1*
_output_shapes
: *
dtype0�
(add_metric_6/div_no_nan/ReadVariableOp_1ReadVariableOp+add_metric_6_assignaddvariableop_1_resource#^add_metric_6/AssignAddVariableOp_1*
_output_shapes
: *
dtype0�
add_metric_6/div_no_nanDivNoNan.add_metric_6/div_no_nan/ReadVariableOp:value:00add_metric_6/div_no_nan/ReadVariableOp_1:value:0*
T0*
_output_shapes
: _
add_metric_6/IdentityIdentityadd_metric_6/div_no_nan:z:0*
T0*
_output_shapes
: k
IdentityIdentitydecoder/conv2d_19/Sigmoid:y:0^NoOp*
T0*&
_output_shapes
:<<c

Identity_1Identity#tf.math.reduce_mean_7/Mean:output:0^NoOp*
T0*
_output_shapes
: �
NoOpNoOp!^add_metric_6/AssignAddVariableOp#^add_metric_6/AssignAddVariableOp_1'^add_metric_6/div_no_nan/ReadVariableOp)^add_metric_6/div_no_nan/ReadVariableOp_1!^add_metric_7/AssignAddVariableOp#^add_metric_7/AssignAddVariableOp_1'^add_metric_7/div_no_nan/ReadVariableOp)^add_metric_7/div_no_nan/ReadVariableOp_1!^conv2d_15/BiasAdd/ReadVariableOp ^conv2d_15/Conv2D/ReadVariableOp!^conv2d_16/BiasAdd/ReadVariableOp ^conv2d_16/Conv2D/ReadVariableOp!^conv2d_17/BiasAdd/ReadVariableOp ^conv2d_17/Conv2D/ReadVariableOp!^conv2d_18/BiasAdd/ReadVariableOp ^conv2d_18/Conv2D/ReadVariableOp)^decoder/conv2d_19/BiasAdd/ReadVariableOp(^decoder/conv2d_19/Conv2D/ReadVariableOp3^decoder/conv2d_transpose_12/BiasAdd/ReadVariableOp<^decoder/conv2d_transpose_12/conv2d_transpose/ReadVariableOp3^decoder/conv2d_transpose_13/BiasAdd/ReadVariableOp<^decoder/conv2d_transpose_13/conv2d_transpose/ReadVariableOp3^decoder/conv2d_transpose_14/BiasAdd/ReadVariableOp<^decoder/conv2d_transpose_14/conv2d_transpose/ReadVariableOp3^decoder/conv2d_transpose_15/BiasAdd/ReadVariableOp<^decoder/conv2d_transpose_15/conv2d_transpose/ReadVariableOp(^decoder/dense_11/BiasAdd/ReadVariableOp'^decoder/dense_11/MatMul/ReadVariableOp ^dense_10/BiasAdd/ReadVariableOp^dense_10/MatMul/ReadVariableOp^dense_9/BiasAdd/ReadVariableOp^dense_9/MatMul/ReadVariableOp)^encoder/conv2d_15/BiasAdd/ReadVariableOp(^encoder/conv2d_15/Conv2D/ReadVariableOp)^encoder/conv2d_16/BiasAdd/ReadVariableOp(^encoder/conv2d_16/Conv2D/ReadVariableOp)^encoder/conv2d_17/BiasAdd/ReadVariableOp(^encoder/conv2d_17/Conv2D/ReadVariableOp)^encoder/conv2d_18/BiasAdd/ReadVariableOp(^encoder/conv2d_18/Conv2D/ReadVariableOp(^encoder/dense_10/BiasAdd/ReadVariableOp'^encoder/dense_10/MatMul/ReadVariableOp'^encoder/dense_9/BiasAdd/ReadVariableOp&^encoder/dense_9/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*e
_input_shapesT
R:<<: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2D
 add_metric_6/AssignAddVariableOp add_metric_6/AssignAddVariableOp2H
"add_metric_6/AssignAddVariableOp_1"add_metric_6/AssignAddVariableOp_12P
&add_metric_6/div_no_nan/ReadVariableOp&add_metric_6/div_no_nan/ReadVariableOp2T
(add_metric_6/div_no_nan/ReadVariableOp_1(add_metric_6/div_no_nan/ReadVariableOp_12D
 add_metric_7/AssignAddVariableOp add_metric_7/AssignAddVariableOp2H
"add_metric_7/AssignAddVariableOp_1"add_metric_7/AssignAddVariableOp_12P
&add_metric_7/div_no_nan/ReadVariableOp&add_metric_7/div_no_nan/ReadVariableOp2T
(add_metric_7/div_no_nan/ReadVariableOp_1(add_metric_7/div_no_nan/ReadVariableOp_12D
 conv2d_15/BiasAdd/ReadVariableOp conv2d_15/BiasAdd/ReadVariableOp2B
conv2d_15/Conv2D/ReadVariableOpconv2d_15/Conv2D/ReadVariableOp2D
 conv2d_16/BiasAdd/ReadVariableOp conv2d_16/BiasAdd/ReadVariableOp2B
conv2d_16/Conv2D/ReadVariableOpconv2d_16/Conv2D/ReadVariableOp2D
 conv2d_17/BiasAdd/ReadVariableOp conv2d_17/BiasAdd/ReadVariableOp2B
conv2d_17/Conv2D/ReadVariableOpconv2d_17/Conv2D/ReadVariableOp2D
 conv2d_18/BiasAdd/ReadVariableOp conv2d_18/BiasAdd/ReadVariableOp2B
conv2d_18/Conv2D/ReadVariableOpconv2d_18/Conv2D/ReadVariableOp2T
(decoder/conv2d_19/BiasAdd/ReadVariableOp(decoder/conv2d_19/BiasAdd/ReadVariableOp2R
'decoder/conv2d_19/Conv2D/ReadVariableOp'decoder/conv2d_19/Conv2D/ReadVariableOp2h
2decoder/conv2d_transpose_12/BiasAdd/ReadVariableOp2decoder/conv2d_transpose_12/BiasAdd/ReadVariableOp2z
;decoder/conv2d_transpose_12/conv2d_transpose/ReadVariableOp;decoder/conv2d_transpose_12/conv2d_transpose/ReadVariableOp2h
2decoder/conv2d_transpose_13/BiasAdd/ReadVariableOp2decoder/conv2d_transpose_13/BiasAdd/ReadVariableOp2z
;decoder/conv2d_transpose_13/conv2d_transpose/ReadVariableOp;decoder/conv2d_transpose_13/conv2d_transpose/ReadVariableOp2h
2decoder/conv2d_transpose_14/BiasAdd/ReadVariableOp2decoder/conv2d_transpose_14/BiasAdd/ReadVariableOp2z
;decoder/conv2d_transpose_14/conv2d_transpose/ReadVariableOp;decoder/conv2d_transpose_14/conv2d_transpose/ReadVariableOp2h
2decoder/conv2d_transpose_15/BiasAdd/ReadVariableOp2decoder/conv2d_transpose_15/BiasAdd/ReadVariableOp2z
;decoder/conv2d_transpose_15/conv2d_transpose/ReadVariableOp;decoder/conv2d_transpose_15/conv2d_transpose/ReadVariableOp2R
'decoder/dense_11/BiasAdd/ReadVariableOp'decoder/dense_11/BiasAdd/ReadVariableOp2P
&decoder/dense_11/MatMul/ReadVariableOp&decoder/dense_11/MatMul/ReadVariableOp2B
dense_10/BiasAdd/ReadVariableOpdense_10/BiasAdd/ReadVariableOp2@
dense_10/MatMul/ReadVariableOpdense_10/MatMul/ReadVariableOp2@
dense_9/BiasAdd/ReadVariableOpdense_9/BiasAdd/ReadVariableOp2>
dense_9/MatMul/ReadVariableOpdense_9/MatMul/ReadVariableOp2T
(encoder/conv2d_15/BiasAdd/ReadVariableOp(encoder/conv2d_15/BiasAdd/ReadVariableOp2R
'encoder/conv2d_15/Conv2D/ReadVariableOp'encoder/conv2d_15/Conv2D/ReadVariableOp2T
(encoder/conv2d_16/BiasAdd/ReadVariableOp(encoder/conv2d_16/BiasAdd/ReadVariableOp2R
'encoder/conv2d_16/Conv2D/ReadVariableOp'encoder/conv2d_16/Conv2D/ReadVariableOp2T
(encoder/conv2d_17/BiasAdd/ReadVariableOp(encoder/conv2d_17/BiasAdd/ReadVariableOp2R
'encoder/conv2d_17/Conv2D/ReadVariableOp'encoder/conv2d_17/Conv2D/ReadVariableOp2T
(encoder/conv2d_18/BiasAdd/ReadVariableOp(encoder/conv2d_18/BiasAdd/ReadVariableOp2R
'encoder/conv2d_18/Conv2D/ReadVariableOp'encoder/conv2d_18/Conv2D/ReadVariableOp2R
'encoder/dense_10/BiasAdd/ReadVariableOp'encoder/dense_10/BiasAdd/ReadVariableOp2P
&encoder/dense_10/MatMul/ReadVariableOp&encoder/dense_10/MatMul/ReadVariableOp2P
&encoder/dense_9/BiasAdd/ReadVariableOp&encoder/dense_9/BiasAdd/ReadVariableOp2N
%encoder/dense_9/MatMul/ReadVariableOp%encoder/dense_9/MatMul/ReadVariableOp:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
K
#__inference__update_step_xla_247566
gradient
variable: *
_XlaMustCompile(*(
_construction_contextkEagerRuntime*
_input_shapes

: : *
	_noinline(:D @

_output_shapes
: 
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�!
�
O__inference_conv2d_transpose_12_layer_call_and_return_conditional_losses_245454

inputsD
(conv2d_transpose_readvariableop_resource:��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�conv2d_transpose/ReadVariableOpI
ShapeShapeinputs*
T0*
_output_shapes
::��]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask_
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_2StridedSliceShape:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskG
mul/yConst*
_output_shapes
: *
dtype0*
value	B :U
mulMulstrided_slice_1:output:0mul/y:output:0*
T0*
_output_shapes
: I
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :Y
mul_1Mulstrided_slice_2:output:0mul_1/y:output:0*
T0*
_output_shapes
: J
stack/3Const*
_output_shapes
: *
dtype0*
value
B :�y
stackPackstrided_slice:output:0mul:z:0	mul_1:z:0stack/3:output:0*
N*
T0*
_output_shapes
:_
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_3StridedSlicestack:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
conv2d_transpose/ReadVariableOpReadVariableOp(conv2d_transpose_readvariableop_resource*(
_output_shapes
:��*
dtype0�
conv2d_transposeConv2DBackpropInputstack:output:0'conv2d_transpose/ReadVariableOp:value:0inputs*
T0*B
_output_shapes0
.:,����������������������������*
paddingSAME*
strides
s
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
BiasAddBiasAddconv2d_transpose:output:0BiasAdd/ReadVariableOp:value:0*
T0*B
_output_shapes0
.:,����������������������������k
ReluReluBiasAdd:output:0*
T0*B
_output_shapes0
.:,����������������������������|
IdentityIdentityRelu:activations:0^NoOp*
T0*B
_output_shapes0
.:,�����������������������������
NoOpNoOp^BiasAdd/ReadVariableOp ^conv2d_transpose/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*E
_input_shapes4
2:,����������������������������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2B
conv2d_transpose/ReadVariableOpconv2d_transpose/ReadVariableOp:j f
B
_output_shapes0
.:,����������������������������
 
_user_specified_nameinputs
�
Y
#__inference__update_step_xla_247541
gradient$
variable:��*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*)
_input_shapes
:��: *
	_noinline(:R N
(
_output_shapes
:��
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�

�
E__inference_conv2d_18_layer_call_and_return_conditional_losses_244998

inputs:
conv2d_readvariableop_resource:��.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp~
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*(
_output_shapes
:��*
dtype0�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
s
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0u
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�P
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:�a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:�w
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:�: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:O K
'
_output_shapes
:�
 
_user_specified_nameinputs
�
W
#__inference__update_step_xla_247481
gradient"
variable: *
_XlaMustCompile(*(
_construction_contextkEagerRuntime*'
_input_shapes
: : *
	_noinline(:P L
&
_output_shapes
: 
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�
�
$__inference_vae_layer_call_fn_246958

inputs!
unknown:
	unknown_0:#
	unknown_1: 
	unknown_2: $
	unknown_3: �
	unknown_4:	�%
	unknown_5:��
	unknown_6:	�
	unknown_7:
��
	unknown_8:
	unknown_9:
��

unknown_10:

unknown_11:
��

unknown_12:
��&

unknown_13:��

unknown_14:	�%

unknown_15:@�

unknown_16:@$

unknown_17: @

unknown_18: $

unknown_19: 

unknown_20:$

unknown_21:

unknown_22:

unknown_23

unknown_24: 

unknown_25: 

unknown_26

unknown_27

unknown_28

unknown_29: 

unknown_30: 
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30*,
Tin%
#2!*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:<<: *:
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *H
fCRA
?__inference_vae_layer_call_and_return_conditional_losses_246510n
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*e
_input_shapesT
R:<<: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
X
#__inference__update_step_xla_247491
gradient#
variable: �*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*(
_input_shapes
: �: *
	_noinline(:Q M
'
_output_shapes
: �
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�	
�
D__inference_dense_10_layer_call_and_return_conditional_losses_248166

inputs2
matmul_readvariableop_resource:
��-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpv
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0`
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0m
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:V
IdentityIdentityBiasAdd:output:0^NoOp*
T0*
_output_shapes

:w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*#
_input_shapes
:
��: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:H D
 
_output_shapes
:
��
 
_user_specified_nameinputs
�

�
E__inference_conv2d_17_layer_call_and_return_conditional_losses_244981

inputs9
conv2d_readvariableop_resource: �.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp}
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*'
_output_shapes
: �*
dtype0�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
s
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0u
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�P
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:�a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:�w
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
: : : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:N J
&
_output_shapes
: 
 
_user_specified_nameinputs
��
�
C__inference_decoder_layer_call_and_return_conditional_losses_247948

inputs;
'dense_11_matmul_readvariableop_resource:
��8
(dense_11_biasadd_readvariableop_resource:
��X
<conv2d_transpose_12_conv2d_transpose_readvariableop_resource:��B
3conv2d_transpose_12_biasadd_readvariableop_resource:	�W
<conv2d_transpose_13_conv2d_transpose_readvariableop_resource:@�A
3conv2d_transpose_13_biasadd_readvariableop_resource:@V
<conv2d_transpose_14_conv2d_transpose_readvariableop_resource: @A
3conv2d_transpose_14_biasadd_readvariableop_resource: V
<conv2d_transpose_15_conv2d_transpose_readvariableop_resource: A
3conv2d_transpose_15_biasadd_readvariableop_resource:B
(conv2d_19_conv2d_readvariableop_resource:7
)conv2d_19_biasadd_readvariableop_resource:
identity�� conv2d_19/BiasAdd/ReadVariableOp�conv2d_19/Conv2D/ReadVariableOp�*conv2d_transpose_12/BiasAdd/ReadVariableOp�3conv2d_transpose_12/conv2d_transpose/ReadVariableOp�*conv2d_transpose_13/BiasAdd/ReadVariableOp�3conv2d_transpose_13/conv2d_transpose/ReadVariableOp�*conv2d_transpose_14/BiasAdd/ReadVariableOp�3conv2d_transpose_14/conv2d_transpose/ReadVariableOp�*conv2d_transpose_15/BiasAdd/ReadVariableOp�3conv2d_transpose_15/conv2d_transpose/ReadVariableOp�dense_11/BiasAdd/ReadVariableOp�dense_11/MatMul/ReadVariableOp�
dense_11/MatMul/ReadVariableOpReadVariableOp'dense_11_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0t
dense_11/MatMulMatMulinputs&dense_11/MatMul/ReadVariableOp:value:0*
T0* 
_output_shapes
:
���
dense_11/BiasAdd/ReadVariableOpReadVariableOp(dense_11_biasadd_readvariableop_resource*
_output_shapes

:��*
dtype0�
dense_11/BiasAddBiasAdddense_11/MatMul:product:0'dense_11/BiasAdd/ReadVariableOp:value:0*
T0* 
_output_shapes
:
��[
dense_11/ReluReludense_11/BiasAdd:output:0*
T0* 
_output_shapes
:
��`
reshape_3/ShapeConst*
_output_shapes
:*
dtype0*
valueB"    � g
reshape_3/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: i
reshape_3/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:i
reshape_3/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
reshape_3/strided_sliceStridedSlicereshape_3/Shape:output:0&reshape_3/strided_slice/stack:output:0(reshape_3/strided_slice/stack_1:output:0(reshape_3/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask[
reshape_3/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :[
reshape_3/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :\
reshape_3/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value
B :��
reshape_3/Reshape/shapePack reshape_3/strided_slice:output:0"reshape_3/Reshape/shape/1:output:0"reshape_3/Reshape/shape/2:output:0"reshape_3/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:�
reshape_3/ReshapeReshapedense_11/Relu:activations:0 reshape_3/Reshape/shape:output:0*
T0*'
_output_shapes
:�r
conv2d_transpose_12/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"            q
'conv2d_transpose_12/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: s
)conv2d_transpose_12/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:s
)conv2d_transpose_12/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
!conv2d_transpose_12/strided_sliceStridedSlice"conv2d_transpose_12/Shape:output:00conv2d_transpose_12/strided_slice/stack:output:02conv2d_transpose_12/strided_slice/stack_1:output:02conv2d_transpose_12/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask]
conv2d_transpose_12/stack/1Const*
_output_shapes
: *
dtype0*
value	B :]
conv2d_transpose_12/stack/2Const*
_output_shapes
: *
dtype0*
value	B :^
conv2d_transpose_12/stack/3Const*
_output_shapes
: *
dtype0*
value
B :��
conv2d_transpose_12/stackPack*conv2d_transpose_12/strided_slice:output:0$conv2d_transpose_12/stack/1:output:0$conv2d_transpose_12/stack/2:output:0$conv2d_transpose_12/stack/3:output:0*
N*
T0*
_output_shapes
:s
)conv2d_transpose_12/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: u
+conv2d_transpose_12/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:u
+conv2d_transpose_12/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
#conv2d_transpose_12/strided_slice_1StridedSlice"conv2d_transpose_12/stack:output:02conv2d_transpose_12/strided_slice_1/stack:output:04conv2d_transpose_12/strided_slice_1/stack_1:output:04conv2d_transpose_12/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
3conv2d_transpose_12/conv2d_transpose/ReadVariableOpReadVariableOp<conv2d_transpose_12_conv2d_transpose_readvariableop_resource*(
_output_shapes
:��*
dtype0�
$conv2d_transpose_12/conv2d_transposeConv2DBackpropInput"conv2d_transpose_12/stack:output:0;conv2d_transpose_12/conv2d_transpose/ReadVariableOp:value:0reshape_3/Reshape:output:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
*conv2d_transpose_12/BiasAdd/ReadVariableOpReadVariableOp3conv2d_transpose_12_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
conv2d_transpose_12/BiasAddBiasAdd-conv2d_transpose_12/conv2d_transpose:output:02conv2d_transpose_12/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�x
conv2d_transpose_12/ReluRelu$conv2d_transpose_12/BiasAdd:output:0*
T0*'
_output_shapes
:�r
conv2d_transpose_13/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"         �   q
'conv2d_transpose_13/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: s
)conv2d_transpose_13/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:s
)conv2d_transpose_13/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
!conv2d_transpose_13/strided_sliceStridedSlice"conv2d_transpose_13/Shape:output:00conv2d_transpose_13/strided_slice/stack:output:02conv2d_transpose_13/strided_slice/stack_1:output:02conv2d_transpose_13/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask]
conv2d_transpose_13/stack/1Const*
_output_shapes
: *
dtype0*
value	B :<]
conv2d_transpose_13/stack/2Const*
_output_shapes
: *
dtype0*
value	B :<]
conv2d_transpose_13/stack/3Const*
_output_shapes
: *
dtype0*
value	B :@�
conv2d_transpose_13/stackPack*conv2d_transpose_13/strided_slice:output:0$conv2d_transpose_13/stack/1:output:0$conv2d_transpose_13/stack/2:output:0$conv2d_transpose_13/stack/3:output:0*
N*
T0*
_output_shapes
:s
)conv2d_transpose_13/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: u
+conv2d_transpose_13/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:u
+conv2d_transpose_13/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
#conv2d_transpose_13/strided_slice_1StridedSlice"conv2d_transpose_13/stack:output:02conv2d_transpose_13/strided_slice_1/stack:output:04conv2d_transpose_13/strided_slice_1/stack_1:output:04conv2d_transpose_13/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
3conv2d_transpose_13/conv2d_transpose/ReadVariableOpReadVariableOp<conv2d_transpose_13_conv2d_transpose_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
$conv2d_transpose_13/conv2d_transposeConv2DBackpropInput"conv2d_transpose_13/stack:output:0;conv2d_transpose_13/conv2d_transpose/ReadVariableOp:value:0&conv2d_transpose_12/Relu:activations:0*
T0*&
_output_shapes
:<<@*
paddingSAME*
strides
�
*conv2d_transpose_13/BiasAdd/ReadVariableOpReadVariableOp3conv2d_transpose_13_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype0�
conv2d_transpose_13/BiasAddBiasAdd-conv2d_transpose_13/conv2d_transpose:output:02conv2d_transpose_13/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<@w
conv2d_transpose_13/ReluRelu$conv2d_transpose_13/BiasAdd:output:0*
T0*&
_output_shapes
:<<@r
conv2d_transpose_14/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"   <   <   @   q
'conv2d_transpose_14/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: s
)conv2d_transpose_14/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:s
)conv2d_transpose_14/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
!conv2d_transpose_14/strided_sliceStridedSlice"conv2d_transpose_14/Shape:output:00conv2d_transpose_14/strided_slice/stack:output:02conv2d_transpose_14/strided_slice/stack_1:output:02conv2d_transpose_14/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask]
conv2d_transpose_14/stack/1Const*
_output_shapes
: *
dtype0*
value	B :<]
conv2d_transpose_14/stack/2Const*
_output_shapes
: *
dtype0*
value	B :<]
conv2d_transpose_14/stack/3Const*
_output_shapes
: *
dtype0*
value	B : �
conv2d_transpose_14/stackPack*conv2d_transpose_14/strided_slice:output:0$conv2d_transpose_14/stack/1:output:0$conv2d_transpose_14/stack/2:output:0$conv2d_transpose_14/stack/3:output:0*
N*
T0*
_output_shapes
:s
)conv2d_transpose_14/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: u
+conv2d_transpose_14/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:u
+conv2d_transpose_14/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
#conv2d_transpose_14/strided_slice_1StridedSlice"conv2d_transpose_14/stack:output:02conv2d_transpose_14/strided_slice_1/stack:output:04conv2d_transpose_14/strided_slice_1/stack_1:output:04conv2d_transpose_14/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
3conv2d_transpose_14/conv2d_transpose/ReadVariableOpReadVariableOp<conv2d_transpose_14_conv2d_transpose_readvariableop_resource*&
_output_shapes
: @*
dtype0�
$conv2d_transpose_14/conv2d_transposeConv2DBackpropInput"conv2d_transpose_14/stack:output:0;conv2d_transpose_14/conv2d_transpose/ReadVariableOp:value:0&conv2d_transpose_13/Relu:activations:0*
T0*&
_output_shapes
:<< *
paddingSAME*
strides
�
*conv2d_transpose_14/BiasAdd/ReadVariableOpReadVariableOp3conv2d_transpose_14_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
conv2d_transpose_14/BiasAddBiasAdd-conv2d_transpose_14/conv2d_transpose:output:02conv2d_transpose_14/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<< w
conv2d_transpose_14/ReluRelu$conv2d_transpose_14/BiasAdd:output:0*
T0*&
_output_shapes
:<< r
conv2d_transpose_15/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"   <   <       q
'conv2d_transpose_15/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: s
)conv2d_transpose_15/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:s
)conv2d_transpose_15/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
!conv2d_transpose_15/strided_sliceStridedSlice"conv2d_transpose_15/Shape:output:00conv2d_transpose_15/strided_slice/stack:output:02conv2d_transpose_15/strided_slice/stack_1:output:02conv2d_transpose_15/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask]
conv2d_transpose_15/stack/1Const*
_output_shapes
: *
dtype0*
value	B :<]
conv2d_transpose_15/stack/2Const*
_output_shapes
: *
dtype0*
value	B :<]
conv2d_transpose_15/stack/3Const*
_output_shapes
: *
dtype0*
value	B :�
conv2d_transpose_15/stackPack*conv2d_transpose_15/strided_slice:output:0$conv2d_transpose_15/stack/1:output:0$conv2d_transpose_15/stack/2:output:0$conv2d_transpose_15/stack/3:output:0*
N*
T0*
_output_shapes
:s
)conv2d_transpose_15/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: u
+conv2d_transpose_15/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:u
+conv2d_transpose_15/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
#conv2d_transpose_15/strided_slice_1StridedSlice"conv2d_transpose_15/stack:output:02conv2d_transpose_15/strided_slice_1/stack:output:04conv2d_transpose_15/strided_slice_1/stack_1:output:04conv2d_transpose_15/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
3conv2d_transpose_15/conv2d_transpose/ReadVariableOpReadVariableOp<conv2d_transpose_15_conv2d_transpose_readvariableop_resource*&
_output_shapes
: *
dtype0�
$conv2d_transpose_15/conv2d_transposeConv2DBackpropInput"conv2d_transpose_15/stack:output:0;conv2d_transpose_15/conv2d_transpose/ReadVariableOp:value:0&conv2d_transpose_14/Relu:activations:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
�
*conv2d_transpose_15/BiasAdd/ReadVariableOpReadVariableOp3conv2d_transpose_15_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
conv2d_transpose_15/BiasAddBiasAdd-conv2d_transpose_15/conv2d_transpose:output:02conv2d_transpose_15/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<w
conv2d_transpose_15/ReluRelu$conv2d_transpose_15/BiasAdd:output:0*
T0*&
_output_shapes
:<<�
conv2d_19/Conv2D/ReadVariableOpReadVariableOp(conv2d_19_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
conv2d_19/Conv2DConv2D&conv2d_transpose_15/Relu:activations:0'conv2d_19/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
�
 conv2d_19/BiasAdd/ReadVariableOpReadVariableOp)conv2d_19_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
conv2d_19/BiasAddBiasAddconv2d_19/Conv2D:output:0(conv2d_19/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<i
conv2d_19/SigmoidSigmoidconv2d_19/BiasAdd:output:0*
T0*&
_output_shapes
:<<c
IdentityIdentityconv2d_19/Sigmoid:y:0^NoOp*
T0*&
_output_shapes
:<<�
NoOpNoOp!^conv2d_19/BiasAdd/ReadVariableOp ^conv2d_19/Conv2D/ReadVariableOp+^conv2d_transpose_12/BiasAdd/ReadVariableOp4^conv2d_transpose_12/conv2d_transpose/ReadVariableOp+^conv2d_transpose_13/BiasAdd/ReadVariableOp4^conv2d_transpose_13/conv2d_transpose/ReadVariableOp+^conv2d_transpose_14/BiasAdd/ReadVariableOp4^conv2d_transpose_14/conv2d_transpose/ReadVariableOp+^conv2d_transpose_15/BiasAdd/ReadVariableOp4^conv2d_transpose_15/conv2d_transpose/ReadVariableOp ^dense_11/BiasAdd/ReadVariableOp^dense_11/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":: : : : : : : : : : : : 2D
 conv2d_19/BiasAdd/ReadVariableOp conv2d_19/BiasAdd/ReadVariableOp2B
conv2d_19/Conv2D/ReadVariableOpconv2d_19/Conv2D/ReadVariableOp2X
*conv2d_transpose_12/BiasAdd/ReadVariableOp*conv2d_transpose_12/BiasAdd/ReadVariableOp2j
3conv2d_transpose_12/conv2d_transpose/ReadVariableOp3conv2d_transpose_12/conv2d_transpose/ReadVariableOp2X
*conv2d_transpose_13/BiasAdd/ReadVariableOp*conv2d_transpose_13/BiasAdd/ReadVariableOp2j
3conv2d_transpose_13/conv2d_transpose/ReadVariableOp3conv2d_transpose_13/conv2d_transpose/ReadVariableOp2X
*conv2d_transpose_14/BiasAdd/ReadVariableOp*conv2d_transpose_14/BiasAdd/ReadVariableOp2j
3conv2d_transpose_14/conv2d_transpose/ReadVariableOp3conv2d_transpose_14/conv2d_transpose/ReadVariableOp2X
*conv2d_transpose_15/BiasAdd/ReadVariableOp*conv2d_transpose_15/BiasAdd/ReadVariableOp2j
3conv2d_transpose_15/conv2d_transpose/ReadVariableOp3conv2d_transpose_15/conv2d_transpose/ReadVariableOp2B
dense_11/BiasAdd/ReadVariableOpdense_11/BiasAdd/ReadVariableOp2@
dense_11/MatMul/ReadVariableOpdense_11/MatMul/ReadVariableOp:F B

_output_shapes

:
 
_user_specified_nameinputs
�
F
*__inference_reshape_3_layer_call_fn_248329

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_reshape_3_layer_call_and_return_conditional_losses_245634`
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:�"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
:
��:H D
 
_output_shapes
:
��
 
_user_specified_nameinputs
�

�
E__inference_conv2d_16_layer_call_and_return_conditional_losses_248096

inputs8
conv2d_readvariableop_resource: -
biasadd_readvariableop_resource: 
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp|
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
: *
dtype0�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
: *
paddingSAME*
strides
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
: *
dtype0t
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
: O
ReluReluBiasAdd:output:0*
T0*&
_output_shapes
: `
IdentityIdentityRelu:activations:0^NoOp*
T0*&
_output_shapes
: w
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:<<: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs
�'
�
C__inference_decoder_layer_call_and_return_conditional_losses_245747

inputs#
dense_11_245715:
��
dense_11_245717:
��6
conv2d_transpose_12_245721:��)
conv2d_transpose_12_245723:	�5
conv2d_transpose_13_245726:@�(
conv2d_transpose_13_245728:@4
conv2d_transpose_14_245731: @(
conv2d_transpose_14_245733: 4
conv2d_transpose_15_245736: (
conv2d_transpose_15_245738:*
conv2d_19_245741:
conv2d_19_245743:
identity��!conv2d_19/StatefulPartitionedCall�+conv2d_transpose_12/StatefulPartitionedCall�+conv2d_transpose_13/StatefulPartitionedCall�+conv2d_transpose_14/StatefulPartitionedCall�+conv2d_transpose_15/StatefulPartitionedCall� dense_11/StatefulPartitionedCall�
 dense_11/StatefulPartitionedCallStatefulPartitionedCallinputsdense_11_245715dense_11_245717*
Tin
2*
Tout
2*
_collective_manager_ids
 * 
_output_shapes
:
��*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dense_11_layer_call_and_return_conditional_losses_245614�
reshape_3/PartitionedCallPartitionedCall)dense_11/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_reshape_3_layer_call_and_return_conditional_losses_245634�
+conv2d_transpose_12/StatefulPartitionedCallStatefulPartitionedCall"reshape_3/PartitionedCall:output:0conv2d_transpose_12_245721conv2d_transpose_12_245723*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_12_layer_call_and_return_conditional_losses_245454�
+conv2d_transpose_13/StatefulPartitionedCallStatefulPartitionedCall4conv2d_transpose_12/StatefulPartitionedCall:output:0conv2d_transpose_13_245726conv2d_transpose_13_245728*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<@*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_13_layer_call_and_return_conditional_losses_245499�
+conv2d_transpose_14/StatefulPartitionedCallStatefulPartitionedCall4conv2d_transpose_13/StatefulPartitionedCall:output:0conv2d_transpose_14_245731conv2d_transpose_14_245733*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<< *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_14_layer_call_and_return_conditional_losses_245544�
+conv2d_transpose_15/StatefulPartitionedCallStatefulPartitionedCall4conv2d_transpose_14/StatefulPartitionedCall:output:0conv2d_transpose_15_245736conv2d_transpose_15_245738*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *X
fSRQ
O__inference_conv2d_transpose_15_layer_call_and_return_conditional_losses_245589�
!conv2d_19/StatefulPartitionedCallStatefulPartitionedCall4conv2d_transpose_15/StatefulPartitionedCall:output:0conv2d_19_245741conv2d_19_245743*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_19_layer_call_and_return_conditional_losses_245667x
IdentityIdentity*conv2d_19/StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<�
NoOpNoOp"^conv2d_19/StatefulPartitionedCall,^conv2d_transpose_12/StatefulPartitionedCall,^conv2d_transpose_13/StatefulPartitionedCall,^conv2d_transpose_14/StatefulPartitionedCall,^conv2d_transpose_15/StatefulPartitionedCall!^dense_11/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":: : : : : : : : : : : : 2F
!conv2d_19/StatefulPartitionedCall!conv2d_19/StatefulPartitionedCall2Z
+conv2d_transpose_12/StatefulPartitionedCall+conv2d_transpose_12/StatefulPartitionedCall2Z
+conv2d_transpose_13/StatefulPartitionedCall+conv2d_transpose_13/StatefulPartitionedCall2Z
+conv2d_transpose_14/StatefulPartitionedCall+conv2d_transpose_14/StatefulPartitionedCall2Z
+conv2d_transpose_15/StatefulPartitionedCall+conv2d_transpose_15/StatefulPartitionedCall2D
 dense_11/StatefulPartitionedCall dense_11/StatefulPartitionedCall:F B

_output_shapes

:
 
_user_specified_nameinputs
�
�
(__inference_decoder_layer_call_fn_247811

inputs
unknown:
��
	unknown_0:
��%
	unknown_1:��
	unknown_2:	�$
	unknown_3:@�
	unknown_4:@#
	unknown_5: @
	unknown_6: #
	unknown_7: 
	unknown_8:#
	unknown_9:

unknown_10:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_decoder_layer_call_and_return_conditional_losses_245747n
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":: : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:F B

_output_shapes

:
 
_user_specified_nameinputs
��
�
C__inference_decoder_layer_call_and_return_conditional_losses_248056

inputs;
'dense_11_matmul_readvariableop_resource:
��8
(dense_11_biasadd_readvariableop_resource:
��X
<conv2d_transpose_12_conv2d_transpose_readvariableop_resource:��B
3conv2d_transpose_12_biasadd_readvariableop_resource:	�W
<conv2d_transpose_13_conv2d_transpose_readvariableop_resource:@�A
3conv2d_transpose_13_biasadd_readvariableop_resource:@V
<conv2d_transpose_14_conv2d_transpose_readvariableop_resource: @A
3conv2d_transpose_14_biasadd_readvariableop_resource: V
<conv2d_transpose_15_conv2d_transpose_readvariableop_resource: A
3conv2d_transpose_15_biasadd_readvariableop_resource:B
(conv2d_19_conv2d_readvariableop_resource:7
)conv2d_19_biasadd_readvariableop_resource:
identity�� conv2d_19/BiasAdd/ReadVariableOp�conv2d_19/Conv2D/ReadVariableOp�*conv2d_transpose_12/BiasAdd/ReadVariableOp�3conv2d_transpose_12/conv2d_transpose/ReadVariableOp�*conv2d_transpose_13/BiasAdd/ReadVariableOp�3conv2d_transpose_13/conv2d_transpose/ReadVariableOp�*conv2d_transpose_14/BiasAdd/ReadVariableOp�3conv2d_transpose_14/conv2d_transpose/ReadVariableOp�*conv2d_transpose_15/BiasAdd/ReadVariableOp�3conv2d_transpose_15/conv2d_transpose/ReadVariableOp�dense_11/BiasAdd/ReadVariableOp�dense_11/MatMul/ReadVariableOp�
dense_11/MatMul/ReadVariableOpReadVariableOp'dense_11_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0t
dense_11/MatMulMatMulinputs&dense_11/MatMul/ReadVariableOp:value:0*
T0* 
_output_shapes
:
���
dense_11/BiasAdd/ReadVariableOpReadVariableOp(dense_11_biasadd_readvariableop_resource*
_output_shapes

:��*
dtype0�
dense_11/BiasAddBiasAdddense_11/MatMul:product:0'dense_11/BiasAdd/ReadVariableOp:value:0*
T0* 
_output_shapes
:
��[
dense_11/ReluReludense_11/BiasAdd:output:0*
T0* 
_output_shapes
:
��`
reshape_3/ShapeConst*
_output_shapes
:*
dtype0*
valueB"    � g
reshape_3/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: i
reshape_3/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:i
reshape_3/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
reshape_3/strided_sliceStridedSlicereshape_3/Shape:output:0&reshape_3/strided_slice/stack:output:0(reshape_3/strided_slice/stack_1:output:0(reshape_3/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask[
reshape_3/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :[
reshape_3/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :\
reshape_3/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value
B :��
reshape_3/Reshape/shapePack reshape_3/strided_slice:output:0"reshape_3/Reshape/shape/1:output:0"reshape_3/Reshape/shape/2:output:0"reshape_3/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:�
reshape_3/ReshapeReshapedense_11/Relu:activations:0 reshape_3/Reshape/shape:output:0*
T0*'
_output_shapes
:�r
conv2d_transpose_12/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"            q
'conv2d_transpose_12/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: s
)conv2d_transpose_12/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:s
)conv2d_transpose_12/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
!conv2d_transpose_12/strided_sliceStridedSlice"conv2d_transpose_12/Shape:output:00conv2d_transpose_12/strided_slice/stack:output:02conv2d_transpose_12/strided_slice/stack_1:output:02conv2d_transpose_12/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask]
conv2d_transpose_12/stack/1Const*
_output_shapes
: *
dtype0*
value	B :]
conv2d_transpose_12/stack/2Const*
_output_shapes
: *
dtype0*
value	B :^
conv2d_transpose_12/stack/3Const*
_output_shapes
: *
dtype0*
value
B :��
conv2d_transpose_12/stackPack*conv2d_transpose_12/strided_slice:output:0$conv2d_transpose_12/stack/1:output:0$conv2d_transpose_12/stack/2:output:0$conv2d_transpose_12/stack/3:output:0*
N*
T0*
_output_shapes
:s
)conv2d_transpose_12/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: u
+conv2d_transpose_12/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:u
+conv2d_transpose_12/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
#conv2d_transpose_12/strided_slice_1StridedSlice"conv2d_transpose_12/stack:output:02conv2d_transpose_12/strided_slice_1/stack:output:04conv2d_transpose_12/strided_slice_1/stack_1:output:04conv2d_transpose_12/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
3conv2d_transpose_12/conv2d_transpose/ReadVariableOpReadVariableOp<conv2d_transpose_12_conv2d_transpose_readvariableop_resource*(
_output_shapes
:��*
dtype0�
$conv2d_transpose_12/conv2d_transposeConv2DBackpropInput"conv2d_transpose_12/stack:output:0;conv2d_transpose_12/conv2d_transpose/ReadVariableOp:value:0reshape_3/Reshape:output:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
*conv2d_transpose_12/BiasAdd/ReadVariableOpReadVariableOp3conv2d_transpose_12_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
conv2d_transpose_12/BiasAddBiasAdd-conv2d_transpose_12/conv2d_transpose:output:02conv2d_transpose_12/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�x
conv2d_transpose_12/ReluRelu$conv2d_transpose_12/BiasAdd:output:0*
T0*'
_output_shapes
:�r
conv2d_transpose_13/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"         �   q
'conv2d_transpose_13/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: s
)conv2d_transpose_13/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:s
)conv2d_transpose_13/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
!conv2d_transpose_13/strided_sliceStridedSlice"conv2d_transpose_13/Shape:output:00conv2d_transpose_13/strided_slice/stack:output:02conv2d_transpose_13/strided_slice/stack_1:output:02conv2d_transpose_13/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask]
conv2d_transpose_13/stack/1Const*
_output_shapes
: *
dtype0*
value	B :<]
conv2d_transpose_13/stack/2Const*
_output_shapes
: *
dtype0*
value	B :<]
conv2d_transpose_13/stack/3Const*
_output_shapes
: *
dtype0*
value	B :@�
conv2d_transpose_13/stackPack*conv2d_transpose_13/strided_slice:output:0$conv2d_transpose_13/stack/1:output:0$conv2d_transpose_13/stack/2:output:0$conv2d_transpose_13/stack/3:output:0*
N*
T0*
_output_shapes
:s
)conv2d_transpose_13/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: u
+conv2d_transpose_13/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:u
+conv2d_transpose_13/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
#conv2d_transpose_13/strided_slice_1StridedSlice"conv2d_transpose_13/stack:output:02conv2d_transpose_13/strided_slice_1/stack:output:04conv2d_transpose_13/strided_slice_1/stack_1:output:04conv2d_transpose_13/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
3conv2d_transpose_13/conv2d_transpose/ReadVariableOpReadVariableOp<conv2d_transpose_13_conv2d_transpose_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
$conv2d_transpose_13/conv2d_transposeConv2DBackpropInput"conv2d_transpose_13/stack:output:0;conv2d_transpose_13/conv2d_transpose/ReadVariableOp:value:0&conv2d_transpose_12/Relu:activations:0*
T0*&
_output_shapes
:<<@*
paddingSAME*
strides
�
*conv2d_transpose_13/BiasAdd/ReadVariableOpReadVariableOp3conv2d_transpose_13_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype0�
conv2d_transpose_13/BiasAddBiasAdd-conv2d_transpose_13/conv2d_transpose:output:02conv2d_transpose_13/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<@w
conv2d_transpose_13/ReluRelu$conv2d_transpose_13/BiasAdd:output:0*
T0*&
_output_shapes
:<<@r
conv2d_transpose_14/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"   <   <   @   q
'conv2d_transpose_14/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: s
)conv2d_transpose_14/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:s
)conv2d_transpose_14/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
!conv2d_transpose_14/strided_sliceStridedSlice"conv2d_transpose_14/Shape:output:00conv2d_transpose_14/strided_slice/stack:output:02conv2d_transpose_14/strided_slice/stack_1:output:02conv2d_transpose_14/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask]
conv2d_transpose_14/stack/1Const*
_output_shapes
: *
dtype0*
value	B :<]
conv2d_transpose_14/stack/2Const*
_output_shapes
: *
dtype0*
value	B :<]
conv2d_transpose_14/stack/3Const*
_output_shapes
: *
dtype0*
value	B : �
conv2d_transpose_14/stackPack*conv2d_transpose_14/strided_slice:output:0$conv2d_transpose_14/stack/1:output:0$conv2d_transpose_14/stack/2:output:0$conv2d_transpose_14/stack/3:output:0*
N*
T0*
_output_shapes
:s
)conv2d_transpose_14/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: u
+conv2d_transpose_14/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:u
+conv2d_transpose_14/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
#conv2d_transpose_14/strided_slice_1StridedSlice"conv2d_transpose_14/stack:output:02conv2d_transpose_14/strided_slice_1/stack:output:04conv2d_transpose_14/strided_slice_1/stack_1:output:04conv2d_transpose_14/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
3conv2d_transpose_14/conv2d_transpose/ReadVariableOpReadVariableOp<conv2d_transpose_14_conv2d_transpose_readvariableop_resource*&
_output_shapes
: @*
dtype0�
$conv2d_transpose_14/conv2d_transposeConv2DBackpropInput"conv2d_transpose_14/stack:output:0;conv2d_transpose_14/conv2d_transpose/ReadVariableOp:value:0&conv2d_transpose_13/Relu:activations:0*
T0*&
_output_shapes
:<< *
paddingSAME*
strides
�
*conv2d_transpose_14/BiasAdd/ReadVariableOpReadVariableOp3conv2d_transpose_14_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
conv2d_transpose_14/BiasAddBiasAdd-conv2d_transpose_14/conv2d_transpose:output:02conv2d_transpose_14/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<< w
conv2d_transpose_14/ReluRelu$conv2d_transpose_14/BiasAdd:output:0*
T0*&
_output_shapes
:<< r
conv2d_transpose_15/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"   <   <       q
'conv2d_transpose_15/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: s
)conv2d_transpose_15/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:s
)conv2d_transpose_15/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
!conv2d_transpose_15/strided_sliceStridedSlice"conv2d_transpose_15/Shape:output:00conv2d_transpose_15/strided_slice/stack:output:02conv2d_transpose_15/strided_slice/stack_1:output:02conv2d_transpose_15/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask]
conv2d_transpose_15/stack/1Const*
_output_shapes
: *
dtype0*
value	B :<]
conv2d_transpose_15/stack/2Const*
_output_shapes
: *
dtype0*
value	B :<]
conv2d_transpose_15/stack/3Const*
_output_shapes
: *
dtype0*
value	B :�
conv2d_transpose_15/stackPack*conv2d_transpose_15/strided_slice:output:0$conv2d_transpose_15/stack/1:output:0$conv2d_transpose_15/stack/2:output:0$conv2d_transpose_15/stack/3:output:0*
N*
T0*
_output_shapes
:s
)conv2d_transpose_15/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: u
+conv2d_transpose_15/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:u
+conv2d_transpose_15/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
#conv2d_transpose_15/strided_slice_1StridedSlice"conv2d_transpose_15/stack:output:02conv2d_transpose_15/strided_slice_1/stack:output:04conv2d_transpose_15/strided_slice_1/stack_1:output:04conv2d_transpose_15/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
3conv2d_transpose_15/conv2d_transpose/ReadVariableOpReadVariableOp<conv2d_transpose_15_conv2d_transpose_readvariableop_resource*&
_output_shapes
: *
dtype0�
$conv2d_transpose_15/conv2d_transposeConv2DBackpropInput"conv2d_transpose_15/stack:output:0;conv2d_transpose_15/conv2d_transpose/ReadVariableOp:value:0&conv2d_transpose_14/Relu:activations:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
�
*conv2d_transpose_15/BiasAdd/ReadVariableOpReadVariableOp3conv2d_transpose_15_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
conv2d_transpose_15/BiasAddBiasAdd-conv2d_transpose_15/conv2d_transpose:output:02conv2d_transpose_15/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<w
conv2d_transpose_15/ReluRelu$conv2d_transpose_15/BiasAdd:output:0*
T0*&
_output_shapes
:<<�
conv2d_19/Conv2D/ReadVariableOpReadVariableOp(conv2d_19_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
conv2d_19/Conv2DConv2D&conv2d_transpose_15/Relu:activations:0'conv2d_19/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
�
 conv2d_19/BiasAdd/ReadVariableOpReadVariableOp)conv2d_19_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
conv2d_19/BiasAddBiasAddconv2d_19/Conv2D:output:0(conv2d_19/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<i
conv2d_19/SigmoidSigmoidconv2d_19/BiasAdd:output:0*
T0*&
_output_shapes
:<<c
IdentityIdentityconv2d_19/Sigmoid:y:0^NoOp*
T0*&
_output_shapes
:<<�
NoOpNoOp!^conv2d_19/BiasAdd/ReadVariableOp ^conv2d_19/Conv2D/ReadVariableOp+^conv2d_transpose_12/BiasAdd/ReadVariableOp4^conv2d_transpose_12/conv2d_transpose/ReadVariableOp+^conv2d_transpose_13/BiasAdd/ReadVariableOp4^conv2d_transpose_13/conv2d_transpose/ReadVariableOp+^conv2d_transpose_14/BiasAdd/ReadVariableOp4^conv2d_transpose_14/conv2d_transpose/ReadVariableOp+^conv2d_transpose_15/BiasAdd/ReadVariableOp4^conv2d_transpose_15/conv2d_transpose/ReadVariableOp ^dense_11/BiasAdd/ReadVariableOp^dense_11/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":: : : : : : : : : : : : 2D
 conv2d_19/BiasAdd/ReadVariableOp conv2d_19/BiasAdd/ReadVariableOp2B
conv2d_19/Conv2D/ReadVariableOpconv2d_19/Conv2D/ReadVariableOp2X
*conv2d_transpose_12/BiasAdd/ReadVariableOp*conv2d_transpose_12/BiasAdd/ReadVariableOp2j
3conv2d_transpose_12/conv2d_transpose/ReadVariableOp3conv2d_transpose_12/conv2d_transpose/ReadVariableOp2X
*conv2d_transpose_13/BiasAdd/ReadVariableOp*conv2d_transpose_13/BiasAdd/ReadVariableOp2j
3conv2d_transpose_13/conv2d_transpose/ReadVariableOp3conv2d_transpose_13/conv2d_transpose/ReadVariableOp2X
*conv2d_transpose_14/BiasAdd/ReadVariableOp*conv2d_transpose_14/BiasAdd/ReadVariableOp2j
3conv2d_transpose_14/conv2d_transpose/ReadVariableOp3conv2d_transpose_14/conv2d_transpose/ReadVariableOp2X
*conv2d_transpose_15/BiasAdd/ReadVariableOp*conv2d_transpose_15/BiasAdd/ReadVariableOp2j
3conv2d_transpose_15/conv2d_transpose/ReadVariableOp3conv2d_transpose_15/conv2d_transpose/ReadVariableOp2B
dense_11/BiasAdd/ReadVariableOpdense_11/BiasAdd/ReadVariableOp2@
dense_11/MatMul/ReadVariableOpdense_11/MatMul/ReadVariableOp:F B

_output_shapes

:
 
_user_specified_nameinputs
�
K
#__inference__update_step_xla_247526
gradient
variable:*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*
_input_shapes

:: *
	_noinline(:D @

_output_shapes
:
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�
Y
#__inference__update_step_xla_247501
gradient$
variable:��*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*)
_input_shapes
:��: *
	_noinline(:R N
(
_output_shapes
:��
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�
�
*__inference_conv2d_16_layer_call_fn_248085

inputs!
unknown: 
	unknown_0: 
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
: *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_16_layer_call_and_return_conditional_losses_244964n
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
: `
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:<<: : 22
StatefulPartitionedCallStatefulPartitionedCall:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs
�
s
D__inference_lambda_3_layer_call_and_return_conditional_losses_248304
inputs_0
inputs_1
identity�V
ShapeConst*
_output_shapes
:*
dtype0*
valueB"      ]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskW
random_normal/shape/1Const*
_output_shapes
: *
dtype0*
value	B :�
random_normal/shapePackstrided_slice:output:0random_normal/shape/1:output:0*
N*
T0*
_output_shapes
:W
random_normal/meanConst*
_output_shapes
: *
dtype0*
valueB
 *    Y
random_normal/stddevConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
"random_normal/RandomStandardNormalRandomStandardNormalrandom_normal/shape:output:0*
T0*
_output_shapes

:*
dtype0*
seed���)*
seed2�֋�
random_normal/mulMul+random_normal/RandomStandardNormal:output:0random_normal/stddev:output:0*
T0*
_output_shapes

:s
random_normalAddV2random_normal/mul:z:0random_normal/mean:output:0*
T0*
_output_shapes

:N
	truediv/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @Y
truedivRealDivinputs_1truediv/y:output:0*
T0*
_output_shapes

:@
ExpExptruediv:z:0*
T0*
_output_shapes

:O
mulMulExp:y:0random_normal:z:0*
T0*
_output_shapes

:H
addAddV2inputs_0mul:z:0*
T0*
_output_shapes

:F
IdentityIdentityadd:z:0*
T0*
_output_shapes

:"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:::H D

_output_shapes

:
"
_user_specified_name
inputs_0:HD

_output_shapes

:
"
_user_specified_name
inputs_1
�
�
H__inference_add_metric_6_layer_call_and_return_conditional_losses_246075

inputs&
assignaddvariableop_resource: (
assignaddvariableop_1_resource: 

identity_1��AssignAddVariableOp�AssignAddVariableOp_1�div_no_nan/ReadVariableOp�div_no_nan/ReadVariableOp_1F
RankConst*
_output_shapes
: *
dtype0*
value	B : M
range/startConst*
_output_shapes
: *
dtype0*
value	B : M
range/deltaConst*
_output_shapes
: *
dtype0*
value	B :c
rangeRangerange/start:output:0Rank:output:0range/delta:output:0*
_output_shapes
: k
SumSuminputsrange:output:0*
T0*&
 _has_manual_control_dependencies(*
_output_shapes
: �
AssignAddVariableOpAssignAddVariableOpassignaddvariableop_resourceSum:output:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0F
SizeConst*
_output_shapes
: *
dtype0*
value	B :K
CastCastSize:output:0*

DstT0*

SrcT0*
_output_shapes
: �
AssignAddVariableOp_1AssignAddVariableOpassignaddvariableop_1_resourceCast:y:0^AssignAddVariableOp*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0�
div_no_nan/ReadVariableOpReadVariableOpassignaddvariableop_resource^AssignAddVariableOp^AssignAddVariableOp_1*
_output_shapes
: *
dtype0�
div_no_nan/ReadVariableOp_1ReadVariableOpassignaddvariableop_1_resource^AssignAddVariableOp_1*
_output_shapes
: *
dtype0

div_no_nanDivNoNan!div_no_nan/ReadVariableOp:value:0#div_no_nan/ReadVariableOp_1:value:0*
T0*
_output_shapes
: E
IdentityIdentitydiv_no_nan:z:0*
T0*
_output_shapes
: F

Identity_1Identityinputs^NoOp*
T0*
_output_shapes
: �
NoOpNoOp^AssignAddVariableOp^AssignAddVariableOp_1^div_no_nan/ReadVariableOp^div_no_nan/ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: : : 2*
AssignAddVariableOpAssignAddVariableOp2.
AssignAddVariableOp_1AssignAddVariableOp_126
div_no_nan/ReadVariableOpdiv_no_nan/ReadVariableOp2:
div_no_nan/ReadVariableOp_1div_no_nan/ReadVariableOp_1:> :

_output_shapes
: 
 
_user_specified_nameinputs
�!
�
O__inference_conv2d_transpose_14_layer_call_and_return_conditional_losses_245544

inputsB
(conv2d_transpose_readvariableop_resource: @-
biasadd_readvariableop_resource: 
identity��BiasAdd/ReadVariableOp�conv2d_transpose/ReadVariableOpI
ShapeShapeinputs*
T0*
_output_shapes
::��]
strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: _
strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:_
strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_sliceStridedSliceShape:output:0strided_slice/stack:output:0strided_slice/stack_1:output:0strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask_
strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_1StridedSliceShape:output:0strided_slice_1/stack:output:0 strided_slice_1/stack_1:output:0 strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask_
strided_slice_2/stackConst*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_2/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_2/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_2StridedSliceShape:output:0strided_slice_2/stack:output:0 strided_slice_2/stack_1:output:0 strided_slice_2/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskG
mul/yConst*
_output_shapes
: *
dtype0*
value	B :U
mulMulstrided_slice_1:output:0mul/y:output:0*
T0*
_output_shapes
: I
mul_1/yConst*
_output_shapes
: *
dtype0*
value	B :Y
mul_1Mulstrided_slice_2:output:0mul_1/y:output:0*
T0*
_output_shapes
: I
stack/3Const*
_output_shapes
: *
dtype0*
value	B : y
stackPackstrided_slice:output:0mul:z:0	mul_1:z:0stack/3:output:0*
N*
T0*
_output_shapes
:_
strided_slice_3/stackConst*
_output_shapes
:*
dtype0*
valueB: a
strided_slice_3/stack_1Const*
_output_shapes
:*
dtype0*
valueB:a
strided_slice_3/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
strided_slice_3StridedSlicestack:output:0strided_slice_3/stack:output:0 strided_slice_3/stack_1:output:0 strided_slice_3/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
conv2d_transpose/ReadVariableOpReadVariableOp(conv2d_transpose_readvariableop_resource*&
_output_shapes
: @*
dtype0�
conv2d_transposeConv2DBackpropInputstack:output:0'conv2d_transpose/ReadVariableOp:value:0inputs*
T0*A
_output_shapes/
-:+��������������������������� *
paddingSAME*
strides
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
BiasAddBiasAddconv2d_transpose:output:0BiasAdd/ReadVariableOp:value:0*
T0*A
_output_shapes/
-:+��������������������������� j
ReluReluBiasAdd:output:0*
T0*A
_output_shapes/
-:+��������������������������� {
IdentityIdentityRelu:activations:0^NoOp*
T0*A
_output_shapes/
-:+��������������������������� �
NoOpNoOp^BiasAdd/ReadVariableOp ^conv2d_transpose/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*D
_input_shapes3
1:+���������������������������@: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2B
conv2d_transpose/ReadVariableOpconv2d_transpose/ReadVariableOp:i e
A
_output_shapes/
-:+���������������������������@
 
_user_specified_nameinputs
�
�
-__inference_add_metric_6_layer_call_fn_248205

inputs
unknown: 
	unknown_0: 
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_add_metric_6_layer_call_and_return_conditional_losses_246075^
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes
: `
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: : : 22
StatefulPartitionedCallStatefulPartitionedCall:> :

_output_shapes
: 
 
_user_specified_nameinputs
�
�
(__inference_encoder_layer_call_fn_245203
input_7!
unknown:
	unknown_0:#
	unknown_1: 
	unknown_2: $
	unknown_3: �
	unknown_4:	�%
	unknown_5:��
	unknown_6:	�
	unknown_7:
��
	unknown_8:
	unknown_9:
��

unknown_10:
identity

identity_1

identity_2��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_7unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10*
Tin
2*
Tout
2*
_collective_manager_ids
 *2
_output_shapes 
:::*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_encoder_layer_call_and_return_conditional_losses_245172f
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:h

Identity_1Identity StatefulPartitionedCall:output:1^NoOp*
T0*
_output_shapes

:h

Identity_2Identity StatefulPartitionedCall:output:2^NoOp*
T0*
_output_shapes

:`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*=
_input_shapes,
*:<<: : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
&
_output_shapes
:<<
!
_user_specified_name	input_7
�
W
#__inference__update_step_xla_247581
gradient"
variable:*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*'
_input_shapes
:: *
	_noinline(:P L
&
_output_shapes
:
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�
W
#__inference__update_step_xla_247571
gradient"
variable: *
_XlaMustCompile(*(
_construction_contextkEagerRuntime*'
_input_shapes
: : *
	_noinline(:P L
&
_output_shapes
: 
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�
�
(__inference_decoder_layer_call_fn_245774
input_8
unknown:
��
	unknown_0:
��%
	unknown_1:��
	unknown_2:	�$
	unknown_3:@�
	unknown_4:@#
	unknown_5: @
	unknown_6: #
	unknown_7: 
	unknown_8:#
	unknown_9:

unknown_10:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_8unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_decoder_layer_call_and_return_conditional_losses_245747n
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":: : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:G C

_output_shapes

:
!
_user_specified_name	input_8
�
K
#__inference__update_step_xla_247556
gradient
variable:@*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*
_input_shapes

:@: *
	_noinline(:D @

_output_shapes
:@
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�

�
E__inference_conv2d_19_layer_call_and_return_conditional_losses_245667

inputs8
conv2d_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp|
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0t
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<U
SigmoidSigmoidBiasAdd:output:0*
T0*&
_output_shapes
:<<Y
IdentityIdentitySigmoid:y:0^NoOp*
T0*&
_output_shapes
:<<w
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:<<: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs
��
�
?__inference_vae_layer_call_and_return_conditional_losses_247212

inputsJ
0encoder_conv2d_15_conv2d_readvariableop_resource:?
1encoder_conv2d_15_biasadd_readvariableop_resource:J
0encoder_conv2d_16_conv2d_readvariableop_resource: ?
1encoder_conv2d_16_biasadd_readvariableop_resource: K
0encoder_conv2d_17_conv2d_readvariableop_resource: �@
1encoder_conv2d_17_biasadd_readvariableop_resource:	�L
0encoder_conv2d_18_conv2d_readvariableop_resource:��@
1encoder_conv2d_18_biasadd_readvariableop_resource:	�B
.encoder_dense_9_matmul_readvariableop_resource:
��=
/encoder_dense_9_biasadd_readvariableop_resource:C
/encoder_dense_10_matmul_readvariableop_resource:
��>
0encoder_dense_10_biasadd_readvariableop_resource:C
/decoder_dense_11_matmul_readvariableop_resource:
��@
0decoder_dense_11_biasadd_readvariableop_resource:
��`
Ddecoder_conv2d_transpose_12_conv2d_transpose_readvariableop_resource:��J
;decoder_conv2d_transpose_12_biasadd_readvariableop_resource:	�_
Ddecoder_conv2d_transpose_13_conv2d_transpose_readvariableop_resource:@�I
;decoder_conv2d_transpose_13_biasadd_readvariableop_resource:@^
Ddecoder_conv2d_transpose_14_conv2d_transpose_readvariableop_resource: @I
;decoder_conv2d_transpose_14_biasadd_readvariableop_resource: ^
Ddecoder_conv2d_transpose_15_conv2d_transpose_readvariableop_resource: I
;decoder_conv2d_transpose_15_biasadd_readvariableop_resource:J
0decoder_conv2d_19_conv2d_readvariableop_resource:?
1decoder_conv2d_19_biasadd_readvariableop_resource:
unknown3
)add_metric_7_assignaddvariableop_resource: 5
+add_metric_7_assignaddvariableop_1_resource: 
	unknown_0
	unknown_1
	unknown_23
)add_metric_6_assignaddvariableop_resource: 5
+add_metric_6_assignaddvariableop_1_resource: 
identity

identity_1�� add_metric_6/AssignAddVariableOp�"add_metric_6/AssignAddVariableOp_1�&add_metric_6/div_no_nan/ReadVariableOp�(add_metric_6/div_no_nan/ReadVariableOp_1� add_metric_7/AssignAddVariableOp�"add_metric_7/AssignAddVariableOp_1�&add_metric_7/div_no_nan/ReadVariableOp�(add_metric_7/div_no_nan/ReadVariableOp_1� conv2d_15/BiasAdd/ReadVariableOp�conv2d_15/Conv2D/ReadVariableOp� conv2d_16/BiasAdd/ReadVariableOp�conv2d_16/Conv2D/ReadVariableOp� conv2d_17/BiasAdd/ReadVariableOp�conv2d_17/Conv2D/ReadVariableOp� conv2d_18/BiasAdd/ReadVariableOp�conv2d_18/Conv2D/ReadVariableOp�(decoder/conv2d_19/BiasAdd/ReadVariableOp�'decoder/conv2d_19/Conv2D/ReadVariableOp�2decoder/conv2d_transpose_12/BiasAdd/ReadVariableOp�;decoder/conv2d_transpose_12/conv2d_transpose/ReadVariableOp�2decoder/conv2d_transpose_13/BiasAdd/ReadVariableOp�;decoder/conv2d_transpose_13/conv2d_transpose/ReadVariableOp�2decoder/conv2d_transpose_14/BiasAdd/ReadVariableOp�;decoder/conv2d_transpose_14/conv2d_transpose/ReadVariableOp�2decoder/conv2d_transpose_15/BiasAdd/ReadVariableOp�;decoder/conv2d_transpose_15/conv2d_transpose/ReadVariableOp�'decoder/dense_11/BiasAdd/ReadVariableOp�&decoder/dense_11/MatMul/ReadVariableOp�dense_10/BiasAdd/ReadVariableOp�dense_10/MatMul/ReadVariableOp�dense_9/BiasAdd/ReadVariableOp�dense_9/MatMul/ReadVariableOp�(encoder/conv2d_15/BiasAdd/ReadVariableOp�'encoder/conv2d_15/Conv2D/ReadVariableOp�(encoder/conv2d_16/BiasAdd/ReadVariableOp�'encoder/conv2d_16/Conv2D/ReadVariableOp�(encoder/conv2d_17/BiasAdd/ReadVariableOp�'encoder/conv2d_17/Conv2D/ReadVariableOp�(encoder/conv2d_18/BiasAdd/ReadVariableOp�'encoder/conv2d_18/Conv2D/ReadVariableOp�'encoder/dense_10/BiasAdd/ReadVariableOp�&encoder/dense_10/MatMul/ReadVariableOp�&encoder/dense_9/BiasAdd/ReadVariableOp�%encoder/dense_9/MatMul/ReadVariableOp�
'encoder/conv2d_15/Conv2D/ReadVariableOpReadVariableOp0encoder_conv2d_15_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
encoder/conv2d_15/Conv2DConv2Dinputs/encoder/conv2d_15/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
�
(encoder/conv2d_15/BiasAdd/ReadVariableOpReadVariableOp1encoder_conv2d_15_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
encoder/conv2d_15/BiasAddBiasAdd!encoder/conv2d_15/Conv2D:output:00encoder/conv2d_15/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<s
encoder/conv2d_15/ReluRelu"encoder/conv2d_15/BiasAdd:output:0*
T0*&
_output_shapes
:<<�
'encoder/conv2d_16/Conv2D/ReadVariableOpReadVariableOp0encoder_conv2d_16_conv2d_readvariableop_resource*&
_output_shapes
: *
dtype0�
encoder/conv2d_16/Conv2DConv2D$encoder/conv2d_15/Relu:activations:0/encoder/conv2d_16/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
: *
paddingSAME*
strides
�
(encoder/conv2d_16/BiasAdd/ReadVariableOpReadVariableOp1encoder_conv2d_16_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
encoder/conv2d_16/BiasAddBiasAdd!encoder/conv2d_16/Conv2D:output:00encoder/conv2d_16/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
: s
encoder/conv2d_16/ReluRelu"encoder/conv2d_16/BiasAdd:output:0*
T0*&
_output_shapes
: �
'encoder/conv2d_17/Conv2D/ReadVariableOpReadVariableOp0encoder_conv2d_17_conv2d_readvariableop_resource*'
_output_shapes
: �*
dtype0�
encoder/conv2d_17/Conv2DConv2D$encoder/conv2d_16/Relu:activations:0/encoder/conv2d_17/Conv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
(encoder/conv2d_17/BiasAdd/ReadVariableOpReadVariableOp1encoder_conv2d_17_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
encoder/conv2d_17/BiasAddBiasAdd!encoder/conv2d_17/Conv2D:output:00encoder/conv2d_17/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�t
encoder/conv2d_17/ReluRelu"encoder/conv2d_17/BiasAdd:output:0*
T0*'
_output_shapes
:��
'encoder/conv2d_18/Conv2D/ReadVariableOpReadVariableOp0encoder_conv2d_18_conv2d_readvariableop_resource*(
_output_shapes
:��*
dtype0�
encoder/conv2d_18/Conv2DConv2D$encoder/conv2d_17/Relu:activations:0/encoder/conv2d_18/Conv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
(encoder/conv2d_18/BiasAdd/ReadVariableOpReadVariableOp1encoder_conv2d_18_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
encoder/conv2d_18/BiasAddBiasAdd!encoder/conv2d_18/Conv2D:output:00encoder/conv2d_18/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�t
encoder/conv2d_18/ReluRelu"encoder/conv2d_18/BiasAdd:output:0*
T0*'
_output_shapes
:�h
encoder/flatten_3/ConstConst*
_output_shapes
:*
dtype0*
valueB"���� � �
encoder/flatten_3/ReshapeReshape$encoder/conv2d_18/Relu:activations:0 encoder/flatten_3/Const:output:0*
T0* 
_output_shapes
:
���
%encoder/dense_9/MatMul/ReadVariableOpReadVariableOp.encoder_dense_9_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
encoder/dense_9/MatMulMatMul"encoder/flatten_3/Reshape:output:0-encoder/dense_9/MatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:�
&encoder/dense_9/BiasAdd/ReadVariableOpReadVariableOp/encoder_dense_9_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
encoder/dense_9/BiasAddBiasAdd encoder/dense_9/MatMul:product:0.encoder/dense_9/BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:�
&encoder/dense_10/MatMul/ReadVariableOpReadVariableOp/encoder_dense_10_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
encoder/dense_10/MatMulMatMul"encoder/flatten_3/Reshape:output:0.encoder/dense_10/MatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:�
'encoder/dense_10/BiasAdd/ReadVariableOpReadVariableOp0encoder_dense_10_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
encoder/dense_10/BiasAddBiasAdd!encoder/dense_10/MatMul:product:0/encoder/dense_10/BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:g
encoder/lambda_3/ShapeConst*
_output_shapes
:*
dtype0*
valueB"      n
$encoder/lambda_3/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: p
&encoder/lambda_3/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:p
&encoder/lambda_3/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
encoder/lambda_3/strided_sliceStridedSliceencoder/lambda_3/Shape:output:0-encoder/lambda_3/strided_slice/stack:output:0/encoder/lambda_3/strided_slice/stack_1:output:0/encoder/lambda_3/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskh
&encoder/lambda_3/random_normal/shape/1Const*
_output_shapes
: *
dtype0*
value	B :�
$encoder/lambda_3/random_normal/shapePack'encoder/lambda_3/strided_slice:output:0/encoder/lambda_3/random_normal/shape/1:output:0*
N*
T0*
_output_shapes
:h
#encoder/lambda_3/random_normal/meanConst*
_output_shapes
: *
dtype0*
valueB
 *    j
%encoder/lambda_3/random_normal/stddevConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
3encoder/lambda_3/random_normal/RandomStandardNormalRandomStandardNormal-encoder/lambda_3/random_normal/shape:output:0*
T0*
_output_shapes

:*
dtype0*
seed���)*
seed2����
"encoder/lambda_3/random_normal/mulMul<encoder/lambda_3/random_normal/RandomStandardNormal:output:0.encoder/lambda_3/random_normal/stddev:output:0*
T0*
_output_shapes

:�
encoder/lambda_3/random_normalAddV2&encoder/lambda_3/random_normal/mul:z:0,encoder/lambda_3/random_normal/mean:output:0*
T0*
_output_shapes

:_
encoder/lambda_3/truediv/yConst*
_output_shapes
: *
dtype0*
valueB
 *   @�
encoder/lambda_3/truedivRealDiv!encoder/dense_10/BiasAdd:output:0#encoder/lambda_3/truediv/y:output:0*
T0*
_output_shapes

:b
encoder/lambda_3/ExpExpencoder/lambda_3/truediv:z:0*
T0*
_output_shapes

:�
encoder/lambda_3/mulMulencoder/lambda_3/Exp:y:0"encoder/lambda_3/random_normal:z:0*
T0*
_output_shapes

:�
encoder/lambda_3/addAddV2 encoder/dense_9/BiasAdd:output:0encoder/lambda_3/mul:z:0*
T0*
_output_shapes

:�
&decoder/dense_11/MatMul/ReadVariableOpReadVariableOp/decoder_dense_11_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
decoder/dense_11/MatMulMatMulencoder/lambda_3/add:z:0.decoder/dense_11/MatMul/ReadVariableOp:value:0*
T0* 
_output_shapes
:
���
'decoder/dense_11/BiasAdd/ReadVariableOpReadVariableOp0decoder_dense_11_biasadd_readvariableop_resource*
_output_shapes

:��*
dtype0�
decoder/dense_11/BiasAddBiasAdd!decoder/dense_11/MatMul:product:0/decoder/dense_11/BiasAdd/ReadVariableOp:value:0*
T0* 
_output_shapes
:
��k
decoder/dense_11/ReluRelu!decoder/dense_11/BiasAdd:output:0*
T0* 
_output_shapes
:
��h
decoder/reshape_3/ShapeConst*
_output_shapes
:*
dtype0*
valueB"    � o
%decoder/reshape_3/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: q
'decoder/reshape_3/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:q
'decoder/reshape_3/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
decoder/reshape_3/strided_sliceStridedSlice decoder/reshape_3/Shape:output:0.decoder/reshape_3/strided_slice/stack:output:00decoder/reshape_3/strided_slice/stack_1:output:00decoder/reshape_3/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maskc
!decoder/reshape_3/Reshape/shape/1Const*
_output_shapes
: *
dtype0*
value	B :c
!decoder/reshape_3/Reshape/shape/2Const*
_output_shapes
: *
dtype0*
value	B :d
!decoder/reshape_3/Reshape/shape/3Const*
_output_shapes
: *
dtype0*
value
B :��
decoder/reshape_3/Reshape/shapePack(decoder/reshape_3/strided_slice:output:0*decoder/reshape_3/Reshape/shape/1:output:0*decoder/reshape_3/Reshape/shape/2:output:0*decoder/reshape_3/Reshape/shape/3:output:0*
N*
T0*
_output_shapes
:�
decoder/reshape_3/ReshapeReshape#decoder/dense_11/Relu:activations:0(decoder/reshape_3/Reshape/shape:output:0*
T0*'
_output_shapes
:�z
!decoder/conv2d_transpose_12/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"            y
/decoder/conv2d_transpose_12/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: {
1decoder/conv2d_transpose_12/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:{
1decoder/conv2d_transpose_12/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
)decoder/conv2d_transpose_12/strided_sliceStridedSlice*decoder/conv2d_transpose_12/Shape:output:08decoder/conv2d_transpose_12/strided_slice/stack:output:0:decoder/conv2d_transpose_12/strided_slice/stack_1:output:0:decoder/conv2d_transpose_12/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maske
#decoder/conv2d_transpose_12/stack/1Const*
_output_shapes
: *
dtype0*
value	B :e
#decoder/conv2d_transpose_12/stack/2Const*
_output_shapes
: *
dtype0*
value	B :f
#decoder/conv2d_transpose_12/stack/3Const*
_output_shapes
: *
dtype0*
value
B :��
!decoder/conv2d_transpose_12/stackPack2decoder/conv2d_transpose_12/strided_slice:output:0,decoder/conv2d_transpose_12/stack/1:output:0,decoder/conv2d_transpose_12/stack/2:output:0,decoder/conv2d_transpose_12/stack/3:output:0*
N*
T0*
_output_shapes
:{
1decoder/conv2d_transpose_12/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: }
3decoder/conv2d_transpose_12/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:}
3decoder/conv2d_transpose_12/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
+decoder/conv2d_transpose_12/strided_slice_1StridedSlice*decoder/conv2d_transpose_12/stack:output:0:decoder/conv2d_transpose_12/strided_slice_1/stack:output:0<decoder/conv2d_transpose_12/strided_slice_1/stack_1:output:0<decoder/conv2d_transpose_12/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
;decoder/conv2d_transpose_12/conv2d_transpose/ReadVariableOpReadVariableOpDdecoder_conv2d_transpose_12_conv2d_transpose_readvariableop_resource*(
_output_shapes
:��*
dtype0�
,decoder/conv2d_transpose_12/conv2d_transposeConv2DBackpropInput*decoder/conv2d_transpose_12/stack:output:0Cdecoder/conv2d_transpose_12/conv2d_transpose/ReadVariableOp:value:0"decoder/reshape_3/Reshape:output:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
2decoder/conv2d_transpose_12/BiasAdd/ReadVariableOpReadVariableOp;decoder_conv2d_transpose_12_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
#decoder/conv2d_transpose_12/BiasAddBiasAdd5decoder/conv2d_transpose_12/conv2d_transpose:output:0:decoder/conv2d_transpose_12/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:��
 decoder/conv2d_transpose_12/ReluRelu,decoder/conv2d_transpose_12/BiasAdd:output:0*
T0*'
_output_shapes
:�z
!decoder/conv2d_transpose_13/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"         �   y
/decoder/conv2d_transpose_13/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: {
1decoder/conv2d_transpose_13/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:{
1decoder/conv2d_transpose_13/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
)decoder/conv2d_transpose_13/strided_sliceStridedSlice*decoder/conv2d_transpose_13/Shape:output:08decoder/conv2d_transpose_13/strided_slice/stack:output:0:decoder/conv2d_transpose_13/strided_slice/stack_1:output:0:decoder/conv2d_transpose_13/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maske
#decoder/conv2d_transpose_13/stack/1Const*
_output_shapes
: *
dtype0*
value	B :<e
#decoder/conv2d_transpose_13/stack/2Const*
_output_shapes
: *
dtype0*
value	B :<e
#decoder/conv2d_transpose_13/stack/3Const*
_output_shapes
: *
dtype0*
value	B :@�
!decoder/conv2d_transpose_13/stackPack2decoder/conv2d_transpose_13/strided_slice:output:0,decoder/conv2d_transpose_13/stack/1:output:0,decoder/conv2d_transpose_13/stack/2:output:0,decoder/conv2d_transpose_13/stack/3:output:0*
N*
T0*
_output_shapes
:{
1decoder/conv2d_transpose_13/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: }
3decoder/conv2d_transpose_13/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:}
3decoder/conv2d_transpose_13/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
+decoder/conv2d_transpose_13/strided_slice_1StridedSlice*decoder/conv2d_transpose_13/stack:output:0:decoder/conv2d_transpose_13/strided_slice_1/stack:output:0<decoder/conv2d_transpose_13/strided_slice_1/stack_1:output:0<decoder/conv2d_transpose_13/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
;decoder/conv2d_transpose_13/conv2d_transpose/ReadVariableOpReadVariableOpDdecoder_conv2d_transpose_13_conv2d_transpose_readvariableop_resource*'
_output_shapes
:@�*
dtype0�
,decoder/conv2d_transpose_13/conv2d_transposeConv2DBackpropInput*decoder/conv2d_transpose_13/stack:output:0Cdecoder/conv2d_transpose_13/conv2d_transpose/ReadVariableOp:value:0.decoder/conv2d_transpose_12/Relu:activations:0*
T0*&
_output_shapes
:<<@*
paddingSAME*
strides
�
2decoder/conv2d_transpose_13/BiasAdd/ReadVariableOpReadVariableOp;decoder_conv2d_transpose_13_biasadd_readvariableop_resource*
_output_shapes
:@*
dtype0�
#decoder/conv2d_transpose_13/BiasAddBiasAdd5decoder/conv2d_transpose_13/conv2d_transpose:output:0:decoder/conv2d_transpose_13/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<@�
 decoder/conv2d_transpose_13/ReluRelu,decoder/conv2d_transpose_13/BiasAdd:output:0*
T0*&
_output_shapes
:<<@z
!decoder/conv2d_transpose_14/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"   <   <   @   y
/decoder/conv2d_transpose_14/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: {
1decoder/conv2d_transpose_14/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:{
1decoder/conv2d_transpose_14/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
)decoder/conv2d_transpose_14/strided_sliceStridedSlice*decoder/conv2d_transpose_14/Shape:output:08decoder/conv2d_transpose_14/strided_slice/stack:output:0:decoder/conv2d_transpose_14/strided_slice/stack_1:output:0:decoder/conv2d_transpose_14/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maske
#decoder/conv2d_transpose_14/stack/1Const*
_output_shapes
: *
dtype0*
value	B :<e
#decoder/conv2d_transpose_14/stack/2Const*
_output_shapes
: *
dtype0*
value	B :<e
#decoder/conv2d_transpose_14/stack/3Const*
_output_shapes
: *
dtype0*
value	B : �
!decoder/conv2d_transpose_14/stackPack2decoder/conv2d_transpose_14/strided_slice:output:0,decoder/conv2d_transpose_14/stack/1:output:0,decoder/conv2d_transpose_14/stack/2:output:0,decoder/conv2d_transpose_14/stack/3:output:0*
N*
T0*
_output_shapes
:{
1decoder/conv2d_transpose_14/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: }
3decoder/conv2d_transpose_14/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:}
3decoder/conv2d_transpose_14/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
+decoder/conv2d_transpose_14/strided_slice_1StridedSlice*decoder/conv2d_transpose_14/stack:output:0:decoder/conv2d_transpose_14/strided_slice_1/stack:output:0<decoder/conv2d_transpose_14/strided_slice_1/stack_1:output:0<decoder/conv2d_transpose_14/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
;decoder/conv2d_transpose_14/conv2d_transpose/ReadVariableOpReadVariableOpDdecoder_conv2d_transpose_14_conv2d_transpose_readvariableop_resource*&
_output_shapes
: @*
dtype0�
,decoder/conv2d_transpose_14/conv2d_transposeConv2DBackpropInput*decoder/conv2d_transpose_14/stack:output:0Cdecoder/conv2d_transpose_14/conv2d_transpose/ReadVariableOp:value:0.decoder/conv2d_transpose_13/Relu:activations:0*
T0*&
_output_shapes
:<< *
paddingSAME*
strides
�
2decoder/conv2d_transpose_14/BiasAdd/ReadVariableOpReadVariableOp;decoder_conv2d_transpose_14_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
#decoder/conv2d_transpose_14/BiasAddBiasAdd5decoder/conv2d_transpose_14/conv2d_transpose:output:0:decoder/conv2d_transpose_14/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<< �
 decoder/conv2d_transpose_14/ReluRelu,decoder/conv2d_transpose_14/BiasAdd:output:0*
T0*&
_output_shapes
:<< z
!decoder/conv2d_transpose_15/ShapeConst*
_output_shapes
:*
dtype0*%
valueB"   <   <       y
/decoder/conv2d_transpose_15/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: {
1decoder/conv2d_transpose_15/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:{
1decoder/conv2d_transpose_15/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
)decoder/conv2d_transpose_15/strided_sliceStridedSlice*decoder/conv2d_transpose_15/Shape:output:08decoder/conv2d_transpose_15/strided_slice/stack:output:0:decoder/conv2d_transpose_15/strided_slice/stack_1:output:0:decoder/conv2d_transpose_15/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_maske
#decoder/conv2d_transpose_15/stack/1Const*
_output_shapes
: *
dtype0*
value	B :<e
#decoder/conv2d_transpose_15/stack/2Const*
_output_shapes
: *
dtype0*
value	B :<e
#decoder/conv2d_transpose_15/stack/3Const*
_output_shapes
: *
dtype0*
value	B :�
!decoder/conv2d_transpose_15/stackPack2decoder/conv2d_transpose_15/strided_slice:output:0,decoder/conv2d_transpose_15/stack/1:output:0,decoder/conv2d_transpose_15/stack/2:output:0,decoder/conv2d_transpose_15/stack/3:output:0*
N*
T0*
_output_shapes
:{
1decoder/conv2d_transpose_15/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: }
3decoder/conv2d_transpose_15/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:}
3decoder/conv2d_transpose_15/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
+decoder/conv2d_transpose_15/strided_slice_1StridedSlice*decoder/conv2d_transpose_15/stack:output:0:decoder/conv2d_transpose_15/strided_slice_1/stack:output:0<decoder/conv2d_transpose_15/strided_slice_1/stack_1:output:0<decoder/conv2d_transpose_15/strided_slice_1/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
;decoder/conv2d_transpose_15/conv2d_transpose/ReadVariableOpReadVariableOpDdecoder_conv2d_transpose_15_conv2d_transpose_readvariableop_resource*&
_output_shapes
: *
dtype0�
,decoder/conv2d_transpose_15/conv2d_transposeConv2DBackpropInput*decoder/conv2d_transpose_15/stack:output:0Cdecoder/conv2d_transpose_15/conv2d_transpose/ReadVariableOp:value:0.decoder/conv2d_transpose_14/Relu:activations:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
�
2decoder/conv2d_transpose_15/BiasAdd/ReadVariableOpReadVariableOp;decoder_conv2d_transpose_15_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
#decoder/conv2d_transpose_15/BiasAddBiasAdd5decoder/conv2d_transpose_15/conv2d_transpose:output:0:decoder/conv2d_transpose_15/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<�
 decoder/conv2d_transpose_15/ReluRelu,decoder/conv2d_transpose_15/BiasAdd:output:0*
T0*&
_output_shapes
:<<�
'decoder/conv2d_19/Conv2D/ReadVariableOpReadVariableOp0decoder_conv2d_19_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
decoder/conv2d_19/Conv2DConv2D.decoder/conv2d_transpose_15/Relu:activations:0/decoder/conv2d_19/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
�
(decoder/conv2d_19/BiasAdd/ReadVariableOpReadVariableOp1decoder_conv2d_19_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
decoder/conv2d_19/BiasAddBiasAdd!decoder/conv2d_19/Conv2D:output:00decoder/conv2d_19/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<y
decoder/conv2d_19/SigmoidSigmoid"decoder/conv2d_19/BiasAdd:output:0*
T0*&
_output_shapes
:<<m
tf.reshape_6/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB:
���������s
tf.reshape_6/ReshapeReshapeinputs#tf.reshape_6/Reshape/shape:output:0*
T0*
_output_shapes

:��m
tf.reshape_7/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB:
����������
tf.reshape_7/ReshapeReshapedecoder/conv2d_19/Sigmoid:y:0#tf.reshape_7/Reshape/shape:output:0*
T0*
_output_shapes

:���
conv2d_15/Conv2D/ReadVariableOpReadVariableOp0encoder_conv2d_15_conv2d_readvariableop_resource*&
_output_shapes
:*
dtype0�
conv2d_15/Conv2DConv2Dinputs'conv2d_15/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<*
paddingSAME*
strides
�
 conv2d_15/BiasAdd/ReadVariableOpReadVariableOp1encoder_conv2d_15_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
conv2d_15/BiasAddBiasAddconv2d_15/Conv2D:output:0(conv2d_15/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
:<<c
conv2d_15/ReluReluconv2d_15/BiasAdd:output:0*
T0*&
_output_shapes
:<<�
conv2d_16/Conv2D/ReadVariableOpReadVariableOp0encoder_conv2d_16_conv2d_readvariableop_resource*&
_output_shapes
: *
dtype0�
conv2d_16/Conv2DConv2Dconv2d_15/Relu:activations:0'conv2d_16/Conv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
: *
paddingSAME*
strides
�
 conv2d_16/BiasAdd/ReadVariableOpReadVariableOp1encoder_conv2d_16_biasadd_readvariableop_resource*
_output_shapes
: *
dtype0�
conv2d_16/BiasAddBiasAddconv2d_16/Conv2D:output:0(conv2d_16/BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
: c
conv2d_16/ReluReluconv2d_16/BiasAdd:output:0*
T0*&
_output_shapes
: �
.tf.math.squared_difference_3/SquaredDifferenceSquaredDifferencetf.reshape_7/Reshape:output:0tf.reshape_6/Reshape:output:0*
T0*
_output_shapes

:���
conv2d_17/Conv2D/ReadVariableOpReadVariableOp0encoder_conv2d_17_conv2d_readvariableop_resource*'
_output_shapes
: �*
dtype0�
conv2d_17/Conv2DConv2Dconv2d_16/Relu:activations:0'conv2d_17/Conv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
 conv2d_17/BiasAdd/ReadVariableOpReadVariableOp1encoder_conv2d_17_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
conv2d_17/BiasAddBiasAddconv2d_17/Conv2D:output:0(conv2d_17/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�d
conv2d_17/ReluReluconv2d_17/BiasAdd:output:0*
T0*'
_output_shapes
:�w
,tf.math.reduce_mean_6/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
����������
tf.math.reduce_mean_6/MeanMean2tf.math.squared_difference_3/SquaredDifference:z:05tf.math.reduce_mean_6/Mean/reduction_indices:output:0*
T0*
_output_shapes
: �
conv2d_18/Conv2D/ReadVariableOpReadVariableOp0encoder_conv2d_18_conv2d_readvariableop_resource*(
_output_shapes
:��*
dtype0�
conv2d_18/Conv2DConv2Dconv2d_17/Relu:activations:0'conv2d_18/Conv2D/ReadVariableOp:value:0*
T0*'
_output_shapes
:�*
paddingSAME*
strides
�
 conv2d_18/BiasAdd/ReadVariableOpReadVariableOp1encoder_conv2d_18_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
conv2d_18/BiasAddBiasAddconv2d_18/Conv2D:output:0(conv2d_18/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:�d
conv2d_18/ReluReluconv2d_18/BiasAdd:output:0*
T0*'
_output_shapes
:�^
tf.math.multiply_12/Mul/yConst*
_output_shapes
: *
dtype0*
valueB
 * �(F�
tf.math.multiply_12/MulMul#tf.math.reduce_mean_6/Mean:output:0"tf.math.multiply_12/Mul/y:output:0*
T0*
_output_shapes
: `
flatten_3/ConstConst*
_output_shapes
:*
dtype0*
valueB"���� � 
flatten_3/ReshapeReshapeconv2d_18/Relu:activations:0flatten_3/Const:output:0*
T0* 
_output_shapes
:
��e
tf.math.multiply_14/MulMulunknowntf.math.multiply_12/Mul:z:0*
T0*
_output_shapes
: �
dense_10/MatMul/ReadVariableOpReadVariableOp/encoder_dense_10_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
dense_10/MatMulMatMulflatten_3/Reshape:output:0&dense_10/MatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:�
dense_10/BiasAdd/ReadVariableOpReadVariableOp0encoder_dense_10_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense_10/BiasAddBiasAdddense_10/MatMul:product:0'dense_10/BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:�
dense_9/MatMul/ReadVariableOpReadVariableOp.encoder_dense_9_matmul_readvariableop_resource* 
_output_shapes
:
��*
dtype0�
dense_9/MatMulMatMulflatten_3/Reshape:output:0%dense_9/MatMul/ReadVariableOp:value:0*
T0*
_output_shapes

:�
dense_9/BiasAdd/ReadVariableOpReadVariableOp/encoder_dense_9_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense_9/BiasAddBiasAdddense_9/MatMul:product:0&dense_9/BiasAdd/ReadVariableOp:value:0*
T0*
_output_shapes

:S
add_metric_7/RankConst*
_output_shapes
: *
dtype0*
value	B : Z
add_metric_7/range/startConst*
_output_shapes
: *
dtype0*
value	B : Z
add_metric_7/range/deltaConst*
_output_shapes
: *
dtype0*
value	B :�
add_metric_7/rangeRange!add_metric_7/range/start:output:0add_metric_7/Rank:output:0!add_metric_7/range/delta:output:0*
_output_shapes
: �
add_metric_7/SumSumtf.math.multiply_12/Mul:z:0add_metric_7/range:output:0*
T0*&
 _has_manual_control_dependencies(*
_output_shapes
: �
 add_metric_7/AssignAddVariableOpAssignAddVariableOp)add_metric_7_assignaddvariableop_resourceadd_metric_7/Sum:output:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0S
add_metric_7/SizeConst*
_output_shapes
: *
dtype0*
value	B :e
add_metric_7/CastCastadd_metric_7/Size:output:0*

DstT0*

SrcT0*
_output_shapes
: �
"add_metric_7/AssignAddVariableOp_1AssignAddVariableOp+add_metric_7_assignaddvariableop_1_resourceadd_metric_7/Cast:y:0!^add_metric_7/AssignAddVariableOp*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0�
&add_metric_7/div_no_nan/ReadVariableOpReadVariableOp)add_metric_7_assignaddvariableop_resource!^add_metric_7/AssignAddVariableOp#^add_metric_7/AssignAddVariableOp_1*
_output_shapes
: *
dtype0�
(add_metric_7/div_no_nan/ReadVariableOp_1ReadVariableOp+add_metric_7_assignaddvariableop_1_resource#^add_metric_7/AssignAddVariableOp_1*
_output_shapes
: *
dtype0�
add_metric_7/div_no_nanDivNoNan.add_metric_7/div_no_nan/ReadVariableOp:value:00add_metric_7/div_no_nan/ReadVariableOp_1:value:0*
T0*
_output_shapes
: _
add_metric_7/IdentityIdentityadd_metric_7/div_no_nan:z:0*
T0*
_output_shapes
: t
tf.__operators__.add_6/AddV2AddV2	unknown_0dense_10/BiasAdd:output:0*
T0*
_output_shapes

:d
tf.math.square_3/SquareSquaredense_9/BiasAdd:output:0*
T0*
_output_shapes

:\
tf.math.exp_3/ExpExpdense_10/BiasAdd:output:0*
T0*
_output_shapes

:�
tf.math.subtract_6/SubSub tf.__operators__.add_6/AddV2:z:0tf.math.square_3/Square:y:0*
T0*
_output_shapes

:y
tf.math.subtract_7/SubSubtf.math.subtract_6/Sub:z:0tf.math.exp_3/Exp:y:0*
T0*
_output_shapes

:l
*tf.math.reduce_sum_3/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
tf.math.reduce_sum_3/SumSumtf.math.subtract_7/Sub:z:03tf.math.reduce_sum_3/Sum/reduction_indices:output:0*
T0*
_output_shapes
:q
tf.math.multiply_13/MulMul	unknown_1!tf.math.reduce_sum_3/Sum:output:0*
T0*
_output_shapes
:k
tf.math.multiply_15/MulMul	unknown_2tf.math.multiply_13/Mul:z:0*
T0*
_output_shapes
:�
tf.__operators__.add_7/AddV2AddV2tf.math.multiply_14/Mul:z:0tf.math.multiply_15/Mul:z:0*
T0*
_output_shapes
:e
tf.math.reduce_mean_7/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
tf.math.reduce_mean_7/MeanMean tf.__operators__.add_7/AddV2:z:0$tf.math.reduce_mean_7/Const:output:0*
T0*
_output_shapes
: S
add_metric_6/RankConst*
_output_shapes
: *
dtype0*
value	B : Z
add_metric_6/range/startConst*
_output_shapes
: *
dtype0*
value	B : Z
add_metric_6/range/deltaConst*
_output_shapes
: *
dtype0*
value	B :�
add_metric_6/rangeRange!add_metric_6/range/start:output:0add_metric_6/Rank:output:0!add_metric_6/range/delta:output:0*
_output_shapes
: �
add_metric_6/SumSum#tf.math.reduce_mean_7/Mean:output:0add_metric_6/range:output:0*
T0*&
 _has_manual_control_dependencies(*
_output_shapes
: �
 add_metric_6/AssignAddVariableOpAssignAddVariableOp)add_metric_6_assignaddvariableop_resourceadd_metric_6/Sum:output:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0S
add_metric_6/SizeConst*
_output_shapes
: *
dtype0*
value	B :e
add_metric_6/CastCastadd_metric_6/Size:output:0*

DstT0*

SrcT0*
_output_shapes
: �
"add_metric_6/AssignAddVariableOp_1AssignAddVariableOp+add_metric_6_assignaddvariableop_1_resourceadd_metric_6/Cast:y:0!^add_metric_6/AssignAddVariableOp*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0�
&add_metric_6/div_no_nan/ReadVariableOpReadVariableOp)add_metric_6_assignaddvariableop_resource!^add_metric_6/AssignAddVariableOp#^add_metric_6/AssignAddVariableOp_1*
_output_shapes
: *
dtype0�
(add_metric_6/div_no_nan/ReadVariableOp_1ReadVariableOp+add_metric_6_assignaddvariableop_1_resource#^add_metric_6/AssignAddVariableOp_1*
_output_shapes
: *
dtype0�
add_metric_6/div_no_nanDivNoNan.add_metric_6/div_no_nan/ReadVariableOp:value:00add_metric_6/div_no_nan/ReadVariableOp_1:value:0*
T0*
_output_shapes
: _
add_metric_6/IdentityIdentityadd_metric_6/div_no_nan:z:0*
T0*
_output_shapes
: k
IdentityIdentitydecoder/conv2d_19/Sigmoid:y:0^NoOp*
T0*&
_output_shapes
:<<c

Identity_1Identity#tf.math.reduce_mean_7/Mean:output:0^NoOp*
T0*
_output_shapes
: �
NoOpNoOp!^add_metric_6/AssignAddVariableOp#^add_metric_6/AssignAddVariableOp_1'^add_metric_6/div_no_nan/ReadVariableOp)^add_metric_6/div_no_nan/ReadVariableOp_1!^add_metric_7/AssignAddVariableOp#^add_metric_7/AssignAddVariableOp_1'^add_metric_7/div_no_nan/ReadVariableOp)^add_metric_7/div_no_nan/ReadVariableOp_1!^conv2d_15/BiasAdd/ReadVariableOp ^conv2d_15/Conv2D/ReadVariableOp!^conv2d_16/BiasAdd/ReadVariableOp ^conv2d_16/Conv2D/ReadVariableOp!^conv2d_17/BiasAdd/ReadVariableOp ^conv2d_17/Conv2D/ReadVariableOp!^conv2d_18/BiasAdd/ReadVariableOp ^conv2d_18/Conv2D/ReadVariableOp)^decoder/conv2d_19/BiasAdd/ReadVariableOp(^decoder/conv2d_19/Conv2D/ReadVariableOp3^decoder/conv2d_transpose_12/BiasAdd/ReadVariableOp<^decoder/conv2d_transpose_12/conv2d_transpose/ReadVariableOp3^decoder/conv2d_transpose_13/BiasAdd/ReadVariableOp<^decoder/conv2d_transpose_13/conv2d_transpose/ReadVariableOp3^decoder/conv2d_transpose_14/BiasAdd/ReadVariableOp<^decoder/conv2d_transpose_14/conv2d_transpose/ReadVariableOp3^decoder/conv2d_transpose_15/BiasAdd/ReadVariableOp<^decoder/conv2d_transpose_15/conv2d_transpose/ReadVariableOp(^decoder/dense_11/BiasAdd/ReadVariableOp'^decoder/dense_11/MatMul/ReadVariableOp ^dense_10/BiasAdd/ReadVariableOp^dense_10/MatMul/ReadVariableOp^dense_9/BiasAdd/ReadVariableOp^dense_9/MatMul/ReadVariableOp)^encoder/conv2d_15/BiasAdd/ReadVariableOp(^encoder/conv2d_15/Conv2D/ReadVariableOp)^encoder/conv2d_16/BiasAdd/ReadVariableOp(^encoder/conv2d_16/Conv2D/ReadVariableOp)^encoder/conv2d_17/BiasAdd/ReadVariableOp(^encoder/conv2d_17/Conv2D/ReadVariableOp)^encoder/conv2d_18/BiasAdd/ReadVariableOp(^encoder/conv2d_18/Conv2D/ReadVariableOp(^encoder/dense_10/BiasAdd/ReadVariableOp'^encoder/dense_10/MatMul/ReadVariableOp'^encoder/dense_9/BiasAdd/ReadVariableOp&^encoder/dense_9/MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*e
_input_shapesT
R:<<: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2D
 add_metric_6/AssignAddVariableOp add_metric_6/AssignAddVariableOp2H
"add_metric_6/AssignAddVariableOp_1"add_metric_6/AssignAddVariableOp_12P
&add_metric_6/div_no_nan/ReadVariableOp&add_metric_6/div_no_nan/ReadVariableOp2T
(add_metric_6/div_no_nan/ReadVariableOp_1(add_metric_6/div_no_nan/ReadVariableOp_12D
 add_metric_7/AssignAddVariableOp add_metric_7/AssignAddVariableOp2H
"add_metric_7/AssignAddVariableOp_1"add_metric_7/AssignAddVariableOp_12P
&add_metric_7/div_no_nan/ReadVariableOp&add_metric_7/div_no_nan/ReadVariableOp2T
(add_metric_7/div_no_nan/ReadVariableOp_1(add_metric_7/div_no_nan/ReadVariableOp_12D
 conv2d_15/BiasAdd/ReadVariableOp conv2d_15/BiasAdd/ReadVariableOp2B
conv2d_15/Conv2D/ReadVariableOpconv2d_15/Conv2D/ReadVariableOp2D
 conv2d_16/BiasAdd/ReadVariableOp conv2d_16/BiasAdd/ReadVariableOp2B
conv2d_16/Conv2D/ReadVariableOpconv2d_16/Conv2D/ReadVariableOp2D
 conv2d_17/BiasAdd/ReadVariableOp conv2d_17/BiasAdd/ReadVariableOp2B
conv2d_17/Conv2D/ReadVariableOpconv2d_17/Conv2D/ReadVariableOp2D
 conv2d_18/BiasAdd/ReadVariableOp conv2d_18/BiasAdd/ReadVariableOp2B
conv2d_18/Conv2D/ReadVariableOpconv2d_18/Conv2D/ReadVariableOp2T
(decoder/conv2d_19/BiasAdd/ReadVariableOp(decoder/conv2d_19/BiasAdd/ReadVariableOp2R
'decoder/conv2d_19/Conv2D/ReadVariableOp'decoder/conv2d_19/Conv2D/ReadVariableOp2h
2decoder/conv2d_transpose_12/BiasAdd/ReadVariableOp2decoder/conv2d_transpose_12/BiasAdd/ReadVariableOp2z
;decoder/conv2d_transpose_12/conv2d_transpose/ReadVariableOp;decoder/conv2d_transpose_12/conv2d_transpose/ReadVariableOp2h
2decoder/conv2d_transpose_13/BiasAdd/ReadVariableOp2decoder/conv2d_transpose_13/BiasAdd/ReadVariableOp2z
;decoder/conv2d_transpose_13/conv2d_transpose/ReadVariableOp;decoder/conv2d_transpose_13/conv2d_transpose/ReadVariableOp2h
2decoder/conv2d_transpose_14/BiasAdd/ReadVariableOp2decoder/conv2d_transpose_14/BiasAdd/ReadVariableOp2z
;decoder/conv2d_transpose_14/conv2d_transpose/ReadVariableOp;decoder/conv2d_transpose_14/conv2d_transpose/ReadVariableOp2h
2decoder/conv2d_transpose_15/BiasAdd/ReadVariableOp2decoder/conv2d_transpose_15/BiasAdd/ReadVariableOp2z
;decoder/conv2d_transpose_15/conv2d_transpose/ReadVariableOp;decoder/conv2d_transpose_15/conv2d_transpose/ReadVariableOp2R
'decoder/dense_11/BiasAdd/ReadVariableOp'decoder/dense_11/BiasAdd/ReadVariableOp2P
&decoder/dense_11/MatMul/ReadVariableOp&decoder/dense_11/MatMul/ReadVariableOp2B
dense_10/BiasAdd/ReadVariableOpdense_10/BiasAdd/ReadVariableOp2@
dense_10/MatMul/ReadVariableOpdense_10/MatMul/ReadVariableOp2@
dense_9/BiasAdd/ReadVariableOpdense_9/BiasAdd/ReadVariableOp2>
dense_9/MatMul/ReadVariableOpdense_9/MatMul/ReadVariableOp2T
(encoder/conv2d_15/BiasAdd/ReadVariableOp(encoder/conv2d_15/BiasAdd/ReadVariableOp2R
'encoder/conv2d_15/Conv2D/ReadVariableOp'encoder/conv2d_15/Conv2D/ReadVariableOp2T
(encoder/conv2d_16/BiasAdd/ReadVariableOp(encoder/conv2d_16/BiasAdd/ReadVariableOp2R
'encoder/conv2d_16/Conv2D/ReadVariableOp'encoder/conv2d_16/Conv2D/ReadVariableOp2T
(encoder/conv2d_17/BiasAdd/ReadVariableOp(encoder/conv2d_17/BiasAdd/ReadVariableOp2R
'encoder/conv2d_17/Conv2D/ReadVariableOp'encoder/conv2d_17/Conv2D/ReadVariableOp2T
(encoder/conv2d_18/BiasAdd/ReadVariableOp(encoder/conv2d_18/BiasAdd/ReadVariableOp2R
'encoder/conv2d_18/Conv2D/ReadVariableOp'encoder/conv2d_18/Conv2D/ReadVariableOp2R
'encoder/dense_10/BiasAdd/ReadVariableOp'encoder/dense_10/BiasAdd/ReadVariableOp2P
&encoder/dense_10/MatMul/ReadVariableOp&encoder/dense_10/MatMul/ReadVariableOp2P
&encoder/dense_9/BiasAdd/ReadVariableOp&encoder/dense_9/BiasAdd/ReadVariableOp2N
%encoder/dense_9/MatMul/ReadVariableOp%encoder/dense_9/MatMul/ReadVariableOp:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
H__inference_add_metric_7_layer_call_and_return_conditional_losses_248248

inputs&
assignaddvariableop_resource: (
assignaddvariableop_1_resource: 

identity_1��AssignAddVariableOp�AssignAddVariableOp_1�div_no_nan/ReadVariableOp�div_no_nan/ReadVariableOp_1F
RankConst*
_output_shapes
: *
dtype0*
value	B : M
range/startConst*
_output_shapes
: *
dtype0*
value	B : M
range/deltaConst*
_output_shapes
: *
dtype0*
value	B :c
rangeRangerange/start:output:0Rank:output:0range/delta:output:0*
_output_shapes
: k
SumSuminputsrange:output:0*
T0*&
 _has_manual_control_dependencies(*
_output_shapes
: �
AssignAddVariableOpAssignAddVariableOpassignaddvariableop_resourceSum:output:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0F
SizeConst*
_output_shapes
: *
dtype0*
value	B :K
CastCastSize:output:0*

DstT0*

SrcT0*
_output_shapes
: �
AssignAddVariableOp_1AssignAddVariableOpassignaddvariableop_1_resourceCast:y:0^AssignAddVariableOp*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0�
div_no_nan/ReadVariableOpReadVariableOpassignaddvariableop_resource^AssignAddVariableOp^AssignAddVariableOp_1*
_output_shapes
: *
dtype0�
div_no_nan/ReadVariableOp_1ReadVariableOpassignaddvariableop_1_resource^AssignAddVariableOp_1*
_output_shapes
: *
dtype0

div_no_nanDivNoNan!div_no_nan/ReadVariableOp:value:0#div_no_nan/ReadVariableOp_1:value:0*
T0*
_output_shapes
: E
IdentityIdentitydiv_no_nan:z:0*
T0*
_output_shapes
: F

Identity_1Identityinputs^NoOp*
T0*
_output_shapes
: �
NoOpNoOp^AssignAddVariableOp^AssignAddVariableOp_1^div_no_nan/ReadVariableOp^div_no_nan/ReadVariableOp_1*"
_acd_function_control_output(*
_output_shapes
 "!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: : : 2*
AssignAddVariableOpAssignAddVariableOp2.
AssignAddVariableOp_1AssignAddVariableOp_126
div_no_nan/ReadVariableOpdiv_no_nan/ReadVariableOp2:
div_no_nan/ReadVariableOp_1div_no_nan/ReadVariableOp_1:> :

_output_shapes
: 
 
_user_specified_nameinputs
�
K
#__inference__update_step_xla_247476
gradient
variable:*
_XlaMustCompile(*(
_construction_contextkEagerRuntime*
_input_shapes

:: *
	_noinline(:D @

_output_shapes
:
"
_user_specified_name
gradient:($
"
_user_specified_name
variable
�V
�
?__inference_vae_layer_call_and_return_conditional_losses_246201
input_7(
encoder_246086:
encoder_246088:(
encoder_246090: 
encoder_246092: )
encoder_246094: �
encoder_246096:	�*
encoder_246098:��
encoder_246100:	�"
encoder_246102:
��
encoder_246104:"
encoder_246106:
��
encoder_246108:"
decoder_246113:
��
decoder_246115:
��*
decoder_246117:��
decoder_246119:	�)
decoder_246121:@�
decoder_246123:@(
decoder_246125: @
decoder_246127: (
decoder_246129: 
decoder_246131:(
decoder_246133:
decoder_246135:
unknown
add_metric_7_246169: 
add_metric_7_246171: 
	unknown_0
	unknown_1
	unknown_2
add_metric_6_246194: 
add_metric_6_246196: 
identity

identity_1��$add_metric_6/StatefulPartitionedCall�$add_metric_7/StatefulPartitionedCall�!conv2d_15/StatefulPartitionedCall�!conv2d_16/StatefulPartitionedCall�!conv2d_17/StatefulPartitionedCall�!conv2d_18/StatefulPartitionedCall�decoder/StatefulPartitionedCall� dense_10/StatefulPartitionedCall�dense_9/StatefulPartitionedCall�encoder/StatefulPartitionedCall�
encoder/StatefulPartitionedCallStatefulPartitionedCallinput_7encoder_246086encoder_246088encoder_246090encoder_246092encoder_246094encoder_246096encoder_246098encoder_246100encoder_246102encoder_246104encoder_246106encoder_246108*
Tin
2*
Tout
2*
_collective_manager_ids
 *2
_output_shapes 
:::*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_encoder_layer_call_and_return_conditional_losses_245243�
decoder/StatefulPartitionedCallStatefulPartitionedCall(encoder/StatefulPartitionedCall:output:2decoder_246113decoder_246115decoder_246117decoder_246119decoder_246121decoder_246123decoder_246125decoder_246127decoder_246129decoder_246131decoder_246133decoder_246135*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*.
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_decoder_layer_call_and_return_conditional_losses_245811m
tf.reshape_6/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB:
���������t
tf.reshape_6/ReshapeReshapeinput_7#tf.reshape_6/Reshape/shape:output:0*
T0*
_output_shapes

:��m
tf.reshape_7/Reshape/shapeConst*
_output_shapes
:*
dtype0*
valueB:
����������
tf.reshape_7/ReshapeReshape(decoder/StatefulPartitionedCall:output:0#tf.reshape_7/Reshape/shape:output:0*
T0*
_output_shapes

:���
!conv2d_15/StatefulPartitionedCallStatefulPartitionedCallinput_7encoder_246086encoder_246088*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_15_layer_call_and_return_conditional_losses_244947�
!conv2d_16/StatefulPartitionedCallStatefulPartitionedCall*conv2d_15/StatefulPartitionedCall:output:0encoder_246090encoder_246092*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
: *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_16_layer_call_and_return_conditional_losses_244964�
.tf.math.squared_difference_3/SquaredDifferenceSquaredDifferencetf.reshape_7/Reshape:output:0tf.reshape_6/Reshape:output:0*
T0*
_output_shapes

:���
!conv2d_17/StatefulPartitionedCallStatefulPartitionedCall*conv2d_16/StatefulPartitionedCall:output:0encoder_246094encoder_246096*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_17_layer_call_and_return_conditional_losses_244981w
,tf.math.reduce_mean_6/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
valueB :
����������
tf.math.reduce_mean_6/MeanMean2tf.math.squared_difference_3/SquaredDifference:z:05tf.math.reduce_mean_6/Mean/reduction_indices:output:0*
T0*
_output_shapes
: �
!conv2d_18/StatefulPartitionedCallStatefulPartitionedCall*conv2d_17/StatefulPartitionedCall:output:0encoder_246098encoder_246100*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_18_layer_call_and_return_conditional_losses_244998^
tf.math.multiply_12/Mul/yConst*
_output_shapes
: *
dtype0*
valueB
 * �(F�
tf.math.multiply_12/MulMul#tf.math.reduce_mean_6/Mean:output:0"tf.math.multiply_12/Mul/y:output:0*
T0*
_output_shapes
: �
flatten_3/PartitionedCallPartitionedCall*conv2d_18/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 * 
_output_shapes
:
��* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_flatten_3_layer_call_and_return_conditional_losses_245010e
tf.math.multiply_14/MulMulunknowntf.math.multiply_12/Mul:z:0*
T0*
_output_shapes
: �
 dense_10/StatefulPartitionedCallStatefulPartitionedCall"flatten_3/PartitionedCall:output:0encoder_246106encoder_246108*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dense_10_layer_call_and_return_conditional_losses_245038�
dense_9/StatefulPartitionedCallStatefulPartitionedCall"flatten_3/PartitionedCall:output:0encoder_246102encoder_246104*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_dense_9_layer_call_and_return_conditional_losses_245022�
$add_metric_7/StatefulPartitionedCallStatefulPartitionedCalltf.math.multiply_12/Mul:z:0add_metric_7_246169add_metric_7_246171*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_add_metric_7_layer_call_and_return_conditional_losses_246026�
tf.__operators__.add_6/AddV2AddV2	unknown_0)dense_10/StatefulPartitionedCall:output:0*
T0*
_output_shapes

:t
tf.math.square_3/SquareSquare(dense_9/StatefulPartitionedCall:output:0*
T0*
_output_shapes

:l
tf.math.exp_3/ExpExp)dense_10/StatefulPartitionedCall:output:0*
T0*
_output_shapes

:�
tf.math.subtract_6/SubSub tf.__operators__.add_6/AddV2:z:0tf.math.square_3/Square:y:0*
T0*
_output_shapes

:y
tf.math.subtract_7/SubSubtf.math.subtract_6/Sub:z:0tf.math.exp_3/Exp:y:0*
T0*
_output_shapes

:l
*tf.math.reduce_sum_3/Sum/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
tf.math.reduce_sum_3/SumSumtf.math.subtract_7/Sub:z:03tf.math.reduce_sum_3/Sum/reduction_indices:output:0*
T0*
_output_shapes
:q
tf.math.multiply_13/MulMul	unknown_1!tf.math.reduce_sum_3/Sum:output:0*
T0*
_output_shapes
:k
tf.math.multiply_15/MulMul	unknown_2tf.math.multiply_13/Mul:z:0*
T0*
_output_shapes
:�
tf.__operators__.add_7/AddV2AddV2tf.math.multiply_14/Mul:z:0tf.math.multiply_15/Mul:z:0*
T0*
_output_shapes
:e
tf.math.reduce_mean_7/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
tf.math.reduce_mean_7/MeanMean tf.__operators__.add_7/AddV2:z:0$tf.math.reduce_mean_7/Const:output:0*
T0*
_output_shapes
: �
add_loss_3/PartitionedCallPartitionedCall#tf.math.reduce_mean_7/Mean:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: : * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *O
fJRH
F__inference_add_loss_3_layer_call_and_return_conditional_losses_246055�
$add_metric_6/StatefulPartitionedCallStatefulPartitionedCall#tf.math.reduce_mean_7/Mean:output:0add_metric_6_246194add_metric_6_246196*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_add_metric_6_layer_call_and_return_conditional_losses_246075v
IdentityIdentity(decoder/StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<c

Identity_1Identity#add_loss_3/PartitionedCall:output:1^NoOp*
T0*
_output_shapes
: �
NoOpNoOp%^add_metric_6/StatefulPartitionedCall%^add_metric_7/StatefulPartitionedCall"^conv2d_15/StatefulPartitionedCall"^conv2d_16/StatefulPartitionedCall"^conv2d_17/StatefulPartitionedCall"^conv2d_18/StatefulPartitionedCall ^decoder/StatefulPartitionedCall!^dense_10/StatefulPartitionedCall ^dense_9/StatefulPartitionedCall ^encoder/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*e
_input_shapesT
R:<<: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 2L
$add_metric_6/StatefulPartitionedCall$add_metric_6/StatefulPartitionedCall2L
$add_metric_7/StatefulPartitionedCall$add_metric_7/StatefulPartitionedCall2F
!conv2d_15/StatefulPartitionedCall!conv2d_15/StatefulPartitionedCall2F
!conv2d_16/StatefulPartitionedCall!conv2d_16/StatefulPartitionedCall2F
!conv2d_17/StatefulPartitionedCall!conv2d_17/StatefulPartitionedCall2F
!conv2d_18/StatefulPartitionedCall!conv2d_18/StatefulPartitionedCall2B
decoder/StatefulPartitionedCalldecoder/StatefulPartitionedCall2D
 dense_10/StatefulPartitionedCall dense_10/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2B
encoder/StatefulPartitionedCallencoder/StatefulPartitionedCall:O K
&
_output_shapes
:<<
!
_user_specified_name	input_7:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
$__inference_vae_layer_call_fn_246888

inputs!
unknown:
	unknown_0:#
	unknown_1: 
	unknown_2: $
	unknown_3: �
	unknown_4:	�%
	unknown_5:��
	unknown_6:	�
	unknown_7:
��
	unknown_8:
	unknown_9:
��

unknown_10:

unknown_11:
��

unknown_12:
��&

unknown_13:��

unknown_14:	�%

unknown_15:@�

unknown_16:@$

unknown_17: @

unknown_18: $

unknown_19: 

unknown_20:$

unknown_21:

unknown_22:

unknown_23

unknown_24: 

unknown_25: 

unknown_26

unknown_27

unknown_28

unknown_29: 

unknown_30: 
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14
unknown_15
unknown_16
unknown_17
unknown_18
unknown_19
unknown_20
unknown_21
unknown_22
unknown_23
unknown_24
unknown_25
unknown_26
unknown_27
unknown_28
unknown_29
unknown_30*,
Tin%
#2!*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:<<: *:
_read_only_resource_inputs
	
*0
config_proto 

CPU

GPU2*0J 8� *H
fCRA
?__inference_vae_layer_call_and_return_conditional_losses_246322n
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*&
_output_shapes
:<<`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*e
_input_shapesT
R:<<: : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
*__inference_conv2d_18_layer_call_fn_248125

inputs#
unknown:��
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_18_layer_call_and_return_conditional_losses_244998o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:�`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:�: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:�
 
_user_specified_nameinputs
�*
�
C__inference_encoder_layer_call_and_return_conditional_losses_245071
input_7*
conv2d_15_244948:
conv2d_15_244950:*
conv2d_16_244965: 
conv2d_16_244967: +
conv2d_17_244982: �
conv2d_17_244984:	�,
conv2d_18_244999:��
conv2d_18_245001:	�"
dense_9_245023:
��
dense_9_245025:#
dense_10_245039:
��
dense_10_245041:
identity

identity_1

identity_2��!conv2d_15/StatefulPartitionedCall�!conv2d_16/StatefulPartitionedCall�!conv2d_17/StatefulPartitionedCall�!conv2d_18/StatefulPartitionedCall� dense_10/StatefulPartitionedCall�dense_9/StatefulPartitionedCall� lambda_3/StatefulPartitionedCall�
!conv2d_15/StatefulPartitionedCallStatefulPartitionedCallinput_7conv2d_15_244948conv2d_15_244950*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
:<<*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_15_layer_call_and_return_conditional_losses_244947�
!conv2d_16/StatefulPartitionedCallStatefulPartitionedCall*conv2d_15/StatefulPartitionedCall:output:0conv2d_16_244965conv2d_16_244967*
Tin
2*
Tout
2*
_collective_manager_ids
 *&
_output_shapes
: *$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_16_layer_call_and_return_conditional_losses_244964�
!conv2d_17/StatefulPartitionedCallStatefulPartitionedCall*conv2d_16/StatefulPartitionedCall:output:0conv2d_17_244982conv2d_17_244984*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_17_layer_call_and_return_conditional_losses_244981�
!conv2d_18/StatefulPartitionedCallStatefulPartitionedCall*conv2d_17/StatefulPartitionedCall:output:0conv2d_18_244999conv2d_18_245001*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:�*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv2d_18_layer_call_and_return_conditional_losses_244998�
flatten_3/PartitionedCallPartitionedCall*conv2d_18/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 * 
_output_shapes
:
��* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_flatten_3_layer_call_and_return_conditional_losses_245010�
dense_9/StatefulPartitionedCallStatefulPartitionedCall"flatten_3/PartitionedCall:output:0dense_9_245023dense_9_245025*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *L
fGRE
C__inference_dense_9_layer_call_and_return_conditional_losses_245022�
 dense_10/StatefulPartitionedCallStatefulPartitionedCall"flatten_3/PartitionedCall:output:0dense_10_245039dense_10_245041*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_dense_10_layer_call_and_return_conditional_losses_245038�
 lambda_3/StatefulPartitionedCallStatefulPartitionedCall(dense_9/StatefulPartitionedCall:output:0)dense_10/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes

:* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *M
fHRF
D__inference_lambda_3_layer_call_and_return_conditional_losses_245066n
IdentityIdentity(dense_9/StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:q

Identity_1Identity)dense_10/StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:q

Identity_2Identity)lambda_3/StatefulPartitionedCall:output:0^NoOp*
T0*
_output_shapes

:�
NoOpNoOp"^conv2d_15/StatefulPartitionedCall"^conv2d_16/StatefulPartitionedCall"^conv2d_17/StatefulPartitionedCall"^conv2d_18/StatefulPartitionedCall!^dense_10/StatefulPartitionedCall ^dense_9/StatefulPartitionedCall!^lambda_3/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0*(
_construction_contextkEagerRuntime*=
_input_shapes,
*:<<: : : : : : : : : : : : 2F
!conv2d_15/StatefulPartitionedCall!conv2d_15/StatefulPartitionedCall2F
!conv2d_16/StatefulPartitionedCall!conv2d_16/StatefulPartitionedCall2F
!conv2d_17/StatefulPartitionedCall!conv2d_17/StatefulPartitionedCall2F
!conv2d_18/StatefulPartitionedCall!conv2d_18/StatefulPartitionedCall2D
 dense_10/StatefulPartitionedCall dense_10/StatefulPartitionedCall2B
dense_9/StatefulPartitionedCalldense_9/StatefulPartitionedCall2D
 lambda_3/StatefulPartitionedCall lambda_3/StatefulPartitionedCall:O K
&
_output_shapes
:<<
!
_user_specified_name	input_7
�

�
E__inference_conv2d_16_layer_call_and_return_conditional_losses_244964

inputs8
conv2d_readvariableop_resource: -
biasadd_readvariableop_resource: 
identity��BiasAdd/ReadVariableOp�Conv2D/ReadVariableOp|
Conv2D/ReadVariableOpReadVariableOpconv2d_readvariableop_resource*&
_output_shapes
: *
dtype0�
Conv2DConv2DinputsConv2D/ReadVariableOp:value:0*
T0*&
_output_shapes
: *
paddingSAME*
strides
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
: *
dtype0t
BiasAddBiasAddConv2D:output:0BiasAdd/ReadVariableOp:value:0*
T0*&
_output_shapes
: O
ReluReluBiasAdd:output:0*
T0*&
_output_shapes
: `
IdentityIdentityRelu:activations:0^NoOp*
T0*&
_output_shapes
: w
NoOpNoOp^BiasAdd/ReadVariableOp^Conv2D/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:<<: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
Conv2D/ReadVariableOpConv2D/ReadVariableOp:N J
&
_output_shapes
:<<
 
_user_specified_nameinputs"�
L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
:
input_7/
serving_default_input_7:0<<:
decoder/
StatefulPartitionedCall:0<<tensorflow/serving/predict:��
�
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer_with_weights-3
layer-4
layer_with_weights-4
layer-5
layer_with_weights-5
layer-6
layer-7
	layer_with_weights-6
	layer-8

layer_with_weights-7

layer-9
layer-10
layer-11
layer-12
layer-13
layer-14
layer-15
layer-16
layer-17
layer-18
layer-19
layer-20
layer-21
layer-22
layer-23
layer-24
layer-25
layer-26
layer-27
layer-28
layer-29
layer-30
 	variables
!trainable_variables
"regularization_losses
#	keras_api
$__call__
*%&call_and_return_all_conditional_losses
&_default_save_signature
'	optimizer
(loss
)
signatures"
_tf_keras_network
"
_tf_keras_input_layer
�
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer_with_weights-2
layer-3
layer_with_weights-3
layer-4
layer-5

layer_with_weights-4

layer-6
	layer_with_weights-5
	layer-7
*layer-8
+	variables
,trainable_variables
-regularization_losses
.	keras_api
/__call__
*0&call_and_return_all_conditional_losses"
_tf_keras_network
�
1layer-0
2layer_with_weights-0
2layer-1
3layer-2
4layer_with_weights-1
4layer-3
5layer_with_weights-2
5layer-4
6layer_with_weights-3
6layer-5
7layer_with_weights-4
7layer-6
8layer_with_weights-5
8layer-7
9	variables
:trainable_variables
;regularization_losses
<	keras_api
=__call__
*>&call_and_return_all_conditional_losses"
_tf_keras_network
�
?	variables
@trainable_variables
Aregularization_losses
B	keras_api
C__call__
*D&call_and_return_all_conditional_losses

Ekernel
Fbias
 G_jit_compiled_convolution_op"
_tf_keras_layer
�
H	variables
Itrainable_variables
Jregularization_losses
K	keras_api
L__call__
*M&call_and_return_all_conditional_losses

Nkernel
Obias
 P_jit_compiled_convolution_op"
_tf_keras_layer
�
Q	variables
Rtrainable_variables
Sregularization_losses
T	keras_api
U__call__
*V&call_and_return_all_conditional_losses

Wkernel
Xbias
 Y_jit_compiled_convolution_op"
_tf_keras_layer
�
Z	variables
[trainable_variables
\regularization_losses
]	keras_api
^__call__
*_&call_and_return_all_conditional_losses

`kernel
abias
 b_jit_compiled_convolution_op"
_tf_keras_layer
�
c	variables
dtrainable_variables
eregularization_losses
f	keras_api
g__call__
*h&call_and_return_all_conditional_losses"
_tf_keras_layer
�
i	variables
jtrainable_variables
kregularization_losses
l	keras_api
m__call__
*n&call_and_return_all_conditional_losses

okernel
pbias"
_tf_keras_layer
�
q	variables
rtrainable_variables
sregularization_losses
t	keras_api
u__call__
*v&call_and_return_all_conditional_losses

wkernel
xbias"
_tf_keras_layer
(
y	keras_api"
_tf_keras_layer
(
z	keras_api"
_tf_keras_layer
(
{	keras_api"
_tf_keras_layer
(
|	keras_api"
_tf_keras_layer
(
}	keras_api"
_tf_keras_layer
(
~	keras_api"
_tf_keras_layer
(
	keras_api"
_tf_keras_layer
)
�	keras_api"
_tf_keras_layer
)
�	keras_api"
_tf_keras_layer
)
�	keras_api"
_tf_keras_layer
)
�	keras_api"
_tf_keras_layer
)
�	keras_api"
_tf_keras_layer
)
�	keras_api"
_tf_keras_layer
)
�	keras_api"
_tf_keras_layer
)
�	keras_api"
_tf_keras_layer
)
�	keras_api"
_tf_keras_layer
)
�	keras_api"
_tf_keras_layer
)
�	keras_api"
_tf_keras_layer
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
E0
F1
N2
O3
W4
X5
`6
a7
w8
x9
o10
p11
�12
�13
�14
�15
�16
�17
�18
�19
�20
�21
�22
�23"
trackable_list_wrapper
�
E0
F1
N2
O3
W4
X5
`6
a7
w8
x9
o10
p11
�12
�13
�14
�15
�16
�17
�18
�19
�20
�21
�22
�23"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
 	variables
!trainable_variables
"regularization_losses
$__call__
&_default_save_signature
*%&call_and_return_all_conditional_losses
&%"call_and_return_conditional_losses"
_generic_user_object
�
�trace_0
�trace_1
�trace_2
�trace_32�
$__inference_vae_layer_call_fn_246390
$__inference_vae_layer_call_fn_246578
$__inference_vae_layer_call_fn_246888
$__inference_vae_layer_call_fn_246958�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0z�trace_1z�trace_2z�trace_3
�
�trace_0
�trace_1
�trace_2
�trace_32�
?__inference_vae_layer_call_and_return_conditional_losses_246083
?__inference_vae_layer_call_and_return_conditional_losses_246201
?__inference_vae_layer_call_and_return_conditional_losses_247212
?__inference_vae_layer_call_and_return_conditional_losses_247466�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0z�trace_1z�trace_2z�trace_3
�
�
capture_24
�
capture_27
�
capture_28
�
capture_29B�
!__inference__wrapped_model_244932input_7"�
���
FullArgSpec
args� 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�
capture_24z�
capture_27z�
capture_28z�
capture_29
�
�
_variables
�_iterations
�_learning_rate
�_index_dict
�
_momentums
�_velocities
�_update_step_xla"
experimentalOptimizer
 "
trackable_dict_wrapper
-
�serving_default"
signature_map
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
v
E0
F1
N2
O3
W4
X5
`6
a7
w8
x9
o10
p11"
trackable_list_wrapper
v
E0
F1
N2
O3
W4
X5
`6
a7
w8
x9
o10
p11"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
+	variables
,trainable_variables
-regularization_losses
/__call__
*0&call_and_return_all_conditional_losses
&0"call_and_return_conditional_losses"
_generic_user_object
�
�trace_0
�trace_1
�trace_2
�trace_32�
(__inference_encoder_layer_call_fn_245203
(__inference_encoder_layer_call_fn_245274
(__inference_encoder_layer_call_fn_247619
(__inference_encoder_layer_call_fn_247652�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0z�trace_1z�trace_2z�trace_3
�
�trace_0
�trace_1
�trace_2
�trace_32�
C__inference_encoder_layer_call_and_return_conditional_losses_245071
C__inference_encoder_layer_call_and_return_conditional_losses_245131
C__inference_encoder_layer_call_and_return_conditional_losses_247717
C__inference_encoder_layer_call_and_return_conditional_losses_247782�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0z�trace_1z�trace_2z�trace_3
"
_tf_keras_input_layer
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses
�kernel
	�bias"
_tf_keras_layer
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses
�kernel
	�bias
!�_jit_compiled_convolution_op"
_tf_keras_layer
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses
�kernel
	�bias
!�_jit_compiled_convolution_op"
_tf_keras_layer
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses
�kernel
	�bias
!�_jit_compiled_convolution_op"
_tf_keras_layer
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses
�kernel
	�bias
!�_jit_compiled_convolution_op"
_tf_keras_layer
�
�	variables
�trainable_variables
�regularization_losses
�	keras_api
�__call__
+�&call_and_return_all_conditional_losses
�kernel
	�bias
!�_jit_compiled_convolution_op"
_tf_keras_layer
�
�0
�1
�2
�3
�4
�5
�6
�7
�8
�9
�10
�11"
trackable_list_wrapper
�
�0
�1
�2
�3
�4
�5
�6
�7
�8
�9
�10
�11"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
9	variables
:trainable_variables
;regularization_losses
=__call__
*>&call_and_return_all_conditional_losses
&>"call_and_return_conditional_losses"
_generic_user_object
�
�trace_0
�trace_1
�trace_2
�trace_32�
(__inference_decoder_layer_call_fn_245774
(__inference_decoder_layer_call_fn_245838
(__inference_decoder_layer_call_fn_247811
(__inference_decoder_layer_call_fn_247840�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0z�trace_1z�trace_2z�trace_3
�
�trace_0
�trace_1
�trace_2
�trace_32�
C__inference_decoder_layer_call_and_return_conditional_losses_245674
C__inference_decoder_layer_call_and_return_conditional_losses_245709
C__inference_decoder_layer_call_and_return_conditional_losses_247948
C__inference_decoder_layer_call_and_return_conditional_losses_248056�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0z�trace_1z�trace_2z�trace_3
.
E0
F1"
trackable_list_wrapper
.
E0
F1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
?	variables
@trainable_variables
Aregularization_losses
C__call__
*D&call_and_return_all_conditional_losses
&D"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
*__inference_conv2d_15_layer_call_fn_248065�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
E__inference_conv2d_15_layer_call_and_return_conditional_losses_248076�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
*:(2conv2d_15/kernel
:2conv2d_15/bias
�2��
���
FullArgSpec
args�
jinputs
jkernel
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 0
.
N0
O1"
trackable_list_wrapper
.
N0
O1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
H	variables
Itrainable_variables
Jregularization_losses
L__call__
*M&call_and_return_all_conditional_losses
&M"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
*__inference_conv2d_16_layer_call_fn_248085�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
E__inference_conv2d_16_layer_call_and_return_conditional_losses_248096�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
*:( 2conv2d_16/kernel
: 2conv2d_16/bias
�2��
���
FullArgSpec
args�
jinputs
jkernel
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 0
.
W0
X1"
trackable_list_wrapper
.
W0
X1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
Q	variables
Rtrainable_variables
Sregularization_losses
U__call__
*V&call_and_return_all_conditional_losses
&V"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
*__inference_conv2d_17_layer_call_fn_248105�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
E__inference_conv2d_17_layer_call_and_return_conditional_losses_248116�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
+:) �2conv2d_17/kernel
:�2conv2d_17/bias
�2��
���
FullArgSpec
args�
jinputs
jkernel
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 0
.
`0
a1"
trackable_list_wrapper
.
`0
a1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
Z	variables
[trainable_variables
\regularization_losses
^__call__
*_&call_and_return_all_conditional_losses
&_"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
*__inference_conv2d_18_layer_call_fn_248125�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
E__inference_conv2d_18_layer_call_and_return_conditional_losses_248136�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
,:*��2conv2d_18/kernel
:�2conv2d_18/bias
�2��
���
FullArgSpec
args�
jinputs
jkernel
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 0
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
c	variables
dtrainable_variables
eregularization_losses
g__call__
*h&call_and_return_all_conditional_losses
&h"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
*__inference_flatten_3_layer_call_fn_248141�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
E__inference_flatten_3_layer_call_and_return_conditional_losses_248147�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
.
o0
p1"
trackable_list_wrapper
.
o0
p1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
i	variables
jtrainable_variables
kregularization_losses
m__call__
*n&call_and_return_all_conditional_losses
&n"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
)__inference_dense_10_layer_call_fn_248156�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
D__inference_dense_10_layer_call_and_return_conditional_losses_248166�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
#:!
��2dense_10/kernel
:2dense_10/bias
.
w0
x1"
trackable_list_wrapper
.
w0
x1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
q	variables
rtrainable_variables
sregularization_losses
u__call__
*v&call_and_return_all_conditional_losses
&v"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
(__inference_dense_9_layer_call_fn_248175�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
C__inference_dense_9_layer_call_and_return_conditional_losses_248185�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
": 
��2dense_9/kernel
:2dense_9/bias
"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
+__inference_add_loss_3_layer_call_fn_248191�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
F__inference_add_loss_3_layer_call_and_return_conditional_losses_248196�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
-__inference_add_metric_6_layer_call_fn_248205�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
H__inference_add_metric_6_layer_call_and_return_conditional_losses_248222�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
-__inference_add_metric_7_layer_call_fn_248231�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
H__inference_add_metric_7_layer_call_and_return_conditional_losses_248248�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
#:!
��2dense_11/kernel
:��2dense_11/bias
6:4��2conv2d_transpose_12/kernel
':%�2conv2d_transpose_12/bias
5:3@�2conv2d_transpose_13/kernel
&:$@2conv2d_transpose_13/bias
4:2 @2conv2d_transpose_14/kernel
&:$ 2conv2d_transpose_14/bias
4:2 2conv2d_transpose_15/kernel
&:$2conv2d_transpose_15/bias
*:(2conv2d_19/kernel
:2conv2d_19/bias
 "
trackable_list_wrapper
�
0
1
2
3
4
5
6
7
	8

9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30"
trackable_list_wrapper
8
�0
�1
�2"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�
�
capture_24
�
capture_27
�
capture_28
�
capture_29B�
$__inference_vae_layer_call_fn_246390input_7"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�
capture_24z�
capture_27z�
capture_28z�
capture_29
�
�
capture_24
�
capture_27
�
capture_28
�
capture_29B�
$__inference_vae_layer_call_fn_246578input_7"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�
capture_24z�
capture_27z�
capture_28z�
capture_29
�
�
capture_24
�
capture_27
�
capture_28
�
capture_29B�
$__inference_vae_layer_call_fn_246888inputs"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�
capture_24z�
capture_27z�
capture_28z�
capture_29
�
�
capture_24
�
capture_27
�
capture_28
�
capture_29B�
$__inference_vae_layer_call_fn_246958inputs"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�
capture_24z�
capture_27z�
capture_28z�
capture_29
�
�
capture_24
�
capture_27
�
capture_28
�
capture_29B�
?__inference_vae_layer_call_and_return_conditional_losses_246083input_7"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�
capture_24z�
capture_27z�
capture_28z�
capture_29
�
�
capture_24
�
capture_27
�
capture_28
�
capture_29B�
?__inference_vae_layer_call_and_return_conditional_losses_246201input_7"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�
capture_24z�
capture_27z�
capture_28z�
capture_29
�
�
capture_24
�
capture_27
�
capture_28
�
capture_29B�
?__inference_vae_layer_call_and_return_conditional_losses_247212inputs"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�
capture_24z�
capture_27z�
capture_28z�
capture_29
�
�
capture_24
�
capture_27
�
capture_28
�
capture_29B�
?__inference_vae_layer_call_and_return_conditional_losses_247466inputs"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�
capture_24z�
capture_27z�
capture_28z�
capture_29
!J	
Const_1jtf.TrackableConstant
J
Constjtf.TrackableConstant
!J	
Const_3jtf.TrackableConstant
!J	
Const_2jtf.TrackableConstant
�
�0
�1
�2
�3
�4
�5
�6
�7
�8
�9
�10
�11
�12
�13
�14
�15
�16
�17
�18
�19
�20
�21
�22
�23
�24
�25
�26
�27
�28
�29
�30
�31
�32
�33
�34
�35
�36
�37
�38
�39
�40
�41
�42
�43
�44
�45
�46
�47
�48"
trackable_list_wrapper
:	 2	iteration
: 2learning_rate
 "
trackable_dict_wrapper
�
�0
�1
�2
�3
�4
�5
�6
�7
�8
�9
�10
�11
�12
�13
�14
�15
�16
�17
�18
�19
�20
�21
�22
�23"
trackable_list_wrapper
�
�0
�1
�2
�3
�4
�5
�6
�7
�8
�9
�10
�11
�12
�13
�14
�15
�16
�17
�18
�19
�20
�21
�22
�23"
trackable_list_wrapper
�
�trace_0
�trace_1
�trace_2
�trace_3
�trace_4
�trace_5
�trace_6
�trace_7
�trace_8
�trace_9
�trace_10
�trace_11
�trace_12
�trace_13
�trace_14
�trace_15
�trace_16
�trace_17
�trace_18
�trace_19
�trace_20
�trace_21
�trace_22
�trace_232�
#__inference__update_step_xla_247471
#__inference__update_step_xla_247476
#__inference__update_step_xla_247481
#__inference__update_step_xla_247486
#__inference__update_step_xla_247491
#__inference__update_step_xla_247496
#__inference__update_step_xla_247501
#__inference__update_step_xla_247506
#__inference__update_step_xla_247511
#__inference__update_step_xla_247516
#__inference__update_step_xla_247521
#__inference__update_step_xla_247526
#__inference__update_step_xla_247531
#__inference__update_step_xla_247536
#__inference__update_step_xla_247541
#__inference__update_step_xla_247546
#__inference__update_step_xla_247551
#__inference__update_step_xla_247556
#__inference__update_step_xla_247561
#__inference__update_step_xla_247566
#__inference__update_step_xla_247571
#__inference__update_step_xla_247576
#__inference__update_step_xla_247581
#__inference__update_step_xla_247586�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 0z�trace_0z�trace_1z�trace_2z�trace_3z�trace_4z�trace_5z�trace_6z�trace_7z�trace_8z�trace_9z�trace_10z�trace_11z�trace_12z�trace_13z�trace_14z�trace_15z�trace_16z�trace_17z�trace_18z�trace_19z�trace_20z�trace_21z�trace_22z�trace_23
�
�
capture_24
�
capture_27
�
capture_28
�
capture_29B�
$__inference_signature_wrapper_246818input_7"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�
capture_24z�
capture_27z�
capture_28z�
capture_29
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trace_0
�trace_12�
)__inference_lambda_3_layer_call_fn_248254
)__inference_lambda_3_layer_call_fn_248260�
���
FullArgSpec)
args!�
jinputs
jmask

jtraining
varargs
 
varkw
 
defaults�

 
p 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0z�trace_1
�
�trace_0
�trace_12�
D__inference_lambda_3_layer_call_and_return_conditional_losses_248282
D__inference_lambda_3_layer_call_and_return_conditional_losses_248304�
���
FullArgSpec)
args!�
jinputs
jmask

jtraining
varargs
 
varkw
 
defaults�

 
p 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0z�trace_1
 "
trackable_list_wrapper
_
0
1
2
3
4
5

6
	7
*8"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
(__inference_encoder_layer_call_fn_245203input_7"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
(__inference_encoder_layer_call_fn_245274input_7"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
(__inference_encoder_layer_call_fn_247619inputs"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
(__inference_encoder_layer_call_fn_247652inputs"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
C__inference_encoder_layer_call_and_return_conditional_losses_245071input_7"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
C__inference_encoder_layer_call_and_return_conditional_losses_245131input_7"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
C__inference_encoder_layer_call_and_return_conditional_losses_247717inputs"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
C__inference_encoder_layer_call_and_return_conditional_losses_247782inputs"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
0
�0
�1"
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
)__inference_dense_11_layer_call_fn_248313�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
D__inference_dense_11_layer_call_and_return_conditional_losses_248324�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
*__inference_reshape_3_layer_call_fn_248329�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
E__inference_reshape_3_layer_call_and_return_conditional_losses_248343�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
0
�0
�1"
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
4__inference_conv2d_transpose_12_layer_call_fn_248352�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
O__inference_conv2d_transpose_12_layer_call_and_return_conditional_losses_248386�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�2��
���
FullArgSpec
args�
jinputs
jkernel
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 0
0
�0
�1"
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
4__inference_conv2d_transpose_13_layer_call_fn_248395�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
O__inference_conv2d_transpose_13_layer_call_and_return_conditional_losses_248429�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�2��
���
FullArgSpec
args�
jinputs
jkernel
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 0
0
�0
�1"
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
4__inference_conv2d_transpose_14_layer_call_fn_248438�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
O__inference_conv2d_transpose_14_layer_call_and_return_conditional_losses_248472�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�2��
���
FullArgSpec
args�
jinputs
jkernel
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 0
0
�0
�1"
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
4__inference_conv2d_transpose_15_layer_call_fn_248481�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
O__inference_conv2d_transpose_15_layer_call_and_return_conditional_losses_248515�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�2��
���
FullArgSpec
args�
jinputs
jkernel
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 0
0
�0
�1"
trackable_list_wrapper
0
�0
�1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
�	variables
�trainable_variables
�regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
*__inference_conv2d_19_layer_call_fn_248524�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
E__inference_conv2d_19_layer_call_and_return_conditional_losses_248535�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�2��
���
FullArgSpec
args�
jinputs
jkernel
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 0
 "
trackable_list_wrapper
X
10
21
32
43
54
65
76
87"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
(__inference_decoder_layer_call_fn_245774input_8"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
(__inference_decoder_layer_call_fn_245838input_8"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
(__inference_decoder_layer_call_fn_247811inputs"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
(__inference_decoder_layer_call_fn_247840inputs"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
C__inference_decoder_layer_call_and_return_conditional_losses_245674input_8"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
C__inference_decoder_layer_call_and_return_conditional_losses_245709input_8"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
C__inference_decoder_layer_call_and_return_conditional_losses_247948inputs"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
C__inference_decoder_layer_call_and_return_conditional_losses_248056inputs"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
*__inference_conv2d_15_layer_call_fn_248065inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
E__inference_conv2d_15_layer_call_and_return_conditional_losses_248076inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
*__inference_conv2d_16_layer_call_fn_248085inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
E__inference_conv2d_16_layer_call_and_return_conditional_losses_248096inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
*__inference_conv2d_17_layer_call_fn_248105inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
E__inference_conv2d_17_layer_call_and_return_conditional_losses_248116inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
*__inference_conv2d_18_layer_call_fn_248125inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
E__inference_conv2d_18_layer_call_and_return_conditional_losses_248136inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
*__inference_flatten_3_layer_call_fn_248141inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
E__inference_flatten_3_layer_call_and_return_conditional_losses_248147inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
)__inference_dense_10_layer_call_fn_248156inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
D__inference_dense_10_layer_call_and_return_conditional_losses_248166inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
(__inference_dense_9_layer_call_fn_248175inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
C__inference_dense_9_layer_call_and_return_conditional_losses_248185inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
+__inference_add_loss_3_layer_call_fn_248191inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
F__inference_add_loss_3_layer_call_and_return_conditional_losses_248196inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
(
�0"
trackable_list_wrapper
 "
trackable_list_wrapper
/
�vae_loss"
trackable_dict_wrapper
�B�
-__inference_add_metric_6_layer_call_fn_248205inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
H__inference_add_metric_6_layer_call_and_return_conditional_losses_248222inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
(
�0"
trackable_list_wrapper
 "
trackable_list_wrapper
:
�reconstruction_loss"
trackable_dict_wrapper
�B�
-__inference_add_metric_7_layer_call_fn_248231inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
H__inference_add_metric_7_layer_call_and_return_conditional_losses_248248inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
R
�	variables
�	keras_api

�total

�count"
_tf_keras_metric
R
�	variables
�	keras_api

�total

�count"
_tf_keras_metric
R
�	variables
�	keras_api

�total

�count"
_tf_keras_metric
/:-2Adam/m/conv2d_15/kernel
/:-2Adam/v/conv2d_15/kernel
!:2Adam/m/conv2d_15/bias
!:2Adam/v/conv2d_15/bias
/:- 2Adam/m/conv2d_16/kernel
/:- 2Adam/v/conv2d_16/kernel
!: 2Adam/m/conv2d_16/bias
!: 2Adam/v/conv2d_16/bias
0:. �2Adam/m/conv2d_17/kernel
0:. �2Adam/v/conv2d_17/kernel
": �2Adam/m/conv2d_17/bias
": �2Adam/v/conv2d_17/bias
1:/��2Adam/m/conv2d_18/kernel
1:/��2Adam/v/conv2d_18/kernel
": �2Adam/m/conv2d_18/bias
": �2Adam/v/conv2d_18/bias
':%
��2Adam/m/dense_9/kernel
':%
��2Adam/v/dense_9/kernel
:2Adam/m/dense_9/bias
:2Adam/v/dense_9/bias
(:&
��2Adam/m/dense_10/kernel
(:&
��2Adam/v/dense_10/kernel
 :2Adam/m/dense_10/bias
 :2Adam/v/dense_10/bias
(:&
��2Adam/m/dense_11/kernel
(:&
��2Adam/v/dense_11/kernel
": ��2Adam/m/dense_11/bias
": ��2Adam/v/dense_11/bias
;:9��2!Adam/m/conv2d_transpose_12/kernel
;:9��2!Adam/v/conv2d_transpose_12/kernel
,:*�2Adam/m/conv2d_transpose_12/bias
,:*�2Adam/v/conv2d_transpose_12/bias
::8@�2!Adam/m/conv2d_transpose_13/kernel
::8@�2!Adam/v/conv2d_transpose_13/kernel
+:)@2Adam/m/conv2d_transpose_13/bias
+:)@2Adam/v/conv2d_transpose_13/bias
9:7 @2!Adam/m/conv2d_transpose_14/kernel
9:7 @2!Adam/v/conv2d_transpose_14/kernel
+:) 2Adam/m/conv2d_transpose_14/bias
+:) 2Adam/v/conv2d_transpose_14/bias
9:7 2!Adam/m/conv2d_transpose_15/kernel
9:7 2!Adam/v/conv2d_transpose_15/kernel
+:)2Adam/m/conv2d_transpose_15/bias
+:)2Adam/v/conv2d_transpose_15/bias
/:-2Adam/m/conv2d_19/kernel
/:-2Adam/v/conv2d_19/kernel
!:2Adam/m/conv2d_19/bias
!:2Adam/v/conv2d_19/bias
�B�
#__inference__update_step_xla_247471gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247476gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247481gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247486gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247491gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247496gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247501gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247506gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247511gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247516gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247521gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247526gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247531gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247536gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247541gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247546gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247551gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247556gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247561gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247566gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247571gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247576gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247581gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
#__inference__update_step_xla_247586gradientvariable"�
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
)__inference_lambda_3_layer_call_fn_248254inputs_0inputs_1"�
���
FullArgSpec)
args!�
jinputs
jmask

jtraining
varargs
 
varkw
 
defaults�

 
p 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
)__inference_lambda_3_layer_call_fn_248260inputs_0inputs_1"�
���
FullArgSpec)
args!�
jinputs
jmask

jtraining
varargs
 
varkw
 
defaults�

 
p 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
D__inference_lambda_3_layer_call_and_return_conditional_losses_248282inputs_0inputs_1"�
���
FullArgSpec)
args!�
jinputs
jmask

jtraining
varargs
 
varkw
 
defaults�

 
p 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
D__inference_lambda_3_layer_call_and_return_conditional_losses_248304inputs_0inputs_1"�
���
FullArgSpec)
args!�
jinputs
jmask

jtraining
varargs
 
varkw
 
defaults�

 
p 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
)__inference_dense_11_layer_call_fn_248313inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
D__inference_dense_11_layer_call_and_return_conditional_losses_248324inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
*__inference_reshape_3_layer_call_fn_248329inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
E__inference_reshape_3_layer_call_and_return_conditional_losses_248343inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
4__inference_conv2d_transpose_12_layer_call_fn_248352inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
O__inference_conv2d_transpose_12_layer_call_and_return_conditional_losses_248386inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
4__inference_conv2d_transpose_13_layer_call_fn_248395inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
O__inference_conv2d_transpose_13_layer_call_and_return_conditional_losses_248429inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
4__inference_conv2d_transpose_14_layer_call_fn_248438inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
O__inference_conv2d_transpose_14_layer_call_and_return_conditional_losses_248472inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
4__inference_conv2d_transpose_15_layer_call_fn_248481inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
O__inference_conv2d_transpose_15_layer_call_and_return_conditional_losses_248515inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
*__inference_conv2d_19_layer_call_fn_248524inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
E__inference_conv2d_19_layer_call_and_return_conditional_losses_248535inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2total
:  (2count
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2add_metric_6/total
:  (2add_metric_6/count
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2add_metric_7/total
:  (2add_metric_7/count�
#__inference__update_step_xla_247471~x�u
n�k
!�
gradient
<�9	%�"
�
�
p
` VariableSpec 
`��҄��?
� "
 �
#__inference__update_step_xla_247476f`�]
V�S
�
gradient
0�-	�
�
�
p
` VariableSpec 
`���ˋ�?
� "
 �
#__inference__update_step_xla_247481~x�u
n�k
!�
gradient 
<�9	%�"
� 
�
p
` VariableSpec 
`��҄��?
� "
 �
#__inference__update_step_xla_247486f`�]
V�S
�
gradient 
0�-	�
� 
�
p
` VariableSpec 
`��҄��?
� "
 �
#__inference__update_step_xla_247491�z�w
p�m
"�
gradient �
=�:	&�#
� �
�
p
` VariableSpec 
`��ӄ��?
� "
 �
#__inference__update_step_xla_247496hb�_
X�U
�
gradient�
1�.	�
��
�
p
` VariableSpec 
`��ӄ��?
� "
 �
#__inference__update_step_xla_247501�|�y
r�o
#� 
gradient��
>�;	'�$
���
�
p
` VariableSpec 
`��ӄ��?
� "
 �
#__inference__update_step_xla_247506hb�_
X�U
�
gradient�
1�.	�
��
�
p
` VariableSpec 
`��ӄ��?
� "
 �
#__inference__update_step_xla_247511rl�i
b�_
�
gradient
��
6�3	�
�
��
�
p
` VariableSpec 
`�����?
� "
 �
#__inference__update_step_xla_247516f`�]
V�S
�
gradient
0�-	�
�
�
p
` VariableSpec 
`�����?
� "
 �
#__inference__update_step_xla_247521rl�i
b�_
�
gradient
��
6�3	�
�
��
�
p
` VariableSpec 
`�����?
� "
 �
#__inference__update_step_xla_247526f`�]
V�S
�
gradient
0�-	�
�
�
p
` VariableSpec 
`�����?
� "
 �
#__inference__update_step_xla_247531rl�i
b�_
�
gradient
��
6�3	�
�
��
�
p
` VariableSpec 
`�����?
� "
 �
#__inference__update_step_xla_247536jd�a
Z�W
�
gradient��
2�/	�
	���
�
p
` VariableSpec 
`������?
� "
 �
#__inference__update_step_xla_247541�|�y
r�o
#� 
gradient��
>�;	'�$
���
�
p
` VariableSpec 
`��ׄ��?
� "
 �
#__inference__update_step_xla_247546hb�_
X�U
�
gradient�
1�.	�
��
�
p
` VariableSpec 
`�����?
� "
 �
#__inference__update_step_xla_247551�z�w
p�m
"�
gradient@�
=�:	&�#
�@�
�
p
` VariableSpec 
`��ׄ��?
� "
 �
#__inference__update_step_xla_247556f`�]
V�S
�
gradient@
0�-	�
�@
�
p
` VariableSpec 
`�����?
� "
 �
#__inference__update_step_xla_247561~x�u
n�k
!�
gradient @
<�9	%�"
� @
�
p
` VariableSpec 
`�����?
� "
 �
#__inference__update_step_xla_247566f`�]
V�S
�
gradient 
0�-	�
� 
�
p
` VariableSpec 
`������?
� "
 �
#__inference__update_step_xla_247571~x�u
n�k
!�
gradient 
<�9	%�"
� 
�
p
` VariableSpec 
`��ׄ��?
� "
 �
#__inference__update_step_xla_247576f`�]
V�S
�
gradient
0�-	�
�
�
p
` VariableSpec 
`�����?
� "
 �
#__inference__update_step_xla_247581~x�u
n�k
!�
gradient
<�9	%�"
�
�
p
` VariableSpec 
`������?
� "
 �
#__inference__update_step_xla_247586f`�]
V�S
�
gradient
0�-	�
�
�
p
` VariableSpec 
`������?
� "
 �
!__inference__wrapped_model_244932�4EFNOWX`awxop��������������������/�,
%�"
 �
input_7<<
� "0�-
+
decoder �
decoder<<�
F__inference_add_loss_3_layer_call_and_return_conditional_losses_248196R�
�
�
inputs 
� "0�-
�
tensor_0 
�
�

tensor_1_0 a
+__inference_add_loss_3_layer_call_fn_2481912�
�
�
inputs 
� "�
unknown �
H__inference_add_metric_6_layer_call_and_return_conditional_losses_248222C���
�
�
inputs 
� "�
�
tensor_0 
� i
-__inference_add_metric_6_layer_call_fn_2482058���
�
�
inputs 
� "�
unknown �
H__inference_add_metric_7_layer_call_and_return_conditional_losses_248248C���
�
�
inputs 
� "�
�
tensor_0 
� i
-__inference_add_metric_7_layer_call_fn_2482318���
�
�
inputs 
� "�
unknown �
E__inference_conv2d_15_layer_call_and_return_conditional_losses_248076aEF.�+
$�!
�
inputs<<
� "+�(
!�
tensor_0<<
� �
*__inference_conv2d_15_layer_call_fn_248065VEF.�+
$�!
�
inputs<<
� " �
unknown<<�
E__inference_conv2d_16_layer_call_and_return_conditional_losses_248096aNO.�+
$�!
�
inputs<<
� "+�(
!�
tensor_0 
� �
*__inference_conv2d_16_layer_call_fn_248085VNO.�+
$�!
�
inputs<<
� " �
unknown �
E__inference_conv2d_17_layer_call_and_return_conditional_losses_248116bWX.�+
$�!
�
inputs 
� ",�)
"�
tensor_0�
� �
*__inference_conv2d_17_layer_call_fn_248105WWX.�+
$�!
�
inputs 
� "!�
unknown��
E__inference_conv2d_18_layer_call_and_return_conditional_losses_248136c`a/�,
%�"
 �
inputs�
� ",�)
"�
tensor_0�
� �
*__inference_conv2d_18_layer_call_fn_248125X`a/�,
%�"
 �
inputs�
� "!�
unknown��
E__inference_conv2d_19_layer_call_and_return_conditional_losses_248535c��.�+
$�!
�
inputs<<
� "+�(
!�
tensor_0<<
� �
*__inference_conv2d_19_layer_call_fn_248524X��.�+
$�!
�
inputs<<
� " �
unknown<<�
O__inference_conv2d_transpose_12_layer_call_and_return_conditional_losses_248386���J�G
@�=
;�8
inputs,����������������������������
� "G�D
=�:
tensor_0,����������������������������
� �
4__inference_conv2d_transpose_12_layer_call_fn_248352���J�G
@�=
;�8
inputs,����������������������������
� "<�9
unknown,�����������������������������
O__inference_conv2d_transpose_13_layer_call_and_return_conditional_losses_248429���J�G
@�=
;�8
inputs,����������������������������
� "F�C
<�9
tensor_0+���������������������������@
� �
4__inference_conv2d_transpose_13_layer_call_fn_248395���J�G
@�=
;�8
inputs,����������������������������
� ";�8
unknown+���������������������������@�
O__inference_conv2d_transpose_14_layer_call_and_return_conditional_losses_248472���I�F
?�<
:�7
inputs+���������������������������@
� "F�C
<�9
tensor_0+��������������������������� 
� �
4__inference_conv2d_transpose_14_layer_call_fn_248438���I�F
?�<
:�7
inputs+���������������������������@
� ";�8
unknown+��������������������������� �
O__inference_conv2d_transpose_15_layer_call_and_return_conditional_losses_248515���I�F
?�<
:�7
inputs+��������������������������� 
� "F�C
<�9
tensor_0+���������������������������
� �
4__inference_conv2d_transpose_15_layer_call_fn_248481���I�F
?�<
:�7
inputs+��������������������������� 
� ";�8
unknown+����������������������������
C__inference_decoder_layer_call_and_return_conditional_losses_245674x������������/�,
%�"
�
input_8
p

 
� "+�(
!�
tensor_0<<
� �
C__inference_decoder_layer_call_and_return_conditional_losses_245709x������������/�,
%�"
�
input_8
p 

 
� "+�(
!�
tensor_0<<
� �
C__inference_decoder_layer_call_and_return_conditional_losses_247948w������������.�+
$�!
�
inputs
p

 
� "+�(
!�
tensor_0<<
� �
C__inference_decoder_layer_call_and_return_conditional_losses_248056w������������.�+
$�!
�
inputs
p 

 
� "+�(
!�
tensor_0<<
� �
(__inference_decoder_layer_call_fn_245774m������������/�,
%�"
�
input_8
p

 
� " �
unknown<<�
(__inference_decoder_layer_call_fn_245838m������������/�,
%�"
�
input_8
p 

 
� " �
unknown<<�
(__inference_decoder_layer_call_fn_247811l������������.�+
$�!
�
inputs
p

 
� " �
unknown<<�
(__inference_decoder_layer_call_fn_247840l������������.�+
$�!
�
inputs
p 

 
� " �
unknown<<�
D__inference_dense_10_layer_call_and_return_conditional_losses_248166Sop(�%
�
�
inputs
��
� "#� 
�
tensor_0
� u
)__inference_dense_10_layer_call_fn_248156Hop(�%
�
�
inputs
��
� "�
unknown�
D__inference_dense_11_layer_call_and_return_conditional_losses_248324U��&�#
�
�
inputs
� "%�"
�
tensor_0
��
� w
)__inference_dense_11_layer_call_fn_248313J��&�#
�
�
inputs
� "�
unknown
���
C__inference_dense_9_layer_call_and_return_conditional_losses_248185Swx(�%
�
�
inputs
��
� "#� 
�
tensor_0
� t
(__inference_dense_9_layer_call_fn_248175Hwx(�%
�
�
inputs
��
� "�
unknown�
C__inference_encoder_layer_call_and_return_conditional_losses_245071�EFNOWX`awxop7�4
-�*
 �
input_7<<
p

 
� "d�a
Z�W
�

tensor_0_0
�

tensor_0_1
�

tensor_0_2
� �
C__inference_encoder_layer_call_and_return_conditional_losses_245131�EFNOWX`awxop7�4
-�*
 �
input_7<<
p 

 
� "d�a
Z�W
�

tensor_0_0
�

tensor_0_1
�

tensor_0_2
� �
C__inference_encoder_layer_call_and_return_conditional_losses_247717�EFNOWX`awxop6�3
,�)
�
inputs<<
p

 
� "d�a
Z�W
�

tensor_0_0
�

tensor_0_1
�

tensor_0_2
� �
C__inference_encoder_layer_call_and_return_conditional_losses_247782�EFNOWX`awxop6�3
,�)
�
inputs<<
p 

 
� "d�a
Z�W
�

tensor_0_0
�

tensor_0_1
�

tensor_0_2
� �
(__inference_encoder_layer_call_fn_245203�EFNOWX`awxop7�4
-�*
 �
input_7<<
p

 
� "T�Q
�
tensor_0
�
tensor_1
�
tensor_2�
(__inference_encoder_layer_call_fn_245274�EFNOWX`awxop7�4
-�*
 �
input_7<<
p 

 
� "T�Q
�
tensor_0
�
tensor_1
�
tensor_2�
(__inference_encoder_layer_call_fn_247619�EFNOWX`awxop6�3
,�)
�
inputs<<
p

 
� "T�Q
�
tensor_0
�
tensor_1
�
tensor_2�
(__inference_encoder_layer_call_fn_247652�EFNOWX`awxop6�3
,�)
�
inputs<<
p 

 
� "T�Q
�
tensor_0
�
tensor_1
�
tensor_2�
E__inference_flatten_3_layer_call_and_return_conditional_losses_248147X/�,
%�"
 �
inputs�
� "%�"
�
tensor_0
��
� {
*__inference_flatten_3_layer_call_fn_248141M/�,
%�"
 �
inputs�
� "�
unknown
���
D__inference_lambda_3_layer_call_and_return_conditional_losses_248282wP�M
F�C
9�6
�
inputs_0
�
inputs_1

 
p
� "#� 
�
tensor_0
� �
D__inference_lambda_3_layer_call_and_return_conditional_losses_248304wP�M
F�C
9�6
�
inputs_0
�
inputs_1

 
p 
� "#� 
�
tensor_0
� �
)__inference_lambda_3_layer_call_fn_248254lP�M
F�C
9�6
�
inputs_0
�
inputs_1

 
p
� "�
unknown�
)__inference_lambda_3_layer_call_fn_248260lP�M
F�C
9�6
�
inputs_0
�
inputs_1

 
p 
� "�
unknown�
E__inference_reshape_3_layer_call_and_return_conditional_losses_248343X(�%
�
�
inputs
��
� ",�)
"�
tensor_0�
� {
*__inference_reshape_3_layer_call_fn_248329M(�%
�
�
inputs
��
� "!�
unknown��
$__inference_signature_wrapper_246818�4EFNOWX`awxop��������������������:�7
� 
0�-
+
input_7 �
input_7<<"0�-
+
decoder �
decoder<<�
?__inference_vae_layer_call_and_return_conditional_losses_246083�4EFNOWX`awxop��������������������7�4
-�*
 �
input_7<<
p

 
� "@�=
!�
tensor_0<<
�
�

tensor_1_0 �
?__inference_vae_layer_call_and_return_conditional_losses_246201�4EFNOWX`awxop��������������������7�4
-�*
 �
input_7<<
p 

 
� "@�=
!�
tensor_0<<
�
�

tensor_1_0 �
?__inference_vae_layer_call_and_return_conditional_losses_247212�4EFNOWX`awxop��������������������6�3
,�)
�
inputs<<
p

 
� "@�=
!�
tensor_0<<
�
�

tensor_1_0 �
?__inference_vae_layer_call_and_return_conditional_losses_247466�4EFNOWX`awxop��������������������6�3
,�)
�
inputs<<
p 

 
� "@�=
!�
tensor_0<<
�
�

tensor_1_0 �
$__inference_vae_layer_call_fn_246390�4EFNOWX`awxop��������������������7�4
-�*
 �
input_7<<
p

 
� " �
unknown<<�
$__inference_vae_layer_call_fn_246578�4EFNOWX`awxop��������������������7�4
-�*
 �
input_7<<
p 

 
� " �
unknown<<�
$__inference_vae_layer_call_fn_246888�4EFNOWX`awxop��������������������6�3
,�)
�
inputs<<
p

 
� " �
unknown<<�
$__inference_vae_layer_call_fn_246958�4EFNOWX`awxop��������������������6�3
,�)
�
inputs<<
p 

 
� " �
unknown<<